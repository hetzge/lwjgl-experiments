package de.hetzge.sgamelwjgl.tile;

public class TileTypeDefinition<T> {
	private final T tileType;
	private final AutoTileDefinition tileDefinition;

	public TileTypeDefinition(T tileType, AutoTileDefinition tileDefinition) {
		this.tileType = tileType;
		this.tileDefinition = tileDefinition;
	}

	public T getTileType() {
		return this.tileType;
	}

	public AutoTileDefinition getTileDefinition() {
		return this.tileDefinition;
	}
}