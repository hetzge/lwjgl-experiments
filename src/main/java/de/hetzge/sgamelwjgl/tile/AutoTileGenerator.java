package de.hetzge.sgamelwjgl.tile;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import de.hetzge.sgamelwjgl.base.EditableTexture;

public final class AutoTileGenerator {

	private AutoTileGenerator() {
	}

	public static void generate(String name, List<AutoTileDefinition> tileDefinitions) {
		final File resultFile = new File("generated/" + name + ".png");

		if (!resultFile.exists()) {

			final EditableTexture test = EditableTexture.create(1, 1);
			test.set(0, 0, EditableTexture.MASK_COLOR);
			System.out.println(test.hasColor(0, 0));
			System.out.println(test.isColor(0, 0, EditableTexture.MASK_COLOR));

			final List<List<Boolean>> combinations = combinations(3, 3);

			final int masterTextureSizeInTiles = (int) Math.ceil(Math.sqrt(combinations.size() * tileDefinitions.size()));
			final int masterTextureSizeInPixels = masterTextureSizeInTiles * 32;
			final EditableTexture masterTexture = EditableTexture.create(masterTextureSizeInPixels, masterTextureSizeInPixels);
			int masterTextureTileIndex = 0;

			for (final AutoTileDefinition tileDefinition : tileDefinitions) {
				for (int combinationIndex = 0; combinationIndex < combinations.size(); combinationIndex++) {
					final List<Boolean> combination = combinations.get(combinationIndex);

					final EditableTexture textureA = EditableTexture.create(tileDefinition.getFileA());
					final EditableTexture textureB = EditableTexture.create(tileDefinition.getFileB());
					final EditableTexture textureC = EditableTexture.create(32, 32);

					final EditableTexture maskTexture = EditableTexture.create(32, 32);
					for (int i = 0; i < combination.size(); i++) {
						final int tileSize = (int) Math.ceil(32f / 3);

						final int x = i % 3;
						final int y = i / 3;

						if (combination.get(i)) {
							maskTexture.fill(x * tileSize, y * tileSize, tileSize, tileSize, EditableTexture.MASK_COLOR);
						}
					}

					tileDefinition.getFileAMask().accept(maskTexture);
					textureA.applyMask(maskTexture, 0, 0);
					tileDefinition.getFileBMask().accept(maskTexture);
					textureB.applyMask(maskTexture, 0, 0);

					final StringBuffer buffer = new StringBuffer();
					for (final Boolean value : combination) {
						buffer.append(value ? 'X' : '_');
					}

					textureC.apply(textureB, 0, 0);
					textureC.apply(textureA, 0, 0);
					// textureC.save(new File("generated/" + buffer + ".png"));

					final int masterTextureX = (masterTextureTileIndex % masterTextureSizeInTiles) * 32;
					final int masterTextureY = (masterTextureTileIndex / masterTextureSizeInTiles) * 32;
					masterTexture.apply(textureC, masterTextureX, masterTextureY);

					masterTextureTileIndex++;
				}
			}

			masterTexture.save(resultFile);
		} else {
			System.out.println("Skip generation: " + name);
		}
	}

	public static List<List<Boolean>> combinations(int width, int height) {
		final Set<List<Boolean>> results = new LinkedHashSet<>();
		final int length = width * height;
		final List<Boolean> values = new ArrayList<>(16);
		for (int i = 0; i < length; i++) {
			values.clear();
			for (int j = 0; j < length; j++) {
				values.add(j <= i ? true : false);
			}
			permute(values, 0, values.size() - 1, result -> {
				results.add(new ArrayList<>(result));
			});
		}
		return new ArrayList<>(results);
	}

	private static void permute(List<Boolean> values, int l, int r, Consumer<List<Boolean>> callback) {
		if (l == r) {
			callback.accept(values);
		} else {
			for (int i = l; i <= r; i++) {
				values = swap(values, l, i);
				permute(values, l + 1, r, callback);
				values = swap(values, l, i);
			}
		}
	}

	private static List<Boolean> swap(List<Boolean> values, int i, int j) {
		final List<Boolean> valuesCopy = new ArrayList<>(values);
		boolean tempValue;
		tempValue = values.get(i);
		valuesCopy.set(i, valuesCopy.get(j));
		valuesCopy.set(j, tempValue);
		return valuesCopy;
	}
}
