package de.hetzge.sgamelwjgl.tile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class AutoTileset {

	private static final Map<Integer, Integer> INDEX_BY_KEY;
	public static final int LENGTH;

	static {
		final List<List<Boolean>> combinations = AutoTileGenerator.combinations(3, 3);
		INDEX_BY_KEY = new HashMap<>(combinations.size());
		for (int combinationIndex = 0; combinationIndex < combinations.size(); combinationIndex++) {
			final List<Boolean> combination = combinations.get(combinationIndex);
			final int key = key(combination.get(0), combination.get(1), combination.get(2), combination.get(3), combination.get(4), combination.get(5), combination.get(6), combination.get(7), combination.get(8));
			INDEX_BY_KEY.put(key, combinationIndex);
		}
		LENGTH = INDEX_BY_KEY.size();
	}

	public static int getTileIndex(boolean topLeft, boolean top, boolean topRight, boolean left, boolean center, boolean right, boolean bottomLeft, boolean bottom, boolean bottomRight) {
		if (center) {
			return INDEX_BY_KEY.get(key(topLeft, top, topRight, left, center, right, bottomLeft, bottom, bottomRight));
		} else {
			return -1;
		}
	}

	private static int key(boolean topLeft, boolean top, boolean topRight, boolean left, boolean center, boolean right, boolean bottomLeft, boolean bottom, boolean bottomRight) {
		int x = 0;
		x = topLeft ? x | 0b000000001 : x;
		x = top ? x | 0b000000010 : x;
		x = topRight ? x | 0b000000100 : x;
		x = left ? x | 0b000001000 : x;
		x = center ? x | 0b000010000 : x;
		x = right ? x | 0b000100000 : x;
		x = bottomLeft ? x | 0b001000000 : x;
		x = bottom ? x | 0b10000000 : x;
		x = bottomRight ? x | 0b100000000 : x;
		return x;
	}
}
