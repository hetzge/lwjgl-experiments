package de.hetzge.sgamelwjgl.tile;

import java.io.File;
import java.util.function.Consumer;

public final class AutoTileDefinition {

	public static final Mask MASK_A = mask -> {
		mask.increaseMaskEdges();
		mask.increaseMask();
	};

	public static final Mask MASK_B = mask -> {
		mask.increaseMask();
		mask.increaseMaskEdges();
		mask.increaseMaskEdges();
		mask.increaseMaskEdges();
		mask.increaseMask();
	};
	
	public static final Mask MASK_C = mask -> {
		mask.increaseMask();
		mask.increaseMask();
		mask.increaseMask();
		mask.increaseMask();
		mask.increaseMask();
		mask.increaseMask();
		mask.increaseMask();
		mask.increaseMask();
	};
	
	private final File fileA;
	private final File fileB;

	private final Mask fileAMask;
	private final Mask fileBMask;

	private AutoTileDefinition(File fileA, Mask fileAMask, File fileB, Mask fileBMask) {
		this.fileA = fileA;
		this.fileB = fileB;

		this.fileAMask = fileAMask;
		this.fileBMask = fileBMask;
	}

	public File getFileA() {
		return this.fileA;
	}

	public File getFileB() {
		return this.fileB;
	}

	public Mask getFileAMask() {
		return this.fileAMask;
	}

	public Mask getFileBMask() {
		return this.fileBMask;
	}

	public static AutoTileDefinition create(File fileA, Mask fileAMask, File fileB, Mask fileBMask) {
		return new AutoTileDefinition(fileA, fileAMask, fileB, fileBMask);
	}
	
	@FunctionalInterface
	public static interface Mask extends Consumer<MaskOperations> {
	}
}