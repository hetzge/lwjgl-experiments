package de.hetzge.sgamelwjgl.tile;

public interface MaskOperations {

	void increaseMaskEdges();

	void increaseMask();

}