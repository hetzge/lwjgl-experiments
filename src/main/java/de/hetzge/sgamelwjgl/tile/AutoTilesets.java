package de.hetzge.sgamelwjgl.tile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import de.hetzge.sgamelwjgl.graphics.base.Texture;

public final class AutoTilesets<T> {

	private final Texture texture;
	private final Map<T, Integer> indexByTileType;

	private AutoTilesets(Texture texture, Map<T, Integer> indexByTileType) {
		this.texture = texture;
		this.indexByTileType = indexByTileType;
	}

	public Texture getTexture() {
		return this.texture;
	}

	public int getTileTypeCount() {
		return this.indexByTileType.size();
	}

	public int getTileTypeIndex(T tileType) {
		return this.indexByTileType.get(tileType);
	}

	public int getTileTypeOffset(T tileType) {
		return this.getTileTypeIndex(tileType) * AutoTileset.LENGTH;
	}

	public int getTileIndex(T tileType, boolean topLeft, boolean top, boolean topRight, boolean left, boolean center, boolean right, boolean bottomLeft, boolean bottom, boolean bottomRight) {
		if (center) {
			return this.getTileTypeOffset(tileType) + AutoTileset.getTileIndex(topLeft, top, topRight, left, center, right, bottomLeft, bottom, bottomRight);
		} else {
			return -1;
		}
	}

	public boolean isSame(int tileIndex, T tileType) {
		if (tileIndex >= 0) {
			return (tileIndex / AutoTileset.LENGTH) == this.getTileTypeIndex(tileType);
		} else {
			return false;
		}
	}

	public static <T> AutoTilesets<T> generate(String name, List<TileTypeDefinition<T>> typeDefinitions) {
		final List<AutoTileDefinition> autoTileDefinitions = typeDefinitions.stream().map(TileTypeDefinition::getTileDefinition).collect(Collectors.toList());
		AutoTileGenerator.generate(name, autoTileDefinitions);

		final Texture texture = Texture.create("generated/" + name + ".png", true, false);

		final Map<T, Integer> indexByTileType = new HashMap<>();
		for (int i = 0; i < typeDefinitions.size(); i++) {
			final TileTypeDefinition<T> tileTypeDefinition = typeDefinitions.get(i);
			final T tileType = tileTypeDefinition.getTileType();
			indexByTileType.put(tileType, i);
		}

		return new AutoTilesets<>(texture, indexByTileType);
	}
}
