package de.hetzge.sgamelwjgl.ui.base;

import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;

public interface Component {
	
	void render(SpriteBatch batch);
}
