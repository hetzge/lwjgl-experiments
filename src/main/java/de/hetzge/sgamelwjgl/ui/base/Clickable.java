package de.hetzge.sgamelwjgl.ui.base;

public interface Clickable {
	boolean click(float x, float y);
}
