package de.hetzge.sgamelwjgl.ui.base;

import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.graphics.base.TextureRegion;

public final class Button implements Component, Clickable {

	private final TextureRegion textureRegion;
	private final float x;
	private final float y;
	private final float width;
	private final float height;
	private final float colorBit;
	private final Runnable callback;

	public Button(TextureRegion textureRegion, float x, float y, float width, float height, float colorBit, Runnable callback) {
		this.textureRegion = textureRegion;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.colorBit = colorBit;
		this.callback = callback;
	}

	@Override
	public void render(SpriteBatch batch) {
		batch.add(this.textureRegion, this.x, this.y, 0f, this.width, this.height, this.colorBit);
	}

	public boolean contains(float x, float y) {
		final float rootX = this.x - (this.width / 2f);
		return (x > rootX) && (x < (rootX + this.width)) && (y < (this.y + this.height)) && (y > this.y);
	}

	@Override
	public boolean click(float x, float y) {
		if (contains(x, y)) {
			this.callback.run();
			return true;
		} else {
			return false;
		}
	}
}
