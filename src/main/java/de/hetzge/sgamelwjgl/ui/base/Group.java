package de.hetzge.sgamelwjgl.ui.base;

import java.util.ArrayList;
import java.util.List;

import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;

public final class Group implements Component, Clickable {

	private final List<Component> components;

	public Group() {
		this(List.of());
	}

	public Group(List<Component> components) {
		this.components = new ArrayList<>(components);
	}

	public void add(Component component) {
		this.components.add(component);
	}

	public void remove(Component component) {
		this.components.remove(component);
	}

	@Override
	public void render(SpriteBatch batch) {
		for (final Component component : this.components) {
			component.render(batch);
		}
	}

	@Override
	public boolean click(float x, float y) {
		return this.components.stream().filter(Clickable.class::isInstance).map(Clickable.class::cast).anyMatch(Clickable -> Clickable.click(x, y));
	}
}
