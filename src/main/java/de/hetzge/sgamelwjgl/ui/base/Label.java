package de.hetzge.sgamelwjgl.ui.base;

import com.badlogic.gdx.graphics.Color;

import de.hetzge.sgamelwjgl.Assets;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;

public final class Label implements Component {

	private final float x;
	private final float y;
	private String text;

	public Label(float x, float y, String text) {
		this.x = x;
		this.y = y;
		this.text = text;
	}

	@Override
	public void render(SpriteBatch batch) {
		Assets.INSTANCE.defaultFontSet.render(batch, this.text, this.x, this.y, 1.0f, Color.WHITE);
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getText() {
		return this.text;
	}
}
