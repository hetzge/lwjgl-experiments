package de.hetzge.sgamelwjgl.ui;

import org.joml.Vector2f;

import de.hetzge.sgamelwjgl.graphics.base.Window.WindowInput;

public final class UiInput {

	private final WindowInput windowInput;
	private final Ui ui;

	private UiInput(WindowInput windowInput, Ui ui) {
		this.windowInput = windowInput;
		this.ui = ui;
	}

	public boolean input() {
		if (this.windowInput.isMouseLeftClicked()) {
			final Vector2f mousePosition = this.windowInput.getMousePositionBottomUp();
			System.out.println(mousePosition.x + " " + mousePosition.y);
			return this.ui.click(mousePosition.x, mousePosition.y);
		} else {
			return false;
		}
	}

	public static UiInput create(WindowInput windowInput, Ui ui) {
		return new UiInput(windowInput, ui);
	}
}
