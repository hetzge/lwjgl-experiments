package de.hetzge.sgamelwjgl.ui;

import com.badlogic.gdx.graphics.Color;

import de.hetzge.sgamelwjgl.Assets;
import de.hetzge.sgamelwjgl.base.Orientation;
import de.hetzge.sgamelwjgl.entity.BuildingState;
import de.hetzge.sgamelwjgl.entity.BuildingType;
import de.hetzge.sgamelwjgl.entity.Game;
import de.hetzge.sgamelwjgl.entity.GameInput;
import de.hetzge.sgamelwjgl.entity.PersonState;
import de.hetzge.sgamelwjgl.entity.PersonType;
import de.hetzge.sgamelwjgl.entity.ResourceType;
import de.hetzge.sgamelwjgl.entity.WayType;
import de.hetzge.sgamelwjgl.graphics.base.FpsCounter;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.ui.base.Button;
import de.hetzge.sgamelwjgl.ui.base.Group;
import de.hetzge.sgamelwjgl.ui.base.Label;

public final class Ui {

	private final Game game;
	private final GameInput gameInput;
	private final FpsCounter fpsCounter;

	private final Group components;
	private final Label fpsLabel;
	private final Label mouseTilePositionLabel;

	private Ui(Game game, GameInput gameInput) {
		this.game = game;
		this.gameInput = gameInput;
		this.fpsCounter = new FpsCounter();
		this.components = new Group();

		this.fpsLabel = new Label(400f, 5f, "0");
		this.mouseTilePositionLabel = new Label(500f, 5f, "0/0 (0)");
		
		this.components.add(this.fpsLabel);
		this.components.add(this.mouseTilePositionLabel);

		// TODO move
		final float size = 32f;
		for (int i = 0; i < BuildingType.VALUES.length; i++) {
			final BuildingType buildingType = BuildingType.VALUES[i];
			this.components.add(new Button(Assets.INSTANCE.building.get(buildingType, BuildingState.IDLE), size + (i * size), size * 0f, size, size, Color.RED.toFloatBits(), () -> {
				gameInput.enableBuildBuildingInputMode(buildingType);
			}));
		}
		for (int i = 0; i < PersonType.VALUES.length; i++) {
			final PersonType personType = PersonType.VALUES[i];
			this.components.add(new Button(Assets.INSTANCE.person.get(personType, PersonState.IDLE, Orientation.SOUTH).getFrame(0), size + (i * size), size * 1f, size, size, Color.RED.toFloatBits(), () -> {
				gameInput.enableBuildPersonInputMode(personType);
			}));
		}
		for (int i = 0; i < WayType.VALUES.length; i++) {
			final WayType wayType = WayType.VALUES[i];
			this.components.add(new Button(Assets.INSTANCE.dotRegion, size + (i * size), size * 2f, size, size, Color.RED.toFloatBits(), () -> {
				gameInput.enableBuildWayInputMode(wayType);
			}));
		}
		for (int i = 0; i < ResourceType.VALUES.length; i++) {
			final ResourceType resourceType = ResourceType.VALUES[i];
			this.components.add(new Button(Assets.INSTANCE.resource.get(resourceType), size + (i * size), size * 3f, size, size, Color.RED.toFloatBits(), () -> {
				gameInput.enableBuildResourceInputMode(resourceType);
			}));
		}
	}

	public void render(SpriteBatch batch) {
		this.fpsCounter.count();
		update();
		this.components.render(batch);
	}
	
	private void update() {
		this.fpsLabel.setText(String.valueOf(this.fpsCounter.getFps()));
		
		final int mouseTileX = this.gameInput.getMouseTileX();
		final int mouseTileY = this.gameInput.getMouseTileY();
		this.mouseTilePositionLabel.setText(String.format("%s/%s (%s)", mouseTileX, mouseTileY, this.game.getWorld().getTileGrid().index(mouseTileX, mouseTileY)));
		
	}

	public boolean click(float x, float y) {
		return this.components.click(x, y);
	}

	public static Ui create(Game game, GameInput gameInput) {
		return new Ui(game, gameInput);
	}
}
