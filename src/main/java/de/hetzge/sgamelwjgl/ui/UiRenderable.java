package de.hetzge.sgamelwjgl.ui;

import org.joml.Vector3f;

import de.hetzge.sgamelwjgl.Assets;
import de.hetzge.sgamelwjgl.DefaultShader;
import de.hetzge.sgamelwjgl.DefaultShader.ColorMode;
import de.hetzge.sgamelwjgl.graphics.base.Camera;
import de.hetzge.sgamelwjgl.graphics.base.GeometryBuffer;
import de.hetzge.sgamelwjgl.graphics.base.MatricesUniformBuffer;
import de.hetzge.sgamelwjgl.graphics.base.Shape;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;

public final class UiRenderable {

	private final Ui ui;
	private final Camera camera;
	private final GeometryBuffer buffer;
	private final SpriteBatch batch;
	private final DefaultShader defaultShader;
	private final MatricesUniformBuffer matricesBuffer;

	private UiRenderable(Ui ui, Camera camera, GeometryBuffer buffer, SpriteBatch batch, DefaultShader defaultShader, MatricesUniformBuffer matricesBuffer) {
		this.ui = ui;
		this.camera = camera;
		this.buffer = buffer;
		this.batch = batch;
		this.defaultShader = defaultShader;
		this.matricesBuffer = matricesBuffer;
	}

	public void render() {

		// TODO
		this.matricesBuffer.set(this.camera.getCombinedMatrix());
		this.matricesBuffer.use();

		this.ui.render(this.batch);

		this.batch.render(this.buffer);
		this.buffer.flush();

		Assets.INSTANCE.atlas.getTexture().bind();
		this.defaultShader.use(ColorMode.REPLACE, this.camera.getCombinedMatrix());
		this.buffer.draw();
	}

	public void onResize(int width, int height) {
		this.camera.setupScreen(width, height);
		this.camera.setCameraPosition(new Vector3f(width / 2f, -height / 2f, 0f));
	}

	public Camera getCamera() {
		return this.camera;
	}

	public GeometryBuffer getBuffer() {
		return this.buffer;
	}

	public static UiRenderable create(Ui ui, int screenWidth, int screenHeight) {
		final Camera camera = Camera.create(screenWidth, screenHeight);
		final DefaultShader defaultShader = DefaultShader.load();
		final GeometryBuffer buffer = GeometryBuffer.create(Shape.QUAD, 1000, defaultShader.getVertexDescription());
		final SpriteBatch batch = SpriteBatch.create(1000);
		final MatricesUniformBuffer matricesBuffer = MatricesUniformBuffer.create(10);

		return new UiRenderable(ui, camera, buffer, batch, defaultShader, matricesBuffer);
	}
}
