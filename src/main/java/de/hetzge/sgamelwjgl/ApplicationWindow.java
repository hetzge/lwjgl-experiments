package de.hetzge.sgamelwjgl;

import de.hetzge.sgamelwjgl.entity.Game;
import de.hetzge.sgamelwjgl.entity.GameInput;
import de.hetzge.sgamelwjgl.entity.GameRenderable;
import de.hetzge.sgamelwjgl.graphics.base.Window;
import de.hetzge.sgamelwjgl.ui.Ui;
import de.hetzge.sgamelwjgl.ui.UiInput;
import de.hetzge.sgamelwjgl.ui.UiRenderable;

// TODO https://en.wikipedia.org/wiki/Octree

public class ApplicationWindow extends Window {

	public static void main(String[] args) {
		new ApplicationWindow().run();
	}

	private Application application;

	public ApplicationWindow() {
		super(800, 600);
	}

	@Override
	protected void init(long windowHandle) {
		Assets.load();
		this.application = new Application(this);
	}

	@Override
	protected void loop() {
		this.application.loop();
	}

	@Override
	protected void onResize(int width, int height) {
		this.application.onResize(width, height);
	}

	private static class Application {
		private final Game game;
		private final GameRenderable gameRenderable;
		private final GameInput gameInput;
		private final Ui ui;
		private final UiRenderable uiRenderable;
		private final UiInput uiInput;
		private final ApplicationWindow window;

		public Application(ApplicationWindow window) {
			this.window = window;
			this.game = Game.create();
			this.gameRenderable = GameRenderable.create(this.game);
			this.gameInput = GameInput.create(window.getInput(), this.gameRenderable.getCamera(), this.game);
			this.ui = Ui.create(this.game, this.gameInput);
			this.uiRenderable = UiRenderable.create(this.ui, 800, 600);
			this.uiInput = UiInput.create(window.getInput(), this.ui);
		}

		public void onResize(int width, int height) {
			this.gameRenderable.onResize(width, height);
			this.uiRenderable.onResize(width, height);
		}

		public void loop() {
			if (!this.uiInput.input()) {
				this.gameInput.input();
			}
			this.gameRenderable.render();
			this.gameInput.render(this.gameRenderable.getBatch(), this.gameRenderable.getBuffer());
			this.uiRenderable.render();
		}
	}
}
