package de.hetzge.sgamelwjgl.base;

import java.io.Serializable;

public final class ReusableIntBuffer implements Serializable {

	private final long[] values;
	private long offset;
	private int max;

	private ReusableIntBuffer(long[] values, long offset, int max) {
		this.values = values;
		this.offset = offset;
		this.max = max;
	}

	public void set(int index, int value) {
		if (value > this.max) {
			this.max = value;
		}
		this.values[index] = this.offset + value;
	}

	public int get(int index) {
		return (int) (this.values[index] - this.offset);
	}

	public void reset() {
		this.offset = this.offset + this.max + 1;
		this.max = 0;

		if (this.offset > (Long.MAX_VALUE - Integer.MAX_VALUE)) {
			for (int i = 0; i < this.values.length; i++) {
				this.values[i] = Long.MIN_VALUE;
			}
			this.offset = Long.MIN_VALUE + 1;
		}
	}

	public static ReusableIntBuffer create(int length) {
		final ReusableIntBuffer reusableIntBuffer = new ReusableIntBuffer(new long[length], 0L, 0);
		reusableIntBuffer.reset();
		return reusableIntBuffer;
	}

	public static void main(String[] args) {

		final int times = 100000000;
		final long[] as = new long[times];
		final int[] bs = new int[times];

		for (int z = 0; z < 100; z++) {
			{
				final long before = System.nanoTime();

				for (int i = 0; i < 10000; i++) {
					for (int j = 0; j < as.length; j++) {
						final int x = (int) as[j];
						final int y = x * 2;
					}
				}
				System.out.println("A: " + (System.nanoTime() - before));
			}

			{
				final long before = System.nanoTime();

				for (int i = 0; i < 10000; i++) {
					for (int j = 0; j < bs.length; j++) {
						final int x = bs[j];
						final int y = x * 2;
					}
				}
				System.out.println("B: " + (System.nanoTime() - before));
			}
		}
	}
}
