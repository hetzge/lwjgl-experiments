package de.hetzge.sgamelwjgl.base;

@FunctionalInterface
public interface PositionFunction<T> {
	T apply(int x, int y);
}
