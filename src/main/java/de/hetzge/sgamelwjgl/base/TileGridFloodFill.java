package de.hetzge.sgamelwjgl.base;

import org.joml.Vector2i;

public final class TileGridFloodFill {

	private final TileGrid tileGrid;
	private final TileConnectionOffset[] offsets;

	private final IntArrayQueue xs;
	private final IntArrayQueue ys;

	private final int[] done;
	private int doneValue;

	private TileGridFloodFill(TileGrid tileGrid, TileConnectionOffset[] offsets) {
		this.tileGrid = tileGrid;
		this.offsets = offsets;

		this.xs = new IntArrayQueue(this.tileGrid.getLength());
		this.ys = new IntArrayQueue(this.tileGrid.getLength());
		this.done = new int[this.tileGrid.getLength()];
		this.doneValue = Integer.MAX_VALUE;
	}

	public Vector2i flood(int fromX, int fromY, PositionPredicate collisionPredicate, PositionPredicate searchPredicate) {
		nextDoneValue();

		this.xs.reset();
		this.ys.reset();

		int currentX = fromX;
		int currentY = fromY;

		do {
			if (searchPredicate.test(currentX, currentY)) {
				return new Vector2i(currentX, currentY);
			}

			if (!collisionPredicate.test(currentX, currentY)) {
				for (int orientationIndex = 0; orientationIndex < this.offsets.length; orientationIndex++) {
					final TileConnectionOffset orientation = this.offsets[orientationIndex];

					final int x = currentX + orientation.getOffsetX();
					final int y = currentY + orientation.getOffsetY();
					addNext(this.tileGrid, x, y);
				}
			}

			currentX = this.xs.poll();
			currentY = this.ys.poll();
		} while ((currentX != Integer.MIN_VALUE) && (currentY != Integer.MIN_VALUE));

		return null;
	}

	private void nextDoneValue() {
		if (this.doneValue == Integer.MAX_VALUE) {
			for (int i = 0; i < this.done.length; i++) {
				this.done[i] = Integer.MIN_VALUE;
			}
			this.doneValue = Integer.MIN_VALUE + 1;
		} else {
			this.doneValue++;
		}
	}

	private void addNext(TileGrid tileGrid, int x, int y) {
		final int index = tileGrid.index(x, y);
		if (tileGrid.isValid(x, y) && (this.done[index] != this.doneValue)) {
			this.done[index] = this.doneValue;
			this.xs.add(x);
			this.ys.add(y);
		}
	}

	public static TileGridFloodFill create(TileGrid tileGrid, TileConnectionOffset[] offsets) {
		return new TileGridFloodFill(tileGrid, offsets);
	}

	public static void main(String[] args) {

		final TileGridFloodFill tiledBuffer = TileGridFloodFill.create(new TileGrid(1000, 1000), Orientation.VALUES);
		final long before = System.nanoTime();
		for (int i = 0; i < 1000; i++) {
			tiledBuffer.flood(0, 0, (x, y) -> false, (x, y) -> (x == 999) && (y == 999));
		}
		System.out.println((System.nanoTime() - before) / 10000);
	}

}
