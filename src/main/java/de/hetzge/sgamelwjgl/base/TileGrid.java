package de.hetzge.sgamelwjgl.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.joml.Vector2i;

public final class TileGrid implements Serializable {

	private final int width;
	private final int height;
	private final int length;

	public TileGrid(int width, int height) {
		this.width = width;
		this.height = height;
		this.length = width * height;
	}

	public void forEach(PositionConsumer consumer) {
		forEachSize(0, 0, this.width, this.height, consumer);
	}

	public void forEachSize(int x, int y, int width, int height, PositionConsumer consumer) {
		forEach(x, y, (x + width) - 1, (y + height) - 1, consumer);
	}

	public void forEach(int fromX, int fromY, int toX, int toY, PositionConsumer consumer) {
		for (int y = Math.min(clampY(fromY), clampY(toY)); y <= Math.max(clampY(fromY), clampY(toY)); y++) {
			for (int x = Math.min(clampX(fromX), clampX(toX)); x <= Math.max(clampX(fromX), clampX(toX)); x++) {
				consumer.accept(x, y);
			}
		}
	}

	public void forEachAround(int x, int y, PositionConsumer consumer) {
		for (int orientationIndex = 0; orientationIndex < Orientation.VALUES.length; orientationIndex++) {
			final Orientation orientation = Orientation.VALUES[orientationIndex];
			final int ox = x + orientation.x;
			final int oy = y + orientation.y;
			if (isValidX(ox) && isValidY(oy)) {
				consumer.accept(ox, oy);
			}
		}
	}

	public boolean allMatchSize(int x, int y, int width, int height, PositionPredicate predicate) {
		return allMatch(x, y, (x + width) - 1, (y + height) - 1, predicate);
	}

	public boolean allMatch(PositionPredicate predicate) {
		return allMatchSize(0, 0, this.width, this.height, predicate);
	}

	public boolean allMatch(int fromX, int fromY, int toX, int toY, PositionPredicate predicate) {
		for (int y = Math.min(clampY(fromY), clampY(toY)); y <= Math.max(clampY(fromY), clampY(toY)); y++) {
			for (int x = Math.min(clampX(fromX), clampX(toX)); x <= Math.max(clampX(fromX), clampX(toX)); x++) {
				if (!predicate.test(x, y)) {
					return false;
				}
			}
		}
		return true;
	}

	public boolean allMatchAround(int x, int y, PositionPredicate predicate) {
		for (int orientationIndex = 0; orientationIndex < Orientation.VALUES.length; orientationIndex++) {
			final Orientation orientation = Orientation.VALUES[orientationIndex];
			final int ox = x + orientation.x;
			final int oy = y + orientation.y;
			if (isValidX(ox) && isValidY(oy)) {
				if (!predicate.test(ox, oy)) {
					return false;
				}
			}
		}
		return true;
	}

	public boolean allMatchAroundSize(int x, int y, int width, int height, PositionPredicate predicate) {
		return allMatchSize(x - 1, y - 1, width + 2, height + 2, (mx, my) -> {
			if ((mx == (x - 1)) || ((mx == (x + width)) && (my == (y - 1))) || (my == (y + height))) {
				return predicate.test(mx, my);
			} else {
				return true;
			}
		});
	}

	public boolean noneMatchSize(int x, int y, int width, int height, PositionPredicate predicate) {
		return !anyMatch(x, y, (x + width) - 1, (y + height) - 1, predicate);
	}

	public boolean noneMatch(PositionPredicate predicate) {
		return !anyMatch(0, 0, this.width, this.height, predicate);
	}

	public boolean noneMatch(int fromX, int fromY, int toX, int toY, PositionPredicate predicate) {
		return !anyMatch(fromX, fromY, toX, toY, predicate);
	}

	public boolean anyMatchSize(int x, int y, int width, int height, PositionPredicate predicate) {
		return anyMatch(x, y, (x + width) - 1, (y + height) - 1, predicate);
	}

	public boolean anyMatch(PositionPredicate predicate) {
		return anyMatch(0, 0, this.width, this.height, predicate);
	}

	public boolean anyMatch(int fromX, int fromY, int toX, int toY, PositionPredicate predicate) {
		for (int y = Math.min(clampY(fromY), clampY(toY)); y <= Math.max(clampY(fromY), clampY(toY)); y++) {
			for (int x = Math.min(clampX(fromX), clampX(toX)); x <= Math.max(clampX(fromX), clampX(toX)); x++) {
				if (predicate.test(x, y)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean anyMatchAround(int x, int y, PositionPredicate predicate) {
		for (int orientationIndex = 0; orientationIndex < Orientation.VALUES.length; orientationIndex++) {
			final Orientation orientation = Orientation.VALUES[orientationIndex];
			final int ox = x + orientation.x;
			final int oy = y + orientation.y;
			if (isValidX(ox) && isValidY(oy)) {
				if (predicate.test(ox, oy)) {
					return true;
				}
			}
		}
		return false;
	}

	public <T> List<T> mapSize(int x, int y, int width, int height, PositionFunction<T> function) {
		return map(x, y, (x + width) - 1, (y + height) - 1, function);
	}

	public <T> List<T> map(int fromX, int fromY, int toX, int toY, PositionFunction<T> function) {
		final List<T> result = new ArrayList<>(width(fromX, toX) * height(fromY, toY));
		for (int y = Math.min(clampY(fromY), clampY(toY)); y <= Math.max(clampY(fromY), clampY(toY)); y++) {
			for (int x = Math.min(clampX(fromX), clampX(toX)); x <= Math.max(clampX(fromX), clampX(toX)); x++) {
				result.add(function.apply(x, y));
			}
		}
		return result;
	}

	public <T> List<T> mapAround(int x, int y, PositionFunction<T> function) {
		final List<T> result = new ArrayList<>(4);
		for (int orientationIndex = 0; orientationIndex < Orientation.VALUES.length; orientationIndex++) {
			final Orientation orientation = Orientation.VALUES[orientationIndex];
			final int ox = x + orientation.x;
			final int oy = y + orientation.y;
			if (isValidX(ox) && isValidY(oy)) {
				result.add(function.apply(ox, oy));
			}
		}
		return result;
	}

	public Vector2i findPositionAround(int x, int y, PositionPredicate predicate) {
		for (int orientationIndex = 0; orientationIndex < Orientation.VALUES.length; orientationIndex++) {
			final Orientation orientation = Orientation.VALUES[orientationIndex];
			final int ox = x + orientation.x;
			final int oy = y + orientation.y;
			if (isValidX(ox) && isValidY(oy) && predicate.test(ox, oy)) {
				return new Vector2i(ox, oy);
			}
		}
		return null;
	}

	public Vector2i findPositionSize(int x, int y, int width, int height, PositionPredicate predicate) {
		return findPosition(x, y, (x + width) - 1, (y + height) - 1, predicate);
	}

	public Vector2i findPosition(PositionPredicate predicate) {
		return findPosition(0, 0, this.width - 1, this.height - 1, predicate);
	}

	public Vector2i findPosition(int fromX, int fromY, int toX, int toY, PositionPredicate predicate) {
		for (int y = Math.min(clampY(fromY), clampY(toY)); y <= Math.max(clampY(fromY), clampY(toY)); y++) {
			for (int x = Math.min(clampX(fromX), clampX(toX)); x <= Math.max(clampX(fromX), clampX(toX)); x++) {
				if (predicate.test(x, y)) {
					return new Vector2i(x, y);
				}
			}
		}
		return null;
	}

	public Vector2i findPositionAroundSize(int x, int y, int width, int height, PositionPredicate predicate) {
		final int fromX = clampX(x - 1);
		final int fromY = clampY(y - 1);
		final int toX = clampX((x - 1) + width + 2);
		final int toY = clampY((y - 1) + height + 2);
		for (int fy = fromY; fy < toY; fy++) {
			for (int fx = fromX; fx < toX; fx++) {
				if ((fx == fromX) || ((fx == (toX - 1)) && (fy == fromY)) || (fy == (toY - 1))) {
					if (predicate.test(fx, fy)) {
						return new Vector2i(fx, fy);
					}
				}
			}
		}
		return null;
	}

	public <T> T findAround(int x, int y, int offset, PositionFunction<T> searchFunction) {
		for (int orientationIndex = 0; orientationIndex < Orientation.VALUES.length; orientationIndex++) {
			final Orientation orientation = Orientation.VALUES[(offset + orientationIndex) % Orientation.VALUES.length];
			final int ox = x + orientation.x;
			final int oy = y + orientation.y;
			if (isValidX(ox) && isValidY(oy)) {
				final T result = searchFunction.apply(ox, oy);
				if (result != null) {
					return result;
				}
			}
		}
		return null;
	}

	public boolean allValid(int x, int y, int width, int height) {
		return allMatchSize(x, y, width, height, (mx, my) -> isValid(mx, my));
	}

	public boolean isValid(int x, int y) {
		return isValidX(x) && isValidY(y);
	}

	public boolean isValidX(int x) {
		return (x >= 0) && (x < this.width);
	}

	public boolean isValidY(int y) {
		return (y >= 0) && (y < this.height);
	}

	public boolean isBorder(int x, int y) {
		return (x == 0) || (y == 0) || (x == (this.width - 1)) || (y == (this.height - 1));
	}

	public boolean isValid(int index) {
		return (index >= 0) && (index < this.length);
	}

	public int width(int fromX, int toX) {
		return Math.abs(clampX(fromX) - clampX(toX));
	}

	public int height(int fromY, int toY) {
		return Math.abs(clampX(fromY) - clampY(toY));
	}

	public int length(int fromX, int fromY, int toX, int toY) {
		return width(fromX, toX) * height(fromY, toY);
	}

	public int index(int x, int y) {
		return (y * this.width) + x;
	}

	public int x(int index) {
		return index % this.width;
	}

	public int y(int index) {
		return index / this.width;
	}

	public int getLength() {
		return this.length;
	}

	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}

	public int clampX(int x) {
		return x < 0 ? 0 : x >= this.width ? this.width - 1 : x;
	}

	public int clampY(int y) {
		return y < 0 ? 0 : y >= this.height ? this.height - 1 : y;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + this.height;
		result = (prime * result) + this.width;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final TileGrid other = (TileGrid) obj;
		if (this.height != other.height) {
			return false;
		}
		if (this.width != other.width) {
			return false;
		}
		return true;
	}
}
