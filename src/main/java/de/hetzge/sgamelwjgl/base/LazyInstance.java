package de.hetzge.sgamelwjgl.base;

import java.util.function.Supplier;

public final class LazyInstance<T> {

	private final Supplier<T> factory;

	private T instance;

	private LazyInstance(Supplier<T> factory) {
		this.factory = factory;
	}

	public T get() {
		if (this.instance == null) {
			this.instance = this.factory.get();
			assert this.instance != null;
		}
		return this.instance;
	}

	public static <T> LazyInstance<T> create(Supplier<T> factory) {
		return new LazyInstance<>(factory);
	}
}
