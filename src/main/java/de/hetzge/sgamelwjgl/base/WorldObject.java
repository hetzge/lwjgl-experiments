package de.hetzge.sgamelwjgl.base;

import de.hetzge.sgamelwjgl.entity.Building;
import de.hetzge.sgamelwjgl.entity.BuildingType;
import de.hetzge.sgamelwjgl.entity.Person;
import de.hetzge.sgamelwjgl.entity.PersonType;

public interface WorldObject {

	int getId();

	int getX();

	int getY();

	int getWidth();

	int getHeight();

	default boolean contains(int x, int y) {
		return (x >= getX()) && (y >= getY()) && (x < (getX() + getWidth())) && (y < (getY() + getHeight()));
	}

	default boolean is(BuildingType buildingType) {
		return (this instanceof Building) && (((Building) this).getBuildingType() == buildingType);
	}
	
	default boolean is(PersonType personType) {
		return (this instanceof Person) && (((Person) this).getPersonType() == personType);
	}
}
