package de.hetzge.sgamelwjgl.base;

import java.util.ArrayList;
import java.util.List;

public final class TileNet {

	private final TileGrid tileGrid;
	private final TileNetNode[] nodes;

	private TileNet(TileGrid tileGrid, TileNetNode[] nodes) {
		this.tileGrid = tileGrid;
		this.nodes = nodes;
	}

	public TileNetNode getNode(int index) {
		return this.nodes[index];
	}

	public int index(int x, int y) {
		return this.tileGrid.index(x, y);
	}

	public static TileNet create(int width, int height) {
		final TileGrid tileGrid = new TileGrid(width, height);
		final TileNetNode[] nodes = new TileNetNode[tileGrid.getLength()];
		tileGrid.forEachSize(0, 0, width, height, (x, y) -> {
			final TileNetNode node = getOrCreate(tileGrid, x, y, nodes);
			if (tileGrid.isValidX(x - 1)) {
				node.getNeighbors().add(getOrCreate(tileGrid, x - 1, y, nodes));
			}
			if (tileGrid.isValidX(x + 1)) {
				node.getNeighbors().add(getOrCreate(tileGrid, x + 1, y, nodes));
			}
			if (tileGrid.isValidY(y - 1)) {
				node.getNeighbors().add(getOrCreate(tileGrid, x, y - 1, nodes));
			}
			if (tileGrid.isValidY(y + 1)) {
				node.getNeighbors().add(getOrCreate(tileGrid, x, y + 1, nodes));
			}
		});

		return new TileNet(tileGrid, nodes);
	}

	private static TileNetNode getOrCreate(TileGrid tileGrid, int x, int y, TileNetNode[] nodes) {
		final int index = tileGrid.index(x, y);
		if (nodes[index] == null) {
			final List<TileNetNode> neighbors = new ArrayList<>(4);
			nodes[index] = new TileNetNode(x, y, index, neighbors);
		}
		return nodes[index];
	}

	public static class TileNetNode {

		final int x;
		final int y;
		final int index;
		final List<TileNetNode> neighbors;

		public TileNetNode(int x, int y, int index, List<TileNetNode> neighbors) {
			this.x = x;
			this.y = y;
			this.index = index;
			this.neighbors = neighbors;
		}

		public int getX() {
			return this.x;
		}

		public int getY() {
			return this.y;
		}

		public int getIndex() {
			return this.index;
		}

		public List<TileNetNode> getNeighbors() {
			return this.neighbors;
		}

		@Override
		public int hashCode() {
			return this.index;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final TileNetNode other = (TileNetNode) obj;
			if (this.index != other.index) {
				return false;
			}
			return true;
		}
	}
}
