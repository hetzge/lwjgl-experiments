package de.hetzge.sgamelwjgl.base;

public interface TileConnectionOffset {

	int getOffsetX();

	int getOffsetY();

}
