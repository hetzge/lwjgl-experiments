package de.hetzge.sgamelwjgl.base;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.joml.Vector2i;

import de.hetzge.sgamelwjgl.Utils;

public final class Pathfinder {

	private final TileGrid tileGrid;
	private final ArrayQueue nextQueue;
	private final ReusableIntBuffer ratings;
	private final ReusableIntBuffer previouses;
	private final TileGridFloodFill floodFill;
	private final IntArrayQueue xs;
	private final IntArrayQueue ys;

	private Pathfinder(TileGrid tileGrid, ArrayQueue nextQueue, ReusableIntBuffer ratings, ReusableIntBuffer previouses, TileGridFloodFill floodFill, IntArrayQueue xs, IntArrayQueue ys) {
		this.tileGrid = tileGrid;
		this.nextQueue = nextQueue;
		this.ratings = ratings;
		this.previouses = previouses;
		this.floodFill = floodFill;
		this.xs = xs;
		this.ys = ys;
	}

	public <T, X> Result<T, X> find(T from, Function<T, List<T>> aroundFunction, Function<T, X> search, int limit) {

		final Map<T, T> previous = new HashMap<T, T>(limit);
		previous.put(from, null);

		this.nextQueue.reset();
		this.nextQueue.add(from);

//		final Queue<T> next = new LinkedList<>(); // TODO use array and index instead (object[])
//		next.add(from);

		int depth = 0;

		T head;
		while ((head = (T) this.nextQueue.poll()) != null) {
			depth++;
			final X value = search.apply(head);
			if (value != null) {
				final List<T> path = new ArrayList<>();
				path.add(head);

				T prev = head;
				while ((prev = previous.get(prev)) != null) {
					path.add(prev);
				}
				Collections.reverse(path);

				System.out.println("depth: " + depth);
				return new Result<>(path, value);
			}

			final List<T> arounds = aroundFunction.apply(head);
			for (final T around : arounds) {
				if (!previous.containsKey(around)) {
					previous.put(around, head);
					this.nextQueue.add(around);

					if (previous.size() > limit) {
						System.out.println("depth: " + depth);
						return null;
					}
				}
			}
		}

		System.out.println("depth: " + depth);
		return null;
	}

	public <T> List<T> find(T from, T to, Function<T, List<T>> aroundFunction, BiFunction<T, T, Double> distanceFunction, int limit) {

		final Map<T, T> previous = new HashMap<T, T>(limit);

		T current = to;
		T next = null;
		int length = 0;
		int count = 0;

		do {
			double distance = Double.MAX_VALUE;
			final List<T> arounds = aroundFunction.apply(current);
			for (final T around : arounds) {
				final double aroundDistance = distanceFunction.apply(from, around);
				if (aroundDistance < distance) {
					next = around;
					distance = aroundDistance;
				}
			}

			if (next.equals(current)) {
				next = previous.get(current);
				length--;
			}

			if (previous.get(next) == null) {
				previous.put(next, current);
				length++;
			}

			if (next.equals(current)) {
				return null;
			}

			current = next;

			count++;

			if (count == limit) {
				return null;
			}
		} while (!current.equals(from));

		final List<T> result = new ArrayList<>(length + 1);
		result.add(from);
		do {
			current = previous.get(current);
			result.add(current);
		} while (!current.equals(to));

		return result;
	}

	public Path findPath(int fromX, int fromY, int toX, int toY, int limit, PositionPredicate collisionPredicate) {
		return find222(fromX, fromY, toX, toY, limit, collisionPredicate);
//		return Path.create(this.tileGrid, find(fromX, fromY, toX, toY, limit, collisionPredicate));
	}

	public Path find222(int fromX, int fromY, int toX, int toY, int limit, PositionPredicate collisionPredicate) {

		this.xs.reset();
		this.ys.reset();

		final Vector2i found = this.floodFill.flood(toX, toY, collisionPredicate, (x, y) -> {
			if (!collisionPredicate.test(x, y)) {
				this.xs.add(x);
				this.ys.add(y);
			}
			return (x == fromX) && (y == fromY);
		});

		if (found != null) {
			int cx = fromX;
			int cy = fromY;
			final Queue<Vector2i> waypoints = new LinkedList<>();
			waypoints.add(new Vector2i(cx, cy));
			do {
				final int x = this.xs.pollTail();
				final int y = this.ys.pollTail();

				if (x != Integer.MIN_VALUE) {
					final int nx = Math.abs(cx - x);
					final int ny = Math.abs(cy - y);
					if (((nx == 1) && (ny == 0)) || ((nx == 0) && (ny == 1))) {
						waypoints.add(new Vector2i(x, y));
						cx = x;
						cy = y;
					}
				} else {
					return null;
				}
			} while ((cx != toX) || (cy != toY));
			return Path.create(waypoints);
		} else {
			return null;
		}

	}

	public int[] find(int fromX, int fromY, int toX, int toY, int limit, PositionPredicate collisionPredicate) {
		if (collisionPredicate.test(fromX, fromY) || collisionPredicate.test(toX, toY)) {
			return null;
		}

		this.previouses.reset();

		final int[] xs = new int[] { -1, 1, 0, 0 };
		final int[] ys = new int[] { 0, 0, -1, 1 };

		int x = toX;
		int y = toY;
		int nextX = 0;
		int nextY = 0;
		int length = 0;
		int count = 0;

		do {
			double distance = Double.MAX_VALUE;
			for (int i = 0; i < 4; i++) {
				final int xsi = xs[i];
				final int ysi = ys[i];

				if (this.tileGrid.isValid(x + xsi, y + ysi) && !collisionPredicate.test(x + xsi, y + ysi)) {
					final double ds = (this.previouses.get(this.tileGrid.index(x + xsi, y + ysi)) < 0) ? Utils.distance(x + xsi, y + ysi, fromX, fromY) : Double.MAX_VALUE;
					if (ds < distance) {
						nextX = x + xsi;
						nextY = y + ysi;
						distance = ds;
					}
				}
			}
			if ((nextX == x) && (nextY == y)) {
				final int nextIndex = this.previouses.get(this.tileGrid.index(x, y));
				nextX = this.tileGrid.x(nextIndex);
				nextY = this.tileGrid.y(nextIndex);
				length--;
			}

			if (this.previouses.get(this.tileGrid.index(nextX, nextY)) < 0) {
				this.previouses.set(this.tileGrid.index(nextX, nextY), this.tileGrid.index(x, y));
				length++;
			}

			if ((nextX == x) && (nextY == y)) {
				return null;
			}

			x = nextX;
			y = nextY;

			count++;

			if (count == limit) {
				return null;
			}
		} while ((x != fromX) || (y != fromY));

		int i = 0;
		final int[] result = new int[length + 1];
		result[0] = this.tileGrid.index(fromX, fromY);
		do {
			final int previousI = this.previouses.get(this.tileGrid.index(x, y));
			x = this.tileGrid.x(previousI);
			y = this.tileGrid.y(previousI);
			result[++i] = previousI;
		} while ((x != toX) || (y != toY));

		return result;
	}

	public static Pathfinder create(TileGrid tileGrid) {
		return new Pathfinder(tileGrid, new ArrayQueue(tileGrid.getLength()), ReusableIntBuffer.create(tileGrid.getLength()), ReusableIntBuffer.create(tileGrid.getLength()), TileGridFloodFill.create(tileGrid, Orientation.VALUES), new IntArrayQueue(tileGrid.getLength()), new IntArrayQueue(tileGrid.getLength()));
	}

	public static void main(String[] args) {
		final TileGrid tileGrid = new TileGrid(1000, 1000);
		final Pathfinder pathfinder = create(tileGrid);

		for (int i = 0; i < 1; i++) {
			final long before = System.nanoTime();
			final Path find222 = pathfinder.find222(10, 10, 100, 100, 999999, (x, y) -> false);
			find222.getWaypoints().forEach(it -> {
				System.out.println(it.x + " " + it.y);
			});
			System.out.println("time: " + (System.nanoTime() - before));
		}

//		find222.getWaypoints().forEach(waypoint -> {
////			System.out.println(waypoint.toString(NumberFormat.getIntegerInstance()));
//		});

//		final List<Integer> arounds = new ArrayList<>(4);
//
//		for (int i = 0; i < 1000000; i++) {
//			{
//				final long before = System.nanoTime();
//				pathfinder.find(tileGrid.index(10, 10), a -> {
//
//					final long innerbefore = System.nanoTime();
//
//					final int x = tileGrid.x(a);
//					final int y = tileGrid.y(a);
//
//					arounds.clear();
//
//					if (tileGrid.isValidX(x - 1)) {
//						arounds.add(tileGrid.index(x - 1, y));
//					}
//					if (tileGrid.isValidX(x + 1)) {
//						arounds.add(tileGrid.index(x + 1, y));
//					}
//					if (tileGrid.isValidY(y - 1)) {
//						arounds.add(tileGrid.index(x, y - 1));
//					}
//					if (tileGrid.isValidY(y + 1)) {
//						arounds.add(tileGrid.index(x, y + 1));
//					}
//
//					System.out.println(System.nanoTime() - innerbefore);
//
//					return arounds;
//				}, a -> {
//					return a.equals(tileGrid.index(50, 50)) ? Boolean.TRUE : null;
//				}, 5000);
//				System.out.println("a: " + (System.nanoTime() - before));
//			}

//			{
//				final long before = System.currentTimeMillis();
//				final int[] path = find(new TileGrid(10000, 10000), 0, 10, 9000, 9000, 100000, (x, y) -> false);
//				System.out.println(path.length);
//				System.out.println("b: " + (System.currentTimeMillis() - before));
//			}
//			{
//				final long before = System.nanoTime();
//				final List<Integer> path = find(tileGrid.index(0, 10), tileGrid.index(9000, 9000), a -> {
//					final int x = tileGrid.x(a);
//					final int y = tileGrid.y(a);
//					
//					arounds.clear();
//
//					if (tileGrid.isValidX(x - 1)) {
//						arounds.add(tileGrid.index(x - 1, y));
//					}
//					if (tileGrid.isValidX(x + 1)) {
//						arounds.add(tileGrid.index(x + 1, y));
//					}
//					if (tileGrid.isValidY(y - 1)) {
//						arounds.add(tileGrid.index(x, y - 1));
//					}
//					if (tileGrid.isValidY(y + 1)) {
//						arounds.add(tileGrid.index(x, y + 1));
//					}
//
//					return arounds;
//				}, (a, b) -> {
//					return tileGrid.distance(tileGrid.x(a), tileGrid.y(a), tileGrid.x(b), tileGrid.y(b));
//				}, 20000);
//				System.out.println(path.size());
//				System.out.println("c: " + (System.nanoTime() - before));
//			}
//		}

	}

	public static class Result<T, X> {
		private final List<T> path;
		private final X value;

		public Result(List<T> path, X value) {
			this.path = path;
			this.value = value;
		}

		public List<T> getPath() {
			return this.path;
		}

		public X getValue() {
			return this.value;
		}
	}
}
