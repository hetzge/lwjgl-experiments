package de.hetzge.sgamelwjgl.base;

@FunctionalInterface
public interface PositionPredicate {
	boolean test(int x, int y);
}
