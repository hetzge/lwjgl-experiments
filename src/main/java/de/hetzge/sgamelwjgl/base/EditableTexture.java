package de.hetzge.sgamelwjgl.base;

import java.awt.Color;
import java.io.File;
import java.nio.ByteBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.stb.STBImage;
import org.lwjgl.stb.STBImageWrite;

import de.hetzge.sgamelwjgl.tile.MaskOperations;

public final class EditableTexture implements MaskOperations {

	public static final Color MASK_COLOR = Color.BLACK;
	private final int width;
	private final int height;
	private final int channelsCount;
	private final ByteBuffer data;

	private EditableTexture(int width, int height, int channelsCount, ByteBuffer data) {
		this.width = width;
		this.height = height;
		this.channelsCount = channelsCount;
		this.data = data;
	}

	public TileGrid getTileGrid() {
		return new TileGrid(this.width, this.height);
	}

	public void save(File file) {
		this.data.clear();
		final String path = file.getPath();
		final boolean stbi_write_png = STBImageWrite.stbi_write_png(path, this.width, this.height, this.channelsCount, this.data, 0);
		if (!stbi_write_png) {
			throw new IllegalStateException(String.format("Failed to write texture to file: '%s'", path));
		}
	}

	public void set(int x, int y, Color color) {
		this.set(x, y, this.toByte(color.getRed()), this.toByte(color.getGreen()), this.toByte(color.getBlue()), this.toByte(color.getAlpha()));
	}

	public void set(int x, int y, byte r, byte g, byte b, byte a) {
		final int i = ((y * this.width) + x) * this.channelsCount;
		this.data.put(i + 0, r);
		this.data.put(i + 1, g);
		this.data.put(i + 2, b);
		this.data.put(i + 3, a);
	}

	public void fill(int x, int y, int width, int height, Color color) {
		if ((width < 0) || (height < 0)) {
			throw new IllegalArgumentException("Illegal fill width/height.");
		}
		for (int yy = y; yy < Math.min(this.height, y + height); yy++) {
			for (int xx = x; xx < Math.min(this.width, x + width); xx++) {
				this.set(xx, yy, color);
			}
		}
	}

	public boolean isColor(int x, int y, Color color) {
		return this.isColor(x, y, this.toByte(color.getRed()), this.toByte(color.getGreen()), this.toByte(color.getBlue()));
	}

	public boolean isColor(int x, int y, byte r, byte g, byte b) {
		final int i = ((y * this.width) + x) * this.channelsCount;
		return (this.data.get(i + 0) == r) && (this.data.get(i + 1) == g) && (this.data.get(i + 2) == b) && this.hasColor(x, y);
	}

	public boolean hasColor(int x, int y) {
		return getAlpha(x, y) > 0;
	}
	
	public int getAlpha(int x, int y) {
		final int i = ((y * this.width) + x) * this.channelsCount;
		return (this.data.get(i + 3) & 0xff);
	}
	
	public int getBlackWhite(int x, int y) {
		final int i = ((y * this.width) + x) * this.channelsCount;
		return ((this.data.get(i + 0) & 0xff) + (this.data.get(i + 1) & 0xff) + (this.data.get(i + 2) & 0xff)) / 3;
	}

	public void apply(EditableTexture texture, int rootX, int rootY) {
		this.apply(texture, false, rootX, rootY);
	}

	public void applyMask(EditableTexture maskTexture, int rootX, int rootY) {
		this.apply(maskTexture, true, rootX, rootY);
	}

	private void apply(EditableTexture texture, boolean mask, int rootX, int rootY) {
		for (int y = rootY; y < Math.min(this.height, rootY + texture.height); y++) {
			for (int x = rootX; x < Math.min(this.width, rootX + texture.width); x++) {
				final int textureX = x - rootX;
				final int textureY = y - rootY;
				if (mask) {
					this.applyMask(texture, x, y, textureX, textureY);
				} else {
					this.apply(texture, x, y, textureX, textureY);
				}
			}
		}
	}

	private void apply(EditableTexture texture, int x, int y, int textureX, int textureY) {
		if (texture.hasColor(textureX, textureY)) {
			final int i = ((y * this.width) + x) * this.channelsCount;
			final int textureI = ((textureY * texture.width) + textureX) * this.channelsCount;
			this.data.put(i + 0, texture.data.get(textureI + 0));
			this.data.put(i + 1, texture.data.get(textureI + 1));
			this.data.put(i + 2, texture.data.get(textureI + 2));
			this.data.put(i + 3, texture.data.get(textureI + 3));
		}
	}

	private void applyMask(EditableTexture maskTexture, int x, int y, int textureX, int textureY) {
		final int i = ((y * this.width) + x) * 4;
		final int textureI = ((textureY * maskTexture.height) + textureX) * 4;
		if (maskTexture.isColor(x, y, MASK_COLOR)) {
			this.data.put(i + 3, maskTexture.data.get(textureI + 3));
		} else {
			this.data.put(i + 3, this.toByte(0));
		}
	}

	@Override
	public void increaseMaskEdges() {
		this.increaseMask(true);
	}

	@Override
	public void increaseMask() {
		this.increaseMask(false);
	}

	private void increaseMask(boolean edge) {
		final EditableTexture changeTexture = create(this.width, this.height);
		for (int y = 0; y < this.height; y++) {
			for (int x = 0; x < this.width; x++) {
				if (!this.isColor(x, y, MASK_COLOR)) {
					if (edge) {
						this.increaseMaskEdge(x, y, changeTexture);
					} else {
						this.increaseMask(x, y, changeTexture);
					}
				}
			}
		}
		this.apply(changeTexture, false, 0, 0);
	}

	private void increaseMask(int x, int y, EditableTexture changeTexture) {
		if ((((x + 1) < this.width) && this.isColor(x + 1, y, MASK_COLOR)) || (((x - 1) >= 0) && this.isColor(x - 1, y, MASK_COLOR)) || (((y + 1) < this.height) && this.isColor(x, y + 1, MASK_COLOR)) || (((y - 1) >= 0) && this.isColor(x, y - 1, MASK_COLOR))) {
			changeTexture.set(x, y, MASK_COLOR);
		}
	}

	private void increaseMaskEdge(int x, int y, EditableTexture changeTexture) {
		final boolean horizontal = (((x + 1) < this.width) && this.isColor(x + 1, y, MASK_COLOR)) || (((x - 1) >= 0) && this.isColor(x - 1, y, MASK_COLOR));
		final boolean vertical = (((y + 1) < this.height) && this.isColor(x, y + 1, MASK_COLOR)) || (((y - 1) >= 0) && this.isColor(x, y - 1, MASK_COLOR));
		if (horizontal && vertical) {
			changeTexture.set(x, y, MASK_COLOR);
		}
	}

	private byte toByte(int value) {
		return (byte) value;
	}

	public static EditableTexture create(int width, int height) {
		return new EditableTexture(width, height, 4, BufferUtils.createByteBuffer(width * height * 4));
	}

	public static EditableTexture create(File file) {
		final int[] widthPointer = new int[1];
		final int[] heightPointer = new int[1];
		final int[] channelsPointer = new int[1];
		final ByteBuffer buffer = STBImage.stbi_load(file.getPath(), widthPointer, heightPointer, channelsPointer, 4);
		return new EditableTexture(widthPointer[0], heightPointer[0], channelsPointer[0], buffer);
	}
}
