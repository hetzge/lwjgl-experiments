package de.hetzge.sgamelwjgl.base;

public final class ArrayQueue {

	private final Object[] elements;
	private int headIndex;
	private int tailIndex;

	public ArrayQueue(int size) {
		this.elements = new Object[size];
		this.headIndex = 0;
		this.tailIndex = 0;
	}

	public void add(Object element) {
		this.elements[this.tailIndex++] = element;
	}

	public Object poll() {
		if (this.headIndex == this.tailIndex) {
			return null;
		} else {
			return this.elements[this.headIndex++];
		}
	}

	public void reset() {
		this.headIndex = 0;
		this.tailIndex = 0;
	}

	public int getHeadIndex() {
		return this.headIndex;
	}

	public int getTailIndex() {
		return this.tailIndex;
	}
}
