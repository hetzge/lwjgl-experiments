package de.hetzge.sgamelwjgl.base;

public final class PixelGrid {

	private final int tileSize;

	public PixelGrid(int tileSize) {
		this.tileSize = tileSize;
	}

	/**
	 * Converts a pixel value to a tile value in a normal grid:
	 * 
	 * <pre>
	 * #----#
	 * |    |
	 * |    |
	 * #----#
	 * </pre>
	 * 
	 * @param value the pixel value
	 * @return the tile value
	 */
	public int toTile(float value) {
		return (int) (value / this.tileSize);
	}

	/**
	 * Converts a pixel value to a tile value in a isometric grid:
	 * 
	 * <pre>
	 *    #----#
	 *   /    /
	 *  /    /
	 * #----#
	 * </pre>
	 * 
	 * @param value the pixel value
	 * @return the tile value
	 */
	public int toIsometricTileX(float xValue, float yValue) {
		return toTile(xValue + isometricXOffset(yValue));
	}

	/**
	 * Converts a pixel value to a tile value in a isometric grid:
	 * 
	 * <pre>
	 *    #----#
	 *   /    /
	 *  /    /
	 * #----#
	 * </pre>
	 * 
	 * @param value the pixel value
	 * @return the tile value
	 */
	public int toIsometricTileY(float yValue) {
		return toTile(yValue);
	}

	public float isometricXOffset(float yValue) {
		return (toIsometricTileY(yValue) * this.tileSize) / 2f;
	}

	public int getTileSize() {
		return this.tileSize;
	}
}
