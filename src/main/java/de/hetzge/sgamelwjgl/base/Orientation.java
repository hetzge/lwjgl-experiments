package de.hetzge.sgamelwjgl.base;

public enum Orientation implements TileConnectionOffset {
	NORTH(0, -1), EAST(1, 0), SOUTH(0, 1), WEST(-1, 0);

	public static final Orientation[] VALUES = values();

	public final int x;
	public final int y;

	private Orientation(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public int getOffsetX() {
		return this.x;
	}

	@Override
	public int getOffsetY() {
		return this.y;
	}

	public static Orientation lookAt(int fromX, int fromY, int toX, int toY) {
		return Math.abs(fromX - toX) > Math.abs(fromY - toY) ? (fromX - toX) > 0 ? WEST : EAST : (fromY - toY) > 0 ? NORTH : SOUTH;
	}
}
