package de.hetzge.sgamelwjgl.base;

public final class IntArrayQueue {

	private final int[] elements;
	private int headIndex;
	private int tailIndex;

	public IntArrayQueue(int size) {
		this.elements = new int[size];
		this.headIndex = 0;
		this.tailIndex = 0;
	}

	public void add(int element) {
		this.elements[this.tailIndex++] = element;
	}

	public int poll() {
		if (this.headIndex == this.tailIndex) {
			return Integer.MIN_VALUE;
		} else {
			return this.elements[this.headIndex++];
		}
	}

	public int pollTail() {
		if (this.headIndex == this.tailIndex) {
			return Integer.MIN_VALUE;
		} else {
			this.tailIndex--;
			return this.elements[this.tailIndex];
		}
	}

	public void reset() {
		this.headIndex = 0;
		this.tailIndex = 0;
	}

	public int getHeadIndex() {
		return this.headIndex;
	}

	public int getTailIndex() {
		return this.tailIndex;
	}
}
