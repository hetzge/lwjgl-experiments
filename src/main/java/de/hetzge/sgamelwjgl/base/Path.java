package de.hetzge.sgamelwjgl.base;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

import org.joml.Vector2i;

public final class Path {

	private final Queue<Vector2i> waypoints;

	private Path(Queue<Vector2i> waypoints) {
		this.waypoints = waypoints;
	}

	public Collection<Vector2i> getWaypoints() {
		return this.waypoints;
	}

	public Vector2i poll() {
		return this.waypoints.poll();
	}

	public Vector2i peek() {
		return this.waypoints.peek();
	}

	public boolean isEmpty() {
		return this.waypoints.isEmpty();
	}
	
	public Path withoutLast() {
		return new Path(new LinkedList<>(new LinkedList<>(this.waypoints).subList(0, this.waypoints.size() - 1)));
	}

	@Override
	public String toString() {
		return "Path [waypoints=" + this.waypoints + "]";
	}

	public static Path create(TileGrid tileGrid, int[] positionIndices) {
		if (positionIndices != null) {
			final LinkedList<Vector2i> waypoints = new LinkedList<>();
			for (int i = 0; i < positionIndices.length; i++) {
				final Vector2i position = new Vector2i(tileGrid.x(positionIndices[i]), tileGrid.y(positionIndices[i]));
				waypoints.add(position);
			}
			return new Path(waypoints);
		} else {
			return null;
		}
	}

	public static Path create(Queue<Vector2i> waypoints) {
		return new Path(waypoints);
	}
}
