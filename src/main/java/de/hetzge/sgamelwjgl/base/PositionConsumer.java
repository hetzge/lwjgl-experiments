package de.hetzge.sgamelwjgl.base;

@FunctionalInterface
public interface PositionConsumer {
	void accept(int x, int y);
}
