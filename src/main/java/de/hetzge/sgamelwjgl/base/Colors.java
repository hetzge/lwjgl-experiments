package de.hetzge.sgamelwjgl.base;

import com.badlogic.gdx.graphics.Color;

public final class Colors {

	public static final Color[] colors = new Color[] { Color.valueOf("#e6194b"), Color.valueOf("#3cb44b"), Color.valueOf("#ffe119"), Color.valueOf("#4363d8"), Color.valueOf("#f58231"), Color.valueOf("#911eb4"), Color.valueOf("#46f0f0"), Color.valueOf("#f032e6"), Color.valueOf("#bcf60c"), Color.valueOf("#fabebe"), Color.valueOf("#008080"), Color.valueOf("#e6beff"), Color.valueOf("#9a6324"), Color.valueOf("#fffac8"), Color.valueOf("#800000"), Color.valueOf("#aaffc3"), Color.valueOf("#808000"), Color.valueOf("#ffd8b1"), Color.valueOf("#000075"), Color.valueOf("#808080"), Color.valueOf("#ffffff"), Color.valueOf("#000000") };
	public static final float[] colorFloatBits = mapToFloatBits(colors);

	private static float[] mapToFloatBits(Color[] colors) {
		final float[] result = new float[colors.length];
		for (int i = 0; i < result.length; i++) {
			result[i] = colors[i].toFloatBits();
		}
		return result;
	}
}
