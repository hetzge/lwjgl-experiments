package de.hetzge.sgamelwjgl.graphics.font;

import java.io.File;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
//import com.badlogic.gdx.graphics.g2d.BitmapFontCache;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.utils.Array;

import de.hetzge.sgamelwjgl.graphics.base.GeometryBuffer;

public final class Font {

	private final BitmapFontCache cache;

	private Font(BitmapFontCache cache) {
		this.cache = cache;
	}

	public GlyphLayout addText(CharSequence str, float x, float y) {
		return this.cache.addText(str, x, y);
	}

	public GlyphLayout addText(CharSequence str, float x, float y, float targetWidth, int halign, boolean wrap) {
		return this.cache.addText(str, x, y, targetWidth, halign, wrap);
	}

	public GlyphLayout addText(CharSequence str, float x, float y, int start, int end, float targetWidth, int halign, boolean wrap) {
		return this.cache.addText(str, x, y, start, end, targetWidth, halign, wrap);
	}

	public GlyphLayout addText(CharSequence str, float x, float y, int start, int end, float targetWidth, int halign, boolean wrap, String truncate) {
		return this.cache.addText(str, x, y, start, end, targetWidth, halign, wrap, truncate);
	}

	public void addText(GlyphLayout layout, float x, float y) {
		this.cache.addText(layout, x, y);
	}

	public void bind() {
		this.cache.getFont().getRegion().getTexture().bind();
	}

	public void clear() {
		this.cache.clear();
	}

	public Font copy() {
		final BitmapFontCache cache = new BitmapFontCache(this.cache.getFont());
		cache.setUseIntegerPositions(false);
		return new Font(cache);
	}

	public void render(GeometryBuffer buffer) {
		final Array<TextureRegion> regions = this.cache.getFont().getRegions();
		for (int j = 0, n = regions.size; j < n; j++) {
			final int vertexCount = this.cache.getVertexCount(j);
			if (vertexCount > 0) {
				final float[] vertices = this.cache.getVertices(j);
				buffer.add(vertices);

//				System.out.println(Arrays.toString(vertices));
//		this.cache.draw(spriteBatch);
			}
		}
//		System.out.println("...");
	}

	public static Font create(File file, FreeTypeFontParameter parameter) {
		final FreeTypeFontGenerator generator = new FreeTypeFontGenerator(new FileHandle(file));
		final BitmapFont font = generator.generateFont(parameter);
		generator.dispose();

		final BitmapFontCache cache = new BitmapFontCache(font);
		cache.setUseIntegerPositions(true);

		return new Font(cache);
	}
}
