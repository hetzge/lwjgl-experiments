package de.hetzge.sgamelwjgl.graphics.font;

import org.joml.Vector2i;

import de.hetzge.sgamelwjgl.graphics.base.TextureRegion;

public final class Character {

	public final TextureRegion textureRegion;
	public final Vector2i size;
	public final Vector2i bearing;
	public final int advance;

	public Character(TextureRegion textureRegion, Vector2i size, Vector2i bearing, int advance) {
		this.textureRegion = textureRegion;
		this.size = size;
		this.bearing = bearing;
		this.advance = advance;
	}
}