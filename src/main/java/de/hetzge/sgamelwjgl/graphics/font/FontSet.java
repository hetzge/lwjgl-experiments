package de.hetzge.sgamelwjgl.graphics.font;

import java.io.File;

import org.joml.Vector2i;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType.Bitmap;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType.Face;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType.Library;
import com.badlogic.gdx.utils.IntMap;

import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.graphics.base.TextureAtlas;
import de.hetzge.sgamelwjgl.graphics.base.TextureRegion;

public class FontSet {
	private final int fontSize;
	private final IntMap<Character> characters;

	private FontSet(int fontSize, IntMap<Character> characters) {
		this.fontSize = fontSize;
		this.characters = characters;
	}

	public void render(SpriteBatch batch, CharSequence text, float x, float y, float scale, Color color) {
		for (int i = 0; i < text.length(); i++) {
			final Character character = this.characters.get(text.charAt(i));
			if (character != null) {

				final float width = character.size.x * scale;
				final float height = character.size.y * scale;

				x += width / 2f;

				final float xpos = x + (character.bearing.x * scale);
				final float ypos = y - ((character.size.y - character.bearing.y) * scale);

				batch.add(character.textureRegion, xpos, ypos, 0.0f, width, height, color.toFloatBits());

//				System.out.println(text.charAt(i) + " -> " + (character.advance >> 6));

				x += ((character.advance >> 6) - (width / 2f)) * scale;
			} else {
				x += (this.fontSize / 3f) * scale;
			}
		}
	}

	public static FontSet create(File fontFile, int fontSize, TextureAtlas atlas, String prefix) {

		Library library = null;
		Face face = null;
		try {

			library = FreeType.initFreeType();
			face = library.newFace(new FileHandle(fontFile), 0);
			face.setPixelSizes(0, fontSize);

			int i = 0;
			final IntMap<Character> characters = new IntMap<>(128);
			for (char c = 0; c < 128; c++) {
				if (face.loadChar(c, FreeType.FT_LOAD_RENDER)) {
					System.out.println(String.format("Loaded '%s'", c));

					final Bitmap bitmap = face.getGlyph().getBitmap();
					final int width = bitmap.getWidth();
					final int height = bitmap.getRows();

					if ((width > 0) && (height > 0)) {

						final int bitmapLeft = face.getGlyph().getBitmapLeft();
						final int bitmapTop = face.getGlyph().getBitmapTop();
						final int advanceX = face.getGlyph().getAdvanceX();

						final TextureRegion region = atlas.getRegion(prefix + i);
						if (region == null) {
							throw new IllegalStateException(String.format("Region not found for '%s' (%s)", c, i));
						}
						characters.put(c, new Character(region, new Vector2i(width, height), new Vector2i(bitmapLeft, bitmapTop), advanceX));
					}
				} else {
					System.out.println(String.format("Failed to load '%s'", c));
				}

				i++;
			}

			return new FontSet(fontSize, characters);
		} finally {
			if (face != null) {
				face.dispose();
			}
			if (library != null) {
				library.dispose();
			}
		}
	}
}