package de.hetzge.sgamelwjgl.graphics.base;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

import java.util.List;

import de.hetzge.sgamelwjgl.graphics.base.VertexAttribute.Format;

public final class GeometryBufferUtils {

	private static final int FLOAT_BYTE_SIZE = Float.SIZE / Byte.SIZE;

	private GeometryBufferUtils() {
	}

	public static int createVao(VertexDescription vertexDescription, int shapeCount, boolean batched) {
		final int[] vao = new int[1];
		glGenVertexArrays(vao);
		glBindVertexArray(vao[0]);

		final int vertexLengthInBytes = vertexDescription.getVertexLengthInBytes();
		final List<VertexAttribute> vertexAttributes = vertexDescription.getAttributes();
		int offset = 0;
		for (final VertexAttribute vertexAttribute : vertexAttributes) {
			final int attributeLength = vertexAttribute.getLength();
			final int attributeHandle = vertexAttribute.getHandle();
			glEnableVertexAttribArray(attributeHandle);
			if (batched) {
				if (vertexAttribute.getFormat() == Format.COLOR) {
					glVertexAttribPointer(attributeHandle, 4, GL_UNSIGNED_BYTE, true, attributeLength * FLOAT_BYTE_SIZE, offset);
				} else if (vertexAttribute.getFormat() == Format.FLOAT) {
					glVertexAttribPointer(attributeHandle, attributeLength, GL_FLOAT, false, attributeLength * FLOAT_BYTE_SIZE, offset);
				} else {
					throw new IllegalArgumentException(String.format("The format '%s' is not supported.", vertexAttribute.getFormat().name()));
				}
				offset += shapeCount * attributeLength * FLOAT_BYTE_SIZE;
			} else {
				if (vertexAttribute.getFormat() == Format.COLOR) {
					glVertexAttribPointer(attributeHandle, 4, GL_UNSIGNED_BYTE, true, vertexLengthInBytes, offset);
				} else if (vertexAttribute.getFormat() == Format.FLOAT) {
					glVertexAttribPointer(attributeHandle, attributeLength, GL_FLOAT, false, vertexLengthInBytes, offset);
				} else {
					throw new IllegalArgumentException(String.format("The format '%s' is not supported.", vertexAttribute.getFormat().name()));
				}
				offset += attributeLength * FLOAT_BYTE_SIZE;
			}
		}

		return vao[0];
	}

	public static int createVbo(Shape shape, int shapeCount, int vertexLengthInBytes) {
		final int[] vbo = new int[1];
		glGenBuffers(vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
		glBufferData(GL_ARRAY_BUFFER, vertexLengthInBytes * shapeCount * shape.getVertexCount(), GL_DYNAMIC_DRAW);
		return vbo[0];
	}

	public static int createEbo(Shape shape, int shapeCount) {
		final int[] ebo = { -1 };
		if (shape == Shape.QUAD) {
			final int[] indices = new int[shapeCount * shape.getIndexCount()];
			for (int i = 0; i < shapeCount; i++) {
				indices[(i * 6) + 0] = (i * 4) + 0;
				indices[(i * 6) + 1] = (i * 4) + 1;

				// top-left, bottom-left, bottom-right, top-right
				indices[(i * 6) + 2] = (i * 4) + 3;
				
				// top-left, top-right, bottom-left, bottom-right,
				// indices[(i * 6) + 2] = (i * 4) + 2;

				indices[(i * 6) + 3] = (i * 4) + 1;
				indices[(i * 6) + 4] = (i * 4) + 2;
				indices[(i * 6) + 5] = (i * 4) + 3;
			}

			glGenBuffers(ebo);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo[0]);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices, GL_STATIC_DRAW);
		}
		return ebo[0];
	}
}
