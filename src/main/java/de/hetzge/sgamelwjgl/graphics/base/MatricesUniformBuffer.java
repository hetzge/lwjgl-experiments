package de.hetzge.sgamelwjgl.graphics.base;

import static org.lwjgl.opengl.GL33.*;

import org.joml.Matrix4f;

public final class MatricesUniformBuffer {

	private static final int SIZE = 16;

	private final int index;
	private final int uboHandle;
	private final float[] buffer;

	private MatricesUniformBuffer(int index, int uboHandle) {
		this.index = index;
		this.uboHandle = uboHandle;
		this.buffer = new float[SIZE];
	}

	public void use() {
		glBindBufferBase(GL_UNIFORM_BUFFER, this.index, this.uboHandle);
	}

	public void set(Matrix4f combinedMatrix) {
		glBindBuffer(GL_UNIFORM_BUFFER, this.uboHandle);
		glBufferSubData(GL_UNIFORM_BUFFER, 0, combinedMatrix.get(this.buffer));
	}

	public static MatricesUniformBuffer create(int index) {

		final int[] ubo = new int[1];
		glGenBuffers(ubo);
		glBindBuffer(GL_UNIFORM_BUFFER, ubo[0]);
		glBufferData(GL_UNIFORM_BUFFER, SIZE * (Float.SIZE / Byte.SIZE), GL_STATIC_DRAW);

		return new MatricesUniformBuffer(index, ubo[0]);
	}
}
