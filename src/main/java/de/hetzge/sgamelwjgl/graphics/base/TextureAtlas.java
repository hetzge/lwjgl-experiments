package de.hetzge.sgamelwjgl.graphics.base;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class TextureAtlas {
	private final Texture texture;
	private final Map<String, TextureRegion> regionsByKey;

	private TextureAtlas(Texture texture, Map<String, TextureRegion> regionsByKey) {
		this.texture = texture;
		this.regionsByKey = regionsByKey;
	}

	public Texture getTexture() {
		return this.texture;
	}

	public TextureRegion getRegion(String name) {
		return this.regionsByKey.get(name);
	}

	public static TextureAtlas create(String filename) {
		final List<Config> configs = loadConfigs(new File(filename));
		final Texture texture = Texture.create("generated/" + configs.get(0).getName(), false, false);
		final Map<String, TextureRegion> regionsByKey = new HashMap<>();
		for (int i = 1; i < configs.size(); i++) {
			final Config config = configs.get(i);
			final int x = Integer.parseInt(config.getValues().get("xy").split(",")[0].trim());
			final int y = Integer.parseInt(config.getValues().get("xy").split(",")[1].trim());
			final int width = Integer.parseInt(config.getValues().get("size").split(",")[0].trim());
			final int height = Integer.parseInt(config.getValues().get("size").split(",")[1].trim());
			regionsByKey.put(config.getName(), TextureRegion.create(texture, x, y, width, height));
		}

		return new TextureAtlas(texture, regionsByKey);
	}

	private static List<Config> loadConfigs(File file) {
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			final List<Config> configs = new ArrayList<>();

			Config config = null;
			String line;
			while ((line = reader.readLine()) != null) {
				final String trimmedLine = line.trim();
				if (!trimmedLine.isBlank()) {
					if (!trimmedLine.contains(":")) {
						config = new Config(trimmedLine, new HashMap<String, String>());
						configs.add(config);
					} else {
						final String[] splitted = trimmedLine.split(":");
						config.getValues().put(splitted[0].trim(), splitted[1].trim());
					}
				}
			}
			return configs;
		} catch (final IOException exception) {
			throw new IllegalStateException(exception);
		}
	}

	private static class Config {
		private final String name;
		private final Map<String, String> values;

		public Config(String name, Map<String, String> values) {
			this.name = name;
			this.values = values;
		}

		public String getName() {
			return this.name;
		}

		public Map<String, String> getValues() {
			return this.values;
		}
	}
}
