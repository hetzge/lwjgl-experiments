package de.hetzge.sgamelwjgl.graphics.base;

import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glDeleteShader;
import static org.lwjgl.opengl.GL20.glGetProgramInfoLog;
import static org.lwjgl.opengl.GL20.glGetProgramiv;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetShaderiv;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL32.GL_GEOMETRY_SHADER;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.FileUtils;

public final class ShaderProgram {

	private final int handle;

	private ShaderProgram(int handle) {
		this.handle = handle;
	}

	public void use() {
		glUseProgram(this.handle);
	}

	public int getHandle() {
		return this.handle;
	}

	@Override
	public String toString() {
		return "ShaderProgram [handle=" + this.handle + "]";
	}

	public static ShaderProgram load(File vertexShaderFile, File geometryShaderFile, File fragmentShaderFile) {
		try {
			final int program = glCreateProgram();
			final int fragmentShader = loadShader(GL_FRAGMENT_SHADER, fragmentShaderFile);
			final int geometryShader = geometryShaderFile != null ? loadShader(GL_GEOMETRY_SHADER, geometryShaderFile) : -1;
			final int vertexShader = loadShader(GL_VERTEX_SHADER, vertexShaderFile);
			glAttachShader(program, fragmentShader);

			if (geometryShader != -1) {
				glAttachShader(program, geometryShader);
			}
			
			glAttachShader(program, vertexShader);
			glLinkProgram(program);

			final int[] success = new int[1];
			glGetProgramiv(program, GL_LINK_STATUS, success);

			if (success[0] == 0) {
				final String infoLog = glGetProgramInfoLog(program);
				throw new IllegalStateException("Failed to link shader program: " + infoLog);
			}

			glDeleteShader(fragmentShader);
			
			if(geometryShader != -1) {
				glDeleteShader(geometryShader);
			}
			
			glDeleteShader(vertexShader);

			return new ShaderProgram(program);
		} catch (final IOException exception) {
			throw new IllegalStateException(exception);
		}
	}

	private static int loadShader(int shaderType, File file) throws IOException {
		final int shader = glCreateShader(shaderType);
		glShaderSource(shader, FileUtils.readFileToString(file, StandardCharsets.UTF_8));
		glCompileShader(shader);

		final int[] success = new int[1];
		glGetShaderiv(shader, GL_COMPILE_STATUS, success);

		if (success[0] == 0) {
			final String infoLog = glGetShaderInfoLog(shader);
			throw new IllegalStateException("Failed to compile shader: " + infoLog);
		}

		return shader;
	}
}
