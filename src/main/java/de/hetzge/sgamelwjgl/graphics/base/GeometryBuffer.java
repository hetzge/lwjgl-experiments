package de.hetzge.sgamelwjgl.graphics.base;

import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferSubData;
import static org.lwjgl.opengl.GL30.glBindVertexArray;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;

public final class GeometryBuffer {

	private final Shape shape;
	private final int shapeCount;
	private final VertexDescription vertexDescription;
	private final int vaoHandle;
	private final int vboHandle;
	private final int eboHandle;
	private final FloatBuffer buffer;
	private int flushedShapeCount;

	private GeometryBuffer(Shape shape, int shapeCount, VertexDescription vertexDescription, int vaoHandle, int vboHandle, int eboHandle) {
		this.shape = shape;
		this.shapeCount = shapeCount;
		this.vertexDescription = vertexDescription;
		this.vaoHandle = vaoHandle;
		this.vboHandle = vboHandle;
		this.eboHandle = eboHandle;
		this.buffer = BufferUtils.createFloatBuffer(shapeCount * vertexDescription.getVertexLength() * shape.getVertexCount());
	}

	public void add(float[] vertices) {
		this.buffer.put(vertices);
	}

	public void add(FloatBuffer vertices) {
		this.buffer.put(vertices);
	}

	public void flush() {
		this.buffer.flip();
		glBindBuffer(GL_ARRAY_BUFFER, this.vboHandle);
		glBufferSubData(GL_ARRAY_BUFFER, 0L, this.buffer);
		this.flushedShapeCount = this.buffer.remaining() / this.vertexDescription.getVertexLength() / this.shape.getVertexCount();
		// System.out.println("flushed shapes: " + this.flushedShapeCount);
		this.buffer.clear();
	}

	public void flushDirect(int elementIndex, float[] vertices) {
		glBindBuffer(GL_ARRAY_BUFFER, this.vboHandle);
		glBufferSubData(GL_ARRAY_BUFFER, elementIndex * vertices.length * Float.BYTES, vertices);

		final int newFlushedShapeCount = (elementIndex * vertices.length) / this.shape.getVertexCount();
		if (newFlushedShapeCount > this.flushedShapeCount) {
			this.flushedShapeCount = newFlushedShapeCount;
		}
	}

	public void draw() {
		glBindVertexArray(this.vaoHandle);

		if (this.shape == Shape.QUAD) {
			glDrawElements(this.shape.getGlShape(), this.flushedShapeCount * this.shape.getIndexCount(), GL_UNSIGNED_INT, 0);
		} else {
			glDrawArrays(this.shape.getGlShape(), 0, this.flushedShapeCount * this.shape.getVertexCount());
		}
	}

	public static GeometryBuffer create(Shape shape, int shapeCount, VertexDescription vertexDescription) {
		final int vbo = GeometryBufferUtils.createVbo(shape, shapeCount, vertexDescription.getVertexLengthInBytes());
		final int vao = GeometryBufferUtils.createVao(vertexDescription, shapeCount, false);
		final int ebo = GeometryBufferUtils.createEbo(shape, shapeCount);

		return new GeometryBuffer(shape, shapeCount, vertexDescription, vao, vbo, ebo);
	}

}
