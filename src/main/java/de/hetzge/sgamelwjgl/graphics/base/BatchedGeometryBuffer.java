package de.hetzge.sgamelwjgl.graphics.base;

import static com.badlogic.gdx.graphics.GL20.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferSubData;
import static org.lwjgl.opengl.GL30.glBindVertexArray;

public final class BatchedGeometryBuffer {

	private static final int FLOAT_BYTE_SIZE = Float.SIZE / Byte.SIZE;

	private final int shapeCount;
	private final Shape shape;
	private final VertexDescription vertexDescription;
	private final int vaoHandle;
	private final int vboHandle;
	private final int eboHandle;

	private BatchedGeometryBuffer(Shape shape, int shapeCount, VertexDescription vertexDescription, int vaoHandle, int vboHandle, int eboHandle) {
		this.shapeCount = shapeCount;
		this.shape = shape;
		this.vertexDescription = vertexDescription;
		this.vaoHandle = vaoHandle;
		this.vboHandle = vboHandle;
		this.eboHandle = eboHandle;
	}

	public void set(VertexAttribute attribute, int index, float[] data) {
		glBindBuffer(GL_ARRAY_BUFFER, this.vboHandle);
		glBufferSubData(GL_ARRAY_BUFFER, ((this.vertexDescription.getOffset(attribute) * this.shapeCount) + (index * attribute.getLength())) * FLOAT_BYTE_SIZE, data);
	}

	public void draw() {
		glBindVertexArray(this.vaoHandle);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this.eboHandle);

		if (this.shape == Shape.QUAD) {
			glDrawElements(this.shape.getGlShape(), this.shapeCount * this.shape.getIndexCount(), GL_UNSIGNED_INT, 0);
		} else {
			glDrawArrays(this.shape.getGlShape(), 0, this.shapeCount * this.shape.getVertexCount());
		}
	}

	public static BatchedGeometryBuffer create(Shape shape, int shapeCount, VertexDescription vertexDescription) {
		final int vbo = GeometryBufferUtils.createVbo(shape, shapeCount, vertexDescription.getVertexLengthInBytes());
		final int vao = GeometryBufferUtils.createVao(vertexDescription, shapeCount, true);
		final int ebo = GeometryBufferUtils.createEbo(shape, shapeCount);

		return new BatchedGeometryBuffer(shape, shapeCount, vertexDescription, vao, vbo, ebo);
	}
}
