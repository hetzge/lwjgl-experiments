package de.hetzge.sgamelwjgl.graphics.base;

import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glBufferSubData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL30.GL_RGBA32F;
import static org.lwjgl.opengl.GL31.GL_TEXTURE_BUFFER;
import static org.lwjgl.opengl.GL31.glTexBuffer;

import java.nio.FloatBuffer;

import com.badlogic.gdx.graphics.Color;

public final class TextureBuffer {

	private static final int FLOAT_BYTE_SIZE = Float.SIZE / Byte.SIZE;

	private final int width;
	private final int height;
	private final int tboHandle;
	private final int textureHandle;
	private final float[] colorBuffer;

	private TextureBuffer(int width, int height, int tboHandle, int textureHandle) {
		this.width = width;
		this.height = height;
		this.tboHandle = tboHandle;
		this.textureHandle = textureHandle;
		this.colorBuffer = new float[4];
	}

	public void fill(Color color) {
		for (int index = 0; index < (this.width * this.height); index++) {
			set(index * 4, color);
		}
	}

	public void set(int index, Color color) {
		this.colorBuffer[0] = color.r;
		this.colorBuffer[1] = color.g;
		this.colorBuffer[2] = color.b;
		this.colorBuffer[3] = color.a;
		this.set(index, this.colorBuffer);
	}

	public void set(int index, FloatBuffer buffer) {
		glBindBuffer(GL_TEXTURE_BUFFER, this.tboHandle);
		glBufferSubData(GL_TEXTURE_BUFFER, index * FLOAT_BYTE_SIZE, buffer);
	}

	public void set(int index, float[] data) {
		glBindBuffer(GL_TEXTURE_BUFFER, this.tboHandle);
		glBufferSubData(GL_TEXTURE_BUFFER, index * FLOAT_BYTE_SIZE, data);
	}

	public void activate(int textureUnit) {
		glActiveTexture(textureUnit);
		glBindTexture(GL_TEXTURE_BUFFER, this.textureHandle);
	}

	public int index(int x, int y) {
		return ((y * this.width) + x) * 4;
	}

	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}

	public static TextureBuffer create(int width, int height) {

		final int[] tbo = new int[1];
		glGenBuffers(tbo);
		glBindBuffer(GL_TEXTURE_BUFFER, tbo[0]);
		glBufferData(GL_TEXTURE_BUFFER, width * height * 4 * FLOAT_BYTE_SIZE, GL_DYNAMIC_DRAW);

		final int[] texture = new int[1];
		glGenTextures(texture);
		glBindTexture(GL_TEXTURE_BUFFER, texture[0]);
		glTexBuffer(GL_TEXTURE_BUFFER, GL_RGBA32F, tbo[0]);

		return new TextureBuffer(width, height, tbo[0], texture[0]);
	}
}
