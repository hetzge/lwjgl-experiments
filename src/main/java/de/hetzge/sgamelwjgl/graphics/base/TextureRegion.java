package de.hetzge.sgamelwjgl.graphics.base;

public final class TextureRegion {

	private static final float[] QUAD_VERTICES = new float[24];

	private final float regionX;
	private final float regionY;
	private final float regionWidth;
	private final float regionHeight;
	private final int originalWidth;
	private final int originalHeight;

	private TextureRegion(float regionX, float regionY, float regionWidth, float regionHeight, int originalWidth, int originalHeight) {
		this.regionX = regionX;
		this.regionY = regionY;
		this.regionWidth = regionWidth;
		this.regionHeight = regionHeight;
		this.originalWidth = originalWidth;
		this.originalHeight = originalHeight;
	}

	public void render(GeometryBuffer buffer, float x, float y, float z, float width, float height) {
		render(buffer, x, y, z, width, height, 0f);
	}

	public void render(GeometryBuffer buffer, float x, float y, float z, float width, float height, float colorBits) {

		int i = 0;

		QUAD_VERTICES[i++] = x - (width / 2);
		QUAD_VERTICES[i++] = y + height;
		QUAD_VERTICES[i++] = z;
		QUAD_VERTICES[i++] = colorBits;
		QUAD_VERTICES[i++] = this.regionX;
		QUAD_VERTICES[i++] = this.regionY;

		QUAD_VERTICES[i++] = QUAD_VERTICES[0] + width;
		QUAD_VERTICES[i++] = QUAD_VERTICES[1];
		QUAD_VERTICES[i++] = z;
		QUAD_VERTICES[i++] = colorBits;
		QUAD_VERTICES[i++] = this.regionX + this.regionWidth;
		QUAD_VERTICES[i++] = this.regionY;

		QUAD_VERTICES[i++] = QUAD_VERTICES[0] + width;
		QUAD_VERTICES[i++] = QUAD_VERTICES[1] - height;
		QUAD_VERTICES[i++] = z;
		QUAD_VERTICES[i++] = colorBits;
		QUAD_VERTICES[i++] = this.regionX + this.regionWidth;
		QUAD_VERTICES[i++] = this.regionY + this.regionHeight;

		QUAD_VERTICES[i++] = QUAD_VERTICES[0];
		QUAD_VERTICES[i++] = QUAD_VERTICES[1] - height;
		QUAD_VERTICES[i++] = z;
		QUAD_VERTICES[i++] = colorBits;
		QUAD_VERTICES[i++] = this.regionX;
		QUAD_VERTICES[i++] = this.regionY + this.regionHeight;

		buffer.add(QUAD_VERTICES);
	}

	public void renderLine(GeometryBuffer buffer, float fromX, float fromY, float toX, float toY, float colorBits) {

		final float width = Math.abs(fromX - toX);
		final float height = Math.abs(fromY - toY);

		final float horizontal = width > height ? height / width : 1f - (width / height);
		final float vertical = 1f - horizontal;

		int i = 0;

		QUAD_VERTICES[i++] = fromX - (1f * horizontal);
		QUAD_VERTICES[i++] = fromY + (1f * vertical);
		QUAD_VERTICES[i++] = 0f;
		QUAD_VERTICES[i++] = colorBits;
		QUAD_VERTICES[i++] = this.regionX;
		QUAD_VERTICES[i++] = this.regionY;

		QUAD_VERTICES[i++] = QUAD_VERTICES[0] + (2f * horizontal);
		QUAD_VERTICES[i++] = QUAD_VERTICES[1] - (2f * vertical);
		QUAD_VERTICES[i++] = 0f;
		QUAD_VERTICES[i++] = colorBits;
		QUAD_VERTICES[i++] = this.regionX + this.regionWidth;
		QUAD_VERTICES[i++] = this.regionY;

		QUAD_VERTICES[i++] = toX - (1f * horizontal);
		QUAD_VERTICES[i++] = toY + (1f * vertical);
		QUAD_VERTICES[i++] = 0f;
		QUAD_VERTICES[i++] = colorBits;
		QUAD_VERTICES[i++] = this.regionX;
		QUAD_VERTICES[i++] = this.regionY + this.regionHeight;

		QUAD_VERTICES[i++] = QUAD_VERTICES[12] + (2f * horizontal);
		QUAD_VERTICES[i++] = QUAD_VERTICES[13] - (2f * vertical);
		QUAD_VERTICES[i++] = 0f;
		QUAD_VERTICES[i++] = colorBits;
		QUAD_VERTICES[i++] = this.regionX + this.regionWidth;
		QUAD_VERTICES[i++] = this.regionY + this.regionHeight;

		buffer.add(QUAD_VERTICES);

//		GL11.glLineWidth(5f);
//		GL11.glColor3f(1.0f, 0.0f, 0.0f);
//		GL11.glBegin(GL11.GL_LINES);
//		GL11.glVertex3f(0.0f, 0.0f, 1.0f);
//		GL11.glVertex3f(50f, -50f, 1f);
//		GL11.glVertex3f(50f, 50f, 1f);
//		GL11.glEnd();
	}

	public float getRegionX() {
		return this.regionX;
	}

	public float getRegionY() {
		return this.regionY;
	}

	public float getRegionWidth() {
		return this.regionWidth;
	}

	public float getRegionHeight() {
		return this.regionHeight;
	}

	public int getOriginalWidth() {
		return this.originalWidth;
	}

	public int getOriginalHeight() {
		return this.originalHeight;
	}

	public static TextureRegion create(Texture texture) {
		return create(texture, 0, 0, texture.getWidth(), texture.getHeight());
	}

	public static TextureRegion create(Texture texture, int x, int y, int width, int height) {
		return new TextureRegion((float) x / texture.getWidth(), (float) y / texture.getHeight(), (float) width / texture.getWidth(), (float) height / texture.getHeight(), width, height);
	}

	public static TextureRegion createFlipped(Texture texture, int x, int y, int width, int height) {
		return new TextureRegion((float) (x + width) / texture.getWidth(), (float) y / texture.getHeight(), (float) -width / texture.getWidth(), (float) height / texture.getHeight(), width, height);
	}

	public static TextureRegion[] create(TextureRegion region, int columns, int rows) {
		final TextureRegion[] regions = new TextureRegion[columns * rows];
		final float regionWidth = region.regionWidth / columns;
		final float regionHeight = region.regionHeight / rows;
		for (int y = 0; y < rows; y++) {
			for (int x = 0; x < columns; x++) {
				regions[(y * columns) + x] = new TextureRegion(region.regionX + (x * regionWidth), region.regionY + (y * regionHeight), regionWidth, regionHeight, region.originalWidth / columns, region.originalHeight / rows);
			}
		}
		return regions;
	}
}
