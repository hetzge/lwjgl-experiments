package de.hetzge.sgamelwjgl.graphics.base;

import java.util.function.Function;

import org.joml.Vector2f;

public final class TriangleTextureRegion {

	private static final float[] VERTICES = new float[18];

	private final Vector2f uv1;
	private final Vector2f uv2;
	private final Vector2f uv3;

	private TriangleTextureRegion(Vector2f uv1, Vector2f uv2, Vector2f uv3) {
		this.uv1 = uv1;
		this.uv2 = uv2;
		this.uv3 = uv3;
	}

	public void render(GeometryBuffer buffer, float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3, float colorBits) {
		render(buffer, x1, y1, z1, colorBits, x2, y2, z2, colorBits, x3, y3, z3, colorBits);
	}

	public void render(GeometryBuffer buffer, float x1, float y1, float z1, float color1, float x2, float y2, float z2, float color2, float x3, float y3, float z3, float color3) {
		buffer.add(fillVerticesBuffer(x1, y1, z1, color1, x2, y2, z2, color2, x3, y3, z3, color3));
	}

	public void renderDirect(GeometryBuffer buffer, int elementIndex, float x1, float y1, float z1, float color1, float x2, float y2, float z2, float color2, float x3, float y3, float z3, float color3) {
		buffer.flushDirect(elementIndex, fillVerticesBuffer(x1, y1, z1, color1, x2, y2, z2, color2, x3, y3, z3, color3));
	}

	private float[] fillVerticesBuffer(float x1, float y1, float z1, float color1, float x2, float y2, float z2, float color2, float x3, float y3, float z3, float color3) {
		int i = 0;

		VERTICES[i++] = x1;
		VERTICES[i++] = y1;
		VERTICES[i++] = z1;
		VERTICES[i++] = color1;
		VERTICES[i++] = this.uv1.x;
		VERTICES[i++] = this.uv1.y;

		VERTICES[i++] = x2;
		VERTICES[i++] = y2;
		VERTICES[i++] = z2;
		VERTICES[i++] = color2;
		VERTICES[i++] = this.uv2.x;
		VERTICES[i++] = this.uv2.y;

		VERTICES[i++] = x3;
		VERTICES[i++] = y3;
		VERTICES[i++] = z3;
		VERTICES[i++] = color3;
		VERTICES[i++] = this.uv3.x;
		VERTICES[i++] = this.uv3.y;

		return VERTICES;
	}

	public static TriangleTextureRegion create(Texture texture, float x1, float y1, float x2, float y2, float x3, float y3) {
		final Function<Float, Float> u = x -> x / texture.getWidth();
		final Function<Float, Float> v = y -> y / texture.getHeight();
		return new TriangleTextureRegion(new Vector2f(u.apply(x1), v.apply(y1)), new Vector2f(u.apply(x2), v.apply(y2)), new Vector2f(u.apply(x3), v.apply(y3)));
	}
}
