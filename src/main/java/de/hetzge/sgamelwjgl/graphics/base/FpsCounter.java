package de.hetzge.sgamelwjgl.graphics.base;

public final class FpsCounter {

	private long i;
	private long frames;
	private long timeBefore;
	private long totalFrames;
	
	private int fps;

	public FpsCounter() {
		this.i = 0;
		this.frames = 0;
		this.timeBefore = 0;
		this.totalFrames = 0;
		this.fps =0;
	}

	public void count() {
		this.i++;
		this.frames++;
		this.totalFrames++;
		if ((this.i % 60) == 0) {
			final long currentTimeMillis = System.currentTimeMillis();
			this.fps = (int) (this.frames / ((currentTimeMillis - this.timeBefore) / 1000f));

			this.frames = 0;
			this.timeBefore = currentTimeMillis;
		}
	}
	
	public int getFps() {
		return this.fps;
	}

	public long getTotalFrames() {
		return this.totalFrames;
	}

	public boolean isXFrame(int xth) {
		return (this.totalFrames % xth) == 0;
	}
}
