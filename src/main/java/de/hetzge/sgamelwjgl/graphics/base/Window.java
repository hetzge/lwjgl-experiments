package de.hetzge.sgamelwjgl.graphics.base;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.GLFW_FALSE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_F12;
import static org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_1;
import static org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_2;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_TRUE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwGetCursorPos;
import static org.lwjgl.glfw.GLFW.glfwGetMouseButton;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwGetWindowSize;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;
import static org.lwjgl.glfw.GLFW.glfwSetWindowSizeCallback;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_STENCIL_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;

import org.joml.Vector2f;
import org.lwjgl.Version;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWScrollCallbackI;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.glfw.GLFWWindowSizeCallbackI;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.system.MemoryStack;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.utils.GdxNativesLoader;
import com.badlogic.gdx.utils.GdxRuntimeException;

public abstract class Window {

	// The window handle
	private long windowHandle;
	private final WindowInput input;
	private int width;
	private int height;

	public Window(int width, int height) {
		this.input = new WindowInput();
		this.width = width;
		this.height = height;
	}

	public void run() {
		System.out.println("Hello LWJGL " + Version.getVersion() + "!");

		// Setup an error callback. The default implementation
		// will print the error message in System.err.
		GLFWErrorCallback.createPrint(System.err).set();

		// Initialize GLFW. Most GLFW functions will not work before doing this.
		if (!glfwInit()) {
			throw new IllegalStateException("Unable to initialize GLFW");
		}

		// Configure GLFW
		glfwDefaultWindowHints(); // optional, the current window hints are already the default
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

		// Create the window
		this.windowHandle = glfwCreateWindow(this.width, this.height, "Hello World!", NULL, NULL);
		if (this.windowHandle == NULL) {
			throw new RuntimeException("Failed to create the GLFW window");
		}

		glfwSetWindowSizeCallback(this.windowHandle, new GLFWWindowSizeCallbackI() {
			@Override
			public void invoke(long window, int width, int height) {
				System.out.println(String.format("Resize %sx%s", width, height));
				Window.this.width = width;
				Window.this.height = height;
				Window.this.onResize(width, height);
			}
		});

		// Setup a key callback. It will be called every time a key is pressed, repeated
		// or released.
		glfwSetKeyCallback(this.windowHandle, (window, key, scancode, action, mods) -> {
			if ((key == GLFW_KEY_F12) && (action == GLFW_RELEASE)) {
				glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
			}
		});

		// Get the thread stack and push a new frame
		try (MemoryStack stack = stackPush()) {
			final IntBuffer pWidth = stack.mallocInt(1); // int*
			final IntBuffer pHeight = stack.mallocInt(1); // int*

			// Get the window size passed to glfwCreateWindow
			glfwGetWindowSize(this.windowHandle, pWidth, pHeight);

			// Get the resolution of the primary monitor
			final GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

			// Center the window
			glfwSetWindowPos(this.windowHandle, (vidmode.width() - pWidth.get(0)) / 2, (vidmode.height() - pHeight.get(0)) / 2);
		} // the stack frame is popped automatically

		// Make the OpenGL context current
		glfwMakeContextCurrent(this.windowHandle);
		// Enable v-sync
		glfwSwapInterval(1);

		// Make the window visible
		glfwShowWindow(this.windowHandle);

		// This line is critical for LWJGL's interoperation with GLFW's
		// OpenGL context, or any context that is managed externally.
		// LWJGL detects the context that is current in the current thread,
		// creates the GLCapabilities instance and makes the OpenGL
		// bindings available for use.
		GL.createCapabilities();

		System.out.println("OpenGL version: " + GL11.glGetString(GL11.GL_VERSION));

		initLibGdx();
		this.init(this.windowHandle);
		Window.this.onResize(this.width, this.height);

		glClearColor(1.0f, 0.8f, 1.0f, 0.0f);

		// the window or has pressed the ESCAPE key.
		while (!glfwWindowShouldClose(this.windowHandle)) {
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT); // clear the framebuffer

			this.loop();

			glfwSwapBuffers(this.windowHandle); // swap the color buffers

			// Poll for window events. The key callback above will only be
			// invoked during this call.
			glfwPollEvents();
		}

		// Free the window callbacks and destroy the window
		glfwFreeCallbacks(this.windowHandle);
		glfwDestroyWindow(this.windowHandle);

		// Terminate GLFW and free the error callback
		glfwTerminate();
		glfwSetErrorCallback(null).free();
	}

	private void initLibGdx() {
		final GL30Impl gl30Impl = new GL30Impl();
		Gdx.gl = gl30Impl;
		Gdx.gl20 = gl30Impl;
		Gdx.gl30 = gl30Impl;
		GdxNativesLoader.load();
//		LwjglNativesLoader.load();
	}

	protected abstract void init(long windowHandle);

	protected abstract void loop();

	protected abstract void onResize(int width, int height);

	public WindowInput getInput() {
		return this.input;
	}

	public class WindowInput {
		private final double[] mouseXBuffer;
		private final double[] mouseYBuffer;

		/** The mouse position relative to screen **/
		private final Vector2f mousePosition;

		/** The mouse position relative to screen bottom up **/
		private final Vector2f mousePositionBottomUp;

		private final int[] lastKeyState;

		public WindowInput() {
			this.mouseXBuffer = new double[1];
			this.mouseYBuffer = new double[1];
			this.mousePosition = new Vector2f();
			this.mousePositionBottomUp = new Vector2f();
			this.lastKeyState = new int[1000];
		}

		public boolean isMouseLeftClicked() {
			return glfwGetMouseButton(Window.this.windowHandle, GLFW_MOUSE_BUTTON_1) == 1;
		}

		public boolean isMouseRightClicked() {
			return glfwGetMouseButton(Window.this.windowHandle, GLFW_MOUSE_BUTTON_2) == 1;
		}

		public Vector2f getMousePosition() {
			glfwGetCursorPos(Window.this.windowHandle, this.mouseXBuffer, this.mouseYBuffer);
			final float x = (float) Math.floor(this.mouseXBuffer[0]);
			final float y = (float) Math.floor(this.mouseYBuffer[0]);
			this.mousePosition.x = x;
			this.mousePosition.y = y;
			return this.mousePosition;
		}

		public Vector2f getMousePositionBottomUp() {
			final Vector2f mousePosition = getMousePosition();
			this.mousePositionBottomUp.x = mousePosition.x;
			this.mousePositionBottomUp.y = Window.this.height - mousePosition.y;
			return this.mousePositionBottomUp;
		}

		public void setScrollCallback(GLFWScrollCallbackI callback) {
			GLFW.glfwSetScrollCallback(Window.this.windowHandle, callback);
		}

		public boolean isKeyPressed(int key) {
			final int state = GLFW.glfwGetKey(Window.this.windowHandle, key);
			this.lastKeyState[key] = state;
			return state == GLFW.GLFW_PRESS;
		}

		public boolean isKeyReleased(int key) {
			final int state = GLFW.glfwGetKey(Window.this.windowHandle, key);
			this.lastKeyState[key] = state;
			return state == GLFW.GLFW_RELEASE;
		}

		public boolean isKeyUp(int key) {
			final int state = GLFW.glfwGetKey(Window.this.windowHandle, key);
			final int lastState = this.lastKeyState[key];
			this.lastKeyState[key] = state;
			return (state == GLFW.GLFW_RELEASE) && (lastState == GLFW.GLFW_PRESS);
		}
	}

	private static class GL30Impl implements GL30 {

		@Override
		public void glActiveTexture(int texture) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBindTexture(int target, int texture) {
			GL11.glBindTexture(target, texture);
		}

		@Override
		public void glBlendFunc(int sfactor, int dfactor) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glClear(int mask) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glClearColor(float red, float green, float blue, float alpha) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glClearDepthf(float depth) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glClearStencil(int s) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glColorMask(boolean red, boolean green, boolean blue, boolean alpha) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glCompressedTexImage2D(int target, int level, int internalformat, int width, int height, int border, int imageSize, Buffer data) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glCompressedTexSubImage2D(int target, int level, int xoffset, int yoffset, int width, int height, int format, int imageSize, Buffer data) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glCopyTexImage2D(int target, int level, int internalformat, int x, int y, int width, int height, int border) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glCopyTexSubImage2D(int target, int level, int xoffset, int yoffset, int x, int y, int width, int height) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glCullFace(int mode) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDeleteTextures(int n, IntBuffer textures) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDeleteTexture(int texture) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDepthFunc(int func) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDepthMask(boolean flag) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDepthRangef(float zNear, float zFar) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDisable(int cap) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDrawArrays(int mode, int first, int count) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDrawElements(int mode, int count, int type, Buffer indices) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glEnable(int cap) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glFinish() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glFlush() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glFrontFace(int mode) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGenTextures(int n, IntBuffer textures) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int glGenTexture() {
			return GL11.glGenTextures();
		}

		@Override
		public int glGetError() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetIntegerv(int pname, IntBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public String glGetString(int name) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glHint(int target, int mode) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glLineWidth(float width) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glPixelStorei(int pname, int param) {
			GL11.glPixelStorei(pname, param);
		}

		@Override
		public void glPolygonOffset(float factor, float units) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glReadPixels(int x, int y, int width, int height, int format, int type, Buffer pixels) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glScissor(int x, int y, int width, int height) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glStencilFunc(int func, int ref, int mask) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glStencilMask(int mask) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glStencilOp(int fail, int zfail, int zpass) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glTexImage2D(int target, int level, int internalformat, int width, int height, int border, int format, int type, Buffer pixels) {
			if (pixels == null) {
				GL11.glTexImage2D(target, level, internalformat, width, height, border, format, type, (ByteBuffer) null);
			} else if (pixels instanceof ByteBuffer) {
				GL11.glTexImage2D(target, level, internalformat, width, height, border, format, type, (ByteBuffer) pixels);
			} else if (pixels instanceof ShortBuffer) {
				GL11.glTexImage2D(target, level, internalformat, width, height, border, format, type, (ShortBuffer) pixels);
			} else if (pixels instanceof IntBuffer) {
				GL11.glTexImage2D(target, level, internalformat, width, height, border, format, type, (IntBuffer) pixels);
			} else if (pixels instanceof FloatBuffer) {
				GL11.glTexImage2D(target, level, internalformat, width, height, border, format, type, (FloatBuffer) pixels);
			} else if (pixels instanceof DoubleBuffer) {
				GL11.glTexImage2D(target, level, internalformat, width, height, border, format, type, (DoubleBuffer) pixels);
			} else {
				throw new GdxRuntimeException("Can't use " + pixels.getClass().getName() + " with this method. Use ByteBuffer, ShortBuffer, IntBuffer, FloatBuffer or DoubleBuffer instead. Blame LWJGL");
			}
		}

		@Override
		public void glTexParameterf(int target, int pname, float param) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glTexSubImage2D(int target, int level, int xoffset, int yoffset, int width, int height, int format, int type, Buffer pixels) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glViewport(int x, int y, int width, int height) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glAttachShader(int program, int shader) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBindAttribLocation(int program, int index, String name) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBindBuffer(int target, int buffer) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBindFramebuffer(int target, int framebuffer) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBindRenderbuffer(int target, int renderbuffer) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBlendColor(float red, float green, float blue, float alpha) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBlendEquation(int mode) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBlendEquationSeparate(int modeRGB, int modeAlpha) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBlendFuncSeparate(int srcRGB, int dstRGB, int srcAlpha, int dstAlpha) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBufferData(int target, int size, Buffer data, int usage) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBufferSubData(int target, int offset, int size, Buffer data) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int glCheckFramebufferStatus(int target) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glCompileShader(int shader) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int glCreateProgram() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int glCreateShader(int type) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDeleteBuffer(int buffer) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDeleteBuffers(int n, IntBuffer buffers) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDeleteFramebuffer(int framebuffer) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDeleteFramebuffers(int n, IntBuffer framebuffers) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDeleteProgram(int program) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDeleteRenderbuffer(int renderbuffer) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDeleteRenderbuffers(int n, IntBuffer renderbuffers) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDeleteShader(int shader) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDetachShader(int program, int shader) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDisableVertexAttribArray(int index) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDrawElements(int mode, int count, int type, int indices) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glEnableVertexAttribArray(int index) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glFramebufferRenderbuffer(int target, int attachment, int renderbuffertarget, int renderbuffer) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glFramebufferTexture2D(int target, int attachment, int textarget, int texture, int level) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int glGenBuffer() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGenBuffers(int n, IntBuffer buffers) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGenerateMipmap(int target) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int glGenFramebuffer() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGenFramebuffers(int n, IntBuffer framebuffers) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int glGenRenderbuffer() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGenRenderbuffers(int n, IntBuffer renderbuffers) {
			throw new UnsupportedOperationException();
		}

		@Override
		public String glGetActiveAttrib(int program, int index, IntBuffer size, Buffer type) {
			throw new UnsupportedOperationException();
		}

		@Override
		public String glGetActiveUniform(int program, int index, IntBuffer size, Buffer type) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetAttachedShaders(int program, int maxcount, Buffer count, IntBuffer shaders) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int glGetAttribLocation(int program, String name) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetBooleanv(int pname, Buffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetBufferParameteriv(int target, int pname, IntBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetFloatv(int pname, FloatBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetFramebufferAttachmentParameteriv(int target, int attachment, int pname, IntBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetProgramiv(int program, int pname, IntBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public String glGetProgramInfoLog(int program) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetRenderbufferParameteriv(int target, int pname, IntBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetShaderiv(int shader, int pname, IntBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public String glGetShaderInfoLog(int shader) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetShaderPrecisionFormat(int shadertype, int precisiontype, IntBuffer range, IntBuffer precision) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetTexParameterfv(int target, int pname, FloatBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetTexParameteriv(int target, int pname, IntBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetUniformfv(int program, int location, FloatBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetUniformiv(int program, int location, IntBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int glGetUniformLocation(int program, String name) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetVertexAttribfv(int index, int pname, FloatBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetVertexAttribiv(int index, int pname, IntBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetVertexAttribPointerv(int index, int pname, Buffer pointer) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean glIsBuffer(int buffer) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean glIsEnabled(int cap) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean glIsFramebuffer(int framebuffer) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean glIsProgram(int program) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean glIsRenderbuffer(int renderbuffer) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean glIsShader(int shader) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean glIsTexture(int texture) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glLinkProgram(int program) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glReleaseShaderCompiler() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glRenderbufferStorage(int target, int internalformat, int width, int height) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glSampleCoverage(float value, boolean invert) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glShaderBinary(int n, IntBuffer shaders, int binaryformat, Buffer binary, int length) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glShaderSource(int shader, String string) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glStencilFuncSeparate(int face, int func, int ref, int mask) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glStencilMaskSeparate(int face, int mask) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glStencilOpSeparate(int face, int fail, int zfail, int zpass) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glTexParameterfv(int target, int pname, FloatBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glTexParameteri(int target, int pname, int param) {
			GL11.glTexParameteri(target, pname, param);
		}

		@Override
		public void glTexParameteriv(int target, int pname, IntBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform1f(int location, float x) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform1fv(int location, int count, FloatBuffer v) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform1fv(int location, int count, float[] v, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform1i(int location, int x) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform1iv(int location, int count, IntBuffer v) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform1iv(int location, int count, int[] v, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform2f(int location, float x, float y) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform2fv(int location, int count, FloatBuffer v) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform2fv(int location, int count, float[] v, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform2i(int location, int x, int y) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform2iv(int location, int count, IntBuffer v) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform2iv(int location, int count, int[] v, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform3f(int location, float x, float y, float z) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform3fv(int location, int count, FloatBuffer v) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform3fv(int location, int count, float[] v, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform3i(int location, int x, int y, int z) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform3iv(int location, int count, IntBuffer v) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform3iv(int location, int count, int[] v, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform4f(int location, float x, float y, float z, float w) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform4fv(int location, int count, FloatBuffer v) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform4fv(int location, int count, float[] v, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform4i(int location, int x, int y, int z, int w) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform4iv(int location, int count, IntBuffer v) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform4iv(int location, int count, int[] v, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniformMatrix2fv(int location, int count, boolean transpose, FloatBuffer value) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniformMatrix2fv(int location, int count, boolean transpose, float[] value, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniformMatrix3fv(int location, int count, boolean transpose, FloatBuffer value) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniformMatrix3fv(int location, int count, boolean transpose, float[] value, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniformMatrix4fv(int location, int count, boolean transpose, FloatBuffer value) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniformMatrix4fv(int location, int count, boolean transpose, float[] value, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUseProgram(int program) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glValidateProgram(int program) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glVertexAttrib1f(int indx, float x) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glVertexAttrib1fv(int indx, FloatBuffer values) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glVertexAttrib2f(int indx, float x, float y) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glVertexAttrib2fv(int indx, FloatBuffer values) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glVertexAttrib3f(int indx, float x, float y, float z) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glVertexAttrib3fv(int indx, FloatBuffer values) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glVertexAttrib4f(int indx, float x, float y, float z, float w) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glVertexAttrib4fv(int indx, FloatBuffer values) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glVertexAttribPointer(int indx, int size, int type, boolean normalized, int stride, int ptr) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glReadBuffer(int mode) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDrawRangeElements(int mode, int start, int end, int count, int type, Buffer indices) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDrawRangeElements(int mode, int start, int end, int count, int type, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glTexImage3D(int target, int level, int internalformat, int width, int height, int depth, int border, int format, int type, Buffer pixels) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glTexImage3D(int target, int level, int internalformat, int width, int height, int depth, int border, int format, int type, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glTexSubImage3D(int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, Buffer pixels) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glTexSubImage3D(int target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int format, int type, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glCopyTexSubImage3D(int target, int level, int xoffset, int yoffset, int zoffset, int x, int y, int width, int height) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGenQueries(int n, int[] ids, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGenQueries(int n, IntBuffer ids) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDeleteQueries(int n, int[] ids, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDeleteQueries(int n, IntBuffer ids) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean glIsQuery(int id) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBeginQuery(int target, int id) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glEndQuery(int target) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetQueryiv(int target, int pname, IntBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetQueryObjectuiv(int id, int pname, IntBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean glUnmapBuffer(int target) {
			throw new UnsupportedOperationException();
		}

		@Override
		public Buffer glGetBufferPointerv(int target, int pname) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDrawBuffers(int n, IntBuffer bufs) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniformMatrix2x3fv(int location, int count, boolean transpose, FloatBuffer value) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniformMatrix3x2fv(int location, int count, boolean transpose, FloatBuffer value) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniformMatrix2x4fv(int location, int count, boolean transpose, FloatBuffer value) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniformMatrix4x2fv(int location, int count, boolean transpose, FloatBuffer value) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniformMatrix3x4fv(int location, int count, boolean transpose, FloatBuffer value) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniformMatrix4x3fv(int location, int count, boolean transpose, FloatBuffer value) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBlitFramebuffer(int srcX0, int srcY0, int srcX1, int srcY1, int dstX0, int dstY0, int dstX1, int dstY1, int mask, int filter) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glRenderbufferStorageMultisample(int target, int samples, int internalformat, int width, int height) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glFramebufferTextureLayer(int target, int attachment, int texture, int level, int layer) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glFlushMappedBufferRange(int target, int offset, int length) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBindVertexArray(int array) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDeleteVertexArrays(int n, int[] arrays, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDeleteVertexArrays(int n, IntBuffer arrays) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGenVertexArrays(int n, int[] arrays, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGenVertexArrays(int n, IntBuffer arrays) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean glIsVertexArray(int array) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBeginTransformFeedback(int primitiveMode) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glEndTransformFeedback() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBindBufferRange(int target, int index, int buffer, int offset, int size) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBindBufferBase(int target, int index, int buffer) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glTransformFeedbackVaryings(int program, String[] varyings, int bufferMode) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glVertexAttribIPointer(int index, int size, int type, int stride, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetVertexAttribIiv(int index, int pname, IntBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetVertexAttribIuiv(int index, int pname, IntBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glVertexAttribI4i(int index, int x, int y, int z, int w) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glVertexAttribI4ui(int index, int x, int y, int z, int w) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetUniformuiv(int program, int location, IntBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int glGetFragDataLocation(int program, String name) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform1uiv(int location, int count, IntBuffer value) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform3uiv(int location, int count, IntBuffer value) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniform4uiv(int location, int count, IntBuffer value) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glClearBufferiv(int buffer, int drawbuffer, IntBuffer value) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glClearBufferuiv(int buffer, int drawbuffer, IntBuffer value) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glClearBufferfv(int buffer, int drawbuffer, FloatBuffer value) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glClearBufferfi(int buffer, int drawbuffer, float depth, int stencil) {
			throw new UnsupportedOperationException();
		}

		@Override
		public String glGetStringi(int name, int index) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glCopyBufferSubData(int readTarget, int writeTarget, int readOffset, int writeOffset, int size) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetUniformIndices(int program, String[] uniformNames, IntBuffer uniformIndices) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetActiveUniformsiv(int program, int uniformCount, IntBuffer uniformIndices, int pname, IntBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int glGetUniformBlockIndex(int program, String uniformBlockName) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetActiveUniformBlockiv(int program, int uniformBlockIndex, int pname, IntBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetActiveUniformBlockName(int program, int uniformBlockIndex, Buffer length, Buffer uniformBlockName) {
			throw new UnsupportedOperationException();
		}

		@Override
		public String glGetActiveUniformBlockName(int program, int uniformBlockIndex) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glUniformBlockBinding(int program, int uniformBlockIndex, int uniformBlockBinding) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDrawArraysInstanced(int mode, int first, int count, int instanceCount) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDrawElementsInstanced(int mode, int count, int type, int indicesOffset, int instanceCount) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetInteger64v(int pname, LongBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetBufferParameteri64v(int target, int pname, LongBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGenSamplers(int count, int[] samplers, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGenSamplers(int count, IntBuffer samplers) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDeleteSamplers(int count, int[] samplers, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDeleteSamplers(int count, IntBuffer samplers) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean glIsSampler(int sampler) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBindSampler(int unit, int sampler) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glSamplerParameteri(int sampler, int pname, int param) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glSamplerParameteriv(int sampler, int pname, IntBuffer param) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glSamplerParameterf(int sampler, int pname, float param) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glSamplerParameterfv(int sampler, int pname, FloatBuffer param) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetSamplerParameteriv(int sampler, int pname, IntBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGetSamplerParameterfv(int sampler, int pname, FloatBuffer params) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glVertexAttribDivisor(int index, int divisor) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glBindTransformFeedback(int target, int id) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDeleteTransformFeedbacks(int n, int[] ids, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glDeleteTransformFeedbacks(int n, IntBuffer ids) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGenTransformFeedbacks(int n, int[] ids, int offset) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glGenTransformFeedbacks(int n, IntBuffer ids) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean glIsTransformFeedback(int id) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glPauseTransformFeedback() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glResumeTransformFeedback() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glProgramParameteri(int program, int pname, int value) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glInvalidateFramebuffer(int target, int numAttachments, IntBuffer attachments) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glInvalidateSubFramebuffer(int target, int numAttachments, IntBuffer attachments, int x, int y, int width, int height) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void glVertexAttribPointer(int indx, int size, int type, boolean normalized, int stride, Buffer ptr) {
			throw new UnsupportedOperationException();
		}

	}
}
