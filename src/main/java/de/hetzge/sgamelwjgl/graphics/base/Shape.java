package de.hetzge.sgamelwjgl.graphics.base;

import org.lwjgl.opengl.GL33;

public enum Shape {

	TRIANGLE(3, 3, GL33.GL_TRIANGLES), QUAD(4, 6, GL33.GL_TRIANGLES), POINT(1, 1, GL33.GL_POINTS);

	private final int vertexCount;
	private final int indexCount;
	private final int glShape;

	private Shape(int vertexCount, int indexCount, int glShape) {
		this.vertexCount = vertexCount;
		this.indexCount = indexCount;
		this.glShape = glShape;
	}

	public int getVertexCount() {
		return this.vertexCount;
	}

	public int getIndexCount() {
		return this.indexCount;
	}

	public int getGlShape() {
		return this.glShape;
	}
}
