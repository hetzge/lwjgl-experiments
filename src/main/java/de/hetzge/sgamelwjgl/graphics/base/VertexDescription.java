package de.hetzge.sgamelwjgl.graphics.base;

import java.util.Arrays;
import java.util.List;

public final class VertexDescription {
	private final List<VertexAttribute> attributes;
	private final int vertexLength;

	private VertexDescription(List<VertexAttribute> attributes) {
		this.attributes = attributes;
		this.vertexLength = this.attributes.stream().mapToInt(VertexAttribute::getLength).sum();
	}

	public VertexAttribute getAttribute(String name) {
		return this.attributes.stream().filter(attribute -> attribute.getName().equals(name)).findFirst().orElseThrow(() -> new IllegalArgumentException(String.format("attribute with name '%s' is not available.", name)));
	}

	public int getOffset(VertexAttribute attribute) {
		final int attributeIndex = this.attributes.indexOf(attribute);
		if (attributeIndex == -1) {
			throw new IllegalArgumentException("attribute is not available");
		}

		int offset = 0;
		for (int i = 0; i < attributeIndex; i++) {
			offset += this.getAttributes().get(i).getLength();
		}

		return offset;
	}

	public int getVertexLength() {
		return this.vertexLength;
	}

	public int getVertexLengthInBytes() {
		return this.getVertexLength() * (Float.SIZE / Byte.SIZE);
	}

	public List<VertexAttribute> getAttributes() {
		return this.attributes;
	}

	@Override
	public String toString() {
		return "VertexDescription [attributes=" + this.attributes + "]";
	}

	public static VertexDescription create(VertexAttribute... vertexAttributes) {
		return new VertexDescription(Arrays.asList(vertexAttributes));
	}
}
