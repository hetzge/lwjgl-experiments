package de.hetzge.sgamelwjgl.graphics.base;

public final class Animation {

	private final TextureRegion[] frames;
	private final int frameDuration;
	private final int totalDuration;

	private Animation(TextureRegion[] frames, int frameDuration) {
		this.frames = frames;
		this.frameDuration = frameDuration;
		this.totalDuration = frames.length * this.frameDuration;
	}

	public TextureRegion getFrame(int frame) {
		return this.frames[(frame % this.totalDuration) / this.frameDuration];
	}
	
	public static Animation create(TextureRegion region, int columns, int rows, int frameDuration) {
		return new Animation(TextureRegion.create(region, columns, rows), frameDuration);
	}
	
	public static Animation create(TextureRegion[] frames, int frameDuration) {
		return new Animation(frames, frameDuration);
	}
}
