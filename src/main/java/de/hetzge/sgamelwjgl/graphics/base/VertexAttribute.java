package de.hetzge.sgamelwjgl.graphics.base;

import static org.lwjgl.opengl.GL20.glGetAttribLocation;

public final class VertexAttribute {
	private final String name;
	private final Format format;
	private final int handle;
	private final int length;

	private VertexAttribute(String name, Format format, int handle, int length) {
		this.name = name;
		this.format = format;
		this.handle = handle;
		this.length = length;
	}

	public String getName() {
		return this.name;
	}

	public Format getFormat() {
		return this.format;
	}

	public int getHandle() {
		return this.handle;
	}

	public int getLength() {
		return this.length;
	}

	public static VertexAttribute create(ShaderProgram program, String name, Format format, int length) {
		if ((format == Format.COLOR) && (length != 1)) {
			throw new IllegalArgumentException("For COLOR Format the length have to be 1.");
		}

		final int handle = glGetAttribLocation(program.getHandle(), name);

		return new VertexAttribute(name, format, handle, length);
	}

	public enum Format {
		FLOAT, COLOR
	}
}
