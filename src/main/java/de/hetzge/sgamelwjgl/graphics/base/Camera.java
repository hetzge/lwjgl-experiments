package de.hetzge.sgamelwjgl.graphics.base;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL11;

import com.badlogic.gdx.math.MathUtils;

import de.hetzge.sgamelwjgl.base.PixelGrid;
import de.hetzge.sgamelwjgl.base.PositionConsumer;
import de.hetzge.sgamelwjgl.base.TileGrid;

public final class Camera {

	private final Vector3f cameraPosition;
	private final Vector3f targetCameraPosition;
	private final Matrix4f perspectiveMatrix;
	private final Matrix4f invertedPerspectiveMatrix;
	private final Matrix4f combinedMatrix;
	private final Matrix4f invertedCombinedMatrix;
	private final Viewport viewport;
	private int width;
	private int height;
	private float zoom;
	private float targetZoom;
	private final boolean isometric;

	private Camera(PixelGrid pixelGrid, TileGrid tileGrid, boolean isometric) {
		this.cameraPosition = new Vector3f(0f, 0f, 800f);
		this.targetCameraPosition = new Vector3f(this.cameraPosition);
		this.perspectiveMatrix = new Matrix4f();
		this.invertedPerspectiveMatrix = new Matrix4f();
		this.combinedMatrix = new Matrix4f();
		this.invertedCombinedMatrix = new Matrix4f();
		this.viewport = new Viewport(pixelGrid, tileGrid);
		this.zoom = 1.0f;
		this.isometric = isometric;
	}

	public final void setupScreen(int width, int height) {
		this.setupScreen(width, height, this.zoom);
	}

	public final void setupScreen(int width, int height, float zoom) {
		this.width = width;
		this.height = height;
		this.targetZoom = Math.round(zoom * 10f) / 10f;
	}

	public void move(float xOffset, float yOffset) {
		final float cameraSpeed = (20f * this.zoom) / 1f;
		this.targetCameraPosition.add(xOffset * cameraSpeed, yOffset * cameraSpeed, 0.0f);
	}

	public void setCameraPosition(Vector3f position) {
		this.targetCameraPosition.set(position);
	}

	public void zoom(float offset) {
		this.setupScreen(this.width, this.height, Math.min(10f, Math.max(0.1f, this.zoom - (offset * 0.1f))));
	}

	public void update() {
		this.cameraPosition.lerp(this.targetCameraPosition, 0.3f, this.cameraPosition);
		this.zoom = MathUtils.lerp(this.zoom, this.targetZoom, 0.3f);
		final float left = (-this.width / 2) * this.zoom;
		final float right = (this.width / 2) * this.zoom;
		final float bottom = (-this.height / 2) * this.zoom;
		final float top = (this.height / 2) * this.zoom;
		GL11.glViewport(0, 0, this.width, this.height);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glOrtho(left, right, bottom, top, 1000f, -1000f);
		this.perspectiveMatrix.setOrtho(left, right, bottom, top, 1000f, -1000f);
		this.invertedPerspectiveMatrix.set(this.perspectiveMatrix).invert();
		GL11.glTranslatef(-this.cameraPosition.x, this.cameraPosition.y, this.cameraPosition.z);
		this.combinedMatrix.set(this.perspectiveMatrix).translate(-this.cameraPosition.x, this.cameraPosition.y, this.cameraPosition.z);
		this.invertedCombinedMatrix.set(this.combinedMatrix).invert();
		this.viewport.update();
	}

	public Viewport getViewport() {
		return this.viewport;
	}

	public Matrix4f getCombinedMatrix() {
		return this.combinedMatrix;
	}

	public float getX() {
		return this.cameraPosition.x;
	}

	public float getScreenX() {
		return this.getX() - (getWidth() / 2f);
	}

	public float getScreenY() {
		return this.getY() - (getHeight() / 2f);
	}

	public float getY() {
		return this.cameraPosition.y;
	}

	public float getWidth() {
		return this.width * this.zoom;
	}

	public float getHeight() {
		return this.height * this.zoom;
	}

	public float getZoom() {
		return this.zoom;
	}

	public Vector2f screenToWorldPosition(Vector2f screenPosition) {
		return new Vector2f(this.cameraPosition.x + ((screenPosition.x - (this.width / 2f)) * this.zoom), this.cameraPosition.y + ((screenPosition.y - (this.height / 2f)) * this.zoom));
	}

	public static Camera create(int width, int height) {
		return create(width, height, new PixelGrid(1), new TileGrid(1, 1));
	}

	public static Camera create(int width, int height, PixelGrid pixelGrid, TileGrid tileGrid) {
		return create(width, height, pixelGrid, tileGrid, false);
	}

	public static Camera createDummy(int width, int height, PixelGrid pixelGrid, TileGrid tileGrid, boolean isometric) {
		return new Camera(pixelGrid, tileGrid, isometric);
	}

	public static Camera create(int width, int height, PixelGrid pixelGrid, TileGrid tileGrid, boolean isometric) {
		final Camera camera = new Camera(pixelGrid, tileGrid, isometric);
		camera.setupScreen(width, height);
		return camera;
	}

	public final class Viewport {

		private int cameraX;
		private int cameraY;
		private int cameraWidth;
		private int cameraHeight;
		private int screenX;
		private int screenY;
		private int fromX;
		private int fromY;
		private int toX;
		private int toY;

		public final PixelGrid pixelGrid;
		public final TileGrid tileGrid;

		private Viewport(PixelGrid pixelGrid, TileGrid tileGrid) {
			this.pixelGrid = pixelGrid;
			this.tileGrid = tileGrid;
		}

		private void update() {
			if (Camera.this.isometric) {
				this.cameraX = this.pixelGrid.toIsometricTileX(getX(), getY());
				this.cameraY = this.pixelGrid.toIsometricTileY(getY());
				this.cameraWidth = this.pixelGrid.toTile(getWidth());
				this.cameraHeight = this.pixelGrid.toIsometricTileY(getHeight());
				this.screenX = this.pixelGrid.toIsometricTileX(Camera.this.getScreenX(), Camera.this.getScreenY());
				this.screenY = this.pixelGrid.toIsometricTileY(Camera.this.getScreenY());
				this.fromX = this.tileGrid.clampX(this.screenX);
				this.fromY = this.tileGrid.clampY(this.screenY);
				this.toX = this.pixelGrid.toIsometricTileX(Camera.this.getScreenX() + getWidth(), Camera.this.getScreenY() + getHeight());
				this.toY = this.pixelGrid.toIsometricTileY(Camera.this.getScreenY() + getHeight());
			} else {
				this.cameraX = this.pixelGrid.toTile(getX());
				this.cameraY = this.pixelGrid.toTile(getY());
				this.cameraWidth = this.pixelGrid.toTile(getWidth());
				this.cameraHeight = this.pixelGrid.toTile(getHeight());
				this.screenX = this.cameraX - (this.cameraWidth / 2);
				this.screenY = this.cameraY - (this.cameraHeight / 2);
				this.fromX = this.tileGrid.clampX(this.screenX - 3);
				this.fromY = this.tileGrid.clampY(this.screenY - 3);
				this.toX = this.tileGrid.clampX(this.screenX + this.cameraWidth + 3);
				this.toY = this.tileGrid.clampY(this.screenY + this.cameraHeight + 3);
			}
		}

		public void forEach(PositionConsumer consumer, int offset) {
			this.tileGrid.forEach(this.fromX - offset, this.fromY - offset, this.toX + offset, this.toY + offset, consumer);
		}

		public boolean contains(int x, int y, int tolerance) {
			return (x > (this.fromX - tolerance)) && (x < (this.toX + tolerance)) && (y > (this.fromY - tolerance)) && (y < (this.toY + tolerance));
		}

		public int getCameraX() {
			return this.cameraX;
		}

		public int getCameraY() {
			return this.cameraY;
		}

		public int getCameraWidth() {
			return this.cameraWidth;
		}

		public int getCameraHeight() {
			return this.cameraHeight;
		}

		public int getScreenX() {
			return this.screenX;
		}

		public int getScreenY() {
			return this.screenY;
		}

		public int getFromX() {
			return this.fromX;
		}

		public int getFromY() {
			return this.fromY;
		}

		public int getToX() {
			return this.toX;
		}

		public int getToY() {
			return this.toY;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = (prime * result) + getEnclosingInstance().hashCode();
			result = (prime * result) + this.cameraHeight;
			result = (prime * result) + this.cameraWidth;
			result = (prime * result) + this.cameraX;
			result = (prime * result) + this.cameraY;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final Viewport other = (Viewport) obj;
			if (!getEnclosingInstance().equals(other.getEnclosingInstance())) {
				return false;
			}
			if (this.cameraHeight != other.cameraHeight) {
				return false;
			}
			if (this.cameraWidth != other.cameraWidth) {
				return false;
			}
			if (this.cameraX != other.cameraX) {
				return false;
			}
			if (this.cameraY != other.cameraY) {
				return false;
			}
			return true;
		}

		private Camera getEnclosingInstance() {
			return Camera.this;
		}
	}
}
