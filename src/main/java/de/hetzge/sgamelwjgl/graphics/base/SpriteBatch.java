package de.hetzge.sgamelwjgl.graphics.base;

import java.util.Arrays;
import java.util.Comparator;

public final class SpriteBatch {

	private static final Comparator<Sprite> COMPARATOR = Comparator.comparingDouble((Sprite s) -> s.z).thenComparing((Sprite s) -> s.y).reversed().thenComparing((Sprite s) -> s.x);

	private final Sprite[] sprites;
	private int index;

	private SpriteBatch(Sprite[] sprites) {
		this.sprites = sprites;
		this.index = 0;
	}

	public void add(TextureRegion textureRegion, float x, float y, float z, float width, float height) {
		add(textureRegion, x, y, z, width, height, 0f);
	}

	public void add(TextureRegion textureRegion, float x, float y, float z, float width, float height, float colorBit) {
		final Sprite sprite = this.sprites[this.index++];
		sprite.textureRegion = textureRegion;
		sprite.x = x;
		sprite.y = y;
		sprite.z = z;
		sprite.width = width;
		sprite.height = height;
		sprite.colorBit = colorBit;
	}

	public void render(GeometryBuffer buffer) {

		Arrays.sort(this.sprites, 0, this.index, COMPARATOR);

		for (int i = 0; i < this.index; i++) {
			final Sprite sprite = this.sprites[i];
			sprite.textureRegion.render(buffer, sprite.x, sprite.y, sprite.z, sprite.width, sprite.height, sprite.colorBit);
		}

		this.index = 0;
	}

	public static SpriteBatch create(int size) {
		final Sprite[] sprites = new Sprite[size];
		for (int i = 0; i < sprites.length; i++) {
			sprites[i] = new Sprite();
		}
		return new SpriteBatch(sprites);
	}

	private static class Sprite {
		TextureRegion textureRegion;
		float x;
		float y;
		float z;
		float width;
		float height;
		float colorBit;
	}
}
