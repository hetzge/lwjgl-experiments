package de.hetzge.sgamelwjgl;

public interface Operations {

	void buildWay(int fromX, int fromY, int toX, int toY, int wayTypeOrdinal, int playerId);
	
	void destroyWay(int fromX, int fromY, int toX, int toY, int playerId);
	
	void buildResource(int fromX, int fromY, int toX, int toY, int resourceTypeOrdinal);
	
	void destroyResource(int fromX, int fromY, int toX, int toY);

	void buildPerson(int x, int y, int personTypeOrdinal, int playerId);
	
	void buildBuilding(int x, int y, int buildingTypeOrdinal, int playerId);
	
	void sendPersonsTo(int[] personIds, int x, int y, int playerId);
	
}
