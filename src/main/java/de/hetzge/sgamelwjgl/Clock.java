package de.hetzge.sgamelwjgl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import de.hetzge.sgamelwjgl.entity.Game;

public final class Clock {

	private Frame currentFrame;

	public Clock() {
		this.currentFrame = new Frame(0L);
	}

	public void tick(Game game) {
		this.currentFrame.tick(game);
		this.currentFrame = this.currentFrame.next();
	}

	public boolean isFuture(long frameId) {
		return this.currentFrame.getId() < frameId;
	}

	public Frame getCurrentFrame() {
		return this.currentFrame;
	}

	public CallbackReference addCallback(long frameId, Callback callback) {
		return getFrame(frameId).addCallback(callback);
	}

	public boolean isXFrame(int xth) {
		return (this.currentFrame.id % xth) == 0;
	}
	
	public Frame getFrame(long frameId) {
		Frame frame = this.currentFrame;
		while (frame.getId() < frameId) {
			frame = frame.next();
		}
		if (frame.getId() == frameId) {
			return frame;
		} else {
			throw new IllegalStateException();
		}
	}

	public Frame getFrameInDuration(long duration) {
		return getFrame(getCurrentFrame().getId() + duration);
	}

	public static final class Frame {
		private final long id;
		private final List<Callback> callbacks;
		private Frame nextFrame;

		public Frame(long id) {
			this.id = id;
			this.callbacks = new LinkedList<>();
		}

		public long getId() {
			return this.id;
		}

		public CallbackReference addCallback(Callback callback) {
			final CallbackReference callbackReference = new CallbackReference(this, callback);
			this.callbacks.add(callback);
			return callbackReference;
		}

		public void removeCallback(Callback callback) {
			this.callbacks.remove(callback);
		}

		public Frame next() {
			if (this.nextFrame == null) {
				this.nextFrame = new Frame(this.id + 1);
			}
			return this.nextFrame;
		}

		private void tick(Game game) {
			// need to create copy of callbacks, otherwise delete mess things up
			for (final Callback callback : new ArrayList<>(this.callbacks)) {
				callback.update(game);
			}
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = (prime * result) + (int) (this.id ^ (this.id >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final Frame other = (Frame) obj;
			if (this.id != other.id) {
				return false;
			}
			return true;
		}
	}

	public static final class CallbackReference {

		private final Frame frame;
		private final Callback callback;

		public CallbackReference(Frame frame, Callback callback) {
			this.frame = frame;
			this.callback = callback;
		}

		public void cancel() {
			this.frame.removeCallback(this.callback);
		}
	}

	public static interface Callback {
		void update(Game game);
	}
}
