package de.hetzge.sgamelwjgl;

import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glUniform1i;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;

import java.io.File;

import org.joml.Matrix4f;

import de.hetzge.sgamelwjgl.graphics.base.ShaderProgram;
import de.hetzge.sgamelwjgl.graphics.base.VertexAttribute;
import de.hetzge.sgamelwjgl.graphics.base.VertexAttribute.Format;
import de.hetzge.sgamelwjgl.graphics.base.VertexDescription;

public final class DefaultShader {

	private final ShaderProgram program;
	private final VertexDescription vertexDescription;
	private final int colorModeUniformLocation;
	private final int combinedUniformLocation;

	private DefaultShader(ShaderProgram program, VertexDescription vertexDescription, int colorModeUniformLocation, int combinedUniformLocation) {
		this.program = program;
		this.vertexDescription = vertexDescription;
		this.colorModeUniformLocation = colorModeUniformLocation;
		this.combinedUniformLocation = combinedUniformLocation;
	}

	public void use(ColorMode colorMode, Matrix4f combinedMatrix) {
		this.program.use();
		setColorMode(colorMode);
		setCombined(combinedMatrix);
	}

	public VertexDescription getVertexDescription() {
		return this.vertexDescription;
	}

	public ShaderProgram getProgram() {
		return this.program;
	}

	private void setColorMode(ColorMode colorMode) {
		glUniform1i(this.colorModeUniformLocation, colorMode.ordinal());
	}

	private void setCombined(Matrix4f combinedMatrix) {
		final float[] buffer = new float[16];
		glUniformMatrix4fv(this.combinedUniformLocation, false, combinedMatrix.get(buffer));
	}

	public static DefaultShader load() {
		return create(ShaderProgram.load(new File("shader/default.vs"), null, new File("shader/default.fs")));
		// return create(ShaderProgram.load(new File("shader/default.vs"), new
		// File("shader/default.gs"), new File("shader/default.fs")));
	}

	private static DefaultShader create(ShaderProgram program) {
		final VertexAttribute positionAttribute = VertexAttribute.create(program, "aPos", Format.FLOAT, 3);
		final VertexAttribute colorAttribute = VertexAttribute.create(program, "aColor", Format.COLOR, 1);
		final VertexAttribute textureCoordinatesAttribute = VertexAttribute.create(program, "aTexCoord", Format.FLOAT, 2);
		final VertexDescription vertexDescription = VertexDescription.create(positionAttribute, colorAttribute, textureCoordinatesAttribute);

		final int colorModeUniformLocation = glGetUniformLocation(program.getHandle(), "u_color_mode");
		final int combinedUniformLocation = glGetUniformLocation(program.getHandle(), "u_combined");

		return new DefaultShader(program, vertexDescription, colorModeUniformLocation, combinedUniformLocation);
	}

	public static enum ColorMode {
		REPLACE, MULTIPLY;
	}
}
