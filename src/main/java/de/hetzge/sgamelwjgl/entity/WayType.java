package de.hetzge.sgamelwjgl.entity;

public enum WayType {

	FREE(WayTileType.FREE), STONE(WayTileType.STONE);

	public static final WayType[] VALUES = values();
	
	private final WayTileType tileType;

	private WayType(WayTileType tileType) {
		this.tileType = tileType;
	}

	public WayTileType getTileType() {
		return this.tileType;
	}
}
