package de.hetzge.sgamelwjgl.entity;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import de.hetzge.sgamelwjgl.base.WorldObject;

public final class Stock implements Serializable {

	private final WorldObject parent;
	private List<Item> items;

	public Stock(WorldObject parent) {
		this.parent = parent;
		this.items = new LinkedList<>();
	}

	public boolean contains(ItemType itemType) {
		for (final Item item : this.items) {
			if (item.getItemType() == itemType) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isEmpty() {
		return this.items.isEmpty();
	}
	
	public boolean isNotEmpty() {
		return !isEmpty();
	}
	
	public void add(Item item) {
		this.items.add(item);
	}
	
	public void remove(Item item) {
		this.items.remove(item);
	}

	public WorldObject getParent() {
		return this.parent;
	}

	public List<Item> getItems() {
		return this.items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}
}
