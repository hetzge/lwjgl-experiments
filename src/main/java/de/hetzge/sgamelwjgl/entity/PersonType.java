package de.hetzge.sgamelwjgl.entity;

public enum PersonType {

	CARRIER(true), WOODWORKER(true), GRADER(true), BUILDER(true);

	public static final PersonType[] VALUES = values();

	private final boolean boundToOwnLand;

	private PersonType(boolean boundToOwnLand) {
		this.boundToOwnLand = boundToOwnLand;
	}

	public boolean isBoundToOwnLand() {
		return this.boundToOwnLand;
	}
}
