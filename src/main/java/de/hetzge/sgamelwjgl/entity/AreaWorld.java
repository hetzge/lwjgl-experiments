package de.hetzge.sgamelwjgl.entity;

import de.hetzge.sgamelwjgl.base.PositionPredicate;
import de.hetzge.sgamelwjgl.base.TileConnectionOffset;
import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.base.TileGridFloodFill;

public final class AreaWorld {

	private static final int NO_AREA = 0;

	private final TileGrid tileGrid;
	private final TileConnectionOffset[] offsets;
	private final int[] areas;
	private int nextArea;

	private transient TileGridFloodFill floodFill;

	private AreaWorld(TileGrid tileGrid, TileConnectionOffset[] offsets, int[] areas, int nextArea) {
		this.tileGrid = tileGrid;
		this.offsets = offsets;
		this.areas = areas;
		this.nextArea = nextArea;
	}

	public void update(int x, int y, ConnectedPredicate connectedPredicate, PositionPredicate collisionPredicate) {
		final int beforeNextArea = this.nextArea;

		if (!collisionPredicate.test(x, y)) {
			final int[] aroundArea = new int[1];
			final boolean anyDifferentAround = this.tileGrid.anyMatchAround(x, y, (ax, ay) -> {
				final int area = getArea(ax, ay);
				if ((area != NO_AREA) && (aroundArea[0] != NO_AREA) && (area != aroundArea[0])) {
					return true;
				} else {
					if (area != NO_AREA) {
						aroundArea[0] = area;
					}
					return false;
				}
			});
			if (!anyDifferentAround) {
				if (aroundArea[0] == NO_AREA) {
					flood(x, y, collisionPredicate);
				} else {
					setArea(x, y, aroundArea[0]);
				}
			} else {
				flood(x, y, collisionPredicate);
			}
		} else {

			setArea(x, y, NO_AREA);

			// TODO shortcuts ?!

			for (int a = 0; a < this.offsets.length; a++) {
				final TileConnectionOffset aOrientation = this.offsets[a];
				final int ax = x + aOrientation.getOffsetX();
				final int ay = y + aOrientation.getOffsetY();

				if (this.tileGrid.isValid(ax, ay) && !collisionPredicate.test(ax, ay)) {
					for (int b = 0; b < this.offsets.length; b++) {
						final TileConnectionOffset bOrientation = this.offsets[b];
						final int bx = x + bOrientation.getOffsetX();
						final int by = y + bOrientation.getOffsetY();

						if ((ax != bx) || (ay != by)) {

							if (this.tileGrid.isValid(bx, by) && !collisionPredicate.test(bx, by)) {
								final int areaA = getArea(ax, ay);
								final int areaB = getArea(bx, by);

								if ((areaA < beforeNextArea) && (areaA == areaB)) {
									if (!connectedPredicate.isConnected(ax, ay, bx, by)) {
										flood(ax, ay, collisionPredicate);
									}
								}
							}
						}
					}
				}
			}

		}
	}

	public boolean isSameArea(int a, int b) {
		return getArea(a) == getArea(b);
	}

	public boolean isSameArea(int ax, int ay, int bx, int by) {
		return getArea(ax, ay) == getArea(bx, by);
	}

	public boolean isNoArea(int x, int y) {
		return getArea(x, y) == NO_AREA;
	}

	public TileGrid getTileGrid() {
		return this.tileGrid;
	}

	private void flood(int x, int y, PositionPredicate collisionPredicate) {
		if (!collisionPredicate.test(x, y)) {
			getFloodFill().flood(x, y, collisionPredicate, (fx, fy) -> {
				if (!collisionPredicate.test(fx, fy)) {
					setArea(fx, fy, this.nextArea);
				}
				return false;
			});
			this.nextArea++;

			if (this.nextArea == Integer.MAX_VALUE) {
				// TODO compress
			}
		}
	}

	public int getArea(int x, int y) {
		return getArea(this.tileGrid.index(x, y));
	}

	public int getArea(int index) {
		return this.areas[index];
	}

	private void setArea(int x, int y, int area) {
		this.areas[this.tileGrid.index(x, y)] = area;
	}

	private TileGridFloodFill getFloodFill() {
		if (this.floodFill == null) {
			this.floodFill = TileGridFloodFill.create(this.tileGrid, this.offsets);
		}
		return this.floodFill;
	}

	private void debugOutput() {
		this.tileGrid.forEach((x, y) -> {
			System.out.print(getArea(x, y));
			if (x == (this.tileGrid.getWidth() - 1)) {
				System.out.print("\n");
			}
		});
	}

	public static AreaWorld create(TileGrid tileGrid, TileConnectionOffset[] offsets) {
		final int[] areas = new int[tileGrid.getLength()];

		return new AreaWorld(tileGrid, offsets, areas, 1);
	}

	@FunctionalInterface
	public static interface ConnectedPredicate {
		boolean isConnected(int fromX, int fromY, int toX, int toY);
	}

//	public static void main(String[] args) {
//
//		final AreaWorld world = create(new TileGrid(10, 10), Orientation.VALUES);
//		final TileGrid tileGrid = world.getTileGrid();
//		final Pathfinder pathfinder = Pathfinder.create(tileGrid);
//
//		final int[] owners = new int[tileGrid.getLength()];
//		tileGrid.forEach((x, y) -> {
//			if (Utils.distance(x, y, 8, 8) < 7) {
//				owners[tileGrid.index(x, y)] = 1;
//			}
//		});
//
//		final boolean[] collision = new boolean[tileGrid.getLength()];
//		tileGrid.forEach((x, y) -> {
//			final int owner = owners[tileGrid.index(x, y)];
//			if (world.getArea(x, y) == 0) {
//				world.flood(x, y, (cx, cy) -> {
//					return (owners[tileGrid.index(cx, cy)] != owner) || collision[tileGrid.index(cx, cy)];
//				});
//			}
//		});
//
//		System.out.println("---");
//
//		collision[tileGrid.index(0, 6)] = true;
//		world.update(0, 6, pathfinder, (cx, cy) -> {
//			return (owners[tileGrid.index(cx, cy)] != owners[tileGrid.index(0, 6)]) || collision[tileGrid.index(cx, cy)];
//		});
//
//		collision[tileGrid.index(1, 6)] = true;
//		world.update(1, 6, pathfinder, (cx, cy) -> {
//			return (owners[tileGrid.index(cx, cy)] != owners[tileGrid.index(1, 6)]) || collision[tileGrid.index(cx, cy)];
//		});
//
//		System.out.println("---");
//
//		collision[tileGrid.index(7, 0)] = true;
//		world.update(7, 0, pathfinder, (cx, cy) -> {
//			return (owners[tileGrid.index(cx, cy)] != owners[tileGrid.index(7, 0)]) || collision[tileGrid.index(cx, cy)];
//		});
//
//		System.out.println("---");
//
//		collision[tileGrid.index(7, 1)] = true;
//		world.update(7, 1, pathfinder, (cx, cy) -> {
//			return (owners[tileGrid.index(cx, cy)] != owners[tileGrid.index(7, 1)]) || collision[tileGrid.index(cx, cy)];
//		});
//
//		System.out.println("---");
//
//		collision[tileGrid.index(7, 0)] = false;
//		world.update(7, 0, pathfinder, (cx, cy) -> {
//			return (owners[tileGrid.index(cx, cy)] != owners[tileGrid.index(7, 0)]) || collision[tileGrid.index(cx, cy)];
//		});
//
//		world.debugOutput();
//	}
}
