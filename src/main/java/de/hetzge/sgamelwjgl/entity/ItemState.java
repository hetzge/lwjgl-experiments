package de.hetzge.sgamelwjgl.entity;

public enum ItemState {
	BLOCKED, SUPPLY, SUPPLIED, DEMAND, DEMANDED, USED
}
