package de.hetzge.sgamelwjgl.entity;

public enum ItemType {
	WOOD, STONE;
	
	public static final ItemType[] VALUES = values();
}
