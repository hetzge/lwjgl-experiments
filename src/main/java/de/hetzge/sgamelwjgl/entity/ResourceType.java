package de.hetzge.sgamelwjgl.entity;

public enum ResourceType {
	FOREST(true), STONE(true);

	public static final ResourceType[] VALUES = values();
	
	private final boolean collision;

	private ResourceType(boolean collision) {
		this.collision = collision;
	}

	public boolean isCollision() {
		return this.collision;
	}
}
