package de.hetzge.sgamelwjgl.entity;

import com.badlogic.gdx.graphics.Color;

import de.hetzge.sgamelwjgl.base.Colors;

public final class Player {

	private final int id;
	private final boolean useWays;
	private final boolean useWaypoints;

	public Player(int id, boolean useWays, boolean useWaypoints) {
		if (useWaypoints && !useWays) {
			throw new IllegalStateException();
		}

		this.id = id;
		this.useWays = useWays;
		this.useWaypoints = useWaypoints;
	}

	public Color getColor() {
		return Colors.colors[this.id];
	}

	public float getColorFloatBits() {
		return Colors.colorFloatBits[this.id];
	}

	public int getId() {
		return this.id;
	}

	public boolean isUseWays() {
		return this.useWays;
	}

	public boolean isUseWaypoints() {
		return this.useWaypoints;
	}

	@Override
	public int hashCode() {
		return this.id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Player other = (Player) obj;
		if (this.id != other.id) {
			return false;
		}
		return true;
	}
}
