package de.hetzge.sgamelwjgl.entity;

public enum TileType {
	GRASS, DESERT, SLOPE
}
