package de.hetzge.sgamelwjgl.entity;

import org.joml.Vector2i;

import de.hetzge.sgamelwjgl.Assets;
import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.graphics.base.TextureRegion;
import de.hetzge.sgamelwjgl.tilemap.Heightmap;

public final class ResourceWorld {

	private final TileGrid tileGrid;
	private final int width;
	private final int height;

	private final ResourceType[] resourceTypes;
	private final byte[] amounts;
	private final boolean[] horizontals;
	private final boolean[] verticals;

	private ResourceWorld(TileGrid tileGrid) {
		this.tileGrid = tileGrid;
		this.width = tileGrid.getWidth();
		this.height = tileGrid.getHeight();
		this.resourceTypes = new ResourceType[tileGrid.getLength()];
		this.amounts = new byte[tileGrid.getLength()];
		this.horizontals = new boolean[tileGrid.getLength()];
		this.verticals = new boolean[tileGrid.getLength()];
	}

	public void unset(int x, int y) {
		set(x, y, null, (byte) 0);
	}

	public void set(int x, int y, ResourceType resourceType, byte amount) {
		final int index = (y * this.width) + x;
		this.resourceTypes[index] = amount > 0 ? resourceType : null;
		this.amounts[index] = amount;

		this.updatePattern(x, y);
		this.updatePattern(x - 1, y);
		this.updatePattern(x + 1, y);
		this.updatePattern(x, y - 1);
		this.updatePattern(x, y + 1);
	}
	
	public boolean reduce(int x, int y) {
		final byte amount = amount(x, y);
		if(amount > 0) {
			set(x, y, getResourceType(x, y), (byte) (amount - 1));
			return true;
		} else {
			return false;
		}
	}

	private void updatePattern(int x, int y) {
		if ((x >= 0) && (x < this.width) && (y >= 0) && (y < this.height)) {
			final int index = (y * this.width) + x;
			final ResourceType resourceType = this.resourceTypes[index];

			this.horizontals[index] = ((x > 0) && this.isResourceType(x - 1, y, resourceType)) || ((x < (this.width - 1)) && this.isResourceType(x + 1, y, resourceType));
			this.verticals[index] = ((y > 0) && this.isResourceType(x, y - 1, resourceType)) || ((y < (this.height - 1)) && this.isResourceType(x, y + 1, resourceType));
		}
	}

	public boolean isHorizontal(int x, int y) {
		return this.horizontals[(y * this.width) + x];
	}

	public boolean isVertical(int x, int y) {
		return this.verticals[(y * this.width) + x];
	}

	public boolean isResourceType(int x, int y, ResourceType resourceType) {
		return this.getResourceType(x, y) == resourceType;
	}

	public boolean isCollision(int x, int y) {
		final ResourceType resourceType = this.getResourceType(x, y);
		return (resourceType != null) && resourceType.isCollision();
	}

	public ResourceType getResourceType(int x, int y) {
		return this.resourceTypes[(y * this.width) + x];
	}

	public byte amount(int x, int y) {
		return this.amounts[(y * this.width) + x];
	}

	public Vector2i findAround(int x, int y, ResourceType resourceType) {
		return this.tileGrid.findPositionAround(x, y, (ax, ay) -> isResourceType(ax, ay, resourceType));
	}

	public void render(int x, int y, Heightmap heightmap, SpriteBatch batch) {
		final float tx = x * 32;
		final float ty = (-y * 32) + (heightmap.getTileHeight(heightmap.index(x, y)) * 32f);
		final ResourceType resourceType = getResourceType(x, y);
		if (resourceType != null) {
			final TextureRegion textureRegion = Assets.INSTANCE.resource.get(resourceType);

			final boolean horizontal = isHorizontal(x, y);
			final boolean vertical = isVertical(x, y);

			if (horizontal && vertical) {
				if (((x % 2) == 0)) {
					batch.add(textureRegion, tx, ty - 16f, 0f, 32f, 32f);
					batch.add(textureRegion, tx + 24f, ty - 12f, 0f, 32f, 32f);
					batch.add(textureRegion, tx + 16f, ty - 32f, 0f, 32f, 32f);
				} else {
					batch.add(textureRegion, tx + 16f, ty - 16f, 0, 32f, 32f);
					batch.add(textureRegion, tx, ty - 32f, 0f, 32f, 32f);
					batch.add(textureRegion, tx + 24f, ty - 28f, 0f, 32f, 32f);
				}
			} else if (horizontal) {
				batch.add(textureRegion, tx + (32f / 4f), ty - 26f, 0f, 32f, 32f);
				batch.add(textureRegion, tx + (32f / 4f) + 16f, ty - 34f, 0f, 32f, 32f);
			} else if (vertical) {
				batch.add(textureRegion, tx + 14f, ty - 16f, 0f, 32f, 32f);
				batch.add(textureRegion, tx + 18f, ty - 32f, 0f, 32f, 32f);
			} else {
				batch.add(textureRegion, tx + 16f, ty - 32f, 0f, 32f, 32f);
			}
		}
	}

	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}

	public static ResourceWorld create(int width, int height, Heightmap heightmap) {
		final ResourceWorld resourceWorld = new ResourceWorld(new TileGrid(width, height));

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if ((Math.random() > 0.9) && (heightmap.getTileSlope(heightmap.index(x, y)) < 0.1)) {
					resourceWorld.set(x, y, ResourceType.FOREST, (byte) 1);
				}
			}
		}

		return resourceWorld;
	}
}
