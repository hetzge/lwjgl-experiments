package de.hetzge.sgamelwjgl.entity;

import org.joml.Vector2i;

import de.hetzge.sgamelwjgl.base.Path;
import de.hetzge.sgamelwjgl.base.PositionPredicate;

public final class WorldNavigation {

	private final World world;
	private final WorldCollision collision;

	private WorldNavigation(World world, WorldCollision collision) {
		this.world = world;
		this.collision = collision;
	}

	public Path path(Person person, int x, int y, Player player) {
		return path(person.getX(), person.getY(), x, y, person.isBoundToOwnLand(), player.isUseWays(), player.getId());
	}

	public Path path(int fromX, int fromY, int toX, int toY, boolean isBoundToOwnLand, boolean useWay, int playerId) {
		if (isPathPossible(fromX, fromY, toX, toY, useWay, isBoundToOwnLand)) {
			return this.world.getPathfinder().findPath(fromX, fromY, toX, toY, 1000, (x, y) -> {
				return (this.collision.isCollision(x, y)) || (useWay && (!this.world.wayWorld.is(x, y) && !((x == fromX) && (y == fromY)) && !((x == toX) && (y == toY)))) || (isBoundToOwnLand && !this.world.ownerWorld.isOwner(x, y, playerId));
			});
		} else {
			return null;
		}
	}

	public boolean isPathPossible(int fromX, int fromY, int toX, int toY, boolean useWay, boolean isBoundToOwnLand) {
		return (!useWay || isConnected(fromX, fromY, toX, toY) || isConnectedAround(fromX, fromY, toX, toY)) && (!isBoundToOwnLand || this.collision.isSamePlayerArea(fromX, fromY, toX, toY)) && (this.collision.isSameGlobalArea(fromX, fromY, toX, toY));
	}

	public Path pathToOwnLand(int fromX, int fromY, int playerId) {
		final PositionPredicate collisionPredicate = this.collision::isCollision;
		final Vector2i to = this.world.getFloodFill().flood(fromX, fromY, collisionPredicate, (x, y) -> this.world.ownerWorld.isOwner(x, y, playerId) && !collisionPredicate.test(x, y));
		return to != null ? this.world.getPathfinder().findPath(fromX, fromY, to.x, to.y, 1000, collisionPredicate) : null;
	}

	public Path pathToWay(int fromX, int fromY, int playerId, boolean isBoundToOwnLand) {
		final PositionPredicate collisionPredicate = isBoundToOwnLand ? this.collision.isOwnerCollisionPredicate(fromX, fromY) : this.collision::isCollision;
		final Vector2i to = this.world.getFloodFill().flood(fromX, fromY, collisionPredicate, this.world.wayWorld::is);
		return to != null ? this.world.getPathfinder().findPath(fromX, fromY, to.x, to.y, 1000, collisionPredicate) : null;
	}

	public Path pathToResource(int fromX, int fromY, boolean isBoundToOwnLand, ResourceType resourceType) {
		final PositionPredicate ownerCollisionPredicate = this.collision.isOwnerCollisionPredicate(fromX, fromY);
		final PositionPredicate collisionPredicate = (x, y) -> {
			if (isBoundToOwnLand) {
				// TODO skip the resource collision could be done more elegant ?!
				return ownerCollisionPredicate.test(x, y) && !this.world.resourceWorld.isResourceType(x, y, resourceType);
			} else {
				return this.collision.isCollision(x, y) && !this.world.resourceWorld.isResourceType(x, y, resourceType);
			}
		};
		final Vector2i to = this.world.getFloodFill().flood(fromX, fromY, collisionPredicate, (x, y) -> this.world.resourceWorld.isResourceType(x, y, resourceType));
		final Path path = to != null ? this.world.getPathfinder().findPath(fromX, fromY, to.x, to.y, 1000, collisionPredicate) : null;
		if (path != null) {
			return resourceType.isCollision() ? path.withoutLast() : path;
		} else {
			return null;
		}
	}

	public Path pathToUngradeTile(int fromX, int fromY, boolean isBoundToOwnLand) {
		final PositionPredicate collisionPredicate = isBoundToOwnLand ? this.collision.isOwnerCollisionPredicate(fromX, fromY) : this.collision::isCollision;
		final Vector2i to = this.world.getFloodFill().flood(fromX, fromY, collisionPredicate, (x, y) -> {
			return this.world.heightmap.isUngradeTile(this.world.tileGrid.index(x, y));
		});
		return to != null ? this.world.getPathfinder().findPath(fromX, fromY, to.x, to.y, 1000, collisionPredicate) : null;
	}

	private boolean isConnected(int ax, int ay, int bx, int by) {
		return this.world.wayWorld.is(ax, ay) && this.collision.isSameWayArea(ax, ay, bx, by);
	}

	private boolean isConnectedAround(int ax, int ay, int bx, int by) {
		return this.world.tileGrid.findAround(ax, ay, 0, (x1, y1) -> {
			return this.world.tileGrid.findAround(bx, by, 0, (x2, y2) -> {
				return isConnected(x1, y1, x2, y2);
			});
		});
	}

	public static WorldNavigation create(World world, WorldCollision collision) {
		return new WorldNavigation(world, collision);
	}
}
