package de.hetzge.sgamelwjgl.entity;

import java.io.File;

import com.badlogic.gdx.utils.IntMap;

import de.hetzge.sgamelwjgl.Assets;
import de.hetzge.sgamelwjgl.Clock;
import de.hetzge.sgamelwjgl.base.PixelGrid;
import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.tilemap.AutoTilemap;
import de.hetzge.sgamelwjgl.tilemap.Heightmap;

public final class Game {

	final Clock clock;
	final World world;
	final WorldCollision collision;
	final WorldObjects objects;
	final WorldNavigation navigation;
	final TileGrid tileGrid;
	final PixelGrid pixelGrid;
	final IntMap<Player> playersById;

	private Game(Clock clock, World world, WorldCollision collision, WorldObjects objects, WorldNavigation navigation) {
		this.clock = clock;
		this.world = world;
		this.collision = collision;
		this.objects = objects;
		this.navigation = navigation;
		this.tileGrid = world.tileGrid;
		this.pixelGrid = new PixelGrid(32);
		this.playersById = new IntMap<>(20);

		this.playersById.put(0, new Player(0, false, false));
	}

	public Player getPlayer(int playerId) {
		return this.playersById.get(playerId);
	}

	public Clock getClock() {
		return this.clock;
	}

	public World getWorld() {
		return this.world;
	}

	public WorldObjects getObjects() {
		return this.objects;
	}

	public TileGrid getTileGrid() {
		return this.tileGrid;
	}

	public PixelGrid getPixelGrid() {
		return this.pixelGrid;
	}

	public static Game create() {
		final int width = 1000;
		final int height = 1000;
		final AutoTilemap<TileType> tilemap = AutoTilemap.create(width, height, 5, Assets.INSTANCE.autoTilesets); // TODO assets not allowed in game
		tilemap.fill(TileType.GRASS);

		final Heightmap heightmap = Heightmap.create(new File("/home/hetzge/Downloads/test Height Map (ASTER 30m).png"));

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (heightmap.isCollisionSlope(heightmap.index(x, y))) {
					tilemap.set(x, y, TileType.SLOPE);
				}
			}
		}

		final World world = World.create(tilemap, heightmap);
		final WorldCollision collision = WorldCollision.create(world);
		final WorldObjects objects = WorldObjects.create(world, collision);
		final WorldNavigation navigation = WorldNavigation.create(world, collision);

		final Game game = new Game(new Clock(), world, collision, objects, navigation);

//		try {
//			new ObjectOutputStream(new GZIPOutputStream(new FileOutputStream(new File("game.save")))).writeObject(game);
//		} catch (final IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		return game;
	}
}
