package de.hetzge.sgamelwjgl.entity;

import de.hetzge.sgamelwjgl.base.WorldObject;
import de.hetzge.sgamelwjgl.entity.ObjectWorld.Buildings;
import de.hetzge.sgamelwjgl.entity.ObjectWorld.Persons;

public final class WorldObjects {

	private final World world;
	private final WorldCollision collision;

	private WorldObjects(World world, WorldCollision collision) {
		this.world = world;
		this.collision = collision;
	}

	public void buildBuilding(int x, int y, BuildingType buildingType, int playerId) {
		final Buildings buildings = this.world.objectWorld.buildings;
		if (canBuildBuilding(x, y, buildingType, playerId)) {
			final int width = buildingType.getWidth();
			final int height = buildingType.getHeight();
			final float averageHeight = this.world.heightmap.getAverageHeight(x, y, width, height);
			final int id = buildings.getNextBuildingId();
			final Building building = new Building(buildingType, id, x, y, averageHeight);
			buildings.register(building);
			buildings.set(building);
			this.collision.update(x, y);
		} else {
			System.out.println(String.format("Can't build building with type '%s' at %s/%s for player %s", buildingType.name(), x, y, playerId));
		}
	}

	public boolean canBuildBuilding(int x, int y, BuildingType buildingType, int player) {
		final int width = buildingType.getWidth();
		final int height = buildingType.getHeight();
		return this.world.tileGrid.allMatchSize(x - 1, y - 1, width + 2, height + 2, (bx, by) -> {
			return !this.collision.isCollision(bx, by) && !this.collision.isTemporaryCollision(bx, by) && this.world.ownerWorld.isOwner(bx, by, player) && !this.world.objectWorld.buildings.isAny(x, y, width, height);
		}) && this.world.heightmap.canGrade(x, y, width, height);
	}

	public void destroyBuilding(int id) {
		final Buildings buildings = this.world.objectWorld.buildings;
		final Building building = buildings.getById(id);
		if (building != null) {
			buildings.unregister(building);
			buildings.unset(building);
			this.world.tileGrid.forEachSize(building.getX(), building.getY(), building.getWidth(), building.getHeight(), (x, y) -> {
				this.collision.update(x, y);
			});
		}
	}

	public Person buildPerson(int x, int y, PersonType personType, int playerId) {
		final Persons persons = this.world.objectWorld.persons;
		if (canBuildPerson(x, y)) {
			final int id = persons.getNextPersonId();
			final Person person = new Person(personType, id, playerId, x, y);
			persons.register(person);
			persons.set(person);
			return person;
		} else {
			System.out.println(String.format("Can't build person with type '%s' at %s/%s for player %s", personType.name(), x, y, playerId));
			return null;
		}
	}

	public boolean canBuildPerson(int x, int y) {
		return !this.collision.isCollision(x, y);
	}

	public void destroyPerson(int id) {
		final Persons persons = this.world.objectWorld.persons;
		final WorldObject object = persons.getById(id);
		if (object instanceof Person) {
			final Person person = (Person) object;
			persons.unregister(person);
			persons.unset(person);
			person.destroy();
		}
	}

	public void buildWay(int fromX, int fromY, int toX, int toY, WayType wayType, int player) {
		this.world.tileGrid.forEach(fromX, fromY, toX, toY, (x, y) -> {
			buildWay(x, y, wayType, player);
		});
	}

	public void buildWay(int x, int y, WayType wayType, int player) {
		if (canBuildWay(x, y, player)) {
			makeFree(x, y);
			this.world.wayWorld.set(x, y, wayType);
			this.collision.update(x, y);
		}
	}

	public boolean canBuildWay(int x, int y, int player) {
		final boolean isOwned = this.world.ownerWorld.isOwner(x, y, player);
		final boolean isNoCollision = !this.collision.isCollision(x, y);
		System.out.println(isOwned + " " + isNoCollision);
		return isOwned && isNoCollision;
	}

	public void destroyWay(int fromX, int fromY, int toX, int toY, int player) {
		this.world.tileGrid.forEach(fromX, fromY, toX, toY, (x, y) -> {
			destroyWay(x, y, player);
		});
	}

	public void destroyWay(int x, int y, int player) {
		final boolean isOwned = this.world.ownerWorld.isOwner(x, y, player);
		if (isOwned) {
			this.world.wayWorld.unset(x, y);
			this.collision.update(x, y);
		}
	}

	public void buildResource(int fromX, int fromY, int toX, int toY, ResourceType resourceType) {
		this.world.tileGrid.forEach(fromX, fromY, toX, toY, (x, y) -> {
			buildResource(x, y, resourceType);
		});
	}

	public void buildResource(int x, int y, ResourceType resourceType) {
		if (canBuildResource(x, y)) {
			makeFree(x, y);
			this.world.resourceWorld.set(x, y, resourceType, (byte) 1);
			this.collision.update(x, y);
		}
	}

	public boolean canBuildResource(int x, int y) {
		return !this.collision.isCollision(x, y) && !this.collision.isTemporaryCollision(x, y);
	}

	public void destroyResource(int fromX, int fromY, int toX, int toY) {
		this.world.tileGrid.forEach(fromX, fromY, toX, toY, (x, y) -> {
			destroyResource(x, y);
		});
	}

	public void destroyResource(int x, int y) {
		this.world.resourceWorld.unset(x, y);
		this.collision.update(x, y);
	}

	public boolean takeResource(int x, int y) {
		final boolean success = this.world.resourceWorld.reduce(x, y);
		this.collision.update(x, y);
		return success;
	}

	private void makeFree(int x, int y) {
		this.world.resourceWorld.unset(x, y);
		this.world.wayWorld.unset(x, y);
		this.collision.update(x, y);

		// TODO destroy
	}

	public static WorldObjects create(World world, WorldCollision collision) {
		return new WorldObjects(world, collision);
	}
}
