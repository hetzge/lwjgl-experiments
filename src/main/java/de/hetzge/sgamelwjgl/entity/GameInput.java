package de.hetzge.sgamelwjgl.entity;

import java.util.Iterator;
import java.util.List;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.lwjgl.glfw.GLFW;

import com.badlogic.gdx.graphics.Color;

import de.hetzge.sgamelwjgl.Assets;
import de.hetzge.sgamelwjgl.Operations;
import de.hetzge.sgamelwjgl.base.Colors;
import de.hetzge.sgamelwjgl.base.Path;
import de.hetzge.sgamelwjgl.graphics.base.Camera;
import de.hetzge.sgamelwjgl.graphics.base.Camera.Viewport;
import de.hetzge.sgamelwjgl.graphics.base.GeometryBuffer;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.graphics.base.Window.WindowInput;
import de.hetzge.sgamelwjgl.tilemap.Heightmap;
import de.hetzge.sgamelwjgl.tilemap.TileMapSelection;
import de.hetzge.sgamelwjgl.tilemap.TileMapSelection.MapSelection;
import de.hetzge.sgamelwjgl.tilemap.TilemapOverlay;

public final class GameInput {

	private final WindowInput windowInput;
	private final Game game;
	private final Operations operations;
	private final Camera camera;

	private final Vector2f mousePosition;
	private final Vector2f worldMousePosition;
	private final TileMapSelection tileMapSelection;
	private MapSelection selection;
	private Vector2f mouseDownWorldPosition;
	private boolean leftMouseDown;
	private boolean rightMouseDown;
	private InputMode inputMode;

	private GameInput(WindowInput windowInput, Game game, Camera camera, TileMapSelection tileMapSelection) {
		this.windowInput = windowInput;
		this.game = game;
		this.operations = new GameOperations(game);
		this.camera = camera;
		this.mousePosition = new Vector2f();
		this.worldMousePosition = new Vector2f();
		this.tileMapSelection = tileMapSelection;
		this.selection = null;
		this.mouseDownWorldPosition = null;
		this.leftMouseDown = false;
		this.rightMouseDown = false;
		enableControlPersonsInputMode();
	}

	public void input() {
		this.mousePosition.set(this.windowInput.getMousePosition());
		this.worldMousePosition.set(this.camera.screenToWorldPosition(this.mousePosition));

		final int mouseTileX = getMouseTileX();
		final int mouseTileY = getMouseTileY();

		if (this.selection != null) {
			this.selection.unselect();
		}
		this.selection = this.tileMapSelection.select(mouseTileX, mouseTileY, 0);

		if (this.windowInput.isMouseLeftClicked() || this.windowInput.isMouseRightClicked()) {
			if (this.mouseDownWorldPosition == null) {
				this.mouseDownWorldPosition = this.camera.screenToWorldPosition(new Vector2f(this.mousePosition));
				this.leftMouseDown = this.windowInput.isMouseLeftClicked();
				this.rightMouseDown = this.windowInput.isMouseRightClicked();
			} else {
				final int mouseDownTileX = this.game.pixelGrid.toTile(this.mouseDownWorldPosition.x);
				final int mouseDownTileY = this.game.pixelGrid.toTile(this.mouseDownWorldPosition.y);

				if (this.selection != null) {
					this.selection.unselect();
				}
				this.selection = this.tileMapSelection.select(mouseTileX, mouseTileY, mouseDownTileX, mouseDownTileY, 0);
			}
		} else {
			if (this.mouseDownWorldPosition != null) {
				final int mouseDownTileX = this.game.pixelGrid.toTile(this.mouseDownWorldPosition.x);
				final int mouseDownTileY = this.game.pixelGrid.toTile(this.mouseDownWorldPosition.y);

				this.inputMode.mouseInput(this.leftMouseDown, this.rightMouseDown, mouseDownTileX, mouseDownTileY, mouseTileX, mouseTileY);
			}
			this.mouseDownWorldPosition = null;
			this.leftMouseDown = false;
			this.rightMouseDown = false;
		}

		if (this.windowInput.isKeyPressed(GLFW.GLFW_KEY_UP)) {
			this.camera.move(0.0f, -1.0f);
		}
		if (this.windowInput.isKeyPressed(GLFW.GLFW_KEY_DOWN)) {
			this.camera.move(0.0f, 1.0f);
		}
		if (this.windowInput.isKeyPressed(GLFW.GLFW_KEY_LEFT)) {
			this.camera.move(-1.0f, 0.0f);
		}
		if (this.windowInput.isKeyPressed(GLFW.GLFW_KEY_RIGHT)) {
			this.camera.move(1.0f, 0.0f);
		}
		if (this.windowInput.isKeyPressed(GLFW.GLFW_KEY_ESCAPE)) {
			enableControlPersonsInputMode();
		}
	}

	public int getMouseTileX() {
		return this.game.pixelGrid.toTile(this.worldMousePosition.x);
	}

	public int getMouseTileY() {
		return this.game.pixelGrid.toTile(this.worldMousePosition.y);
	}

	public void render(SpriteBatch batch, GeometryBuffer buffer) {
		final Viewport viewport = this.camera.getViewport();
		this.tileMapSelection.render(viewport);

		this.inputMode.render(batch, buffer);

		Assets.INSTANCE.dotRegion.renderLine(buffer, 0f, 0f, 32f, 32f, Colors.colorFloatBits[0]);
		Assets.INSTANCE.dotRegion.renderLine(buffer, 0f, 0f, 32f, 16f, Colors.colorFloatBits[1]);
		Assets.INSTANCE.dotRegion.renderLine(buffer, 0f, 0f, 32f, 0f, Colors.colorFloatBits[2]);
		Assets.INSTANCE.dotRegion.renderLine(buffer, 0f, 0f, 32f, -16f, Colors.colorFloatBits[3]);
		Assets.INSTANCE.dotRegion.renderLine(buffer, 0f, 0f, 32f, -32f, Colors.colorFloatBits[4]);

	}

	private void onScroll(float offset) {
		this.camera.zoom(offset);
	}

	public void enableBuildBuildingInputMode(BuildingType buildingType) {
		enableInputMode(new BuildBuildingInputMode(buildingType));
	}

	public void enableBuildPersonInputMode(PersonType personType) {
		enableInputMode(new BuildPersonInputMode(personType));
	}

	public void enableBuildResourceInputMode(ResourceType resourceType) {
		enableInputMode(new BuildResourceInputMode(resourceType));
	}

	public void enableBuildWayInputMode(WayType wayType) {
		enableInputMode(new BuildWayInputMode(wayType));
	}

	public void enableControlPersonsInputMode() {
		enableInputMode(new ControlPersonsInputMode(List.of()));
	}

	private void enableInputMode(InputMode inputMode) {
		if (this.inputMode != null) {
			this.inputMode.disable();
		}
		this.inputMode = inputMode;
		this.inputMode.enable();
	}

	public Vector2f getMousePosition() {
		return this.mousePosition;
	}

	public Vector2f getWorldMousePosition() {
		return this.worldMousePosition;
	}

	public static GameInput create(WindowInput windowInput, Camera camera, Game game) {
		final TileMapSelection tileMapSelection = TileMapSelection.create(game.world.tileGrid, Assets.INSTANCE.mainTexture);
		final GameInput input = new GameInput(windowInput, game, camera, tileMapSelection);
		windowInput.setScrollCallback((window, xoffset, yoffset) -> input.onScroll((float) yoffset));
		return input;
	}

	private interface InputMode {
		void mouseInput(boolean leftButton, boolean rightButton, int mouseDownTileX, int mouseDownTileY, int mouseUpTileX, int mouseUpTileY);

		default void enable() {
		}

		default void disable() {
		}

		default void render(SpriteBatch batch, GeometryBuffer buffer) {
		}
	}

	private class BuildBuildingInputMode implements InputMode {

		private final BuildingType buildingType;
		private final Building previewBuilding;

		public BuildBuildingInputMode(BuildingType buildingType) {
			this.buildingType = buildingType;
			this.previewBuilding = new Building(buildingType, -1, 0, 0, 0.0f);
			this.previewBuilding.setDone(1.0f);
		}

		@Override
		public void mouseInput(boolean leftButton, boolean rightButton, int mouseDownTileX, int mouseDownTileY, int mouseUpTileX, int mouseUpTileY) {
			if (leftButton) {
				GameInput.this.operations.buildBuilding(mouseUpTileX, mouseUpTileY, this.buildingType.ordinal(), 0);
			}
		}

		@Override
		public void enable() {
			InputMode.super.enable();
			updateColorOverlay(GameInput.this.camera.getViewport(), 150);
		}

		@Override
		public void disable() {
			GameInput.this.game.world.getColorOverlay().disable();
		}

		@Override
		public void render(SpriteBatch batch, GeometryBuffer buffer) {
			if (GameInput.this.game.clock.isXFrame(30)) {
				updateColorOverlay(GameInput.this.camera.getViewport(), 30);
			}

			renderPreviewBuilding(batch, buffer);
		}

		private void renderPreviewBuilding(SpriteBatch batch, GeometryBuffer buffer) {
			final int mouseTileX = getMouseTileX();
			final int mouseTileY = getMouseTileY();
			final float averageHeight = GameInput.this.game.world.heightmap.getAverageHeight(mouseTileX, mouseTileY, this.buildingType.getWidth(), this.buildingType.getHeight());

			final boolean canBuild = GameInput.this.game.objects.canBuildBuilding(mouseTileX, mouseTileY, this.buildingType, 0);
			final float buildingWidth = this.previewBuilding.getWidth() * 32f;
			final float buildingheight = this.previewBuilding.getHeight() * 32f;
			Assets.INSTANCE.dotRegion.render(buffer, (mouseTileX * 32f) + (buildingWidth / 2f), (-mouseTileY * 32f) + (averageHeight * 32f), 2f, buildingWidth, -buildingheight, (canBuild ? Color.GREEN : Color.RED).toFloatBits());

			this.previewBuilding.setX(mouseTileX);
			this.previewBuilding.setY(mouseTileY);
			this.previewBuilding.setGroundHeight(averageHeight);
			this.previewBuilding.render(batch);
		}

		private void updateColorOverlay(Viewport viewport, int offset) {
			final TilemapOverlay colorOverlay = GameInput.this.game.world.getColorOverlay();
			colorOverlay.enable();

			final int fromX = viewport.getFromX() - offset;
			final int fromY = viewport.getFromY() - offset;
			final int toX = viewport.getToX() + offset;
			final int toY = viewport.getToY() + offset;

			final Color color = new Color();

			// y from south to north to prevent overriding already evaluated areas
			for (int y = toY; y >= fromY; y--) {
				for (int x = toX; x >= fromX; x--) {
					if (GameInput.this.game.world.tileGrid.isValid(x, y)) {
						if (GameInput.this.game.objects.canBuildBuilding(x, y, this.buildingType, 0)) {
							for (int by = 0; by < this.buildingType.getHeight(); by++) {
								for (int bx = 0; bx < this.buildingType.getWidth(); bx++) {
									final float slope = GameInput.this.game.world.heightmap.getTileSlope(GameInput.this.game.world.heightmap.index(x + bx, y + by));
									final float slopeOffset = (slope / Heightmap.COLLISION_SLOPE_LIMIT) * 0.4f;

									color.r = 0.6f;
									color.g = 1.0f - slopeOffset;
									color.b = 0.6f;
									color.a = 1.0f;
									colorOverlay.set(x + bx, y + by, color);
								}
							}
						} else {

							color.r = 1.0f;
							color.g = 0.8f;
							color.b = 0.8f;
							color.a = 1.0f;
							colorOverlay.set(x, y, color);
						}
					}
				}
			}
		}
	}

	private class BuildPersonInputMode implements InputMode {

		private final PersonType personType;

		public BuildPersonInputMode(PersonType personType) {
			this.personType = personType;
		}

		@Override
		public void mouseInput(boolean leftButton, boolean rightButton, int mouseDownTileX, int mouseDownTileY, int mouseUpTileX, int mouseUpTileY) {
			if (leftButton) {
				GameInput.this.operations.buildPerson(mouseUpTileX, mouseUpTileY, this.personType.ordinal(), 0);
			}
		}
	}

	private class BuildResourceInputMode implements InputMode {

		private final ResourceType resourceType;

		public BuildResourceInputMode(ResourceType resourceType) {
			this.resourceType = resourceType;
		}

		@Override
		public void mouseInput(boolean leftButton, boolean rightButton, int mouseDownTileX, int mouseDownTileY, int mouseUpTileX, int mouseUpTileY) {
			if (leftButton) {
				GameInput.this.operations.buildResource(mouseDownTileX, mouseDownTileY, mouseUpTileX, mouseUpTileY, this.resourceType.ordinal());
			}
		}
	}

	private class BuildWayInputMode implements InputMode {

		private final WayType wayType;

		public BuildWayInputMode(WayType wayType) {
			this.wayType = wayType;
		}

		@Override
		public void mouseInput(boolean leftButton, boolean rightButton, int mouseDownTileX, int mouseDownTileY, int mouseUpTileX, int mouseUpTileY) {
			if (leftButton) {
				GameInput.this.operations.buildWay(mouseDownTileX, mouseDownTileY, mouseUpTileX, mouseUpTileY, this.wayType.ordinal(), 0);
			}
		}
	}

	private class ControlPersonsInputMode implements InputMode {

		private final List<Person> selectedPersons;

		public ControlPersonsInputMode(List<Person> selectedPersons) {
			this.selectedPersons = selectedPersons;
		}

		@Override
		public void mouseInput(boolean leftButton, boolean rightButton, int mouseDownTileX, int mouseDownTileY, int mouseUpTileX, int mouseUpTileY) {
			if (leftButton) {
				enableInputMode(new ControlPersonsInputMode(GameInput.this.game.world.objectWorld.persons.get(mouseDownTileX, mouseDownTileY, mouseUpTileX, mouseUpTileY)));
			} else if (rightButton) {
				GameInput.this.operations.sendPersonsTo(this.selectedPersons.stream().mapToInt(Person::getId).toArray(), mouseUpTileX, mouseUpTileY, 0);
			}
		}

		@Override
		public void render(SpriteBatch batch, GeometryBuffer buffer) {
			final Viewport viewport = GameInput.this.camera.getViewport();

			for (final Person person : this.selectedPersons) {
				renderPath(buffer, person);

				if (viewport.contains(person.getX(), person.getY(), 1)) {
					person.renderSelection(buffer);
				}
			}
		}

		private void renderPath(GeometryBuffer buffer, Person person) {
			final Path path = person.getPath();
			if (path != null) {
				final Iterator<Vector2i> pathIterator = path.getWaypoints().iterator();
				Vector2i last = new Vector2i(person.getX(), person.getY());
				while (pathIterator.hasNext()) {
					final Vector2i current = pathIterator.next();

					final float fromHeightOffset = GameInput.this.game.world.heightmap.getTileHeight(GameInput.this.game.world.heightmap.index(last.x, last.y)) * 32f;
					final float toHeightOffset = GameInput.this.game.world.heightmap.getTileHeight(GameInput.this.game.world.heightmap.index(current.x, current.y)) * 32f;
					Assets.INSTANCE.dotRegion.renderLine(buffer, (last.x * 32f) + 16f, ((-last.y * 32f) - 16f) + fromHeightOffset, (current.x * 32f) + 16f, ((-current.y * 32f) - 16f) + toHeightOffset, Colors.colorFloatBits[0]);

					last = current;
				}
			}
		}
	}
}
