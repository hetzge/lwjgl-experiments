package de.hetzge.sgamelwjgl.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.joml.Vector2i;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.IntSet;

import de.hetzge.sgamelwjgl.Assets;
import de.hetzge.sgamelwjgl.Clock;
import de.hetzge.sgamelwjgl.Clock.Callback;
import de.hetzge.sgamelwjgl.Clock.CallbackReference;
import de.hetzge.sgamelwjgl.Utils;
import de.hetzge.sgamelwjgl.base.Colors;
import de.hetzge.sgamelwjgl.base.Orientation;
import de.hetzge.sgamelwjgl.base.Path;
import de.hetzge.sgamelwjgl.base.WorldObject;
import de.hetzge.sgamelwjgl.entity.ObjectWorld.Persons;
import de.hetzge.sgamelwjgl.graphics.base.GeometryBuffer;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.graphics.base.TextureRegion;

public final class Person implements WorldObject, Serializable, Callback {

	private final PersonType personType;
	private final int id;
	private final int playerId;

	private int lastX;
	private int lastY;
	private int x;
	private int y;
	private transient float renderX;
	private transient float renderY;
	private Orientation orientation;
	private Path path;
	private Building building;
	private PersonState state;
	private long startFrameId;
	private long targetFrameId;
	private final Stock stock;
	private CallbackReference callbackReference;

	public Person(PersonType personType, int id, int playerId, int x, int y) {
		this.personType = personType;
		this.id = id;
		this.playerId = playerId;
		this.state = PersonState.IDLE;
		this.orientation = Orientation.SOUTH;
		this.x = x;
		this.y = y;
		this.lastX = x;
		this.lastY = y;
		this.stock = new Stock(this);
	}

	public void render(long currentFrameId, float heightOffset, SpriteBatch batch) {
		final float fx;
		final float fy;
		int frame = 0;
		if (((this.state == PersonState.WALK) || (this.state == PersonState.CARRY)) && (currentFrameId >= this.startFrameId) && (currentFrameId < this.targetFrameId)) {
			frame = (int) (currentFrameId - this.startFrameId) % Integer.MAX_VALUE;
			final float percentage = MathUtils.clamp((float) (currentFrameId - this.startFrameId) / (this.targetFrameId - this.startFrameId), 0.0f, 1.0f);

			if ((this.lastX != this.x) || (this.lastY != this.y)) {
				fx = (this.lastX + ((this.x - this.lastX) * percentage)) * 32f;
				fy = (this.lastY + ((this.y - this.lastY) * percentage)) * 32f;
			} else {
				fx = this.x * 32f;
				fy = this.y * 32f;
			}
		} else {
			fx = this.x * 32f;
			fy = this.y * 32f;
		}

		this.renderX = fx + 16f;
		this.renderY = (-fy + heightOffset) - 16f;

		final TextureRegion frameTextureRegion = Assets.INSTANCE.person.get(this.personType, this.state, this.orientation).getFrame(frame);
		batch.add(frameTextureRegion, this.renderX, this.renderY, 0f, frameTextureRegion.getOriginalWidth(), frameTextureRegion.getOriginalHeight(), Colors.colorFloatBits[this.playerId]);
	}

	public void renderSelection(GeometryBuffer buffer) {
		Assets.INSTANCE.dotRegion.renderLine(buffer, this.renderX - 8f, this.renderY + 3f, this.renderX - 8f, this.renderY - 2f, Colors.colorFloatBits[this.playerId]);
		Assets.INSTANCE.dotRegion.renderLine(buffer, this.renderX + 8f, this.renderY + 3f, this.renderX + 8f, this.renderY - 2f, Colors.colorFloatBits[this.playerId]);
		Assets.INSTANCE.dotRegion.renderLine(buffer, this.renderX - 8f, this.renderY - 2f, this.renderX + 8f, this.renderY - 2f, Colors.colorFloatBits[this.playerId]);
	}

	public void destroy() {
		if (this.callbackReference != null) {
			this.callbackReference.cancel();
		}
	}

	@Override
	public void update(Game game) {
		final CallbackReference callbackBefore = this.callbackReference;
		final int index = game.world.tileGrid.index(this.x, this.y);

		// TODO isInBuilding ?!
		decision: if (!isInBuilding()) {
			if (hasPath()) {
				walk(game);
			} else {
				final boolean boundToOwnLand = isBoundToOwnLand();
				if (boundToOwnLand && !game.world.ownerWorld.isOwner(this.x, this.y, this.playerId)) {
					final Path path = game.navigation.pathToOwnLand(this.x, this.y, this.playerId);
					if (path != null) {
						setPath(path);
						walk(game);
						break decision;
					}
				}

				if (isUseWays(game) && !game.world.wayWorld.is(this.x, this.y)) {
					final Path path = game.navigation.pathToWay(this.x, this.y, this.playerId, boundToOwnLand);
					if (path != null) {
						setPath(path);
						walk(game);
						break decision;
					}
				}

				if ((this.state == PersonState.IDLE) && game.world.objectWorld.persons.isOtherThen(index, this)) {
					// STEP AWAY
					goAway2(game, new IntSet());
					if (this.path != null) {
						walk(game);
					}
					break decision;
				}

				if (this.building != null) {
					final BuildingType buildingType = this.building.getBuildingType();
					final int doorX = this.building.getDoorX();
					final int doorY = this.building.getDoorY();
					final float groundHeight = this.building.getGroundHeight();
					final int buildingX = this.building.getX();
					final int buildingY = this.building.getY();
					final int buildingWidth = this.building.getWidth();
					final int buildingHeight = this.building.getHeight();

					if ((this.personType == PersonType.WOODWORKER) && (buildingType == BuildingType.LUMBERJACK)) {
						// WOODWORKER
						if (this.stock.isNotEmpty()) {
							if (isAtDoor()) {
								// DROP ALL ITEMS
								for (final Item item : this.stock.getItems()) {
									item.supply(this.building.getStock());
								}
							} else {
								goHome(game);
							}
						} else {
							final int resourceX = this.x + this.orientation.x;
							final int resourceY = this.y + this.orientation.y;
							final boolean isForestInOrientation = game.world.resourceWorld.isResourceType(resourceX, resourceY, ResourceType.FOREST);
							if ((this.state == PersonState.WORK) && isForestInOrientation) {
								// TAKE ITEM
								if (game.objects.takeResource(resourceX, resourceY)) {
									Item.create(ItemType.WOOD).supplied(this.stock, this.building.getStock());
								}
							} else {
								final Vector2i forestAroundPosition = game.world.resourceWorld.findAround(this.x, this.y, ResourceType.FOREST);
								if (forestAroundPosition != null) {
									// WORK
									final Orientation orientation = Orientation.lookAt(this.x, this.y, forestAroundPosition.x, forestAroundPosition.y);
									setAction(game.clock, PersonState.WORK, 60, orientation);
								} else {
									// GOTO FOREST
									final Path path = game.navigation.pathToResource(this.x, this.y, boundToOwnLand, ResourceType.FOREST);
									if (path != null) {
										setPath(path);
										walk(game);
									} else {
										goHome(game);
									}
								}
							}
						}

					} else {
						if (this.personType == PersonType.GRADER) {
							if (isAtBuilding()) {
								// SEARCH TILE TO GRADE
								final Vector2i positionToGrade = game.world.tileGrid.findPositionSize(buildingX, buildingY, buildingWidth, buildingHeight, (fx, fy) -> {
									return !game.world.heightmap.isHeight(game.world.heightmap.index(fx, fy), groundHeight) && !game.world.objectWorld.persons.isOtherThen(game.world.objectWorld.index(fx, fy), this);
								});
								if (positionToGrade != null) {
									System.out.println("A");

									System.out.println(String.format("%s, %s, %s, %s", this.x, positionToGrade.x, this.y, positionToGrade.y));
									if ((this.x == positionToGrade.x) && (this.y == positionToGrade.y)) {
										System.out.println("AA");
										if (this.state == PersonState.WORK) {
											System.out.println("AAA");
											// GRADE
											game.world.heightmap.gradeTileStep(index, groundHeight);
										} else {
											System.out.println("AAB");
											// WORK
											setAction(game.clock, PersonState.WORK, 60, this.orientation);
										}
									} else {
										System.out.println("AB");
										final Path path = game.navigation.path(this.x, this.y, positionToGrade.x, positionToGrade.y, boundToOwnLand, false, this.playerId);
										if (path != null) {
											System.out.println("ABA");
											System.out.println(path);
											setPath(path);
											walk(game);
										} else {
											System.out.println("ABB");
											// GO TO DOOR (AWAY)
											final Path pathToDoor = game.navigation.path(this.x, this.y, doorX, doorY, boundToOwnLand, false, this.playerId);
											if (pathToDoor != null) {
												setPath(pathToDoor);
												walk(game);
												unsetBuilding();
											} else {
												throw new IllegalStateException();
											}
										}
									}
								} else {
									System.out.println("B");

									// GO TO DOOR (AWAY)
									final Path pathToDoor = game.navigation.path(this.x, this.y, doorX, doorY, boundToOwnLand, false, this.playerId);
									if (pathToDoor != null) {
										setPath(pathToDoor);
										walk(game);
										unsetBuilding();
									} else {
										throw new IllegalStateException();
									}
								}

							} else {
								// GOTO BUILDING
								final Path path = game.navigation.path(this.x, this.y, doorX, doorY, boundToOwnLand, false, this.playerId);
								if (path != null) {
									setPath(path);
									walk(game);
								} else {
									unsetBuilding();
								}
							}
						} else if (this.personType == PersonType.BUILDER) {

							final Vector2i buildingAroundPosition = getBuildingAroundPosition(game);
							if (buildingAroundPosition != null) {

								if (game.world.objectWorld.persons.isOtherThen(index, this)) {
									// SEARCH FREE SPOT
									final Vector2i aroundPosition = game.world.tileGrid.findPositionAroundSize(buildingX, buildingY, buildingWidth, buildingHeight, (ax, ay) -> {
										final boolean isAroundBuilding = getBuildingAroundPosition(game, ax, ay) != null;
										final boolean isReachable = game.navigation.isPathPossible(this.x, this.y, ax, ay, isUseWays(game), boundToOwnLand);
										final boolean isFree = !game.world.objectWorld.isTemporaryCollision(game.world.tileGrid.index(ax, ay));

										return isAroundBuilding && isReachable && isFree;
									});
									if (aroundPosition != null) {
										final Path path = game.navigation.path(this.x, this.y, aroundPosition.x, aroundPosition.y, boundToOwnLand, isUseWays(game), this.playerId);
										if (path != null) {
											setPath(path);
											walk(game);
										}
									}
								} else {
									if (this.state == PersonState.WORK) {
										// BUILD
										this.building.buildStep();
										if (this.building.isDone()) {
											for (final Person buildingPerson : new ArrayList<>(this.building.getPersons())) {
												buildingPerson.unsetBuilding();
											}
										}
									} else {
										// WORK
										final Orientation orientation = Orientation.lookAt(this.x, this.y, buildingAroundPosition.x, buildingAroundPosition.y);
										setAction(game.clock, PersonState.WORK, 60, orientation);
									}
								}
							} else {
								System.out.println("go home");
								goHome(game);
							}
						} else {
							if (isAtDoor()) {
								game.world.objectWorld.persons.unset(this);
								this.x = buildingX;
								this.y = buildingY;
								// TODO enter building
							} else {
								goHome(game);
							}
						}
					}
				} else {
					// Has no building

					if (this.personType == PersonType.WOODWORKER) {
						this.building = searchBuilding(game, BuildingType.LUMBERJACK, true, true, true, true);
					} else if (this.personType == PersonType.GRADER) {
						final Building foundBuilding = searchBuilding(game, null, false, false, true, true);
						if (foundBuilding != null) {
							setBuilding(foundBuilding);
						}
					} else if (this.personType == PersonType.BUILDER) {
						final Building foundBuilding = searchBuilding(game, null, true, false, true, true);
						if (foundBuilding != null) {
							setBuilding(foundBuilding);
						}
					} else {
						this.building = searchBuilding(game, BuildingType.MAIN, true, true, true, false);
					}
				}
			}
		} else {
			// in building
			// TODO
		}

		if (this.callbackReference == callbackBefore) {
			idle(game);
		}
	}

	private void setBuilding(Building building) {
		this.building = building;
		this.building.addPerson(this);
	}

	private void unsetBuilding() {
		this.building.removePerson(this);
		this.building = null;
	}

	private void goHome(Game game) {
		if (this.building == null) {
			throw new IllegalStateException("No building to go to");
		}

		final Path path = game.navigation.path(this, this.building.getDoorX(), this.building.getDoorY(), game.getPlayer(this.playerId));
		if (path != null) {
			setPath(path);
			walk(game);
		} else {
			this.building = null;
		}
	}

	private boolean isAtDoor() {
		return (this.building != null) && (this.x == this.building.getDoorX()) && (this.y == this.building.getDoorY());
	}

	private boolean isAtBuilding() {
		return (this.building != null) && this.building.contains(this.x, this.y);
	}

	private Vector2i getBuildingAroundPosition(Game game) {
		return getBuildingAroundPosition(game, this.x, this.y);
	}

	private Vector2i getBuildingAroundPosition(Game game, int x, int y) {
		if (this.building != null) {
			return game.world.tileGrid.findPositionAround(x, y, (ax, ay) -> {
				return Objects.equals(game.world.objectWorld.buildings.get(game.world.tileGrid.index(ax, ay)), this.building);
			});
		} else {
			return null;
		}
	}

	private Building searchBuilding(Game game, BuildingType buildingType, boolean graded, boolean done, boolean sortByDistance, boolean sortByPersonCount) {

		final List<Building> buildings = game.world.objectWorld.buildings.find(building -> {
			final boolean isBuildingType = (buildingType == null) || (building.getBuildingType() == buildingType);
			final boolean isOwnedByPlayer = game.world.ownerWorld.isOwner(building.getX(), building.getY(), this.playerId);
			final boolean isReachable = game.navigation.isPathPossible(this.x, this.y, building.getDoorX(), building.getDoorY(), isUseWays(game), isBoundToOwnLand());
			final boolean isGraded = (graded && isGraded(game, building)) || (!graded && !isGraded(game, building));
			final boolean isDone = (done && building.isDone()) || (!done && building.isNotDone());
			final boolean isNotFull = !building.isMaxPersonCount();

			return isOwnedByPlayer && isBuildingType && isReachable && isGraded && isDone && isNotFull;
		}, (a, b) -> {
			final int distanceOrder = sortByDistance ? Utils.compareDistance(this, a, b) : 0;
			final int personCountOrder = sortByPersonCount ? Integer.compare(a.getPersonsCount(), b.getPersonsCount()) : 0;

			return distanceOrder != 0 ? distanceOrder : personCountOrder;
		});

		return !buildings.isEmpty() ? buildings.get(0) : null;
	}

	private boolean isGraded(Game game, Building building) {
		return game.world.heightmap.isHeight(building.getX(), building.getY(), building.getWidth(), building.getHeight(), building.getGroundHeight());
	}

	private boolean isUseWays(Game game) {
		return game.getPlayer(this.playerId).isUseWays();
	}

	public boolean isBoundToOwnLand() {
		return this.personType.isBoundToOwnLand();
	}

	private void walk(Game game) {
		if (hasPath()) {
			final Vector2i next = this.path.peek();
			if (game.objects.canBuildPerson(next.x, next.y)) {
				nextStep(game, this.path.poll());
			} else if (game.collision.isCollision(next.x, next.y)) {
				idle(game);
			} else {
				setPath(null);
			}
		}
	}

	private void nextStep(Game game, Vector2i next) {
		if ((this.path != null) && this.path.isEmpty()) {
			this.path = null;
		}

		final Persons persons = game.world.objectWorld.persons;
		persons.unset(this);

		this.lastX = this.x;
		this.lastY = this.y;
		this.x = next.x;
		this.y = next.y;
		final PersonState state = this.stock.isEmpty() ? PersonState.WALK : PersonState.CARRY;
		final Orientation orientation = Orientation.lookAt(this.lastX, this.lastY, this.x, this.y);

		setAction(game.clock, state, 60, orientation);
		persons.set(this);

		final int index = game.world.objectWorld.index(this.x, this.y);
		final Person[] otherPersons = persons.get(index);
		if (otherPersons.length > 1) {
			otherPersonsLoop: for (final Person otherPerson : otherPersons) {
				if (!otherPerson.equals(this) && otherPerson.isDone()) {
					final IntSet blacklist = new IntSet();
					blacklist.add(index);
					otherPerson.goAway2(game, blacklist);
//					otherPerson.update(game);
					break otherPersonsLoop;
				}
			}
		}
	}

	private boolean goAway2(Game game, IntSet blacklist) {

		// First search for empty spots around ...
		for (final Orientation orientation : Orientation.VALUES) {
			final int ax = this.x + orientation.x;
			final int ay = this.y + orientation.y;
			final int aroundIndex = game.world.tileGrid.index(ax, ay);
			if (!game.world.objectWorld.persons.is(aroundIndex) && game.navigation.isPathPossible(this.x, this.y, ax, ay, isUseWays(game), isBoundToOwnLand())) {
				setPath(game.navigation.path(this, ax, ay, game.getPlayer(getPlayerId())));
				return true;
			}
		}

		// ... then try to push other persons away
		for (final Orientation orientation : Orientation.VALUES) {
			final int ax = this.x + orientation.x;
			final int ay = this.y + orientation.y;
			final int aroundIndex = game.world.tileGrid.index(ax, ay);
			if (!blacklist.contains(aroundIndex) && game.navigation.isPathPossible(this.x, this.y, ax, ay, isUseWays(game), isBoundToOwnLand())) {
				if (isGoAway(game, aroundIndex, blacklist)) {
					setPath(game.navigation.path(this, ax, ay, game.getPlayer(getPlayerId())));
					return true;
				}
			}
		}

		return false;
	}

	private boolean isGoAway(Game game, int index, IntSet blacklist) {
		blacklist.add(index);
		final Person[] aroundPersons = game.world.objectWorld.persons.get(index);
		if (aroundPersons != null) {
			for (final Person aroundPerson : aroundPersons) {
				if (aroundPerson.goAway2(game, blacklist)) {
					return true;
				}
			}
		} else {
			return true;
		}

		return false;
	}

	private void idle(Game game) {
		this.lastX = this.x;
		this.lastY = this.y;
		this.path = null;
		setAction(game.clock, PersonState.IDLE, isInBuilding() ? 240 : 60, this.orientation);
	}

	private void setAction(Clock clock, PersonState state, long duration, Orientation orientation) {
		this.state = state;
		this.startFrameId = clock.getCurrentFrame().getId();
		this.targetFrameId = this.startFrameId + duration;
		this.orientation = orientation;

		if (this.callbackReference != null) {
			this.callbackReference.cancel();
		}
		this.callbackReference = clock.addCallback(this.targetFrameId, this);
	}

	public void setPath(Path path) {
		while ((path != null) && (path.peek() != null) && (path.peek().x == this.x) && (path.peek().y == this.y)) {
			path.poll();
		}

		this.path = path;
	}

	public boolean isDone() {
		return !hasPath() && (this.state == PersonState.IDLE);
	}

	public boolean hasPath() {
		return (this.path != null) && (this.path.peek() != null);
	}

	public boolean isInBuilding() {
		return (this.building != null) && this.building.isDone() && (this.x == this.building.getX()) && (this.y == this.building.getY());
	}

	public boolean isReservedInBuilding() {
		return (this.building != null) && !isInBuilding();
	}

	@Override
	public int getX() {
		return this.x;
	}

	@Override
	public int getY() {
		return this.y;
	}

	@Override
	public int getWidth() {
		return 1;
	}

	@Override
	public int getHeight() {
		return 1;
	}

	public Orientation getOrientation() {
		return this.orientation;
	}

	public void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}

	public Building getBuilding() {
		return this.building;
	}

	public PersonState getState() {
		return this.state;
	}

	public PersonType getPersonType() {
		return this.personType;
	}

	@Override
	public int getId() {
		return this.id;
	}

	public int getPlayerId() {
		return this.playerId;
	}

	public long getStartFrameId() {
		return this.startFrameId;
	}

	public long getTargetFrameId() {
		return this.targetFrameId;
	}

	public Path getPath() {
		return this.path;
	}

	@Override
	public int hashCode() {
		return this.id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final Person other = (Person) obj;
		if (this.id != other.id) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Person [id=" + this.id + "]";
	}
}
