package de.hetzge.sgamelwjgl.entity;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.graphics.Color;

import de.hetzge.sgamelwjgl.Assets;
import de.hetzge.sgamelwjgl.base.WorldObject;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.graphics.base.TextureRegion;

public final class Building implements WorldObject, Serializable {

	private static final float BUILDING_LOT_COLOR_FLOAT_BITS = new Color(1f, 1f, 1f, 0.5f).toFloatBits();

	private final BuildingType buildingType;
	private final int id;

	private int x;
	private int y;
	private long startFrame;
	private long targetFrame;
	private BuildingState state;
	private List<Person> persons;
	private float done;
	private Stock stock;
	private float groundHeight;

	public Building(BuildingType buildingType, int id, int x, int y, float groundHeight) {
		this.buildingType = buildingType;
		this.id = id;
		this.x = x;
		this.y = y;
		this.persons = new LinkedList<>();
		this.done = 0.0f;
		this.stock = new Stock(this);
		this.groundHeight = groundHeight;
	}

	public void render(SpriteBatch batch) {
		final TextureRegion textureRegion = isDone() ? Assets.INSTANCE.building.get(this.buildingType, this.state) : Assets.INSTANCE.building.getBuildingLot(this.buildingType, this.done);
		batch.add(textureRegion, (this.x * 32f) + ((this.buildingType.getWidth() * 32f) / 2f), (-(this.y * 32f) - (this.buildingType.getHeight() * 32f)) + (this.groundHeight * 32f), 0f, this.buildingType.getWidth() * 32f, this.buildingType.getHeight() * 32f, BUILDING_LOT_COLOR_FLOAT_BITS);
	}

	@Override
	public int getX() {
		return this.x;
	}

	public void setX(int x) {
		this.x = x;
	}

	@Override
	public int getY() {
		return this.y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean isDoor(int x, int y) {
		return (x == getDoorX()) && (y == getDoorY());
	}

	public int getDoorX() {
		return this.x + this.buildingType.getDoorX();
	}

	public int getDoorY() {
		return this.y + this.buildingType.getDoorY();
	}

	@Override
	public int getWidth() {
		return this.buildingType.getWidth();
	}

	@Override
	public int getHeight() {
		return this.buildingType.getHeight();
	}

	public long getStartFrame() {
		return this.startFrame;
	}

	public void setStartFrame(long startFrame) {
		this.startFrame = startFrame;
	}

	public long getTargetFrame() {
		return this.targetFrame;
	}

	public void setTargetFrame(long targetFrame) {
		this.targetFrame = targetFrame;
	}

	public List<Person> getPersons() {
		return this.persons;
	}

	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}

	public int getPersonsCount() {
		return this.persons.size();
	}

	public void addPerson(Person person) {
		if (this.persons.contains(person)) {
			throw new IllegalStateException();
		}
		this.persons.add(person);
	}

	public void removePerson(Person person) {
		if (!this.persons.contains(person)) {
			throw new IllegalStateException();
		}
		this.persons.remove(person);
	}

	public int personIndex(Person person) {
		final int index = this.persons.indexOf(person);
		if (index == -1) {
			throw new IllegalStateException();
		}
		return index;
	}

	public int maxPersonCount() {
		// Building lot can have only the half of its tiles
		return (int) (!isDone() ? Math.max(1, (getWidth() * getHeight()) / 2f) : Integer.MAX_VALUE);
	}

	public boolean isMaxPersonCount() {
		return this.persons.size() >= maxPersonCount();
	}

	public BuildingType getBuildingType() {
		return this.buildingType;
	}

	@Override
	public int getId() {
		return this.id;
	}

	public BuildingState getState() {
		return this.state;
	}

	public void setState(BuildingState state) {
		this.state = state;
	}

	public Stock getStock() {
		return this.stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public float getDone() {
		return this.done;
	}

	public void setDone(float done) {
		this.done = done;
	}

	public boolean isDone() {
		return this.done >= 1f;
	}

	public boolean isNotDone() {
		return !isDone();
	}
	
	public boolean isUnbuild() {
		return this.done == 0.0f;
	}

	public void buildStep() {
		this.done = Math.min(1f, this.done + 0.1f);
	}

	public float getGroundHeight() {
		return this.groundHeight;
	}

	public void setGroundHeight(float groundHeight) {
		this.groundHeight = groundHeight;
	}

	@Override
	public int hashCode() {
		return this.id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final Building other = (Building) obj;
		if (this.id != other.id) {
			return false;
		}
		return true;
	}
}
