package de.hetzge.sgamelwjgl.entity;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.glEnable;

import java.io.Serializable;

import org.lwjgl.opengl.GL11;

import de.hetzge.sgamelwjgl.Clock;
import de.hetzge.sgamelwjgl.base.Orientation;
import de.hetzge.sgamelwjgl.base.Pathfinder;
import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.base.TileGridFloodFill;
import de.hetzge.sgamelwjgl.graphics.base.Camera.Viewport;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.tilemap.AutoTilemap;
import de.hetzge.sgamelwjgl.tilemap.Heightmap;
import de.hetzge.sgamelwjgl.tilemap.TilemapOverlay;

public final class World implements Serializable {

	final TileGrid tileGrid;
	final AutoTilemap<TileType> tilemap;
	final Heightmap heightmap;
	final ResourceWorld resourceWorld;
	final WayWorld wayWorld;
	final OwnerWorld ownerWorld;
	final ObjectWorld objectWorld;

	private transient TilemapOverlay colorOverlay;
	private transient TileGridFloodFill floodFill;
	private transient Pathfinder pathfinder;

	private World(TileGrid tileGrid, AutoTilemap<TileType> tilemap, Heightmap heightmap, ResourceWorld resourceWorld, WayWorld wayWorld, OwnerWorld ownerWorld, ObjectWorld objectWorld) {
		this.tileGrid = tileGrid;
		this.tilemap = tilemap;
		this.heightmap = heightmap;
		this.resourceWorld = resourceWorld;
		this.wayWorld = wayWorld;
		this.ownerWorld = ownerWorld;
		this.objectWorld = objectWorld;
	}

	public float getCollisionValue(int x, int y) {
		return this.resourceWorld.isCollision(x, y) || (this.heightmap.isCollisionSlope(this.heightmap.index(x, y))) ? 1.0f : this.heightmap.getTileSlope(this.heightmap.index(x, y)) / 2f;
	}

	public void render(Clock clock, Viewport viewport, SpriteBatch batch) {
		final TilemapOverlay colorOverlay = getColorOverlay();

		colorOverlay.render();
		this.heightmap.render();
		this.tilemap.render(viewport, true, colorOverlay.isActive());
		this.wayWorld.render(viewport);

		glEnable(GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		for (int y = viewport.getFromY(); y <= viewport.getToY(); y++) {
			for (int x = viewport.getFromX(); x <= viewport.getToX(); x++) {
				this.resourceWorld.render(x, y, this.heightmap, batch);
				this.ownerWorld.render(x, y, this.heightmap, batch);
				this.objectWorld.render(clock, x, y, this.heightmap, batch);
			}
		}
	}

	public TileGrid getTileGrid() {
		return this.tileGrid;
	}

	public int getWidth() {
		return this.tileGrid.getWidth();
	}

	public int getHeight() {
		return this.tileGrid.getHeight();
	}

	public TilemapOverlay getColorOverlay() {
		if (this.colorOverlay == null) {
			this.colorOverlay = TilemapOverlay.create(this.tileGrid);
		}
		return this.colorOverlay;
	}

	public TileGridFloodFill getFloodFill() {
		if (this.floodFill == null) {
			this.floodFill = TileGridFloodFill.create(this.tileGrid, Orientation.VALUES);
		}
		return this.floodFill;
	}

	public Pathfinder getPathfinder() {
		if (this.pathfinder == null) {
			this.pathfinder = Pathfinder.create(this.tileGrid);
		}
		return this.pathfinder;
	}

	public static World create(AutoTilemap<TileType> tilemap, Heightmap heightmap) {
		final int width = heightmap.getWidth();
		final int height = heightmap.getHeight();
		final TileGrid tileGrid = new TileGrid(width, height);
		final ResourceWorld resourceWorld = ResourceWorld.create(width, height, heightmap);
		final WayWorld wayWorld = WayWorld.create(width, height);
		final OwnerWorld ownerWorld = OwnerWorld.create(width, height);
		final ObjectWorld objectWorld = ObjectWorld.create(width, height);

		return new World(tileGrid, tilemap, heightmap, resourceWorld, wayWorld, ownerWorld, objectWorld);
	}
}
