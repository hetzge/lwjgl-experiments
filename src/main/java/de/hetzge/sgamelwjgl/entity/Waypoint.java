package de.hetzge.sgamelwjgl.entity;

import java.io.Serializable;
import java.util.List;

public final class Waypoint implements Serializable {

	private final int x;
	private final int y;
	private List<Waypoint> connections;

	public Waypoint(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public List<Waypoint> getConnections() {
		return this.connections;
	}

	public void setConnections(List<Waypoint> connections) {
		this.connections = connections;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + this.x;
		result = (prime * result) + this.y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final Waypoint other = (Waypoint) obj;
		if (this.x != other.x) {
			return false;
		}
		if (this.y != other.y) {
			return false;
		}
		return true;
	}
}
