package de.hetzge.sgamelwjgl.entity;

import java.io.Serializable;

public final class Item implements Serializable {

	private final ItemType itemType;

	private Stock sourceStock;
	private Stock targetStock;

	public Item(ItemType itemType) {
		this.itemType = itemType;
	}

	public ItemState state(Stock stock) {
		if (this.sourceStock == this.targetStock) {
			return ItemState.BLOCKED;
		} else if ((this.sourceStock == stock) && (this.targetStock == null)) {
			return ItemState.SUPPLY;
		} else if ((this.sourceStock == stock) && (this.targetStock != null)) {
			return ItemState.SUPPLIED;
		} else if ((this.sourceStock == null) && (this.targetStock == stock)) {
			return ItemState.DEMAND;
		} else if ((this.sourceStock != null) && (this.targetStock == stock)) {
			return ItemState.DEMANDED;
		} else {
			throw new IllegalStateException();
		}
	}

	public Item block(Stock stock) {
		free();
		stock.add(this);
		this.sourceStock = stock;
		this.targetStock = stock;
		return this;
	}

	public Item supply(Stock stock) {
		free();
		stock.add(this);
		this.sourceStock = stock;
		this.targetStock = null;
		return this;
	}

	public Item demand(Stock stock) {
		free();
		stock.add(this);
		this.sourceStock = null;
		this.targetStock = stock;
		return this;
	}

	public Item supplied(Stock stock, Stock targetStock) {
		free();
		stock.add(this);
		targetStock.add(this);
		this.sourceStock = stock;
		this.targetStock = targetStock;
		return this;
	}

	private void free() {
		if (this.sourceStock != null) {
			this.sourceStock.remove(this);
		}
		if (this.targetStock != null) {
			this.targetStock.remove(this);
		}
	}

	public Stock getSourceStock() {
		return this.sourceStock;
	}

	public void setSourceStock(Stock sourceStock) {
		this.sourceStock = sourceStock;
	}

	public Stock getTargetStock() {
		return this.targetStock;
	}

	public void setTargetStock(Stock targetStock) {
		this.targetStock = targetStock;
	}

	public ItemType getItemType() {
		return this.itemType;
	}
	
	public static Item create(ItemType itemType) {
		return new Item(itemType);
	}
}
