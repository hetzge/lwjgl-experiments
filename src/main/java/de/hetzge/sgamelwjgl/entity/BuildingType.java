package de.hetzge.sgamelwjgl.entity;

import java.util.List;

public enum BuildingType {

	MAIN(2, 2, 0, 1), STOCK(1, 1, 0, 0), LUMBERJACK(2, 2, 0, 2);

	public static final BuildingType[] VALUES = values();

	private final int width;
	private final int height;

	private final int doorX;
	private final int doorY;

	private final List<ItemRequirement> requirements;

	private BuildingType(int width, int height, int doorX, int doorY) {
		this.width = width;
		this.height = height;
		this.doorX = doorX;
		this.doorY = doorY;
		this.requirements = List.of(new ItemRequirement(ItemType.WOOD, 4));
	}

	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}

	public int getDoorX() {
		return this.doorX;
	}

	public int getDoorY() {
		return this.doorY;
	}

	public List<ItemRequirement> getRequiresToBuild() {
		return this.requirements;
	}

	public static class ItemRequirement {
		private final ItemType itemType;
		private final int count;

		public ItemRequirement(ItemType itemType, int count) {
			this.itemType = itemType;
			this.count = count;
		}

		public ItemType getItemType() {
			return this.itemType;
		}

		public int getCount() {
			return this.count;
		}
	}
}
