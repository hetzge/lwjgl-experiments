package de.hetzge.sgamelwjgl.entity;

import java.io.Serializable;

import de.hetzge.sgamelwjgl.Assets;
import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.graphics.base.Camera.Viewport;
import de.hetzge.sgamelwjgl.tile.AutoTilesets;
import de.hetzge.sgamelwjgl.tilemap.AutoTilemap;

public final class WayWorld implements Serializable {

	final TileGrid tileGrid;
	final Way[] ways;
	final AutoTilemap<WayTileType> tilemap;

	private WayWorld(int width, int height, Way[] ways, AutoTilemap<WayTileType> tilemap) {
		this.tileGrid = new TileGrid(width, height);
		this.ways = ways;
		this.tilemap = tilemap;
	}

	public void set(int x, int y, WayType wayType) {
		this.ways[index(x, y)] = new Way(x, y, wayType);
		this.tilemap.unset(x, y);
		this.tilemap.set(x, y, wayType.getTileType());
	}

	public void unset(int x, int y) {
		this.ways[index(x, y)] = null;
		this.tilemap.unset(x, y);
	}

	public Way get(int x, int y) {
		return this.ways[index(x, y)];
	}

	public boolean is(int x, int y) {
		return this.ways[index(x, y)] != null;
	}

	public boolean isNot(int x, int y) {
		return !is(x, y);
	}

	public int index(int x, int y) {
		return (y * this.tileGrid.getWidth()) + x;
	}

	public void render(Viewport viewport) {
		this.tilemap.render(viewport, true, false);
	}

	public int getWidth() {
		return this.tileGrid.getWidth();
	}

	public int getHeight() {
		return this.tileGrid.getHeight();
	}

	public static WayWorld create(int width, int height) {
		final Way[] ways = new Way[width * height];
		final AutoTilesets<WayTileType> tilesets = Assets.INSTANCE.wayAutoTilesets; // TODO assets are not allowed here
		final AutoTilemap<WayTileType> tilemap = AutoTilemap.create(width, height, 2, tilesets);

		return new WayWorld(width, height, ways, tilemap);
	}
}
