package de.hetzge.sgamelwjgl.entity;

import java.io.Serializable;
import java.util.Arrays;

import de.hetzge.sgamelwjgl.Assets;
import de.hetzge.sgamelwjgl.base.Colors;
import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.tilemap.Heightmap;

public final class OwnerWorld implements Serializable {

	public static final int NO_OWNER = -1;

	private final TileGrid tileGrid;
	private final int[] owners;

	private OwnerWorld(TileGrid tileGrid, int[] owners) {
		this.tileGrid = tileGrid;
		this.owners = owners;
	}

	public void setOwner(int x, int y, int owner) {
		this.owners[index(x, y)] = owner;
	}

	public int getOwner(int x, int y) {
		return this.owners[index(x, y)];
	}

	public boolean isOwner(int x, int y, int owner) {
		return getOwner(x, y) == owner;
	}

	public int getBorderOwner(int x, int y) {
		final int owner = getOwner(x, y);
		if (((owner != NO_OWNER) && (((x - 1) > 0) && (getOwner(x - 1, y) != owner))) || (((x + 1) < this.tileGrid.getWidth()) && (getOwner(x + 1, y) != owner)) || (((y - 1) > 0) && (getOwner(x, y - 1) != owner)) || (((y + 1) < this.tileGrid.getHeight()) && (getOwner(x, y + 1) != owner))) {
			return owner;
		} else {
			return NO_OWNER;
		}
	}

	public void render(int x, int y, Heightmap heightmap, SpriteBatch batch) {
		final float tx = x * 32;
		final float ty = ((-y * 32) + (heightmap.getTileHeight(heightmap.index(x, y)) * 32f)) - 32f;
		final int borderOwner = getBorderOwner(x, y);
		if (borderOwner != NO_OWNER) {
			batch.add(Assets.INSTANCE.owner.border, tx + 16, ty, 0.0f, 32f, 32f, Colors.colorFloatBits[borderOwner]);
		}
	}

	public int index(int x, int y) {
		return (y * this.tileGrid.getWidth()) + x;
	}

	public int getWidth() {
		return this.tileGrid.getWidth();
	}

	public int getHeight() {
		return this.tileGrid.getHeight();
	}

	public static OwnerWorld create(int width, int height) {
		final int[] owners = new int[width * height];
		Arrays.fill(owners, NO_OWNER);

		final TileGrid tileGrid = new TileGrid(width, height);
		final OwnerWorld ownerWorld = new OwnerWorld(tileGrid, owners);
		tileGrid.forEachSize(0, 0, 100, 100, (x, y) -> {
			ownerWorld.setOwner(x, y, 0);
		});
		

		return ownerWorld;
	}
}
