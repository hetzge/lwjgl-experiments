package de.hetzge.sgamelwjgl.entity;

public enum PersonState {
	IDLE, WALK, CARRY, WORK;
	
	public static final PersonState[] VALUES = values();
}
