package de.hetzge.sgamelwjgl.entity;

import java.util.Arrays;
import java.util.Objects;

import de.hetzge.sgamelwjgl.Operations;
import de.hetzge.sgamelwjgl.base.Path;

public final class GameOperations implements Operations {

	private final Game game;

	public GameOperations(Game game) {
		this.game = game;
	}

	@Override
	public void buildWay(int fromX, int fromY, int toX, int toY, int wayTypeOrdinal, int playerId) {
		this.game.objects.buildWay(fromX, fromY, toX, toY, WayType.VALUES[wayTypeOrdinal], playerId);
	}

	@Override
	public void destroyWay(int fromX, int fromY, int toX, int toY, int playerId) {
		this.game.objects.destroyWay(fromX, fromY, toX, toY, playerId);
	}

	@Override
	public void buildPerson(int x, int y, int personTypeOrdinal, int playerId) {
		final Person person = this.game.objects.buildPerson(x, y, PersonType.VALUES[personTypeOrdinal], playerId);
		if (person != null) {
			person.update(this.game);
		}
	}

	@Override
	public void buildBuilding(int x, int y, int buildingTypeOrdinal, int playerId) {
		this.game.objects.buildBuilding(x, y, BuildingType.VALUES[buildingTypeOrdinal], playerId);
	}

	@Override
	public void buildResource(int fromX, int fromY, int toX, int toY, int resourceTypeOrdinal) {
		this.game.objects.buildResource(fromX, fromY, toX, toY, ResourceType.VALUES[resourceTypeOrdinal]);
	}

	@Override
	public void destroyResource(int fromX, int fromY, int toX, int toY) {
		this.game.objects.destroyResource(fromX, fromY, toX, toY);
	}

	@Override
	public void sendPersonsTo(int[] personIds, int x, int y, int playerId) {
		Arrays.stream(personIds).mapToObj(this.game.world.objectWorld.persons::getById).filter(Objects::nonNull).forEach(person -> {
			final Player player = this.game.getPlayer(person.getPlayerId());
			final Path path = this.game.navigation.path(person, x, y, player);
			if (path != null) {
				person.setPath(path);
			}
		});
	}
}
