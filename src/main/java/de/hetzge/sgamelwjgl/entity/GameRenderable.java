package de.hetzge.sgamelwjgl.entity;

import de.hetzge.sgamelwjgl.Assets;
import de.hetzge.sgamelwjgl.DefaultShader;
import de.hetzge.sgamelwjgl.DefaultShader.ColorMode;
import de.hetzge.sgamelwjgl.graphics.base.Camera;
import de.hetzge.sgamelwjgl.graphics.base.Camera.Viewport;
import de.hetzge.sgamelwjgl.graphics.base.GeometryBuffer;
import de.hetzge.sgamelwjgl.graphics.base.MatricesUniformBuffer;
import de.hetzge.sgamelwjgl.graphics.base.Shape;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;

public final class GameRenderable {

	private final Game game;
	private final Camera camera;
	private final GeometryBuffer buffer;
	private final SpriteBatch batch;
	private final DefaultShader defaultShader;
	private final MatricesUniformBuffer matricesBuffer;

	private GameRenderable(Game game, Camera camera, GeometryBuffer buffer, SpriteBatch batch, DefaultShader defaultShader, MatricesUniformBuffer matricesBuffer) {
		this.game = game;
		this.camera = camera;
		this.buffer = buffer;
		this.batch = batch;
		this.defaultShader = defaultShader;
		this.matricesBuffer = matricesBuffer;
	}

	public void render() {
		this.game.clock.tick(this.game);

		// TODO
		this.matricesBuffer.set(this.camera.getCombinedMatrix());
		this.matricesBuffer.use();

		final Viewport viewport = this.camera.getViewport();
		this.game.world.render(this.game.clock, viewport, this.batch);

		this.batch.render(this.buffer);
		this.buffer.flush();

		Assets.INSTANCE.atlas.getTexture().bind();
		this.defaultShader.use(ColorMode.REPLACE, this.camera.getCombinedMatrix());
		this.buffer.draw();
	}

	public void onResize(int width, int height) {
		this.camera.setupScreen(width, height);
	}

	public Camera getCamera() {
		return this.camera;
	}

	public GeometryBuffer getBuffer() {
		return this.buffer;
	}
	
	public SpriteBatch getBatch() {
		return this.batch;
	}

	public static GameRenderable create(Game game) {
		final Camera camera = Camera.create(800, 600, game.pixelGrid, game.tileGrid);
		final DefaultShader defaultShader = DefaultShader.load();
		final GeometryBuffer buffer = GeometryBuffer.create(Shape.QUAD, 500000, defaultShader.getVertexDescription());
		final SpriteBatch batch = SpriteBatch.create(500000);
		final MatricesUniformBuffer matricesBuffer = MatricesUniformBuffer.create(10);

		return new GameRenderable(game, camera, buffer, batch, defaultShader, matricesBuffer);
	}
}
