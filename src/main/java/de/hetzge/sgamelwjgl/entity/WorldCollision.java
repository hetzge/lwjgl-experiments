package de.hetzge.sgamelwjgl.entity;

import de.hetzge.sgamelwjgl.base.Orientation;
import de.hetzge.sgamelwjgl.base.PositionPredicate;

public final class WorldCollision {

	private final World world;
	private final AreaWorld globalAreaWorld;
	private final AreaWorld playerAreaWorld;
	private final AreaWorld wayAreaWorld;

	private WorldCollision(World world, AreaWorld globalAreaWorld, AreaWorld playerAreaWorld, AreaWorld wayAreaWorld) {
		this.world = world;
		this.globalAreaWorld = globalAreaWorld;
		this.playerAreaWorld = playerAreaWorld;
		this.wayAreaWorld = wayAreaWorld;
	}

	private void init() {
		this.world.tileGrid.forEach((x, y) -> {
			if (this.globalAreaWorld.isNoArea(x, y) && !isCollision(x, y)) {
				updateGlobal(x, y);
			}
			if (this.playerAreaWorld.isNoArea(x, y) && !this.isOwnerCollisionPredicate(x, y).test(x, y)) {
				updatePlayer(x, y);
			}
			if (this.wayAreaWorld.isNoArea(x, y) && this.world.wayWorld.is(x, y)) {
				updateWay(x, y);
			}
		});
	}

	public void update(int x, int y) {
		updateGlobal(x, y);
		updatePlayer(x, y);
		updateWay(x, y);
	}

	private void updateGlobal(int x, int y) {
		this.globalAreaWorld.update(x, y, (ax, ay, bx, by) -> {
			return null != this.world.getPathfinder().find(ax, ay, bx, by, 25, this::isCollision);
		}, this::isCollision);
	}

	private void updatePlayer(int x, int y) {
		final PositionPredicate ownerPositionPredicate = this.isOwnerCollisionPredicate(x, y);
		this.playerAreaWorld.update(x, y, (ax, ay, bx, by) -> {
			return null != this.world.getPathfinder().find(ax, ay, bx, by, 25, ownerPositionPredicate);
		}, ownerPositionPredicate);
	}

	private void updateWay(int x, int y) {
		this.wayAreaWorld.update(x, y, (ax, ay, bx, by) -> {
			return null != this.world.getPathfinder().find(ax, ay, bx, by, 25, this.world.wayWorld::isNot);
		}, this.world.wayWorld::isNot);
	}

	public boolean isSameGlobalArea(int ax, int ay, int bx, int by) {
		return this.globalAreaWorld.isSameArea(ax, ay, bx, by);
	}

	public boolean isSamePlayerArea(int ax, int ay, int bx, int by) {
		return this.playerAreaWorld.isSameArea(ax, ay, bx, by);
	}

	public boolean isSameWayArea(int ax, int ay, int bx, int by) {
		return this.wayAreaWorld.isSameArea(ax, ay, bx, by);
	}

	public boolean isCollision(int x, int y) {
		return this.world.heightmap.isCollisionSlope(this.world.heightmap.index(x, y)) || this.world.resourceWorld.isCollision(x, y) || this.world.objectWorld.isCollision(this.world.objectWorld.index(x, y));
	}

	public boolean isTemporaryCollision(int x, int y) {
		return this.world.objectWorld.isTemporaryCollision(this.world.objectWorld.index(x, y));
	}

	public PositionPredicate isOwnerCollisionPredicate(int x, int y) {
		final int playerId = this.world.ownerWorld.getOwner(x, y);
		return (cx, cy) -> {
			return isCollision(cx, cy) || !this.world.ownerWorld.isOwner(cx, cy, playerId);
		};
	}

	public static WorldCollision create(World world) {
		final AreaWorld globalAreaWorld = AreaWorld.create(world.getTileGrid(), Orientation.VALUES);
		final AreaWorld playerAreaWorld = AreaWorld.create(world.getTileGrid(), Orientation.VALUES);
		final AreaWorld wayAreaWorld = AreaWorld.create(world.getTileGrid(), Orientation.VALUES);

		final WorldCollision worldCollision = new WorldCollision(world, globalAreaWorld, playerAreaWorld, wayAreaWorld);
		worldCollision.init();

		return worldCollision;
	}
}
