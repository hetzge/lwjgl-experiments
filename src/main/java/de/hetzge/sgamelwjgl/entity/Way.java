package de.hetzge.sgamelwjgl.entity;

import java.io.Serializable;

public final class Way implements Serializable {

	private final int x;
	private final int y;
	private final WayType wayType;

	private Waypoint waypoint;

	public Way(int x, int y, WayType wayType) {
		this.x = x;
		this.y = y;
		this.wayType = wayType;
	}

	public Waypoint getWaypoint() {
		return this.waypoint;
	}

	public void setWaypoint(Waypoint waypoint) {
		this.waypoint = waypoint;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public WayType getWayType() {
		return this.wayType;
	}
}
