package de.hetzge.sgamelwjgl.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

import com.badlogic.gdx.utils.IntMap;

import de.hetzge.sgamelwjgl.Clock;
import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.tilemap.Heightmap;

public final class ObjectWorld implements Serializable {

	private final TileGrid tileGrid;
	final Persons persons;
	final Buildings buildings;

	private ObjectWorld(TileGrid tileGrid) {
		this.tileGrid = tileGrid;
		this.persons = new Persons(new Person[tileGrid.getLength()][], new IntMap<Person>());
		this.buildings = new Buildings(new Building[tileGrid.getLength()], new IntMap<Building>());
	}

	public boolean isCollision(int index) {
		final Building building = this.buildings.get(index);
		return (building != null) && !(building).isDoor(this.tileGrid.x(index), this.tileGrid.y(index)) && !building.isUnbuild();
	}

	public boolean isTemporaryCollision(int index) {
		return this.persons.is(index);
	}

	public int index(int x, int y) {
		return this.tileGrid.index(x, y);
	}

	public void render(Clock clock, int x, int y, Heightmap heightmap, SpriteBatch batch) {
		final long currentFrameId = clock.getCurrentFrame().getId();
		final int index = index(x, y);

		final Building building = this.buildings.get(index);
		if ((building != null) && (building.getX() == x) && (building.getY() == y)) {
			building.render(batch);
		}

		final Person[] persons = this.persons.get(index);
		if (persons != null) {
			for (final Person person : persons) {
				if (person != null) {
					final float tileHeight = heightmap.getTileHeight(index);
					final float heightOffset = tileHeight * 32f;
					person.render(currentFrameId, heightOffset, batch);
				}
			}
		}
	}

	public static ObjectWorld create(int width, int height) {
		return new ObjectWorld(new TileGrid(width, height));
	}

	public class Buildings {
		private final Building[] buildings;
		private final IntMap<Building> buildingsById;
		private int nextBuildingId;

		private Buildings(Building[] buildings, IntMap<Building> buildingsById) {
			this.buildings = buildings;
			this.buildingsById = buildingsById;
			this.nextBuildingId = 0;
		}

		public synchronized int getNextBuildingId() {
			return this.nextBuildingId++;
		}

		public void register(Building building) {
			final int id = building.getId();
			if (this.buildingsById.containsKey(id)) {
				throw new IllegalStateException(String.format("Building with id '%s' is already registered.", id));
			}
			this.buildingsById.put(id, building);
		}

		public void unregister(Building building) {
			this.buildingsById.remove(building.getId());
		}

		public Building getById(int id) {
			return this.buildingsById.get(id);
		}

		public void set(Building building) {
			final int x = building.getX();
			final int y = building.getY();
			final int width = building.getWidth();
			final int height = building.getHeight();
			if (isAny(x, y, width, height)) {
				throw new IllegalStateException(String.format("x: %s, y: %s, w: %s, h: %s", x, y, width, height));
			}

			ObjectWorld.this.tileGrid.forEachSize(x, y, width, height, (ix, iy) -> {
				this.buildings[index(ix, iy)] = building;
			});
		}

		public Building get(int index) {
			return this.buildings[index];
		}

		public List<Building> get(int fromX, int fromY, int toX, int toY) {
			final List<Building> result = new ArrayList<>();
			ObjectWorld.this.tileGrid.forEach(fromX, fromY, toX, toY, (x, y) -> {
				final Building building = get(index(x, y));
				if (building != null) {
					result.add(building);
				}
			});
			return result;
		}

		public boolean is(int index) {
			return get(index) != null;
		}

		public boolean isAny(int x, int y, int width, int height) {
			return ObjectWorld.this.tileGrid.anyMatchSize(x, y, width, height, (ix, iy) -> {
				return is(index(ix, iy));
			});
		}

		public void unset(Building building) {
			final int x = building.getX();
			final int y = building.getY();
			final int width = building.getWidth();
			final int height = building.getHeight();
			ObjectWorld.this.tileGrid.forEachSize(x, y, width, height, this::unset);
		}

		private void unset(int x, int y) {
			this.buildings[index(x, y)] = null;
		}

		public List<Building> find(Predicate<Building> predicate, Comparator<Building> order) {
			final List<Building> result = new ArrayList<>(10);
			for (final Building building : this.buildingsById.values()) {
				if (predicate.test(building)) {
					result.add(building);
				}
			}
			result.sort((a, b) -> {
				final int orderValue = order.compare(a, b);
				return orderValue != 0 ? orderValue : Integer.compare(a.getId(), b.getId());
			});
			return result;
		}
	}

	public class Persons {
		private final Person[][] persons;
		private final IntMap<Person> personsById;
		private int nextPersonId;

		private Persons(Person[][] persons, IntMap<Person> personsById) {
			this.persons = persons;
			this.personsById = personsById;
			this.nextPersonId = 0;
		}

		public synchronized int getNextPersonId() {
			return this.nextPersonId++;
		}

		public void register(Person person) {
			final int id = person.getId();
			if (this.personsById.containsKey(id)) {
				throw new IllegalStateException(String.format("Person with id '%s' is already registered.", id));
			}
			this.personsById.put(id, person);
		}

		public void unregister(Person person) {
			this.personsById.remove(person.getId());
		}

		public Person getById(int id) {
			return this.personsById.get(id);
		}

		public void set(Person person) {
			final int x = person.getX();
			final int y = person.getY();

			final int index = index(x, y);
			final Person[] values = this.persons[index];
			if (values != null) {
				this.persons[index] = Arrays.copyOf(values, values.length + 1);
				this.persons[index][values.length] = person;
			} else {
				this.persons[index] = new Person[] { person };
			}
		}

		public Person[] get(int index) {
			return this.persons[index];
		}

		public List<Person> get(int fromX, int fromY, int toX, int toY) {
			final List<Person> result = new ArrayList<>();
			ObjectWorld.this.tileGrid.forEach(fromX, fromY, toX, toY, (x, y) -> {
				final Person[] persons = get(index(x, y));
				if (persons != null) {
					for (final Person person : persons) {
						result.add(person);
					}
				}
			});
			return result;
		}

		public boolean is(int index) {
			return get(index) != null;
		}

		public boolean is(int index, PersonType personType) {
			final Person[] persons = this.persons[index];
			if (persons != null) {
				for (final Person person : persons) {
					if (person.is(personType)) {
						return true;
					}
				}
			}
			return false;
		}

		public boolean isOtherThen(int index, Person otherThen) {
			final Person[] persons = this.persons[index];
			if (persons != null) {
				for (final Person person : persons) {
					if (!person.equals(otherThen)) {
						return true;
					}
				}
			}
			return false;
		}

		public int count(int index) {
			final Person[] persons = get(index);
			return persons != null ? persons.length : 0;
		}

		public boolean isAny(int x, int y, int width, int height) {
			return ObjectWorld.this.tileGrid.anyMatchSize(x, y, width, height, (ix, iy) -> {
				return is(index(ix, iy));
			});
		}

		public void unset(Person person) {
			final int index = index(person.getX(), person.getY());
			final Person[] persons = get(index);
			if (persons.length <= 1) {
				this.persons[index] = null;
			} else {
				final Person[] newPersons = new Person[persons.length - 1];
				int j = 0;
				for (int i = 0; i < persons.length; i++) {
					if (!persons[i].equals(person)) {
						newPersons[j] = persons[i];
						j++;
					}
				}
				this.persons[index] = newPersons;
			}
		}

		public List<Person> find(Predicate<Person> predicate, Comparator<Person> order) {
			final List<Person> result = new ArrayList<>(10);
			for (final Person person : this.personsById.values()) {
				if (predicate.test(person)) {
					result.add(person);
				}
			}
			result.sort((a, b) -> {
				final int orderValue = order.compare(a, b);
				return orderValue != 0 ? orderValue : Integer.compare(a.getId(), b.getId());
			});
			return result;
		}
	}
}
