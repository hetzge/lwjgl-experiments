package de.hetzge.sgamelwjgl;

import de.hetzge.sgamelwjgl.base.WorldObject;

public final class Utils {

	private Utils() {
	}

	public static double distance(int fromX, int fromY, int toX, int toY) {
		final int a = Math.abs(fromX - toX);
		final int b = Math.abs(fromY - toY);
		return Math.sqrt((a * a) + (b * b));
	}

	public static int compareDistance(WorldObject from, WorldObject toA, WorldObject toB) {
		return compareDistance(from.getX(), from.getY(), toA.getX(), toA.getY(), toB.getX(), toB.getY());
	}

	public static int compareDistance(int fromX, int fromY, int ax, int ay, int bx, int by) {
		return Double.compare(distance(fromX, fromY, ax, ay), distance(fromX, fromY, bx, by));
	}
}
