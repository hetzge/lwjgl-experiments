package de.hetzge.sgamelwjgl.tilemap;

import java.io.File;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.stb.STBImage;

import de.hetzge.sgamelwjgl.base.TileGrid;

public final class Heightmap implements Serializable {

	public static final float COLLISION_SLOPE_LIMIT = 0.35f;

	final TileGrid tileGrid;
	final FloatBuffer heights;
	final float[] tileHeightsBuffer;

	transient HeightmapRenderable renderable;

	public Heightmap(int width, int height) {
		this.tileGrid = new TileGrid(width, height);
		this.heights = BufferUtils.createFloatBuffer((width * height) * 4);
		this.tileHeightsBuffer = new float[4];
	}

	public void setTileHeight(int index, float value) {
		this.setTileHeight(index, value, value, value, value);
	}

	public void setTileHeight(int index, float a, float b, float c, float d) {
		this.setEdge(index, 0, a);
		this.setEdge(index, 1, b);
		this.setEdge(index, 2, c);
		this.setEdge(index, 3, d);
	}

	public float[] getTileHeights(int index) {
		System.arraycopy(this.heights, index * 4, this.tileHeightsBuffer, 0, 4);
		return this.tileHeightsBuffer;
	}

	public float getTileHeightCompact(int index) {
		final float[] tileHeights = this.getTileHeights(index);
		return (tileHeights[0] * 1000f) + (tileHeights[1] * 100f) + (tileHeights[2] * 10f) + (tileHeights[3] * 1f);
	}

	private void setEdgeHeight(int index, int edge, float value) {
		this.heights.put((index * 4) + edge, value);
	}

	public float getEdgeHeight(int index, int edge) {
		return this.heights.get((index * 4) + edge);
	}

	public float getAverageHeight(int x, int y, int width, int height) {
		return (float) this.tileGrid.map(x, y, x + width, y + height, (mx, my) -> {
			return getAverageTileHeight(this.tileGrid.index(mx, my));
		}).stream().mapToDouble(Float::doubleValue).average().orElse(Double.NaN);
	}

	public float getAverageTileHeight(int index) {
		return (getEdgeHeight(index, 0) + getEdgeHeight(index, 1) + getEdgeHeight(index, 2) + getEdgeHeight(index, 2)) / 4;
	}

	public boolean isHeight(int x, int y, int width, int height, float tileHeight) {
		return this.tileGrid.allMatchSize(x, y, width, height, (fx, fy) -> isHeight(index(fx, fy), tileHeight));
	}

	public boolean isHeight(int index, float tileHeight) {
		return getTileHeight(index) == tileHeight;
	}

	public float getTileHeight(int index) {
		return (this.getEdgeHeight(index, 0) + this.getEdgeHeight(index, 1) + this.getEdgeHeight(index, 2) + this.getEdgeHeight(index, 3)) / 4f;
	}

	public float getTopTileHeight(int index) {
		return (this.getEdgeHeight(index, 0) + this.getEdgeHeight(index, 1)) / 2f;
	}

	public float getTileSlope(int index) {
		final float min = Math.min(Math.min(this.getEdgeHeight(index, 0), this.getEdgeHeight(index, 1)), Math.min(this.getEdgeHeight(index, 2), this.getEdgeHeight(index, 3)));
		final float max = Math.max(Math.max(this.getEdgeHeight(index, 0), this.getEdgeHeight(index, 1)), Math.max(this.getEdgeHeight(index, 2), this.getEdgeHeight(index, 3)));
		return max - min;
	}

	public boolean isCollisionSlope(int index) {
		return getTileSlope(index) > COLLISION_SLOPE_LIMIT;
	}

	public boolean isUngradeTile(int index) {
		return (getTileSlope(index) > 0f) && !isCollisionSlope(index);
	}

	public void gradeTileStep(int index, float height) {
		final float change = 0.1f;
		for (int edge = 0; edge < 4; edge++) {
			final float edgeHeight = getEdgeHeight(index, edge);
			if (edgeHeight < height) {
				setEdge(index, edge, Math.min(height, edgeHeight + change));
			} else if (edgeHeight > height) {
				setEdge(index, edge, Math.max(height, edgeHeight - change));
			}
		}
	}

	public boolean canGrade(int x, int y, int width, int height) {
		if (this.tileGrid.allValid(x, y, width, height)) {
			// TODO respect other building lots ground height
			final float averageHeight = getAverageHeight(x, y, width, height);
			return this.tileGrid.allMatchAroundSize(x, y, width, height, (mx, my) -> {
				// TODO not perfect (prevent collision slope)
				return Math.abs(getAverageTileHeight(this.tileGrid.index(mx, my)) - averageHeight) < (COLLISION_SLOPE_LIMIT / 2f);
			});
		} else {
			return false;
		}

	}

	public void transformTile(int index, float change) {
		this.transformTile(index, change, change, change, change);
	}

	public void transformTile(int index, float a, float b, float c, float d) {
		this.setEdge(index, 0, this.getEdgeHeight(index, 0) + a);
		this.setEdge(index, 1, this.getEdgeHeight(index, 1) + b);
		this.setEdge(index, 2, this.getEdgeHeight(index, 2) + c);
		this.setEdge(index, 3, this.getEdgeHeight(index, 3) + d);
	}

	public void setEdge(int index, int edge, float newValue) {

		final int width = this.tileGrid.getWidth();

		final int[] offsets;
		switch (edge) {
		case 0:
			offsets = new int[] { -4 + 1, (-width * 4) + 2, ((-width * 4) - 4) + 3 };
			break;
		case 1:
			offsets = new int[] { 4 + 0, (-width * 4) + 3, ((-width * 4) + 4) + 2 };
			break;
		case 2:
			offsets = new int[] { -4 + 3, (width * 4) + 0, ((width * 4) - 4) + 1 };
			break;
		case 3:
			offsets = new int[] { 4 + 2, (width * 4) + 1, ((width * 4) + 4) + 0 };
			break;
		default:
			throw new IllegalArgumentException(String.format("Edge '%s' is not a valid edge.", edge));
		}

		final float oldValue = this.getEdgeHeight(index, edge);
		this.setEdgeHeight(index, edge, this.clamp(newValue));
		this.getRenderable().setEdge(index);
		for (int i = 0; i < offsets.length; i++) {
			final int aroundEdgeIndex = (index * 4) + offsets[i];
			final boolean isValid = (aroundEdgeIndex >= 0) && (aroundEdgeIndex < this.heights.capacity());
			if (!isValid) {
				continue;
			}
			final double rowDiff = Math.abs(Math.floor(index / width) - (Math.floor(aroundEdgeIndex / 4 / width)));
			if (((i == 0) && (rowDiff != 0.0d)) || ((i != 0) && (rowDiff != 1.0d))) {
				continue;
			}
			if (this.heights.get(aroundEdgeIndex) == oldValue) {
				this.heights.put(aroundEdgeIndex, this.clamp(newValue));
				this.getRenderable().setEdge(aroundEdgeIndex / 4);
			}
		}

	}

	public int index(int x, int y) {
		return this.tileGrid.index(x, y);
	}

	public int getWidth() {
		return this.tileGrid.getWidth();
	}

	public int getHeight() {
		return this.tileGrid.getHeight();
	}

	private float clamp(float value) {
		return Math.min(1.0f, Math.max(-1.0f, value));
	}

	public void render() {
		this.getRenderable().render();
	}

	private HeightmapRenderable getRenderable() {
		if (this.renderable == null) {
			this.renderable = HeightmapRenderable.create(this);
		}
		return this.renderable;
	}

	public static Heightmap create(File pngFile) {
		STBImage.stbi_set_flip_vertically_on_load(false);

		final int[] widthPointer = new int[1];
		final int[] heightPointer = new int[1];
		final int[] channelsPointer = new int[1];
		final ByteBuffer imageBuffer = STBImage.stbi_load(pngFile.getAbsolutePath(), widthPointer, heightPointer, channelsPointer, 4);

		final int width = widthPointer[0];
		final int height = heightPointer[0];
		final int channels = channelsPointer[0];

		System.out.println(width + " " + height + " " + channels);

		final Heightmap heightmap = new Heightmap(width, height);

		for (int i = 0; i < (width * height); i++) {
			final int value = imageBuffer.get(i * channels) & 0xff;
			final float tileHeight = -1f + (value / 256f);

//			System.out.println(i + " -> " + value + " -> " + tileHeight);

			heightmap.setTileHeight(i, tileHeight);
		}

		return heightmap;
	}
}
