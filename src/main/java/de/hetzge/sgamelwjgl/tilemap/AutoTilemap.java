package de.hetzge.sgamelwjgl.tilemap;

import java.util.Objects;

import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.graphics.base.Camera.Viewport;
import de.hetzge.sgamelwjgl.tile.AutoTilesets;

public final class AutoTilemap<T> {
	private final T[] tileTypes;
	private final AutoTilesets<T> tilesets;
	private final Tilemap tilemap;
	private final int layerCount;

	@SuppressWarnings("unchecked")
	private AutoTilemap(int width, int height, int layerCount, AutoTilesets<T> tilesets) {
		this.layerCount = layerCount;
		this.tileTypes = (T[]) new Object[width * height * layerCount];
		this.tilesets = tilesets;
		this.tilemap = new Tilemap(new TileGrid(width, height), layerCount);
	}

	public void set(int x, int y, T tileType) {
		final int tile = this.tilesets.getTileTypeOffset(tileType);
		final int index = this.tilemap.index(getNextFreeLayerIndex(x, y, tileType), x, y);
		this.tilemap.setTile(index, tile);
		this.tileTypes[index] = tileType;
		update(x, y);
	}

	public void unset(int x, int y) {
		for (int layerIndex = this.layerCount - 1; layerIndex >= 0; layerIndex--) {
			final int index = this.tilemap.index(layerIndex, x, y);
			this.tilemap.setTile(index, -1);
			this.tileTypes[index] = null;
		}
		update(x, y);
	}

	public void unset(int x, int y, T tileType) {
		for (int layerIndex = this.layerCount - 1; layerIndex >= 0; layerIndex--) {
			if (tileType.equals(this.tileTypes[index(layerIndex, x, y)])) {
				final int index = this.tilemap.index(layerIndex, x, y);
				this.tilemap.setTile(index, -1);
				this.tileTypes[index] = null;
			}
		}
		update(x, y);
	}

	public void fill(T tileType) {
		final int width = this.tilemap.getTileGrid().getWidth();
		final int height = this.tilemap.getTileGrid().getHeight();
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				set(x, y, tileType);
			}
		}
	}

	public void render(Viewport viewport, boolean useHeightShading, boolean useColorShading) {
		this.tilemap.render(viewport, this.tilesets.getTexture(), useHeightShading, useColorShading);
	}

	public AutoTilesets<T> getTilesets() {
		return this.tilesets;
	}

	public Tilemap getTilemap() {
		return this.tilemap;
	}

	private void update(int x, int y) {
		this.set0(x - 1, y - 1);
		this.set0(x - 0, y - 1);
		this.set0(x + 1, y - 1);
		this.set0(x - 1, y - 0);
		this.set0(x - 0, y - 0);
		this.set0(x + 1, y - 0);
		this.set0(x - 1, y + 1);
		this.set0(x - 0, y + 1);
		this.set0(x + 1, y + 1);
	}

	private void set0(int x, int y) {
		if (this.tilemap.isValid(x, y)) {
			for (int layerIndex = 0; layerIndex < this.layerCount; layerIndex++) {
				final T tileType = this.tileTypes[index(layerIndex, x, y)];
				if (tileType != null) {

					// @formatter:off
					final int tileIndex = this.tilesets.getTileIndex(tileType, 
							isSame(x - 1, y - 1, tileType) && isSame(x - 1, y - 0, tileType) && isSame(x - 0, y - 1, tileType), 
							isSame(x - 0, y - 1, tileType),
							isSame(x + 1, y - 1, tileType) && isSame(x + 1, y - 0, tileType) && isSame(x + 0, y - 1, tileType), 
							isSame(x - 1, y - 0, tileType), 
							isSame(x - 0, y - 0, tileType),
							isSame(x + 1, y - 0, tileType), 
							isSame(x - 1, y + 1, tileType) && isSame(x - 1, y + 0, tileType) && isSame(x - 0, y + 1, tileType), 
							isSame(x - 0, y + 1, tileType),
							isSame(x + 1, y + 1, tileType) && isSame(x + 1, y + 0, tileType) && isSame(x + 0, y + 1, tileType));
					// @formatter:on

					this.tilemap.setTile(this.tilemap.index(layerIndex, x, y), tileIndex);
				}
			}
		}
	}

	private boolean isSame(int x, int y, T tileType) {
		if (this.tilemap.isValid(x, y)) {
			for (int layerIndex = 0; layerIndex < this.layerCount; layerIndex++) {
				if (tileType.equals(this.tileTypes[index(layerIndex, x, y)])) {
					return true;
				}
			}
		}
		return false;
	}

	private int getNextFreeLayerIndex(int x, int y, T tileType) {
		for (int layerIndex = 0; layerIndex < this.layerCount; layerIndex++) {
			final T layerTileType = this.tileTypes[index(layerIndex, x, y)];
			if ((layerTileType == null) || Objects.equals(layerTileType, tileType)) {
				return layerIndex;
			}
		}
		throw new IllegalStateException("No free layer found.");
	}

	private int index(int layerIndex, int x, int y) {
		return this.tilemap.index(layerIndex, x, y);
	}

	public static <T> AutoTilemap<T> create(int width, int height, int layerCount, AutoTilesets<T> tilesets) {
		return new AutoTilemap<>(width, height, layerCount, tilesets);
	}
}
