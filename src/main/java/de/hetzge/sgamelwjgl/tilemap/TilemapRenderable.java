package de.hetzge.sgamelwjgl.tilemap;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.glEnable;

import java.util.concurrent.atomic.AtomicReference;

import org.lwjgl.opengl.GL11;

import de.hetzge.sgamelwjgl.graphics.base.BatchedGeometryBuffer;
import de.hetzge.sgamelwjgl.graphics.base.Camera.Viewport;
import de.hetzge.sgamelwjgl.graphics.base.Shape;
import de.hetzge.sgamelwjgl.graphics.base.Texture;
import de.hetzge.sgamelwjgl.graphics.base.VertexDescription;

final class TilemapRenderable {

	static final AtomicReference<TilemapShader> shaderReference = new AtomicReference<>();
	static final int AREA_SIZE = 10;

	final Tilemap tilemap;
	final TilemapShader shader;
	final int widthInAreas;
	final int heightInAreas;
	final int length;
	final Area[] areas;
	final int areaLengthInTiles;

	private final boolean[] updateAreaFlags;
	private final float[] tileDataBuffer;

	private TilemapRenderable(Tilemap tilemap, TilemapShader shader, BatchedGeometryBuffer[][] tileLayerBuffers) {
		this.tilemap = tilemap;
		this.shader = shader;
		this.widthInAreas = tilemap.getTileGrid().getWidth() / AREA_SIZE;
		this.heightInAreas = tilemap.getTileGrid().getHeight() / AREA_SIZE;
		this.length = this.widthInAreas * this.heightInAreas;
		this.areas = new Area[this.length];
		for (int areaIndex = 0; areaIndex < this.areas.length; areaIndex++) {
			final int x = areaIndex % this.widthInAreas;
			final int y = areaIndex / this.widthInAreas;
			this.areas[areaIndex] = new Area(x, y, tileLayerBuffers[areaIndex]);
		}
		this.updateAreaFlags = new boolean[tileLayerBuffers.length];
		for (int i = 0; i < tileLayerBuffers.length; i++) {
			this.updateAreaFlags[i] = true;
		}
		this.tileDataBuffer = new float[AREA_SIZE * AREA_SIZE];
		this.areaLengthInTiles = tilemap.layerCount * AREA_SIZE;
	}

	public void setTile(int tileIndex) {
		final int width = this.tilemap.getTileGrid().getWidth();
		final int layerLength = this.tilemap.getLayerLength();
		final int x = tileIndex % layerLength % width;
		final int y = (tileIndex % layerLength) / width;
		final int areaX = x / AREA_SIZE;
		final int areaY = y / AREA_SIZE;
		this.updateAreaFlags[(areaY * this.widthInAreas) + areaX] = true;
	}

	public void render(Viewport viewport, Texture texture, boolean useHeightShading, boolean useColorShading) {
		this.update();

		glEnable(GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
		GL11.glDepthFunc(GL11.GL_LEQUAL);

		final int fromAreaX = viewport.getFromX() / AREA_SIZE;
		final int fromAreaY = viewport.getFromY() / AREA_SIZE;
		final int toAreaX = (viewport.getToX() / AREA_SIZE) + 1;
		final int toAreaY = (viewport.getToY() / AREA_SIZE) + 1;

		final int tilesetColumns = texture.getWidth() / this.tilemap.tileSize;

		texture.bind();
		final int columns = this.tilemap.getTileGrid().getWidth();
		this.shader.use(this.tilemap.tileSize, tilesetColumns, columns, useHeightShading, useColorShading);
		for (int y = fromAreaY - 10; y < toAreaY; y++) {
			for (int x = fromAreaX; x < toAreaX; x++) {
				final int areaIndex = (y * this.widthInAreas) + x;
				if ((areaIndex >= 0) && (areaIndex < this.areas.length)) {
					this.areas[areaIndex].render();
				}
			}
		}
	}

	private void update() {
		for (int areaIndex = 0; areaIndex < this.areas.length; areaIndex++) {
			if (this.updateAreaFlags[areaIndex]) {
				this.areas[areaIndex].update();
				this.updateAreaFlags[areaIndex] = false;
			}
		}
	}

	public static TilemapRenderable create(Tilemap tilemap) {
		final int width = tilemap.getTileGrid().getWidth();
		final int height = tilemap.getTileGrid().getHeight();
		final int xDivider = width / AREA_SIZE;
		final int yDivider = height / AREA_SIZE;

		final float[][][] indexData = new float[yDivider][][];
		for (int y = 0; y < indexData.length; y++) {
			indexData[y] = new float[xDivider][];
			for (int x = 0; x < indexData[y].length; x++) {
				indexData[y][x] = new float[AREA_SIZE * AREA_SIZE];
				for (int yy = 0; yy < AREA_SIZE; yy++) {
					for (int xx = 0; xx < AREA_SIZE; xx++) {
						indexData[y][x][(yy * AREA_SIZE) + xx] = (y * AREA_SIZE * width) + (x * AREA_SIZE) + (yy * width) + xx;
					}
				}
			}
		}

		final TilemapShader shader = shaderReference.updateAndGet(existingShader -> existingShader == null ? TilemapShader.load() : existingShader);
		final VertexDescription vertexDescription = shader.getVertexDescription();

		final BatchedGeometryBuffer[][] areaBuffers = new BatchedGeometryBuffer[xDivider * yDivider][];

		for (int y = 0; y < yDivider; y++) {
			for (int x = 0; x < xDivider; x++) {
				final BatchedGeometryBuffer[] buffers = new BatchedGeometryBuffer[tilemap.layerCount];
				for (int layerIndex = 0; layerIndex < buffers.length; layerIndex++) {
					buffers[layerIndex] = BatchedGeometryBuffer.create(Shape.POINT, AREA_SIZE * AREA_SIZE, vertexDescription);
					buffers[layerIndex].set(vertexDescription.getAttribute(TilemapShader.INDEX_ATTRIBUTE), 0, indexData[y][x]);
				}
				areaBuffers[(y * xDivider) + x] = buffers;
			}
		}

		return new TilemapRenderable(tilemap, shader, areaBuffers);
	}

	private class Area {
		final int x;
		final int y;
		final Layer[] layers;

		public Area(int x, int y, BatchedGeometryBuffer[] layerBuffers) {
			this.x = x;
			this.y = y;
			this.layers = new Layer[layerBuffers.length];
			for (int layerIndex = 0; layerIndex < this.layers.length; layerIndex++) {
				this.layers[layerIndex] = new Layer(layerIndex, layerBuffers[layerIndex]);
			}
		}

		public void render() {
			for (int layerIndex = 0; layerIndex < this.layers.length; layerIndex++) {
				this.layers[layerIndex].render();
			}
		}

		public void update() {
			for (int layerIndex = 0; layerIndex < this.layers.length; layerIndex++) {
				this.layers[layerIndex].update();
			}
		}

		private class Layer {
			final int layerIndex;
			final BatchedGeometryBuffer buffer;

			public Layer(int layerIndex, BatchedGeometryBuffer buffer) {
				this.layerIndex = layerIndex;
				this.buffer = buffer;
			}

			public void render() {
				this.buffer.draw();
			}

			public void update() {
				for (int index = 0; index < TilemapRenderable.this.tileDataBuffer.length; index++) {
					final int x = (Area.this.x * AREA_SIZE) + (index % AREA_SIZE);
					final int y = (Area.this.y * AREA_SIZE) + (index / AREA_SIZE);
					TilemapRenderable.this.tileDataBuffer[index] = TilemapRenderable.this.tilemap.tiles[(this.layerIndex * TilemapRenderable.this.tilemap.getLayerLength()) + TilemapRenderable.this.tilemap.index(0, x, y)];
				}
				this.buffer.set(TilemapRenderable.this.shader.getVertexDescription().getAttribute(TilemapShader.TILE_ATTRIBUTE), 0, TilemapRenderable.this.tileDataBuffer);
			}
		}
	}
}