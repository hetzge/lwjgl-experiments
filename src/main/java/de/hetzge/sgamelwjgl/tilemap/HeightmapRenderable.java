package de.hetzge.sgamelwjgl.tilemap;

import org.lwjgl.opengl.GL13;

import de.hetzge.sgamelwjgl.graphics.base.TextureBuffer;

class HeightmapRenderable {

	final Heightmap heightmap;
	final TextureBuffer heightsBuffer;

	final boolean[] updateRowFlags;

	public HeightmapRenderable(Heightmap heightmap, TextureBuffer heightsBuffer) {
		this.heightmap = heightmap;
		this.heightsBuffer = heightsBuffer;
		this.updateRowFlags = new boolean[heightmap.getHeight()];
	}

	public void setEdge(int index) {
		this.updateRowFlags[index / this.heightmap.getWidth()] = true;
	}

	public void render() {
		this.heightsBuffer.activate(GL13.GL_TEXTURE1);

		this.update();
	}

	private void update() {
		for (int rowIndex = 0; rowIndex < this.heightmap.getHeight(); rowIndex++) {
			if (this.updateRowFlags[rowIndex]) {
				this.updateRow(rowIndex);
				this.updateRowFlags[rowIndex] = false;
			}
		}
	}

	private void updateRow(int rowIndex) {
		final int startEdgeIndex = rowIndex * this.heightmap.getWidth() * 4;
		this.heightmap.heights.position(startEdgeIndex);
		this.heightmap.heights.limit(startEdgeIndex + (this.heightmap.getWidth() * 4));
		this.heightsBuffer.set(startEdgeIndex, this.heightmap.heights);
		this.heightmap.heights.position(0);
		this.heightmap.heights.limit(this.heightmap.heights.capacity());
	}

	public static HeightmapRenderable create(Heightmap heightmap) {
		final TextureBuffer heightsBuffer = TextureBuffer.create(heightmap.getWidth(), heightmap.getHeight());

		return new HeightmapRenderable(heightmap, heightsBuffer);
	}
}
