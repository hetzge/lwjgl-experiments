package de.hetzge.sgamelwjgl.tilemap;

import org.lwjgl.opengl.GL13;

import com.badlogic.gdx.graphics.Color;

import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.graphics.base.TextureBuffer;

public final class TilemapOverlay {

	private final TextureBuffer colorBuffer;
	private boolean active;

	private TilemapOverlay(TextureBuffer colorBuffer) {
		this.colorBuffer = colorBuffer;
		this.active = true;
	}

	public void set(int x, int y, Color color) {
		this.colorBuffer.set(this.colorBuffer.index(x, y), color);
	}

	public void render() {
		this.colorBuffer.activate(GL13.GL_TEXTURE2);

//		Assets.INSTANCE.defaultFontSet.render(batch, "X", x * 32f, (-y * 32f) - 32f, 0.2f, Color.RED);
	}

	public void enable() {
		this.active = true;
	}

	public void disable() {
		this.active = false;
	}

	public boolean isActive() {
		return this.active;
	}

	public static TilemapOverlay create(TileGrid tileGrid) {
		final TextureBuffer colorBuffer = TextureBuffer.create(tileGrid.getWidth(), tileGrid.getHeight());
		colorBuffer.fill(new Color(1.0f, 1.0f, 1.0f, 1.0f));

		return new TilemapOverlay(colorBuffer);
	}
}
