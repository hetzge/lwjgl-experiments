package de.hetzge.sgamelwjgl.tilemap;

import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glUniform1f;
import static org.lwjgl.opengl.GL20.glUniform1i;
import static org.lwjgl.opengl.GL31.glGetUniformBlockIndex;
import static org.lwjgl.opengl.GL31.glUniformBlockBinding;

import java.io.File;

import de.hetzge.sgamelwjgl.graphics.base.ShaderProgram;
import de.hetzge.sgamelwjgl.graphics.base.VertexAttribute;
import de.hetzge.sgamelwjgl.graphics.base.VertexAttribute.Format;
import de.hetzge.sgamelwjgl.graphics.base.VertexDescription;

public final class TilemapShader {

	public static final String INDEX_ATTRIBUTE = "a_index";
	public static final String TILE_ATTRIBUTE = "a_tile";

	private final ShaderProgram program;
	private final VertexDescription vertexDescription;
	private final int columnsUniformLocation;
	private final int tileSizeUniformLocation;
	private final int tilesetColumnsUniformLocation;
	private final int texture1UniformLocation;
	private final int texture2UniformLocation;
	private final int texture3UniformLocation;
	private final int useHeightShadingUniformLocation;
	private final int useColorShadingUniformLocation;

	private TilemapShader(ShaderProgram program, VertexDescription vertexDescription, int columnsUniformLocation, int tileSizeUniformLocation, int tilesetColumnsUniformLocation, int texture1UniformLocation, int texture2UniformLocation, int texture3UniformLocation, int useHeightShadingUniformLocation, int useColorShadingUniformLocation) {
		this.program = program;
		this.vertexDescription = vertexDescription;
		this.columnsUniformLocation = columnsUniformLocation;
		this.tileSizeUniformLocation = tileSizeUniformLocation;
		this.tilesetColumnsUniformLocation = tilesetColumnsUniformLocation;
		this.texture1UniformLocation = texture1UniformLocation;
		this.texture2UniformLocation = texture2UniformLocation;
		this.texture3UniformLocation = texture3UniformLocation;
		this.useHeightShadingUniformLocation = useHeightShadingUniformLocation;
		this.useColorShadingUniformLocation = useColorShadingUniformLocation;
	}

	public void use(int tileSize, int tilesetColumns, int columns, boolean useHeightShading, boolean useColorShading) {
		this.program.use();
		this.setTileSize(tileSize);
		this.setTilesetColumns(tilesetColumns);
		this.setColumns(columns);
		this.setUseHeightShading(useHeightShading);
		this.setUseColorShading(useColorShading);
		this.setTextures(0, 1, 2);
	}

	public VertexDescription getVertexDescription() {
		return this.vertexDescription;
	}

	private void setTilesetColumns(int columns) {
		glUniform1f(this.tilesetColumnsUniformLocation, columns);
	}

	private void setColumns(int columns) {
		glUniform1i(this.columnsUniformLocation, columns);
	}

	private void setTileSize(int tileSize) {
		glUniform1i(this.tileSizeUniformLocation, tileSize);
	}

	private void setUseHeightShading(boolean value) {
		glUniform1i(this.useHeightShadingUniformLocation, value ? 1 : 0);
	}

	private void setUseColorShading(boolean value) {
		glUniform1i(this.useColorShadingUniformLocation, value ? 1 : 0);
	}

	private void setTextures(int tileTextureId, int heightTextureId, int colorTextureId) {
		glUniform1i(this.texture1UniformLocation, tileTextureId);
		glUniform1i(this.texture2UniformLocation, heightTextureId);
		glUniform1i(this.texture3UniformLocation, colorTextureId);
	}

	public static TilemapShader load() {
		return create(ShaderProgram.load(new File("shader/world.vs"), new File("shader/world.gs"), new File("shader/world.fs")));
	}

	private static TilemapShader create(ShaderProgram program) {
		final VertexAttribute indexAttribute = VertexAttribute.create(program, INDEX_ATTRIBUTE, Format.FLOAT, 1);
		final VertexAttribute tileAttribute = VertexAttribute.create(program, TILE_ATTRIBUTE, Format.FLOAT, 1);
		final VertexDescription vertexDescription = VertexDescription.create(indexAttribute, tileAttribute);

		final int columnsUniformLocation = glGetUniformLocation(program.getHandle(), "u_columns");
		final int tileSizeUniformLocation = glGetUniformLocation(program.getHandle(), "u_tile_size");
		final int tilesetColumnsUniformLocation = glGetUniformLocation(program.getHandle(), "u_tileset_columns");
		final int useHeightShadingUniformLocation = glGetUniformLocation(program.getHandle(), "u_use_height_shading");
		final int useColorShadingUniformLocation = glGetUniformLocation(program.getHandle(), "u_use_color_shading");
		final int texture1UniformLocation = glGetUniformLocation(program.getHandle(), "u_texture1");
		final int texture2UniformLocation = glGetUniformLocation(program.getHandle(), "u_texture2");
		final int texture3UniformLocation = glGetUniformLocation(program.getHandle(), "u_texture3");

		final int matricesUniformBlockIndex = glGetUniformBlockIndex(program.getHandle(), "Matrices");
		glUniformBlockBinding(program.getHandle(), matricesUniformBlockIndex, 10);

		return new TilemapShader(program, vertexDescription, columnsUniformLocation, tileSizeUniformLocation, tilesetColumnsUniformLocation, texture1UniformLocation, texture2UniformLocation, texture3UniformLocation, useHeightShadingUniformLocation, useColorShadingUniformLocation);
	}
}
