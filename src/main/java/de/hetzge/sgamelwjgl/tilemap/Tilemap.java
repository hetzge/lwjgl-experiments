package de.hetzge.sgamelwjgl.tilemap;

import java.io.Serializable;

import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.graphics.base.Camera.Viewport;
import de.hetzge.sgamelwjgl.graphics.base.Texture;

public final class Tilemap implements Serializable {

	final TileGrid tileGrid;
	final int layerCount;
	final int tileSize;

	final int[] tiles;

	transient TilemapRenderable renderable;

	public Tilemap(TileGrid tileGrid, int layerCount) {
		if (((tileGrid.getWidth() % 100) != 0) || ((tileGrid.getHeight() % 100) != 0)) {
			throw new IllegalArgumentException("Tilemap width and height must be divisable by 100");
		}

		this.tileGrid = tileGrid;
		this.layerCount = layerCount;
		this.tiles = new int[tileGrid.getLength() * layerCount];
		for (int i = 0; i < this.tiles.length; i++) {
			this.tiles[i] = -1;
		}
		this.tileSize = 32;
	}

	public void fillLayer(int layerIndex, int tile) {
		final int layerLength = this.tileGrid.getLength();
		for (int index = 0; index < layerLength; index++) {
			this.setTile((layerIndex * layerLength) + index, tile);
		}
	}

	public void setTile(int index, int tile) {
		this.tiles[index] = tile;
		this.getRenderable().setTile(index);
	}

	public int getTile(int layerIndex, int x, int y) {
		return this.getTile(layerIndex, this.index(0, x, y));
	}

	public int getTile(int layerIndex, int index) {
		final int layerLength = this.tileGrid.getLength();
		if ((index >= 0) && (index < layerLength)) {
			final int tileIndex = (layerIndex * layerLength) + index;
			return this.tiles[tileIndex];
		} else {
			return -1;
		}
	}

	public void render(Viewport viewport, Texture texture, boolean useHeightShading, boolean useColorShading) {
		this.getRenderable().render(viewport, texture, useHeightShading, useColorShading);
	}

	public int getTileSize() {
		return this.tileSize;
	}

	public int getLayerLength() {
		return this.tileGrid.getLength();
	}

	public boolean isValid(int x, int y) {
		return this.tileGrid.isValid(x, y);
	}

	public TileGrid getTileGrid() {
		return this.tileGrid;
	}

	public int getLayerCount() {
		return this.layerCount;
	}

	public int index(int layerIndex, int x, int y) {
		return (layerIndex * getLayerLength()) + this.tileGrid.index(x, y);
	}

	private TilemapRenderable getRenderable() {
		if (this.renderable == null) {
			this.renderable = TilemapRenderable.create(this);
		}
		return this.renderable;
	}
}
