package de.hetzge.sgamelwjgl.tilemap;

import java.util.List;

import org.joml.Vector2i;

import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.graphics.base.Camera.Viewport;
import de.hetzge.sgamelwjgl.graphics.base.Texture;

public final class TileMapSelection {

	// TODO Autotile ?! (Line Autotile)

	private final Tilemap tilemap;
	private final Texture texture;

	private TileMapSelection(Tilemap tilemap, Texture texture) {
		this.tilemap = tilemap;
		this.texture = texture;
	}

	public MapSelection select(int x, int y, int tile) {
		return select(x, y, x, y, tile);
	}

	public MapSelection select(int fromX, int fromY, int toX, int toY, int tile) {
		return select(new MapSelection(this.tilemap.tileGrid.map(fromX, fromY, toX, toY, Vector2i::new), tile));
	}

	private MapSelection select(MapSelection selection) {
		selection.positions.forEach(position -> {
			this.tilemap.setTile(this.tilemap.tileGrid.index(position.x, position.y), selection.getTile());
		});
		return selection;
	}

	private void unselect(MapSelection selection) {
		selection.positions.forEach(position -> {
			this.tilemap.setTile(this.tilemap.tileGrid.index(position.x, position.y), -1);
		});
	}

	public void render(Viewport viewport) {
		this.tilemap.render(viewport, this.texture, false, false);
	}

	public static TileMapSelection create(TileGrid tileGrid, Texture texture) {
		return new TileMapSelection(new Tilemap(tileGrid, 1), texture);
	}

	public final class MapSelection {

		private final List<Vector2i> positions;
		private final int tile;

		private MapSelection(List<Vector2i> positions, int tile) {
			this.positions = positions;
			this.tile = tile;
		}

		public void unselect() {
			TileMapSelection.this.unselect(this);
		}

		public List<Vector2i> getPositions() {
			return this.positions;
		}

		public int getTile() {
			return this.tile;
		}
	}
}
