package de.hetzge.sgamelwjgl.prototype.world;

import java.util.HashMap;
import java.util.Map;

import de.hetzge.sgamelwjgl.graphics.base.Texture;
import de.hetzge.sgamelwjgl.graphics.base.TriangleTextureRegion;

public final class WorldTextureMapping {

	private final Map<WorldTextureMapping.MappingKey, TriangleTextureRegion> mapping;
	private final TriangleTextureRegion empty;
	private final Texture texture;

	public WorldTextureMapping(Texture texture) {
		this.texture = texture;
		this.mapping = new HashMap<>();
		this.empty = TriangleTextureRegion.create(texture, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
	}

	public void register(Ground ground, float x, float y, float gridSize) {

		// INNER GROUND
		{
			final float xOffset = x;
			final float yOffset = y;

			// TOP
			register(WorldTriangleDirection.UP, Ground.NONE, Ground.NONE, ground, TriangleTextureRegion.create(this.texture, xOffset + (gridSize * 1f), yOffset + 0f, xOffset + 0f, yOffset + (gridSize * 2f), xOffset + (gridSize * 2f), yOffset + (gridSize * 2f)));
			register(WorldTriangleDirection.DOWN, Ground.NONE, Ground.NONE, ground, TriangleTextureRegion.create(this.texture, xOffset + (gridSize * 1f), yOffset + 0f, xOffset + (gridSize * 3f), yOffset + 0f, xOffset + (gridSize * 2f), yOffset + (gridSize * 2f)));
			register(WorldTriangleDirection.UP, Ground.NONE, ground, Ground.NONE, TriangleTextureRegion.create(this.texture, xOffset + (gridSize * 3f), yOffset + 0f, xOffset + (gridSize * 2f), yOffset + (gridSize * 2f), xOffset + (gridSize * 4f), yOffset + (gridSize * 2f)));

			// BOTTOM
			register(WorldTriangleDirection.DOWN, Ground.NONE, ground, Ground.NONE, TriangleTextureRegion.create(this.texture, xOffset + 0f, yOffset + (gridSize * 2f), xOffset + (gridSize * 2f), yOffset + (gridSize * 2f), xOffset + (gridSize * 1f), yOffset + (gridSize * 4f)));
			register(WorldTriangleDirection.UP, ground, Ground.NONE, Ground.NONE, TriangleTextureRegion.create(this.texture, xOffset + (gridSize * 2f), yOffset + (gridSize * 2f), xOffset + (gridSize * 1f), yOffset + (gridSize * 4f), xOffset + (gridSize * 3f), yOffset + (gridSize * 4f)));
			register(WorldTriangleDirection.DOWN, ground, Ground.NONE, Ground.NONE, TriangleTextureRegion.create(this.texture, xOffset + (gridSize * 2f), yOffset + (gridSize * 2f), xOffset + (gridSize * 4f), yOffset + (gridSize * 2f), xOffset + (gridSize * 3f), yOffset + (gridSize * 4f)));
		}

		// OUTER GROUND
		{
			final float xOffset = x + 64.0f;
			final float yOffset = y;

			// TOP
			register(WorldTriangleDirection.UP, ground, ground, Ground.NONE, TriangleTextureRegion.create(this.texture, xOffset + (gridSize * 1f), yOffset + 0f, xOffset + 0f, yOffset + (gridSize * 2f), xOffset + (gridSize * 2f), yOffset + (gridSize * 2f)));
			register(WorldTriangleDirection.DOWN, ground, ground, Ground.NONE, TriangleTextureRegion.create(this.texture, xOffset + (gridSize * 1f), yOffset + 0f, xOffset + (gridSize * 3f), yOffset + 0f, xOffset + (gridSize * 2f), yOffset + (gridSize * 2f)));
			register(WorldTriangleDirection.UP, ground, Ground.NONE, ground, TriangleTextureRegion.create(this.texture, xOffset + (gridSize * 3f), yOffset + 0f, xOffset + (gridSize * 2f), yOffset + (gridSize * 2f), xOffset + (gridSize * 4f), yOffset + (gridSize * 2f)));

			// BOTTOM
			register(WorldTriangleDirection.DOWN, ground, Ground.NONE, ground, TriangleTextureRegion.create(this.texture, xOffset + 0f, yOffset + (gridSize * 2f), xOffset + (gridSize * 2f), yOffset + (gridSize * 2f), xOffset + (gridSize * 1f), yOffset + (gridSize * 4f)));
			register(WorldTriangleDirection.UP, Ground.NONE, ground, ground, TriangleTextureRegion.create(this.texture, xOffset + (gridSize * 2f), yOffset + (gridSize * 2f), xOffset + (gridSize * 1f), yOffset + (gridSize * 4f), xOffset + (gridSize * 3f), yOffset + (gridSize * 4f)));
			register(WorldTriangleDirection.DOWN, Ground.NONE, ground, ground, TriangleTextureRegion.create(this.texture, xOffset + (gridSize * 2f), yOffset + (gridSize * 2f), xOffset + (gridSize * 4f), yOffset + (gridSize * 2f), xOffset + (gridSize * 3f), yOffset + (gridSize * 4f)));
		}

		// FULL
		{
			final float xOffset = x + 128.0f;
			final float yOffset = y;
			register(WorldTriangleDirection.UP, ground, ground, ground, TriangleTextureRegion.create(this.texture, xOffset + (gridSize * 1f), yOffset + 0f, xOffset + 0f, yOffset + (gridSize * 2f), xOffset + (gridSize * 2f), yOffset + (gridSize * 2f)));
			register(WorldTriangleDirection.DOWN, ground, ground, ground, TriangleTextureRegion.create(this.texture, xOffset + (gridSize * 1f), yOffset + 0f, xOffset + (gridSize * 3f), yOffset + 0f, xOffset + (gridSize * 2f), yOffset + (gridSize * 2f)));
		}
	}

	private void register(WorldTriangleDirection direction, Ground ground1, Ground ground2, Ground ground3, TriangleTextureRegion textureRegion) {
		this.mapping.computeIfAbsent(new MappingKey(direction, ground1, ground2, ground3), key -> textureRegion);
	}

	public TriangleTextureRegion get(WorldTriangleDirection direction, Ground ground1, Ground ground2, Ground ground3) {
		return this.mapping.getOrDefault(new MappingKey(direction, ground1, ground2, ground3), this.empty);
	}

	public TriangleTextureRegion getEmpty() {
		return this.empty;
	}
	
	public Texture getTexture() {
		return this.texture;
	}

	private static class MappingKey {

		private final WorldTriangleDirection direction;
		private final Ground ground1;
		private final Ground ground2;
		private final Ground ground3;

		public MappingKey(WorldTriangleDirection direction, Ground ground1, Ground ground2, Ground ground3) {
			this.direction = direction;
			this.ground1 = ground1;
			this.ground2 = ground2;
			this.ground3 = ground3;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = (prime * result) + ((this.direction == null) ? 0 : this.direction.hashCode());
			result = (prime * result) + ((this.ground1 == null) ? 0 : this.ground1.hashCode());
			result = (prime * result) + ((this.ground2 == null) ? 0 : this.ground2.hashCode());
			result = (prime * result) + ((this.ground3 == null) ? 0 : this.ground3.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final WorldTextureMapping.MappingKey other = (WorldTextureMapping.MappingKey) obj;
			if (this.direction != other.direction) {
				return false;
			}
			if (this.ground1 != other.ground1) {
				return false;
			}
			if (this.ground2 != other.ground2) {
				return false;
			}
			if (this.ground3 != other.ground3) {
				return false;
			}
			return true;
		}
	}
}