package de.hetzge.sgamelwjgl.prototype.world;

import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glEnable;

import org.lwjgl.opengl.GL11;

import com.badlogic.gdx.graphics.Color;

import de.hetzge.sgamelwjgl.DefaultShader.ColorMode;
import de.hetzge.sgamelwjgl.graphics.base.Camera;
import de.hetzge.sgamelwjgl.graphics.base.Camera.Viewport;
import de.hetzge.sgamelwjgl.graphics.base.GeometryBuffer;
import de.hetzge.sgamelwjgl.graphics.base.Shape;
import de.hetzge.sgamelwjgl.graphics.base.TextureRegion;
import de.hetzge.sgamelwjgl.graphics.font.Font;
import de.hetzge.sgamelwjgl.prototype.Assets;

final class WorldInterfaceRenderable {
	private static final float NEUTRAL_FLOAT_BITS = new Color(1.0f, 1.0f, 1.0f, 1.0f).toFloatBits();

	final WorldData world;
	final Assets assets;
	final GeometryBuffer geometryBuffer;
	final GeometryBuffer fontGeometryBuffer;
	final Camera camera;
	final Marker[] markers;
	final String[] labels;
	final Font font;

	private WorldInterfaceRenderable(WorldData world, Assets assets, Font font, GeometryBuffer geometryBuffer, GeometryBuffer fontGeometryBuffer, Camera camera) {
		this.world = world;
		this.assets = assets;
		this.geometryBuffer = geometryBuffer;
		this.fontGeometryBuffer = fontGeometryBuffer;
		this.camera = camera;
		this.markers = new Marker[world.tileGrid.getLength()];
		this.labels = new String[world.tileGrid.getLength()];
		this.font = font;
	}

	public void draw() {
		update();

		this.assets.shader.use(ColorMode.MULTIPLY, this.camera.getCombinedMatrix());

		glEnable(GL11.GL_BLEND);
		glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		this.assets.textureRegions.texture.bind();
		this.geometryBuffer.draw();
	}

	public void drawFont() {
		this.assets.shader.use(ColorMode.MULTIPLY, this.camera.getCombinedMatrix());

		glEnable(GL11.GL_BLEND);
		glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		this.font.render(this.fontGeometryBuffer);
		this.fontGeometryBuffer.flush();
		this.font.bind();
		this.fontGeometryBuffer.draw();
	}

	private void update() {
		this.font.clear();
		final Viewport viewport = this.camera.getViewport();
		viewport.forEach(this::update, 2);
		this.geometryBuffer.flush();
	}

	private void update(int x, int y) {
		if (!this.world.tileGrid.isBorder(x, y)) {
			final int index = this.world.tileGrid.index(x, y);

			// MARKER
			final Marker marker = this.markers[index];
			if (marker != null) {
				final Viewport viewport = this.camera.getViewport();
				final float tileHeight = this.world.heights[index];
				final int tileSize = viewport.pixelGrid.getTileSize();

				getTextureRegion(marker).render(this.geometryBuffer, (x - (y * 0.5f)) * tileSize, ((-y - 0.5f) * tileSize) + (tileHeight * tileSize), 0f, tileSize, tileSize, marker.colorFloatBits);
			}

			// LABEL
			final String label = this.labels[index];
			if (label != null) {
				final Viewport viewport = this.camera.getViewport();
				final float tileHeight = this.world.heights[index];
				final int tileSize = viewport.pixelGrid.getTileSize();
				this.font.addText(label, (x - 0.5f - (y * 0.5f)) * tileSize, (-y * tileSize) + (tileHeight * tileSize));
			}
		}
	}

	private TextureRegion getTextureRegion(Marker marker) {
		if (marker.type == MarkerType.A) {
			return this.assets.textureRegions.marker;
		} else if (marker.type == MarkerType.B) {
			return this.assets.textureRegions.marker;
		} else {
			return this.assets.textureRegions.marker;
		}
	}

	public static WorldInterfaceRenderable createDummy(WorldData world, Camera camera) {
		return new WorldInterfaceRenderable(world, null, null, null, null, camera);
	}

	public static WorldInterfaceRenderable create(WorldData world, Assets assets, Camera camera) {
		final GeometryBuffer geometryBuffer = GeometryBuffer.create(Shape.QUAD, world.tileGrid.getLength(), assets.shader.getVertexDescription());
		final GeometryBuffer fontGeometryBuffer = GeometryBuffer.create(Shape.QUAD, world.tileGrid.getLength() * 10, assets.shader.getVertexDescription());
		return new WorldInterfaceRenderable(world, assets, assets.font.copy(), geometryBuffer, fontGeometryBuffer, camera);
	}
}
