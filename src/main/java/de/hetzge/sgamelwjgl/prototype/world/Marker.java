package de.hetzge.sgamelwjgl.prototype.world;

import com.badlogic.gdx.graphics.Color;

public final class Marker {
	MarkerType type;
	float colorFloatBits;

	public Marker(MarkerType type, Color color) {
		this.type = type;
		this.colorFloatBits = color.toFloatBits();
	}
}