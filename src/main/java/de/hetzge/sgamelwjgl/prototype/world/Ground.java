package de.hetzge.sgamelwjgl.prototype.world;

import java.awt.Color;

public enum Ground {
	NONE(Color.WHITE, true), WATER(Color.BLUE, true), DESERT(Color.YELLOW, false), GRASS(Color.GREEN, false), MOUNTAIN(Color.BLACK, false), LAVA(Color.RED, true);

	public static final Ground FLATTENED = DESERT;
	
	public final Color color;
	public final boolean collision;

	private Ground(Color color, boolean collision) {
		this.color = color;
		this.collision = collision;
	}
}