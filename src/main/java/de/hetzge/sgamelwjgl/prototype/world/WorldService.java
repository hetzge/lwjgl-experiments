package de.hetzge.sgamelwjgl.prototype.world;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.hetzge.sgamelwjgl.base.EditableTexture;
import de.hetzge.sgamelwjgl.base.PositionConsumer;
import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.graphics.base.Camera;
import de.hetzge.sgamelwjgl.prototype.Assets;
import de.hetzge.sgamelwjgl.prototype.environment.Resource;

public final class WorldService {

	final WorldData data;
	final WorldRenderable renderable;
	final WorldInterfaceRenderable interfaceRenderable;
	final List<PositionConsumer> collisionCallbacks;

	WorldService(WorldData data, WorldRenderable renderable, WorldInterfaceRenderable interfaceRenderable) {
		this.data = data;
		this.renderable = renderable;
		this.interfaceRenderable = interfaceRenderable;
		this.collisionCallbacks = new ArrayList<>();
	}

	public void registerCollisionCallback(PositionConsumer callback) {
		this.collisionCallbacks.add(callback);
	}

	public void unregisterCollisionCallback(PositionConsumer callback) {
		this.collisionCallbacks.remove(callback);
	}

	public TileGrid getTileGrid() {
		return this.data.tileGrid;
	}

	public void draw(Camera.Viewport viewport) {
		this.renderable.update(viewport);
		this.renderable.draw();
	}

	public void drawInterface() {
		this.interfaceRenderable.draw();
	}

	public void drawFont() {
		this.interfaceRenderable.drawFont();
	}

	public void setCollision(int x, int y, boolean collision) {
		this.data.collisions[this.data.tileGrid.index(x, y)] = collision;
		for (final PositionConsumer callback : this.collisionCallbacks) {
			callback.accept(x, y);
		}
	}

	public boolean isCollision(int x, int y) {
		return this.data.collisions[this.data.tileGrid.index(x, y)];
	}

	public void setReserved(int x, int y, boolean reserved) {
		this.data.reserved[this.data.tileGrid.index(x, y)] = reserved;
	}

	public boolean isReserved(int x, int y) {
		return this.data.reserved[this.data.tileGrid.index(x, y)];
	}

	public void setMarker(int x, int y, Marker marker) {
		this.interfaceRenderable.markers[this.data.tileGrid.index(x, y)] = marker;
	}

	public Marker getMarker(int x, int y) {
		return this.interfaceRenderable.markers[this.data.tileGrid.index(x, y)];
	}

	public void setLabel(int x, int y, String label) {
		this.interfaceRenderable.labels[this.data.tileGrid.index(x, y)] = label;
	}

	public String getLabel(int x, int y) {
		return this.interfaceRenderable.labels[this.data.tileGrid.index(x, y)];
	}

	public void setHeight(int x, int y, float height) {
		this.data.heights[this.data.tileGrid.index(x, y)] = height;
		this.data.tileGrid.forEachSize(x - 2, y - 2, 5, 5, this.renderable::updateShading);
//		this.data.tileGrid.forEachSize(x - 2, y - 2, 5, 5, (ex, ey) -> this.renderable.update(ex, ey, true));
	}

	public float getHeight(int x, int y) {
		return this.data.heights[this.data.tileGrid.index(x, y)];
	}

	public void setGround(int x, int y, Ground ground) {
		this.data.grounds[this.data.tileGrid.index(x, y)] = ground;
//		this.data.tileGrid.forEachSize(x - 1, y - 1, 3, 3, (ex, ey) -> {
//			this.renderable.update(ex, ey, true);
//		});
	}

	public Ground getGround(int x, int y) {
		return this.data.grounds[this.data.tileGrid.index(x, y)];
	}

	public void setFogOfWar(int x, int y, float value) {
		final int index = this.data.tileGrid.index(x, y);
		if (this.renderable.fogOfWar[index] != value) {
			this.renderable.fogOfWar[index] = value;
			this.data.tileGrid.forEachSize(x - 1, y - 1, 3, 3, (ex, ey) -> {
				this.renderable.updateShading();
			});
//			this.data.tileGrid.forEachSize(x - 1, y - 1, 3, 3, (ex, ey) -> {
//				this.renderable.update(ex, ey, true);
//			});
		}
	}

	public float getFogOfWar(int x, int y) {
		return this.renderable.fogOfWar[this.data.tileGrid.index(x, y)];
	}

	public int getOwner(int x, int y) {
		return this.data.owners[this.data.tileGrid.index(x, y)];
	}

	public boolean isOwner(int x, int y, int playerId) {
		return getOwner(x, y) == playerId;
	}

	public void setOwner(int x, int y, int playerId) {
		this.data.owners[this.data.tileGrid.index(x, y)] = playerId;
	}

	public float[] getRenderPosition(int x, int y) {
		final int tileSize = this.renderable.camera.getViewport().pixelGrid.getTileSize();
		return new float[] { //
				(x * tileSize) - (y * (tileSize / 2f)), //
				-(y * tileSize) - (this.data.heights[this.data.tileGrid.index(x, y)] * tileSize) };
	}

	public static WorldService createDummy(TileGrid tileGrid, Camera camera) {
		final WorldData world = emptyData(tileGrid);
		final WorldRenderable renderable = WorldRenderable.createDummy(world, camera);
		final WorldInterfaceRenderable interfaceRenderable = WorldInterfaceRenderable.createDummy(world, camera);
		return new WorldService(world, renderable, interfaceRenderable);
	}

	public static WorldService create(Assets assets, Camera camera) {
		final WorldData data = loadData(new File("/home/hetzge/Pictures/colormap.png"), new File("/home/hetzge/Pictures/heightmap.png"));
		final WorldRenderable renderable = WorldRenderable.create(data, assets, camera);
		final WorldInterfaceRenderable interfaceRenderable = WorldInterfaceRenderable.create(data, assets, camera);
		return new WorldService(data, renderable, interfaceRenderable);
	}

	private static WorldData emptyData(TileGrid tileGrid) {
		final Ground[] grounds = new Ground[tileGrid.getLength()];
		final Resource[] resources = new Resource[tileGrid.getLength()];
		final byte[] resourceCounts = new byte[tileGrid.getLength()];
		final float[] heights = new float[tileGrid.getLength()];
		final boolean[] collisions = new boolean[tileGrid.getLength()];
		final boolean[] reserved = new boolean[tileGrid.getLength()];
		final int[] owners = new int[tileGrid.getLength()];

		Arrays.fill(grounds, Ground.GRASS);
		Arrays.fill(resources, Resource.NONE);
		Arrays.fill(resourceCounts, (byte) 0);
		Arrays.fill(heights, 0f);
		Arrays.fill(collisions, false);
		Arrays.fill(reserved, false);
		Arrays.fill(owners, 0);

		applyGroundCollision(grounds, collisions);

		return new WorldData(tileGrid, grounds, resources, resourceCounts, heights, collisions, reserved, owners);
	}

	private static WorldData generateData(TileGrid tileGrid) {
		final Ground[] grounds = new Ground[tileGrid.getLength()];
		final Resource[] resources = new Resource[tileGrid.getLength()];
		final byte[] resourceCounts = new byte[tileGrid.getLength()];
		final float[] heights = new float[tileGrid.getLength()];
		final boolean[] collisions = new boolean[tileGrid.getLength()];
		final boolean[] reserved = new boolean[tileGrid.getLength()];
		final int[] owners = new int[tileGrid.getLength()];

		Arrays.fill(grounds, Ground.GRASS);
		Arrays.fill(resources, Resource.NONE);
		Arrays.fill(resourceCounts, (byte) 0);
		Arrays.fill(heights, 0f);
		Arrays.fill(collisions, false);
		Arrays.fill(reserved, false);
		Arrays.fill(owners, 0);

		for (int i = 0; i < grounds.length; i++) {
			final double random = Math.random();
			if (((random) - 0.3f) > ((double) i / grounds.length)) {
				grounds[i] = Ground.GRASS;
				heights[i] = ((float) Math.random() * 1.0f);
			} else if (random > 0.3d) {
				grounds[i] = Ground.WATER;
			} else {
				grounds[i] = Ground.DESERT;
				heights[i] = ((float) Math.random());
			}
		}

		applyGroundCollision(grounds, collisions);

		return new WorldData(tileGrid, grounds, resources, resourceCounts, heights, collisions, reserved, owners);
	}

	private static WorldData loadData(File groundMapImageFile, File heightMapImageFile) {
		final EditableTexture groundTexture = EditableTexture.create(groundMapImageFile);
		final EditableTexture heightTexture = EditableTexture.create(heightMapImageFile);

		if (groundTexture.getTileGrid().equals(heightTexture.getTileGrid())) {
			final TileGrid tileGrid = groundTexture.getTileGrid();
			final Ground[] grounds = new Ground[tileGrid.getLength()];
			final Resource[] resources = new Resource[tileGrid.getLength()];
			final byte[] resourceCounts = new byte[tileGrid.getLength()];
			final float[] heights = new float[tileGrid.getLength()];
			final boolean[] collisions = new boolean[tileGrid.getLength()];
			final boolean[] reserved = new boolean[tileGrid.getLength()];
			final int[] owners = new int[tileGrid.getLength()];

			Arrays.fill(grounds, Ground.GRASS);
			Arrays.fill(resources, Resource.NONE);
			Arrays.fill(resourceCounts, (byte) 0);
			Arrays.fill(heights, 0f);
			Arrays.fill(collisions, false);
			Arrays.fill(reserved, false);
			Arrays.fill(owners, 0);

			final Ground[] groundValues = Ground.values();
			tileGrid.forEach((x, y) -> {
				for (final Ground ground : groundValues) {
					if (groundTexture.isColor(x, y, ground.color)) {
						grounds[tileGrid.index(x, y)] = ground;
						break;
					}
				}
				heights[tileGrid.index(x, y)] = (heightTexture.getBlackWhite(x, y) / (float) 255) * 2f;
			});

			applyGroundCollision(grounds, collisions);

			return new WorldData(tileGrid, grounds, resources, resourceCounts, heights, collisions, reserved, owners);
		} else {
			throw new IllegalArgumentException("groundMapImageFile and heightMapImageFile does not match in size");
		}
	}

	private static void applyGroundCollision(Ground[] grounds, boolean[] collisions) {
		for (int i = 0; i < collisions.length; i++) {
			if (grounds[i].collision) {
				collisions[i] = true;
			}
		}
	}

}
