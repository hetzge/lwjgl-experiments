package de.hetzge.sgamelwjgl.prototype.world;

import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.prototype.environment.Resource;

final class WorldData {

	final TileGrid tileGrid;
	final Ground[] grounds;
	final float[] heights;
	final boolean[] collisions;
	final boolean[] reserved;
	final int[] owners;

	WorldData(TileGrid tileGrid, Ground[] grounds, Resource[] resources, byte[] resourceCounts, float[] heights, boolean[] collisions, boolean[] reserved, int[] owners) {
		this.tileGrid = tileGrid;
		this.grounds = grounds;
		this.heights = heights;
		this.collisions = collisions;
		this.reserved = reserved;
		this.owners = owners;
	}
}
