package de.hetzge.sgamelwjgl.prototype.world;

import java.util.Arrays;

import org.lwjgl.opengl.GL11;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;

import de.hetzge.sgamelwjgl.DefaultShader;
import de.hetzge.sgamelwjgl.DefaultShader.ColorMode;
import de.hetzge.sgamelwjgl.graphics.base.Camera;
import de.hetzge.sgamelwjgl.graphics.base.GeometryBuffer;
import de.hetzge.sgamelwjgl.graphics.base.Shape;
import de.hetzge.sgamelwjgl.graphics.base.TriangleTextureRegion;
import de.hetzge.sgamelwjgl.prototype.Assets;

/**
 * Responsible for rendering a {@link WorldData}.
 */
final class WorldRenderable {
	private static final float NO_SHADING_FLOAT_BITS = new Color(1.0f, 1.0f, 1.0f, 0.0f).toFloatBits();

	final WorldData world;
	final WorldTextureMapping textureMapping;
	final GeometryBuffer geometryBuffer;
	final DefaultShader shader;
	final float[] shadings;
	final Integer[] order;
	final Ground[] grounds;
	final float[] fogOfWar;
	final Camera camera;

	private WorldRenderable(WorldData world, WorldTextureMapping textureMapping, GeometryBuffer geometryBuffer, DefaultShader shader, Camera camera) {
		this.world = world;
		this.textureMapping = textureMapping;
		this.geometryBuffer = geometryBuffer;
		this.shader = shader;
		this.shadings = new float[world.tileGrid.getLength()];
		this.camera = camera;
		this.order = new Integer[] { 0, 1, 2 };
		this.grounds = new Ground[3];
		this.fogOfWar = new float[world.tileGrid.getLength()];
		Arrays.fill(this.shadings, NO_SHADING_FLOAT_BITS);
		Arrays.fill(this.fogOfWar, 0.0f);
	}

	public void draw() {
		this.shader.use(ColorMode.MULTIPLY, this.camera.getCombinedMatrix());

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

		this.textureMapping.getTexture().bind();

		this.geometryBuffer.draw();
	}

	/**
	 * Updates the shading for the whole world. See {@link #updateShading(int, int)}
	 * for details.
	 */
	public void updateShading() {
		final int width = this.world.tileGrid.getWidth();
		final int height = this.world.tileGrid.getHeight();
		this.world.tileGrid.forEach(1, 1, width - 2, height - 2, this::updateShading);
	}

	/**
	 * <p>
	 * Update the shading of a single position (defined by <code>x</code> and
	 * <code>y</code>) in the world.
	 * </p>
	 * 
	 * <p>
	 * Internally this method is calculating the shading for all triangles around
	 * the given position and then use the average as shading value (color float
	 * bits that are multiplied with the textures color).
	 * </p>
	 * 
	 * @param x the x of the tile to calculate the shading for (in tile steps)
	 * @param y the y of the tile to calculate the shading for (in tile steps)
	 */
	public void updateShading(int x, int y) {
		if (this.world.tileGrid.isBorder(x, y)) {
			return;
		}

		final int i = this.world.tileGrid.index(x, y);
		final float average = Interpolation.circleOut.apply((this.world.heights[i] / 2f) + 0.1f);
		this.shadings[i] = new Color(average, average, average, 1f - this.fogOfWar[i]).toFloatBits();
	}

	/**
	 * Render the complete world to the given {@link GeometryBuffer}. See
	 * {@link #update(GeometryBuffer, int, int)} for details.
	 * 
	 * @param direct if true the world will be directly rendered to gpu (tile by
	 *               tile)
	 */
	public void update(boolean direct) {
		this.world.tileGrid.forEach(0, 0, this.world.tileGrid.getWidth(), this.world.tileGrid.getHeight(), (x, y) -> update(x, y, direct));
		if (!direct) {
			this.geometryBuffer.flush();
		}
	}

	public void update(Camera.Viewport viewport) {
		this.world.tileGrid.forEach(viewport.getFromX() - 1, viewport.getFromY() - 3, viewport.getToX() + 1, viewport.getToY() + 3, (x, y) -> update(x, y, false));
		this.geometryBuffer.flush();
	}

	/**
	 * Render a square build by two rectangles (A =
	 * {@link WorldTriangleDirection#UP}, B = {@link WorldTriangleDirection#DOWN}).
	 * The square have width of {@link #tileSize} and is relative to the coordinates
	 * (see (1) in the following graphic) of the tile defined by <code>x</code> and
	 * <code>y</code>.
	 * 
	 * <pre>
	 *     (1)--- 2  
	 *     / \ B /   
	 *    / A \ /    
	 *   3 --- 4
	 * </pre>
	 * 
	 * @param x      the x of the tile to render (in tile steps)
	 * @param y      the y of the tile to render (in tile steps)
	 * @param direct if true the world will be directly rendered to gpu
	 */

	public void update(int x, int y, boolean direct) {

		// Draw dummy triangles for the world border
		if (this.world.tileGrid.isBorder(x, y)) {
			if (!direct) {
				for (int i = 0; i < 6; i++) {
					this.textureMapping.getEmpty().render(this.geometryBuffer, //
							0f, 0f, 0f, 0f, //
							0f, 0f, 0f, 0f, //
							0f, 0f, 0f, 0f);
				}
			}
			return;
		}

		final int tileSize = this.camera.getViewport().pixelGrid.getTileSize();

		final int i1 = this.world.tileGrid.index(x, y);
		final int i2 = this.world.tileGrid.index(x + 1, y);
		final int i3 = this.world.tileGrid.index(x, y + 1);
		final int i4 = this.world.tileGrid.index(x + 1, y + 1);

		final float h1 = this.world.heights[i1];
		final float h2 = this.world.heights[i2];
		final float h3 = this.world.heights[i3];
		final float h4 = this.world.heights[i4];

		final float s1 = this.shadings[i1];
		final float s2 = this.shadings[i2];
		final float s3 = this.shadings[i3];
		final float s4 = this.shadings[i4];

		final float x1 = (x * tileSize) - (y * (tileSize / 2f));
		final float y1 = (y * tileSize) - (h1 * tileSize);
		final float x2 = x1 + tileSize;
		final float y2 = (y * tileSize) - (h2 * tileSize);
		final float x3 = x1 - (tileSize / 2f);
		final float y3 = ((y + 1) * tileSize) - (h3 * tileSize);
		final float x4 = x1 + (tileSize / 2f);
		final float y4 = ((y + 1) * tileSize) - (h4 * tileSize);

		{
			// The grounds of the UP triangle corners.
			this.grounds[0] = this.world.grounds[i1];
			this.grounds[1] = this.world.grounds[i3];
			this.grounds[2] = this.world.grounds[i4];

			/*
			 * Evaluate the order the different grounds are rendered with. Use the order of
			 * the values in the Ground enum. This needs to be done to ensure that as
			 * example GRASS is always draw above water.
			 */
			Arrays.sort(this.order, (a, b) -> this.grounds[a].compareTo(this.grounds[b]));
			for (int ii = 0; ii < this.order.length; ii++) {
				final int i = this.order[ii];

				final TriangleTextureRegion textureRegionUp = this.textureMapping.get(WorldTriangleDirection.UP, this.grounds[0] == this.grounds[i] ? this.grounds[i] : Ground.NONE, this.grounds[1] == this.grounds[i] ? this.grounds[i] : Ground.NONE, this.grounds[2] == this.grounds[i] ? this.grounds[i] : Ground.NONE);
				if (direct) {
					textureRegionUp.renderDirect(this.geometryBuffer, (i1 * 6) + ii, //
							x1, -y1, 0f, s1, //
							x3, -y3, 0f, s3, //
							x4, -y4, 0f, s4);
				} else {
					textureRegionUp.render(this.geometryBuffer, //
							x1, -y1, 0f, s1, //
							x3, -y3, 0f, s3, //
							x4, -y4, 0f, s4);
				}
			}
		}
		{
			// The grounds of the DOWN triangle corners.
			this.grounds[0] = this.world.grounds[i1];
			this.grounds[1] = this.world.grounds[i2];
			this.grounds[2] = this.world.grounds[i4];

			/*
			 * Evaluate the order the different grounds are rendered with. Use the order of
			 * the values in the Ground enum. This needs to be done to ensure that as
			 * example GRASS is always draw above water.
			 */
			Arrays.sort(this.order, (a, b) -> this.grounds[a].compareTo(this.grounds[b]));
			for (int ii = 0; ii < this.order.length; ii++) {
				final int i = this.order[ii];

				final TriangleTextureRegion textureRegionDown = this.textureMapping.get(WorldTriangleDirection.DOWN, this.grounds[0] == this.grounds[i] ? this.grounds[i] : Ground.NONE, this.grounds[1] == this.grounds[i] ? this.grounds[i] : Ground.NONE, this.grounds[2] == this.grounds[i] ? this.grounds[i] : Ground.NONE);
				if (direct) {
					textureRegionDown.renderDirect(this.geometryBuffer, (i1 * 6) + 3 + ii, //
							x1, -y1, 0f, s1, //
							x2, -y2, 0f, s2, //
							x4, -y4, 0f, s4);
				} else {
					textureRegionDown.render(this.geometryBuffer, //
							x1, -y1, 0f, s1, //
							x2, -y2, 0f, s2, //
							x4, -y4, 0f, s4);
				}
			}
		}
	}

	public static WorldRenderable createDummy(WorldData world, Camera camera) {
		return new WorldRenderable(world, null, null, null, camera);
	}

	public static WorldRenderable create(WorldData world, Assets assets, Camera camera) {
		final GeometryBuffer geometryBuffer = GeometryBuffer.create(Shape.TRIANGLE, world.tileGrid.getLength() * 2 * 3, assets.shader.getVertexDescription());
		final WorldRenderable worldRenderable = new WorldRenderable(world, assets.worldTextureMapping, geometryBuffer, assets.shader, camera);
		worldRenderable.updateShading();
		worldRenderable.update(false);
		return worldRenderable;
	}
}