package de.hetzge.sgamelwjgl.prototype.util;

import java.util.Arrays;
import java.util.Objects;

public final class ArrayUtil {

	private ArrayUtil() {
		// private utils class constructor
	}

	public static <T> T[] add(T[] array, T value) {
		final T[] newArray = Arrays.copyOf(array, array.length + 1);
		newArray[array.length] = value;
		return newArray;
	}

	public static <T> T[] remove(T[] array, int indexToRemove) {
		if ((array.length > 0) && (indexToRemove >= 0)) {
			final T[] newArray = Arrays.copyOf(array, array.length - 1);
			for (int i = indexToRemove; i < (array.length - 1); i++) {
				newArray[i] = array[i + 1];
			}
			return newArray;
		} else {
			return array;
		}
	}

	public static <T> int indexOf(T[] array, T value) {
		for (int i = 0; i < array.length; i++) {
			if (Objects.equals(array[i], value)) {
				return i;
			}
		}
		return -1;
	}
}
