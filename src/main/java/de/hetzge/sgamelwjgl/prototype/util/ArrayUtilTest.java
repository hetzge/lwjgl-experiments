package de.hetzge.sgamelwjgl.prototype.util;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

class ArrayUtilTest {

	@Test
	void test_remove() {
		assertArrayEquals(new String[] { "a", "c", "d" }, ArrayUtil.remove(new String[] { "a", "b", "c", "d" }, 1));
		assertArrayEquals(new String[] {}, ArrayUtil.remove(new String[] {}, 1));
		assertArrayEquals(new String[] { "a", "b", "c" }, ArrayUtil.remove(new String[] { "a", "b", "c", "d" }, 3));
		assertArrayEquals(new String[] { "b", "c", "d" }, ArrayUtil.remove(new String[] { "a", "b", "c", "d" }, 0));
		assertArrayEquals(new String[] { "a", "b", "c", "d" }, ArrayUtil.remove(new String[] { "a", "b", "c", "d" }, -1));
	}

	@Test
	void test_indexOf() {
		assertEquals(0, ArrayUtil.indexOf(new String[] { "a", "b", "c", "d" }, "a"));
		assertEquals(1, ArrayUtil.indexOf(new String[] { "a", "b", "c", "d" }, "b"));
		assertEquals(2, ArrayUtil.indexOf(new String[] { "a", "b", "c", "d" }, "c"));
		assertEquals(3, ArrayUtil.indexOf(new String[] { "a", "b", "c", "d" }, "d"));
		assertEquals(-1, ArrayUtil.indexOf(new String[] { "a", "b", "c", "d" }, "e"));
	}

}
