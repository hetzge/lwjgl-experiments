package de.hetzge.sgamelwjgl.prototype;

import org.joml.Vector2f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import de.hetzge.sgamelwjgl.graphics.base.Camera;
import de.hetzge.sgamelwjgl.graphics.base.Camera.Viewport;
import de.hetzge.sgamelwjgl.graphics.base.Window.WindowInput;
import de.hetzge.sgamelwjgl.prototype.base.effect.Effect;
import de.hetzge.sgamelwjgl.prototype.world.WorldService;

public final class MousePositionTileMarkerEffect implements Effect {

	private final Camera camera;
	private final WindowInput windowInput;
	private final WorldService worldService;

	public MousePositionTileMarkerEffect(Camera camera, WindowInput windowInput, WorldService worldService) {
		this.camera = camera;
		this.windowInput = windowInput;
		this.worldService = worldService;
	}

	@Override
	public void render() {

		final Vector2f mousePosition = this.windowInput.getMousePosition();
		final Viewport viewport = this.camera.getViewport();
		final float zoom = this.camera.getZoom();
		final int tileX = viewport.pixelGrid.toIsometricTileX(this.camera.getScreenX() + (mousePosition.x * zoom), this.camera.getScreenY() + (mousePosition.y * zoom));
		final int tileY = viewport.pixelGrid.toIsometricTileY(this.camera.getScreenY() + (mousePosition.y * zoom));

		if (this.worldService.getTileGrid().isValid(tileX, tileY)) {
			GL20.glUseProgram(0);
			GL11.glPointSize(10f);
			GL11.glColor3f(1.0f, 0.0f, 0.0f);
			GL11.glBegin(GL11.GL_POINTS);
			{
				GL11.glVertex2fv(this.worldService.getRenderPosition(tileX, tileY));
			}
			GL11.glEnd();
		}
	}
}