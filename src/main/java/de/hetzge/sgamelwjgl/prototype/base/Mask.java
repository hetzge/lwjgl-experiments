package de.hetzge.sgamelwjgl.prototype.base;

import de.hetzge.sgamelwjgl.base.PositionPredicate;
import de.hetzge.sgamelwjgl.base.TileGrid;

public final class Mask implements PositionPredicate {

	private final TileGrid tileGrid;
	private final boolean[] data;

	public Mask(TileGrid tileGrid, boolean[] data) {
		this.tileGrid = tileGrid;
		this.data = data;
	}

	public int getWidth() {
		return this.tileGrid.getWidth();
	}

	public int getHeight() {
		return this.tileGrid.getHeight();
	}

	@Override
	public boolean test(int x, int y) {
		return this.data[this.tileGrid.index(x, y)];
	}

	public static Mask create(char character, String... rows) {
		assertRowLengths(rows);

		if (rows.length == 0) {
			return new Mask(new TileGrid(0, 0), new boolean[0]);
		} else {
			final TileGrid tileGrid = new TileGrid(rows[0].length(), rows.length);
			final boolean[] data = new boolean[tileGrid.getLength()];
			for (int y = 0; y < tileGrid.getHeight(); y++) {
				for (int x = 0; x < tileGrid.getWidth(); x++) {
					data[tileGrid.index(x, y)] = rows[y].charAt(x) == character;
				}
			}
			return new Mask(tileGrid, data);
		}
	}

	private static void assertRowLengths(String... rows) {
		for (int i = 0; i < rows.length; i++) {
			for (int j = 0; j < rows.length; j++) {
				if (rows[i].length() != rows[j].length()) {
					throw new IllegalArgumentException("All rows must have the same length");
				}
			}
		}
	}
}
