package de.hetzge.sgamelwjgl.prototype.base;

import de.hetzge.sgamelwjgl.base.TileConnectionOffset;

public enum Direction implements TileConnectionOffset {

	NORTH(0, -1), //
	EAST(1, 0), //
	SOUTH_EAST(1, 1), //
	SOUTH(0, 1), //
	WEST(-1, 0), //
	NORTH_WEST(-1, -1);

	public static final Direction[] VALUES = values();

	public final float xDistanceFactor;
	public final float yDistanceFactor;
	public final int tileOffsetX;
	public final int tileOffsetY;

	private Direction(int tileOffsetX, int tileOffsetY) {
		this.xDistanceFactor = tileOffsetX;
		this.yDistanceFactor = tileOffsetY;
		this.tileOffsetX = tileOffsetX;
		this.tileOffsetY = tileOffsetY;
	}

	@Override
	public int getOffsetX() {
		return this.tileOffsetX;
	}

	@Override
	public int getOffsetY() {
		return this.tileOffsetY;
	}

	public static Direction direction(int fromX, int fromY, int toX, int toY) {
		if ((fromX == toX) && (fromY == toY)) {
			throw new IllegalArgumentException("Direction not possible");
		}
		if (fromX < toX) {
			if (fromY < toY) {
				return Direction.SOUTH_EAST;
			} else {
				return Direction.EAST;
			}
		} else if (fromX > toX) {
			if (fromY > toY) {
				return Direction.NORTH_WEST;
			} else {
				return Direction.WEST;
			}
		} else {
			if (fromY < toY) {
				return Direction.SOUTH;
			} else {
				return Direction.NORTH;
			}
		}
	}
}
