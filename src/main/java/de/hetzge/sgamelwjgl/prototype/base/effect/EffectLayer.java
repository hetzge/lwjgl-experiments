package de.hetzge.sgamelwjgl.prototype.base.effect;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

public final class EffectLayer implements Effect {

	private final List<Effect> effects;

	EffectLayer(List<Effect> effects) {
		this.effects = effects;
	}

	public boolean add(Effect effect) {
		return this.effects.add(effect);
	}

	public boolean addAll(Collection<? extends Effect> effects) {
		return this.effects.addAll(effects);
	}

	public void clear() {
		this.effects.clear();
	}

	@Override
	public void render() {
		final ListIterator<Effect> listIterator = this.effects.listIterator();
		while (listIterator.hasNext()) {
			final Effect effect = listIterator.next();
			if (effect.isDone()) {
				listIterator.remove();
			} else if (effect.isActive()) {
				effect.render();
			}
		}
	}

	public static EffectLayer create() {
		return new EffectLayer(new ArrayList<>());
	}
}