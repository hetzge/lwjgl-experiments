package de.hetzge.sgamelwjgl.prototype.base.effect;

import de.hetzge.sgamelwjgl.prototype.base.Clock;

public interface Effect {
	void render();

	default boolean isActive() {
		return true;
	}

	default boolean isDone() {
		return false;
	}

	public static Effect withLifetime(Clock clock, int frames, Effect effect) {
		final long startFrameId = clock.getFrameId();
		return new WrappedEffect(effect) {
			@Override
			public boolean isDone() {
				return (clock.getFrameId() > (startFrameId + frames)) || super.isDone();
			}
		};
	}

	static class WrappedEffect implements Effect {

		private final Effect effect;

		public WrappedEffect(Effect effect) {
			this.effect = effect;
		}

		@Override
		public void render() {
			this.effect.render();
		}

		@Override
		public boolean isActive() {
			return this.effect.isActive();
		}

		@Override
		public boolean isDone() {
			return this.effect.isDone();
		}

	}
}