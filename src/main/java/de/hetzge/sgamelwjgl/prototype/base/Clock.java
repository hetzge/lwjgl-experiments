package de.hetzge.sgamelwjgl.prototype.base;

import java.time.Duration;

public final class Clock {

	private final int fps;
	private long frameId;

	public Clock(int fps, long frameId) {
		this.fps = fps;
		this.frameId = frameId;
	}

	public long getFrameId() {
		return this.frameId;
	}

	public long getFrameId(Duration duration) {
		return (long) (getFrameId() + ((duration.toMillis() / 1000f) * this.fps));
	}

	public void tick() {
		this.frameId++;
	}
}