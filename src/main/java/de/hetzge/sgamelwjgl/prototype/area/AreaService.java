package de.hetzge.sgamelwjgl.prototype.area;

import de.hetzge.sgamelwjgl.base.PositionPredicate;
import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.entity.AreaWorld;
import de.hetzge.sgamelwjgl.entity.AreaWorld.ConnectedPredicate;
import de.hetzge.sgamelwjgl.prototype.base.Direction;
import de.hetzge.sgamelwjgl.prototype.person.PathfinderService;
import de.hetzge.sgamelwjgl.prototype.world.WorldService;

public final class AreaService {

	private final WorldService worldService;
	private final PathfinderService pathfinderService;
	private final AreaWorld globalAreaWorld;
	private final AreaWorld playerAreaWorld;

	AreaService(WorldService worldService, PathfinderService pathfinderService, AreaWorld globalAreaWorld, AreaWorld playerAreaWorld) {
		this.worldService = worldService;
		this.pathfinderService = pathfinderService;
		this.globalAreaWorld = globalAreaWorld;
		this.playerAreaWorld = playerAreaWorld;
	}

	private void init() {
		this.worldService.getTileGrid().forEach(this::update);
		this.worldService.registerCollisionCallback(this::update);
	}

	public void update(int x, int y) {
		final PositionPredicate playerCollisionPredicate = playerCollisionPredicate(this.worldService.getOwner(x, y));

		this.globalAreaWorld.update(x, y, this::connectedPredicate, this.worldService::isCollision);
		this.playerAreaWorld.update(x, y, playerConnectedPredicate(x, y, playerCollisionPredicate), playerCollisionPredicate);
	}

	public boolean isSameArea(int ax, int ay, int bx, int by) {
		return this.globalAreaWorld.isSameArea(ax, ay, bx, by) && this.playerAreaWorld.isSameArea(ax, ay, bx, by);
	}

	public boolean isSameArea(int a, int b) {
		return this.globalAreaWorld.isSameArea(a, b) && this.playerAreaWorld.isSameArea(a, b);
	}

	public boolean isNoArea(int x, int y) {
		return this.globalAreaWorld.isNoArea(x, y) || this.playerAreaWorld.isNoArea(x, y);
	}

	public int getGlobalArea(int x, int y) {
		return this.globalAreaWorld.getArea(x, y);
	}

	public int getPlayerArea(int x, int y) {
		return this.playerAreaWorld.getArea(x, y);
	}

	private PositionPredicate playerCollisionPredicate(int playerId) {
		return (x, y) -> {
			return !this.worldService.isOwner(x, y, playerId) || this.worldService.isCollision(x, y);
		};
	}

	private boolean connectedPredicate(int fromX, int fromY, int toX, int toY) {
		return this.pathfinderService.findPath(fromX, fromY, toX, toY, this.worldService::isCollision) != null;
	}

	private ConnectedPredicate playerConnectedPredicate(int x, int y, PositionPredicate colliPositionPredicate) {
		return (int fromX, int fromY, int toX, int toY) -> {
			return this.pathfinderService.findPath(fromX, fromY, toX, toY, colliPositionPredicate) != null;
		};
	}

	public static AreaService create(WorldService worldService, PathfinderService pathfinderService) {
		final TileGrid tileGrid = worldService.getTileGrid();
		final AreaWorld globalAreaWorld = AreaWorld.create(tileGrid, Direction.VALUES);
		final AreaWorld playerAreaWorld = AreaWorld.create(tileGrid, Direction.VALUES);

		final AreaService areaService = new AreaService(worldService, pathfinderService, globalAreaWorld, playerAreaWorld);
		areaService.init();
		return areaService;
	}
}
