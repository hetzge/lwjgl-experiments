package de.hetzge.sgamelwjgl.prototype.person;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import de.hetzge.sgamelwjgl.prototype.base.Direction;

class DirectionTest {

	@Test
	void test() {
		assertEquals(Direction.EAST, Direction.direction(0, 0, 1, 0));
		assertEquals(Direction.NORTH, Direction.direction(1, 1, 1, 0));
		assertEquals(Direction.NORTH_WEST, Direction.direction(1, 1, 0, 0));
		assertEquals(Direction.SOUTH, Direction.direction(0, 0, 0, 1));
		assertEquals(Direction.SOUTH_EAST, Direction.direction(0, 0, 1, 1));
		assertEquals(Direction.WEST, Direction.direction(1, 0, 0, 0));
	}
}
