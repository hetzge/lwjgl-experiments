package de.hetzge.sgamelwjgl.prototype.person;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicLong;

import org.joml.Vector2i;
import org.tinylog.Logger;

import de.hetzge.sgamelwjgl.base.Path;
import de.hetzge.sgamelwjgl.entity.PersonState;
import de.hetzge.sgamelwjgl.graphics.base.Camera;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.prototype.Assets;
import de.hetzge.sgamelwjgl.prototype.area.AreaService;
import de.hetzge.sgamelwjgl.prototype.base.Clock;
import de.hetzge.sgamelwjgl.prototype.base.Direction;
import de.hetzge.sgamelwjgl.prototype.building.BuildingBuildService;
import de.hetzge.sgamelwjgl.prototype.building.BuildingData;
import de.hetzge.sgamelwjgl.prototype.building.BuildingFlattenService;
import de.hetzge.sgamelwjgl.prototype.building.BuildingsService;
import de.hetzge.sgamelwjgl.prototype.economy.EconomyService;
import de.hetzge.sgamelwjgl.prototype.world.WorldService;

public final class PersonsService {

	final AtomicLong nextId;
	final PersonsData data;
	final Clock clock;
	final PersonsRenderable renderable;
	final WorldService worldService;
	final PathfinderService pathfinderService;
	final AreaService areaService;
	final EconomyService economyService;
	final BuildingsService buildingsService;
	final BuildingFlattenService buildingFlattenService;
	final BuildingBuildService buildingBuildService;

	public static PersonsService create(Assets assets, Camera camera, Clock clock, WorldService worldService, PathfinderService pathfinderService, AreaService areaService, EconomyService economyService, BuildingsService buildingsService, BuildingFlattenService buildingFlattenService, BuildingBuildService buildingBuildService) {
		final PersonsData data = new PersonsData(new PersonData[1000000], 0);
		final PersonsRenderable renderable = PersonsRenderable.create(data, assets, camera, clock, worldService, economyService);

		return new PersonsService(data, clock, renderable, worldService, pathfinderService, areaService, economyService, buildingsService, buildingFlattenService, buildingBuildService);
	}

	PersonsService(PersonsData data, Clock clock, PersonsRenderable renderable, WorldService worldService, PathfinderService pathfinderService, AreaService areaService, EconomyService economyService, BuildingsService buildingsService, BuildingFlattenService buildingFlattenService, BuildingBuildService buildingBuildService) {
		this.nextId = new AtomicLong(0L);
		this.data = data;
		this.clock = clock;
		this.renderable = renderable;
		this.worldService = worldService;
		this.pathfinderService = pathfinderService;
		this.areaService = areaService;
		this.economyService = economyService;
		this.buildingsService = buildingsService;
		this.buildingFlattenService = buildingFlattenService;
		this.buildingBuildService = buildingBuildService;
	}

	public void draw(SpriteBatch spriteBatch) {
		this.renderable.update(spriteBatch);
	}

	public boolean canCreatePerson(int x, int y) {
		return !this.worldService.isCollision(x, y);
	}

	public void createPerson(int x, int y, PersonType type) {
		if (!canCreatePerson(x, y)) {
			throw new IllegalStateException(String.format("Can't create person at %sx%s", x, y));
		}
		final PersonData personData = new PersonData(this.nextId.getAndIncrement(), Direction.SOUTH_EAST, PersonState.IDLE, type, this.clock.getFrameId(), this.clock.getFrameId(Duration.ofSeconds(10)), x, y, PersonData.NOT_IN_BUILDING, null, 0);
		this.data.persons[this.data.nextPersonIndex] = personData;
		for (int i = this.data.nextPersonIndex; i < this.data.persons.length; i++) {
			if (this.data.persons[i] == null) {
				this.data.nextPersonIndex = i;
				return;
			}
		}
		throw new IllegalStateException("Person overflow");
	}

	public void update() {
		final long frameId = this.clock.getFrameId();
		for (final PersonData personData : this.data.persons) {
			if ((personData != null)) {
				if (personData.endFrameId == frameId) {
					update(personData);
				}
			}
		}
	}

	private void update(PersonData person) {

		// TODO if current position is collision beam to next free position

		Logger.debug("[Person#{}] Update", person.id);

		if (person.hasPath()) {
			walk(person);
		} else {
			if (person.type == PersonType.CARRIER) {
				carrierJob(person);
			} else if (person.type == PersonType.FLATTENER) {
				flattenerJob(person);
			} else if (person.type == PersonType.BUILDER) {
				builderJob(person);
			}
		}

		if (person.endFrameId == this.clock.getFrameId()) {
			person.setupIdleState(this.clock, Duration.ofSeconds(5));
		}

		person.startFrameId = PersonsService.this.clock.getFrameId();
		if (person.endFrameId <= person.startFrameId) {
			throw new IllegalStateException("Illegal end frame id");
		}
	}

	private void setPath(PersonData personData, Path path) {
		Vector2i next;
		while ((next = path.peek()) != null) {
			if ((personData.x != next.x) || (personData.y != next.y)) {
				personData.path = path;
				personData.path = !path.isEmpty() ? path : null;
				return;
			} else {
				path.poll();
			}
		}
	}

	private void walk(PersonData person) {
		Vector2i next;
		if ((person.path != null) && ((next = person.path.poll()) != null)) {
			person.direction = Direction.direction(person.x, person.y, next.x, next.y);
			person.x = next.x;
			person.y = next.y;

			if (person.path.isEmpty()) {
				person.path = null;
			}

			person.setupWalkState(PersonsService.this.clock, Duration.ofSeconds(1));
		}
	}

	private void carrierJob(PersonData person) {
		final boolean hasItem = person.item != null;
		if (hasItem) {
			final Vector2i position = this.economyService.nextPosition(person.item);
			if ((person.x == position.x) && (person.y == position.y)) {
				if (person.item.isFrom(this.worldService.getTileGrid().index(person.x, person.y))) {
					// take the transaction
					Logger.debug("[Person#{}] Take item at {}x{}", person.id, person.x, person.y);
					person.item = this.economyService.takeTransaction(person.item);
				} else if (person.item.isTo(PersonsService.this.worldService.getTileGrid().index(person.x, person.y))) {
					// drop the item at transaction target
					Logger.debug("[Person#{}] Drop item at {}x{}", person.id, person.x, person.y);
					person.item = this.economyService.finishTransaction(person.item);
				}
			} else {
				// walk to transaction target
				Logger.debug("[Person#{}] Go to item", person.id);
				final Path path = this.pathfinderService.findPath(person.x, person.y, position.x, position.y, this::collisionPredicate);
				if (path != null) {
					setPath(person, path);
				} else {
					this.economyService.cancelTransaction(person.item, person.x, person.y);
					person.item = null;
				}
			}
		} else {
			// find a new transaction
			Logger.debug("[Person#{}] Find new item from {}x{}", person.id, person.x, person.y);
			this.economyService.findTransaction(person.x, person.y).ifPresent(transactionItem -> {
				Logger.debug("[Person#{}] Found transaction", person.id);
				person.item = transactionItem;
			});
		}
	}

	private void flattenerJob(PersonData person) {
		if (person.state == PersonState.WORK) {
			// flatten
			this.buildingFlattenService.flatten(person.x, person.y);
			Logger.debug("[Person#{}] Finish flatten at {}x{}", person.x, person.y);

			// finish flatten phase, start build phase
			final BuildingData building = this.buildingsService.building(person.x, person.y);
			if (this.buildingFlattenService.isFlattened(building)) {
				this.buildingBuildService.startBuildBuilding(building);
			}
		}

		// unlink with building
		if (person.isInBuilding()) {
			this.buildingsService.building(person.buildingId).leave(person);
		}

		// next
		final Vector2i position = this.buildingFlattenService.findFlattenPosition(person.x, person.y, person);
		if (position != null) {
			// link with building
			this.buildingsService.building(position.x, position.y).enter(person);
			if ((person.x == position.x) && (person.y == position.y)) {
				// start flatten
				Logger.debug("[Person#{}] Start flatten at {}x{}", person.x, person.y);
				person.setupWorkState(this.clock, Duration.ofSeconds(3));
			} else {
				// walk to flatten position
				Logger.debug("[Person#{}] Go to flatten position", person.id);
				final Path path = this.pathfinderService.findPath(person.x, person.y, position.x, position.y, this::collisionPredicate);
				if (path != null) {
					setPath(person, path);
				}
			}
		} else {
			//
		}
	}

	private void builderJob(PersonData person) {
		if (person.state == PersonState.WORK) {
			this.buildingBuildService.build(person.x, person.y);
		}

		// unlink with building
		if (person.isInBuilding()) {
			this.buildingsService.building(person.buildingId).leave(person);
		}

		// next
		final Vector2i position = this.buildingBuildService.findBuildPosition(person.x, person.y, person);
		if (position != null) {
			// link with building
			this.buildingsService.building(position.x, position.y).enter(person);
			if ((person.x == position.x) && (person.y == position.y)) {
				// start build
				Logger.debug("[Person#{}] Start build at {}x{}", person.x, person.y);
				person.setupWorkState(this.clock, Duration.ofSeconds(3));
			} else {
				// walk to build position
				Logger.debug("[Person#{}] Go to build position", person.id);
				final Path path = this.pathfinderService.findPath(person.x, person.y, position.x, position.y, this::collisionPredicate);
				if (path != null) {
					setPath(person, path);
				}
			}
		} else {
			//
		}
	}

	private boolean collisionPredicate(int x, int y) {
		return this.worldService.isCollision(x, y) || this.areaService.isNoArea(x, y);
	}
}
