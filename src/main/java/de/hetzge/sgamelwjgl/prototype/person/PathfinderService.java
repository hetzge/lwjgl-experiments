package de.hetzge.sgamelwjgl.prototype.person;

import java.util.LinkedList;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.joml.Vector2i;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.DefaultConnection;
import com.badlogic.gdx.ai.pfa.DefaultGraphPath;
import com.badlogic.gdx.ai.pfa.Heuristic;
import com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder;
import com.badlogic.gdx.ai.pfa.indexed.IndexedGraph;
import com.badlogic.gdx.utils.Array;

import de.hetzge.sgamelwjgl.base.Path;
import de.hetzge.sgamelwjgl.base.PositionPredicate;
import de.hetzge.sgamelwjgl.base.TileGrid;

public final class PathfinderService {

	private final TileGrid tileGrid;
	private final Vector2i[] nodes;

	public static PathfinderService create(TileGrid tileGrid) {
		final Vector2i[] nodes = new Vector2i[tileGrid.getLength()];
		for (int i = 0; i < nodes.length; i++) {
			nodes[i] = new Vector2i(tileGrid.x(i), tileGrid.y(i));
		}
		return new PathfinderService(tileGrid, nodes);
	}

	public PathfinderService(TileGrid tileGrid, Vector2i[] nodes) {
		this.tileGrid = tileGrid;
		this.nodes = nodes;
	}

	public Path findPath(int startX, int startY, int endX, int endY, PositionPredicate collisionPredicate) {
		final int startNode = this.tileGrid.index(startX, startY);
		final int endNode = this.tileGrid.index(endX, endY);

		final DefaultGraphPath<Vector2i> path = new DefaultGraphPath<>();
		final boolean success = new IndexedAStarPathFinder<>(new Graph(collisionPredicate)).searchNodePath(this.nodes[startNode], this.nodes[endNode], new NodeHeuristic(), path);

		if (success) {
			return Path.create(new LinkedList<>(StreamSupport.stream(path.spliterator(), false).collect(Collectors.toList())));
		} else {
			return null;
		}
	}

	private final class NodeHeuristic implements Heuristic<Vector2i> {
		@Override
		public float estimate(Vector2i node, Vector2i endNode) {
			return Vector2i.distanceSquared(node.x, node.y, endNode.x, endNode.y);
		}
	}

	private final class Graph implements IndexedGraph<Vector2i> {

		private final PositionPredicate collisionPredicate;

		public Graph(PositionPredicate collisionPredicate) {
			this.collisionPredicate = collisionPredicate;
		}

		@Override
		public Array<Connection<Vector2i>> getConnections(Vector2i fromNode) {
			final TileGrid tileGrid = PathfinderService.this.tileGrid;
			final int value = tileGrid.index(fromNode.x, fromNode.y);
			final int width = tileGrid.getWidth();
			if (!tileGrid.isBorder(fromNode.x, fromNode.y)) {
				final Array<Connection<Vector2i>> array = new Array<>(6);

				{
					final Vector2i node = PathfinderService.this.nodes[value - width - 1];
					if (!this.collisionPredicate.test(node.x, node.y)) {
						array.add(new DefaultConnection<>(fromNode, node));
					}
				}
				{
					final Vector2i node = PathfinderService.this.nodes[value - width];
					if (!this.collisionPredicate.test(node.x, node.y)) {
						array.add(new DefaultConnection<>(fromNode, node));
					}
				}
				{
					final Vector2i node = PathfinderService.this.nodes[value - 1];
					if (!this.collisionPredicate.test(node.x, node.y)) {
						array.add(new DefaultConnection<>(fromNode, node));
					}
				}
				{
					final Vector2i node = PathfinderService.this.nodes[value + 1];
					if (!this.collisionPredicate.test(node.x, node.y)) {
						array.add(new DefaultConnection<>(fromNode, node));
					}
				}
				{
					final Vector2i node = PathfinderService.this.nodes[value + width];
					if (!this.collisionPredicate.test(node.x, node.y)) {
						array.add(new DefaultConnection<>(fromNode, node));
					}
				}
				{
					final Vector2i node = PathfinderService.this.nodes[value + width + 1];
					if (!this.collisionPredicate.test(node.x, node.y)) {
						array.add(new DefaultConnection<>(fromNode, node));
					}
				}

				return array;
			} else {
				return new Array<>(0);
			}
		}

		@Override
		public int getIndex(Vector2i node) {
			return PathfinderService.this.tileGrid.index(node.x, node.y);
		}

		@Override
		public int getNodeCount() {
			return PathfinderService.this.tileGrid.getLength();
		}
	}
}
