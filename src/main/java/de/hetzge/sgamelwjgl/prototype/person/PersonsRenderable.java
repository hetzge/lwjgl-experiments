package de.hetzge.sgamelwjgl.prototype.person;

import com.badlogic.gdx.math.Interpolation;

import de.hetzge.sgamelwjgl.entity.PersonState;
import de.hetzge.sgamelwjgl.graphics.base.Camera;
import de.hetzge.sgamelwjgl.graphics.base.Camera.Viewport;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.graphics.base.TextureRegion;
import de.hetzge.sgamelwjgl.prototype.Assets;
import de.hetzge.sgamelwjgl.prototype.base.Clock;
import de.hetzge.sgamelwjgl.prototype.base.Direction;
import de.hetzge.sgamelwjgl.prototype.economy.EconomyService;
import de.hetzge.sgamelwjgl.prototype.world.WorldService;

class PersonsRenderable {

	final PersonsData data;
	final Assets assets;
	final Camera camera;
	final Clock clock;
	final WorldService worldService;
	final EconomyService economyService;

	private PersonsRenderable(PersonsData data, Assets assets, Camera camera, Clock clock, WorldService worldService, EconomyService economyService) {
		this.data = data;
		this.assets = assets;
		this.camera = camera;
		this.clock = clock;
		this.worldService = worldService;
		this.economyService = economyService;
	}

	public void update(SpriteBatch spriteBatch) {
		final Viewport viewport = this.camera.getViewport();
		final int tileSize = viewport.pixelGrid.getTileSize();
		for (final PersonData personData : this.data.persons) {
			if (personData != null) {
				if (viewport.contains(personData.x, personData.y, 5)) {

					final float progress = Interpolation.sineIn.apply(calculateProgress(personData));
					final Direction direction = personData.direction;
					final float tileX = personData.x - calculateOffset(direction.xDistanceFactor, progress);
					final float tileY = personData.y - calculateOffset(direction.yDistanceFactor, progress);
					final float currentTileHeight = this.worldService.getHeight(personData.x, personData.y);
					final float lastTileHeight = this.worldService.getHeight(personData.x - direction.tileOffsetX, personData.y - direction.tileOffsetY);
					final float tileHeight = currentTileHeight - calculateOffset(currentTileHeight - lastTileHeight, progress);

					final TextureRegion textureRegion = this.assets.textureRegions.person[direction.ordinal()];
					spriteBatch.add(textureRegion, (tileX - (tileY * 0.5f)) * tileSize, (-tileY + tileHeight) * tileSize, 0.0f, textureRegion.getOriginalWidth(), textureRegion.getOriginalHeight());

					if ((personData.item != null) && personData.item.isReal()) {
						spriteBatch.add(this.economyService.textureRegeion(personData.item), (tileX - (tileY * 0.5f)) * tileSize, (-tileY + tileHeight) * tileSize, 0.0f, 8f, 8f);
					}
				}
			}
		}
	}

	private float calculateOffset(float value, float progress) {
		return value * (1.0f - progress);
	}

	private float calculateProgress(PersonData personData) {
		if (personData.state == PersonState.WALK) {
			return (this.clock.getFrameId() - personData.startFrameId) / (float) (personData.endFrameId - personData.startFrameId);
		} else {
			return 1f;
		}
	}

	public static PersonsRenderable create(PersonsData data, Assets assets, Camera camera, Clock clock, WorldService worldService, EconomyService economyService) {
		return new PersonsRenderable(data, assets, camera, clock, worldService, economyService);
	}
}
