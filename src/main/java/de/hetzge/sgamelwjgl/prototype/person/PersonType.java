package de.hetzge.sgamelwjgl.prototype.person;

public enum PersonType {
	CARRIER, FLATTENER, BUILDER;
}
