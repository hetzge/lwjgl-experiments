package de.hetzge.sgamelwjgl.prototype.person;

import java.time.Duration;

import de.hetzge.sgamelwjgl.base.Path;
import de.hetzge.sgamelwjgl.entity.PersonState;
import de.hetzge.sgamelwjgl.prototype.base.Clock;
import de.hetzge.sgamelwjgl.prototype.base.Direction;
import de.hetzge.sgamelwjgl.prototype.building.BuildingData;
import de.hetzge.sgamelwjgl.prototype.economy.ItemData;

public final class PersonData {

	public static final int NOT_IN_BUILDING = -1;

	public final long id;
	Direction direction;
	PersonState state;
	PersonType type;
	long startFrameId;
	long endFrameId;
	int x;
	int y;
	int buildingId;
	Path path;
	ItemData item;

	public PersonData(long id, Direction direction, PersonState state, PersonType type, long startFrameId, long endFrameId, int x, int y, int buildingId, Path path, int pathIndex) {
		this.id = id;
		this.direction = direction;
		this.state = state;
		this.type = type;
		this.startFrameId = startFrameId;
		this.endFrameId = endFrameId;
		this.x = x;
		this.y = y;
		this.buildingId = buildingId;
		this.path = path;
	}

	public void setBuilding(BuildingData buildingData) {
		this.buildingId = buildingData.id;
	}

	public void unsetBuilding() {
		this.buildingId = NOT_IN_BUILDING;
	}

	public boolean isInBuilding() {
		return this.buildingId != NOT_IN_BUILDING;
	}

	public boolean isInBuilding(BuildingData buildingData) {
		return this.buildingId == buildingData.id;
	}

	public boolean hasPath() {
		return this.path != null;
	}

	public void setupIdleState(Clock clock, Duration duration) {
		this.state = PersonState.IDLE;
		this.endFrameId = clock.getFrameId(Duration.ofSeconds(2));
	}

	public void setupWorkState(Clock clock, Duration duration) {
		this.state = PersonState.WORK;
		this.endFrameId = clock.getFrameId(duration);
	}

	public void setupWalkState(Clock clock, Duration duration) {
		this.state = PersonState.WALK;
		this.endFrameId = clock.getFrameId(duration);
	}

	@Override
	public String toString() {
		return String.format("PersonData [id=%s, direction=%s, state=%s, type=%s, startFrameId=%s, endFrameId=%s, x=%s, y=%s, buildingId=%s, path=%s, item=%s]", this.id, this.direction, this.state, this.type, this.startFrameId, this.endFrameId, this.x, this.y, this.buildingId, this.path, this.item);
	}
}
