package de.hetzge.sgamelwjgl.prototype.person;

final class PersonsData {

	final PersonData[] persons;
	int nextPersonIndex;

	public PersonsData(PersonData[] persons, int nextPersonIndex) {
		this.persons = persons;
		this.nextPersonIndex = nextPersonIndex;
	}
}
