package de.hetzge.sgamelwjgl.prototype.economy;

public enum ItemState {
	/**
	 * Describes an transaction fulfilled by an person.
	 */
	TRANSACTION(false),
	/**
	 * Available for any usage.
	 */
	SUPPLY(true),
	/**
	 * Reserved to an JOB counterpart.
	 */
	SUPPLIED(true),
	/**
	 * Needed but not matched by any available item.
	 */
	DEMAND(false),
	/**
	 * Needed and matched by an reserved item.
	 */
	DEMANDED(false),
	/**
	 * Blocked for special (non carrier) usage.
	 */
	BLOCKED(true);

	public final boolean real;

	private ItemState(boolean real) {
		this.real = real;
	}
}
