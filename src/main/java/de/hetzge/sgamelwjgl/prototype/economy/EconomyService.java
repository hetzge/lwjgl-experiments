package de.hetzge.sgamelwjgl.prototype.economy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.joml.Vector2i;

import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.graphics.base.Camera;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.graphics.base.TextureRegion;
import de.hetzge.sgamelwjgl.prototype.Assets;
import de.hetzge.sgamelwjgl.prototype.area.AreaService;
import de.hetzge.sgamelwjgl.prototype.util.ArrayUtil;
import de.hetzge.sgamelwjgl.prototype.world.WorldService;

public final class EconomyService {

	private static final ItemData[] EMPTY_STOCK = new ItemData[0];

	final WorldService worldService;
	final AreaService areaService;
	final EconomyData data;
	final EconomyRenderable renderable;

	EconomyService(WorldService worldService, AreaService areaService, EconomyData data, EconomyRenderable renderable) {
		this(worldService, areaService, data, renderable, new LinkedList<>(), new LinkedList<>());
	}

	EconomyService(WorldService worldService, AreaService areaService, EconomyData data, EconomyRenderable renderable, List<ItemData> matchedItems, List<ItemData> unmatchedItems) {
		this.worldService = worldService;
		this.areaService = areaService;
		this.data = data;
		this.renderable = renderable;
	}

	public void update() {

		final List<ItemData> unmatched = new ArrayList<>();
		for (final ItemData[] stock : this.data.stocks) {
			for (final ItemData item : stock) {
				/*
				 * Cleanup is only responsible for SUPPLY state ... person is responsible for
				 * SUPPLIED
				 */
				if ((item.state == ItemState.SUPPLY) && (item.to != ItemData.UNKNOWN) && !hasValidTarget(item)) {
					unmatch(item);
				}

				if (item.to == ItemData.UNKNOWN) {
					unmatched.add(item);
				}
			}
		}

		// match
		final Set<ItemData> blacklist = new HashSet<>(10);
		for (final ItemData itemA : unmatched) {
			for (final ItemData itemB : unmatched) {
				if (!itemA.equals(itemB)) {
					if (itemA.to == ItemData.UNKNOWN) {
						if (itemA.state == ItemState.SUPPLY) {
							if (itemB.state == ItemState.DEMAND) {
								if (this.areaService.isSameArea(itemA.from, itemB.from)) {
									if (!blacklist.contains(itemA) && !blacklist.contains(itemB)) {
										final ItemData newItemA = itemA.withTo(itemB.from);
										final ItemData newItemB = itemB.withTo(itemA.from);
										this.data.replace(itemA, newItemA);
										this.data.replace(itemB, newItemB);
										assert matchingItem(newItemA).equals(newItemB);
										blacklist.add(itemA);
										blacklist.add(itemB);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public void draw(SpriteBatch spriteBatch) {
		this.renderable.draw(spriteBatch);
	}

	public int itemCount(int x, int y) {
		final int index = this.worldService.getTileGrid().index(x, y);
		return this.data.stocks[index].length;
	}

	public int itemCount(int x, int y, ItemType type, ItemState state) {
		final int index = this.worldService.getTileGrid().index(x, y);
		int count = 0;
		for (final ItemData item : this.data.stocks[index]) {
			if ((item.type == type) && (item.state == state)) {
				count++;
			}
		}
		return count;
	}

	public void createItem(int x, int y, ItemType type, ItemState state) {
		createItem0(x, y, type, state, ItemData.UNKNOWN);
	}

	public void createItems(int fromX, int fromY, ItemType type, int toX, int toY) {
		createItem0(fromX, fromY, type, ItemState.SUPPLY, this.worldService.getTileGrid().index(toX, toY));
		createItem0(toX, toY, type, ItemState.DEMAND, this.worldService.getTileGrid().index(fromX, fromY));
	}

	private void createItem0(int x, int y, ItemType type, ItemState state, int to) {
		final int index = this.worldService.getTileGrid().index(x, y);
		final ItemData item = new ItemData(type, state, index, to);
		this.data.stocks[index] = ArrayUtil.add(this.data.stocks[index], item);
	}

	private void removeItem0(int x, int y, ItemData item) {
		final int index = this.worldService.getTileGrid().index(x, y);
		removeItem0(index, item);
	}

	private void removeItem0(int index, ItemData item) {
		final int arrayIndex = ArrayUtil.indexOf(this.data.stocks[index], item);
		if (arrayIndex >= 0) {
			this.data.stocks[index] = ArrayUtil.remove(this.data.stocks[index], arrayIndex);
		} else {
			throw new IllegalStateException("Item is not part of stock");
		}
	}

	private boolean hasValidTarget(ItemData item) {
		return (item.to == ItemData.UNKNOWN) || this.areaService.isSameArea(item.from, item.to);
	}

	public Optional<ItemData> findTransaction(int x, int y) {
		final TileGrid tileGrid = this.worldService.getTileGrid();
		for (final ItemData[] stock : this.data.stocks) {
			for (final ItemData fromItem : stock) {
				if ((fromItem.state == ItemState.SUPPLY) && (fromItem.to != ItemData.UNKNOWN)) {
					if (this.areaService.isSameArea(tileGrid.index(x, y), fromItem.from)) {
						if (hasValidTarget(fromItem)) {
							final ItemData toItem = matchingItem(fromItem);
							this.data.replace(fromItem, fromItem.withState(ItemState.SUPPLIED));
							this.data.replace(toItem, toItem.withState(ItemState.DEMANDED));
							return Optional.of(new ItemData(fromItem.type, ItemState.TRANSACTION, fromItem.from, toItem.from));
						} else {
							unmatch(fromItem);
						}
					}
				}
			}
		}

		return Optional.empty();
	}

	public ItemData takeTransaction(ItemData transactionItem) {
		if (transactionItem.state != ItemState.TRANSACTION) {
			throw new IllegalStateException();
		}

		final ItemData fromItem = get(transactionItem.from, transactionItem.type, ItemState.SUPPLIED, transactionItem.from, transactionItem.to);
		final ItemData toItem = matchingItem(fromItem);
		removeItem0(transactionItem.from, fromItem);
		this.data.replace(toItem, toItem.withTo(ItemData.UNKNOWN));
		return transactionItem.withFrom(ItemData.UNKNOWN);
	}

	public ItemData finishTransaction(ItemData transactionItem) {
		if (transactionItem.from != -1) {
			throw new IllegalStateException("Transaction item is not taken.Can not finish. " + transactionItem);
		}

		final ItemData fromItemTemplate = new ItemData(transactionItem.type, ItemState.SUPPLIED, transactionItem.from, transactionItem.to);
		final ItemData toItem = matchingItem(fromItemTemplate);
		this.data.replace(toItem, toItem.withState(ItemState.BLOCKED));
		return null;
	}

	public void cancelTransaction(ItemData transactionItem, int x, int y) {
		createItem(x, y, transactionItem.type, ItemState.SUPPLY);
	}

	public Vector2i nextPosition(ItemData itemData) {
		final TileGrid tileGrid = this.worldService.getTileGrid();
		final int index = itemData.from != ItemData.UNKNOWN ? itemData.from : itemData.to;
		return new Vector2i(tileGrid.x(index), tileGrid.y(index));
	}

	private void unmatch(ItemData fromItem) {
		final ItemData toItem = matchingItem(fromItem);
		this.data.replace(fromItem, fromItem.withTo(ItemData.UNKNOWN));
		this.data.replace(toItem, toItem.withTo(ItemData.UNKNOWN));
	}

	private ItemData matchingItem(ItemData fromItem) {
		if (fromItem.to == ItemData.UNKNOWN) {
			throw new IllegalStateException(fromItem.toString());
		}
		if ((fromItem.state != ItemState.SUPPLY) && (fromItem.state != ItemState.SUPPLIED)) {
			throw new IllegalStateException(fromItem.toString());
		}
		final ItemData[] stock = this.data.stocks[fromItem.to];
		for (final ItemData toItem : stock) {
			if (fromItem.type == toItem.type) {
				if (fromItem.from == toItem.to) {
					if ((fromItem.state == ItemState.SUPPLY) && (toItem.state == ItemState.DEMAND)) {
						return toItem;
					} else if ((fromItem.state == ItemState.SUPPLIED) && (toItem.state == ItemState.DEMANDED)) {
						return toItem;
					}
				}
			}
		}

		for (final ItemData toItem : stock) {
			System.out.println(toItem);
		}
		throw new IllegalStateException("No matching item found for " + fromItem);
	}

	private ItemData get(int index, ItemType type, ItemState state, int from, int to) {
		final ItemData[] stock = this.data.stocks[index];
		for (final ItemData item : stock) {
			if ((item.type == type) && (item.state == state) && (item.from == from) && (item.to == to)) {
				return item;
			}
		}
		throw new IllegalStateException();
	}

	public TextureRegion textureRegeion(ItemData item) {
		return this.renderable.textureRegeion(item);
	}

	public static EconomyService createDummy(Camera camera, WorldService worldService, AreaService areaService) {
		final ItemData[][] stocks = new ItemData[worldService.getTileGrid().getLength()][];
		Arrays.fill(stocks, EMPTY_STOCK);

		final EconomyData economy = new EconomyData(stocks);
		final EconomyRenderable renderable = EconomyRenderable.createDummy(economy, camera, worldService);
		return new EconomyService(worldService, areaService, economy, renderable);
	}

	public static EconomyService create(Assets assets, Camera camera, WorldService worldService, AreaService areaService) {
		final ItemData[][] stocks = new ItemData[worldService.getTileGrid().getLength()][];
		Arrays.fill(stocks, EMPTY_STOCK);

		final EconomyData economy = new EconomyData(stocks);
		final EconomyRenderable renderable = EconomyRenderable.create(economy, assets, camera, worldService);
		return new EconomyService(worldService, areaService, economy, renderable);
	}
}
