package de.hetzge.sgamelwjgl.prototype.economy;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

import de.hetzge.sgamelwjgl.base.PixelGrid;
import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.graphics.base.Camera;
import de.hetzge.sgamelwjgl.prototype.area.AreaService;
import de.hetzge.sgamelwjgl.prototype.person.PathfinderService;
import de.hetzge.sgamelwjgl.prototype.world.WorldService;

class EconomyServiceTest {

	final int fromX = 1;
	final int fromY = 1;
	final int fromIndex = 11;
	final int toX = 9;
	final int toY = 9;
	final int toIndex = 99;
	final TileGrid tileGrid = new TileGrid(10, 10);

	@Test
	void test_createItem_matched() {
		// given
		final EconomyService economyService = createEconomyService();

		// when
		economyService.createItems(this.fromX, this.fromY, ItemType.WOOD, this.toX, this.toY);

		economyService.update();

		// should be contained in matched items
		assertEquals(new ItemData(ItemType.WOOD, ItemState.SUPPLY, this.fromIndex, this.toIndex), economyService.data.stocks[this.fromIndex][0]);
		assertEquals(new ItemData(ItemType.WOOD, ItemState.DEMAND, this.toIndex, this.fromIndex), economyService.data.stocks[this.toIndex][0]);
	}

	@Test
	void test_createItem_matched_by_update() {
		// given
		final EconomyService economyService = createEconomyService();

		// when
		economyService.createItem(this.fromX, this.fromY, ItemType.WOOD, ItemState.SUPPLY);
		economyService.createItem(this.toX, this.toY, ItemType.WOOD, ItemState.DEMAND);

		economyService.update();

		// should be contained in matched items
		assertEquals(new ItemData(ItemType.WOOD, ItemState.SUPPLY, this.fromIndex, this.toIndex), economyService.data.stocks[this.fromIndex][0]);
		assertEquals(new ItemData(ItemType.WOOD, ItemState.DEMAND, this.toIndex, this.fromIndex), economyService.data.stocks[this.toIndex][0]);
	}

	@Test
	void test_createItem_update() {
		// given
		final EconomyService economyService = createEconomyService();
		economyService.createItems(this.fromX, this.fromY, ItemType.WOOD, this.toX, this.toY);
		economyService.createItems(this.fromX, this.fromY, ItemType.STONE, 1, 9);
		// collision separate the items
		IntStream.range(0, 10).forEach(y -> economyService.worldService.setCollision(5, y, true));

		// when
		economyService.update();

		// then
		assertEquals(ItemState.SUPPLY, economyService.data.stocks[this.fromIndex][0].state);
		assertEquals(ItemState.DEMAND, economyService.data.stocks[this.toIndex][0].state);
		assertEquals(-1, economyService.data.stocks[this.fromIndex][0].to);
		assertEquals(-1, economyService.data.stocks[this.toIndex][0].to);
	}

	@Test
	void test_findTransaction() {
		// given
		final EconomyService economyService = createEconomyService();
		economyService.createItems(1, 1, ItemType.WOOD, 9, 9);

		// should find transaction
		final Optional<ItemData> transactionOptional = economyService.findTransaction(1, 1);
		assertTrue(transactionOptional.isPresent());
		assertEquals(11, transactionOptional.get().from);
		assertEquals(99, transactionOptional.get().to);
		assertEquals(ItemState.TRANSACTION, transactionOptional.get().state);
		assertEquals(ItemType.WOOD, transactionOptional.get().type);

		// should not find second transaction
		final Optional<ItemData> transactionOptional2 = economyService.findTransaction(1, 1);
		assertFalse(transactionOptional2.isPresent());
	}

	@Test
	void test_findTransaction_state() {
		// given
		final EconomyService economyService = createEconomyService();
		economyService.createItems(1, 1, ItemType.WOOD, 9, 9);

		// when
		final Optional<ItemData> transactionOptional = economyService.findTransaction(1, 1);

		// should update item state
		assertEquals(ItemState.TRANSACTION, transactionOptional.get().state);
		assertEquals(ItemState.SUPPLIED, economyService.data.stocks[this.tileGrid.index(1, 1)][0].state);
		assertEquals(ItemState.DEMANDED, economyService.data.stocks[this.tileGrid.index(9, 9)][0].state);
	}

	@Test
	void test_findTransaction_not_same_area() {
		// given
		final EconomyService economyService = createEconomyService();
		IntStream.range(0, 10).forEach(y -> economyService.worldService.setCollision(5, y, true));
		economyService.createItems(1, 1, ItemType.WOOD, 9, 9);

		// should not find transaction
		final Optional<ItemData> transactionOptional = economyService.findTransaction(1, 1);
		assertFalse(transactionOptional.isPresent());
	}

	@Test
	void test_takeTransaction() {
		// given
		final EconomyService economyService = createEconomyService();
		economyService.createItems(1, 1, ItemType.WOOD, 9, 9);
		final ItemData transactionItem = economyService.findTransaction(1, 1).get();

		// when
		final ItemData takenTransactionItem = economyService.takeTransaction(transactionItem);

		// should remove the from item
		assertEquals(0, economyService.data.stocks[11].length);
		// should update the transaction item
		assertEquals(ItemData.UNKNOWN, takenTransactionItem.from);
		assertEquals(99, takenTransactionItem.to);
		// should update the to item
		assertEquals(99, economyService.data.stocks[99][0].from);
		assertEquals(ItemData.UNKNOWN, economyService.data.stocks[99][0].to);
	}

	@Test
	void test_finishTransaction() {
		// given
		final EconomyService economyService = createEconomyService();
		economyService.createItems(1, 1, ItemType.WOOD, 9, 9);
		final ItemData transactionItem = economyService.findTransaction(1, 1).get();
		final ItemData takenTransactionItem = economyService.takeTransaction(transactionItem);

		// when
		economyService.finishTransaction(takenTransactionItem);

		// should block to item
		assertEquals(ItemState.BLOCKED, economyService.data.stocks[99][0].state);
	}

	@Test
	void test_scenario() {
		///
		// EXAMPLE State From To:
		// ----------------------
		// A = Stock at index 0
		// B = Person
		// C = Stock at index 1
		// ---------------------- createItem
		// A --> SUPPLY 0 -1
		// B --> null
		// C --> DEMAND 1 -1
		// ---------------------- createItem
		// A --> SUPPLY 0 1
		// B --> null
		// C --> DEMAND 1 0
		// ---------------------- findTransaction
		// A --> SUPPLIED 0 1
		// B --> TRANSACTION 0 1
		// C --> DEMANDED 1 0
		// ---------------------- takeTransaction
		// A --> null
		// B --> TRANSACTION -1 1
		// C --> DEMANDED 1 -1
		// ---------------------- finishTransaction
		// A --> null
		// B --> null
		// C --> BLOCKED 1 -1
		// ----------------------
		///

		final EconomyService economyService = createEconomyService();
		economyService.createItem(0, 0, ItemType.STONE, ItemState.SUPPLY);
		economyService.createItem(1, 0, ItemType.STONE, ItemState.DEMAND);

		economyService.update();

		// findTransaction
		final Optional<ItemData> transactionItemOptional = economyService.findTransaction(2, 0);
		assertTrue(transactionItemOptional.isPresent());
		final ItemData transactionItemA = transactionItemOptional.get();
		assertEquals(0, economyService.data.stocks[0][0].from);
		assertEquals(1, economyService.data.stocks[0][0].to);
		assertEquals(ItemState.SUPPLIED, economyService.data.stocks[0][0].state);
		assertEquals(0, transactionItemA.from);
		assertEquals(1, transactionItemA.to);
		assertEquals(ItemState.TRANSACTION, transactionItemA.state);
		assertEquals(1, economyService.data.stocks[1][0].from);
		assertEquals(0, economyService.data.stocks[1][0].to);
		assertEquals(ItemState.DEMANDED, economyService.data.stocks[1][0].state);

		// takeTransaction
		final ItemData transactionItemB = economyService.takeTransaction(transactionItemA);
		assertEquals(0, economyService.data.stocks[0].length);
		assertEquals(ItemData.UNKNOWN, transactionItemB.from);
		assertEquals(1, transactionItemB.to);
		assertEquals(1, economyService.data.stocks[1][0].from);
		assertEquals(ItemData.UNKNOWN, economyService.data.stocks[1][0].to);
		assertEquals(ItemState.DEMANDED, economyService.data.stocks[1][0].state);

		// finishTransaction
		economyService.finishTransaction(transactionItemB);
		assertEquals(0, economyService.data.stocks[0].length);
		assertEquals(1, economyService.data.stocks[1][0].from);
		assertEquals(ItemData.UNKNOWN, economyService.data.stocks[1][0].to);
		assertEquals(ItemState.BLOCKED, economyService.data.stocks[1][0].state);

	}

	private EconomyService createEconomyService() {
		final Camera camera = Camera.createDummy(100, 100, new PixelGrid(1), new TileGrid(1, 1), false);
		final WorldService worldService = WorldService.createDummy(this.tileGrid, camera);
		final PathfinderService pathfinderService = PathfinderService.create(this.tileGrid);
		final AreaService areaService = AreaService.create(worldService, pathfinderService);
		return EconomyService.createDummy(camera, worldService, areaService);
	}
}
