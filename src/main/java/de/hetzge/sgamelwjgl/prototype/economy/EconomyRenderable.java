package de.hetzge.sgamelwjgl.prototype.economy;

import com.badlogic.gdx.graphics.Color;

import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.graphics.base.Camera;
import de.hetzge.sgamelwjgl.graphics.base.Camera.Viewport;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.graphics.base.TextureRegion;
import de.hetzge.sgamelwjgl.prototype.Assets;
import de.hetzge.sgamelwjgl.prototype.world.WorldService;

public final class EconomyRenderable {

	private static final float ITEM_SIZE = 8f;
	private static final float FULL_COLOR = new Color(1.0f, 1.0f, 1.0f, 1.0f).toFloatBits();

	private final EconomyData economy;
	private final Assets assets;
	private final Camera camera;
	private final WorldService worldService;

	EconomyRenderable(EconomyData economy, Assets assets, Camera camera, WorldService worldService) {
		this.economy = economy;
		this.assets = assets;
		this.camera = camera;
		this.worldService = worldService;
	}

	public void draw(SpriteBatch spriteBatch) {
		final TileGrid tileGrid = this.worldService.getTileGrid();
		final Viewport viewport = this.camera.getViewport();
		final float tileSize = viewport.pixelGrid.getTileSize();

		viewport.forEach((x, y) -> {
			final int index = tileGrid.index(x, y);
			final ItemData[] stock = this.economy.stocks[index];

			for (final ItemData item : stock) {
				if (item.state.real) {
					spriteBatch.add(textureRegeion(item), ((x - (y * 0.5f)) * tileSize), ((-y - this.worldService.getHeight(x, y)) * tileSize) + (ITEM_SIZE / 2f), 0f, ITEM_SIZE, ITEM_SIZE, FULL_COLOR);
				}
			}

		}, 3);
	}

	public TextureRegion textureRegeion(ItemData item) {
		final TextureRegion textureRegion;
		if (item.type == ItemType.WOOD) {
			textureRegion = this.assets.textureRegions.tree;
		} else if (item.type == ItemType.STONE) {
			textureRegion = this.assets.textureRegions.stone;
		} else {
			throw new IllegalStateException();
		}
		return textureRegion;
	}

	public static EconomyRenderable createDummy(EconomyData economy, Camera camera, WorldService worldService) {
		return new EconomyRenderable(economy, null, camera, worldService);
	}

	public static EconomyRenderable create(EconomyData economy, Assets assets, Camera camera, WorldService worldService) {
		return new EconomyRenderable(economy, assets, camera, worldService);
	}

}
