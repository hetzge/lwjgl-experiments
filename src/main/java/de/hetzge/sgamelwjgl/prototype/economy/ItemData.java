package de.hetzge.sgamelwjgl.prototype.economy;

public class ItemData {
	public static final int UNKNOWN = -1;

	final ItemType type;
	final ItemState state;

	/** the actual position */
	final int from;

	/** the target/source position */
	final int to;

	public ItemData(ItemType type, ItemState state, int from, int to) {
		this.type = type;
		this.state = state;
		this.from = from;
		this.to = to;
	}

	public ItemData withFrom(int from) {
		return new ItemData(this.type, this.state, from, this.to);
	}

	public ItemData withTo(int to) {
		return new ItemData(this.type, this.state, this.from, to);
	}

	public ItemData withState(ItemState state) {
		return new ItemData(this.type, state, this.from, this.to);
	}

	public boolean isReal() {
		return this.state.real || ((this.state == ItemState.TRANSACTION) && (this.from == -1));
	}

	public boolean isFrom(int index) {
		return this.from == index;
	}

	public boolean isTo(int index) {
		return this.to == index;
	}

	@Override
	public int hashCode() {
		return this.from;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ItemData other = (ItemData) obj;
		if (this.from != other.from) {
			return false;
		}
		if (this.state != other.state) {
			return false;
		}
		if (this.to != other.to) {
			return false;
		}
		return this.type == other.type;
	}

	@Override
	public String toString() {
		return "ItemData [type=" + this.type + ", state=" + this.state + ", from=" + this.from + ", to=" + this.to + "]";
	}
}
