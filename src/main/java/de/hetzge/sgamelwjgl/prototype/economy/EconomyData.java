package de.hetzge.sgamelwjgl.prototype.economy;

import java.util.Arrays;

import de.hetzge.sgamelwjgl.prototype.util.ArrayUtil;

final class EconomyData {
	ItemData[][] stocks;

	EconomyData(ItemData[][] stocks) {
		this.stocks = stocks;
	}

	public void replace(ItemData old, ItemData value) {
		final ItemData[] stock = this.stocks[value.from];
		final int index = ArrayUtil.indexOf(stock, old);
		if (index < 0) {
			throw new IllegalStateException(String.format("The stock %s does not contain the item %s", Arrays.toString(stock), old));
		}
		stock[index] = value;
	}
}
