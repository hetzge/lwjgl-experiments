package de.hetzge.sgamelwjgl.prototype;

import java.io.File;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

import de.hetzge.sgamelwjgl.DefaultShader;
import de.hetzge.sgamelwjgl.graphics.base.Texture;
import de.hetzge.sgamelwjgl.graphics.base.TextureRegion;
import de.hetzge.sgamelwjgl.graphics.font.Font;
import de.hetzge.sgamelwjgl.prototype.base.Direction;
import de.hetzge.sgamelwjgl.prototype.world.Ground;
import de.hetzge.sgamelwjgl.prototype.world.WorldTextureMapping;

public final class Assets {

	public final DefaultShader shader;
	public final WorldTextureMapping worldTextureMapping;
	public final TextureRegions textureRegions;
	public final Font font;

	public Assets() {
		final Texture texture = Texture.create("/home/hetzge/git/lwjgl-experiments/example_ground_texture.png", false, false);

		final WorldTextureMapping textureMapping = new WorldTextureMapping(texture);
		textureMapping.register(Ground.GRASS, 0f, 0f, 16f);
		textureMapping.register(Ground.WATER, 0f, 64f, 16f);
		textureMapping.register(Ground.DESERT, 0f, 128f, 16f);
		textureMapping.register(Ground.LAVA, 0f, 192f, 16f);
		textureMapping.register(Ground.MOUNTAIN, 0f, 256f, 16f);

		this.shader = DefaultShader.load();
		this.worldTextureMapping = textureMapping;
		this.textureRegions = new TextureRegions(texture);

		this.font = Font.create(new File("/home/hetzge/Downloads/VCR_OSD_MONO_1.001.ttf"), new FreeTypeFontParameter() {
			{
				this.size = 12;
				this.color = Color.BLUE;
				this.borderColor = Color.RED;
				this.borderWidth = 0;
				this.characters = FreeTypeFontGenerator.DEFAULT_CHARS;
			}
		});
	}

	public static class TextureRegions {
		public final Texture texture;
		public final TextureRegion tree;
		public final TextureRegion stone;
		public final TextureRegion marker;
		public final TextureRegion marker2;
		public final TextureRegion[] person;
		public final TextureRegion building;

		public TextureRegions(Texture texture) {
			this.texture = texture;
			this.tree = TextureRegion.create(texture, 192, 0, 32, 64);
			this.stone = TextureRegion.create(texture, 224, 0, 32, 32);
			this.marker = TextureRegion.create(texture, 240, 32, 16, 16);
			this.marker2 = TextureRegion.create(texture, 224, 48, 16, 16);

			this.person = new TextureRegion[Direction.values().length];
			this.person[Direction.EAST.ordinal()] = TextureRegion.create(texture, 256, 0, 16, 16);
			this.person[Direction.SOUTH_EAST.ordinal()] = TextureRegion.create(texture, 256, 16, 16, 16);
			this.person[Direction.SOUTH.ordinal()] = TextureRegion.createFlipped(texture, 256, 16, 16, 16);
			this.person[Direction.WEST.ordinal()] = TextureRegion.createFlipped(texture, 256, 0, 16, 16);
			this.person[Direction.NORTH_WEST.ordinal()] = TextureRegion.createFlipped(texture, 256, 32, 16, 16);
			this.person[Direction.NORTH.ordinal()] = TextureRegion.create(texture, 256, 32, 16, 16);
			
			this.building = TextureRegion.create(texture, 256 + 16, 0, 48, 48);
		}
	}
}
