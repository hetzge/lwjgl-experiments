package de.hetzge.sgamelwjgl.prototype.environment;

public enum Resource {
	NONE, WOOD, STONE, FISH;
}