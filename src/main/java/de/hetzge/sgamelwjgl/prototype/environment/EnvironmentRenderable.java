package de.hetzge.sgamelwjgl.prototype.environment;

import com.badlogic.gdx.graphics.Color;

import de.hetzge.sgamelwjgl.graphics.base.Camera;
import de.hetzge.sgamelwjgl.graphics.base.Camera.Viewport;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.graphics.base.TextureRegion;
import de.hetzge.sgamelwjgl.prototype.Assets;
import de.hetzge.sgamelwjgl.prototype.world.WorldService;

public final class EnvironmentRenderable {

	private static final float FULL_COLOR = new Color(1.0f, 1.0f, 1.0f, 1.0f).toFloatBits();

	private final EnvironmentData environment;
	private final Assets assets;
	private final Camera camera;
	private final WorldService worldService;

	private EnvironmentRenderable(EnvironmentData environment, Assets assets, Camera camera, WorldService worldService) {
		this.environment = environment;
		this.assets = assets;
		this.camera = camera;
		this.worldService = worldService;
	}

	public void draw(SpriteBatch spriteBatch) {
		final Viewport viewport = this.camera.getViewport();
		final float tileSize = viewport.pixelGrid.getTileSize();
		final float halfTileSize = tileSize / 2f;
		viewport.forEach((x, y) -> {
			final TextureRegion textureRegion;
			if (this.environment.resources[this.environment.tileGrid.index(x, y)] == Resource.WOOD) {
				textureRegion = this.assets.textureRegions.tree;
			} else if (this.environment.resources[this.environment.tileGrid.index(x, y)] == Resource.STONE) {
				textureRegion = this.assets.textureRegions.stone;
			} else {
				return;
			}

			final int width = textureRegion.getOriginalWidth();
			final int height = textureRegion.getOriginalHeight();

			spriteBatch.add(textureRegion, (x * tileSize) - (y * halfTileSize), ((-y * tileSize) + (this.worldService.getHeight(x, y) * tileSize)) - halfTileSize, 0f, width, height, FULL_COLOR);
		}, 3);
	}

	public static EnvironmentRenderable create(EnvironmentData environment, Assets assets, Camera camera, WorldService worldService) {
		return new EnvironmentRenderable(environment, assets, camera, worldService);
	}
}
