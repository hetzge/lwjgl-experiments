package de.hetzge.sgamelwjgl.prototype.environment;

import de.hetzge.sgamelwjgl.base.TileGrid;

final class EnvironmentData {

	final TileGrid tileGrid;
	final Resource[] resources;
	final byte[] resourceCounts;

	EnvironmentData(TileGrid tileGrid, Resource[] resources, byte[] resourceCounts) {
		this.tileGrid = tileGrid;
		this.resources = resources;
		this.resourceCounts = resourceCounts;
	}
}
