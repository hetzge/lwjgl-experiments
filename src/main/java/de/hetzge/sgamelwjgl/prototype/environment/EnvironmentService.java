package de.hetzge.sgamelwjgl.prototype.environment;

import java.util.Arrays;

import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.graphics.base.Camera;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.prototype.Assets;
import de.hetzge.sgamelwjgl.prototype.world.Ground;
import de.hetzge.sgamelwjgl.prototype.world.WorldService;

public class EnvironmentService {

	private final EnvironmentData data;
	private final EnvironmentRenderable renderable;
	private final WorldService worldService;

	private EnvironmentService(EnvironmentData data, EnvironmentRenderable renderable, WorldService worldService) {
		this.data = data;
		this.renderable = renderable;
		this.worldService = worldService;
	}

	public void draw(SpriteBatch spriteBatch) {
		this.renderable.draw(spriteBatch);
	}

	public static EnvironmentService create(Assets assets, Camera camera, WorldService worldService) {
		final EnvironmentData environment = generateData(worldService.getTileGrid(), worldService);
		final EnvironmentRenderable environmentRenderable = EnvironmentRenderable.create(environment, assets, camera, worldService);

		return new EnvironmentService(environment, environmentRenderable, worldService);
	}

	private static EnvironmentData generateData(TileGrid tileGrid, WorldService worldService) {
		final Resource[] resources = new Resource[tileGrid.getLength()];
		final byte[] resourceCounts = new byte[tileGrid.getLength()];

		Arrays.fill(resources, Resource.NONE);

		for (int i = 0; i < resources.length; i++) {
			final int x = tileGrid.x(i);
			final int y = tileGrid.y(i);

			if (!worldService.isCollision(x, y)) {
				if (Math.random() > 0.95d) {
					if (worldService.getGround(x, y) == Ground.GRASS) {
						resources[i] = Resource.WOOD;
						worldService.setCollision(x, y, true);
					}
				} else if (Math.random() > 0.90d) {
					resources[i] = Resource.STONE;
					worldService.setCollision(x, y, true);
				}
			}
		}

		return new EnvironmentData(tileGrid, resources, resourceCounts);
	}
}
