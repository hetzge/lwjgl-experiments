package de.hetzge.sgamelwjgl.prototype.building;

import org.joml.Vector2i;

import de.hetzge.sgamelwjgl.base.PositionPredicate;
import de.hetzge.sgamelwjgl.prototype.area.AreaService;
import de.hetzge.sgamelwjgl.prototype.person.PersonData;
import de.hetzge.sgamelwjgl.prototype.world.Ground;
import de.hetzge.sgamelwjgl.prototype.world.WorldService;

public final class BuildingFlattenService {

	final BuildingsService buildingService;
	final AreaService areaService;
	final WorldService worldService;

	BuildingFlattenService(BuildingsService buildingService, AreaService areaService, WorldService worldService) {
		this.buildingService = buildingService;
		this.areaService = areaService;
		this.worldService = worldService;
	}

	public static BuildingFlattenService create(BuildingsService buildingService, AreaService areaService, WorldService worldService) {
		return new BuildingFlattenService(buildingService, areaService, worldService);
	}

	public Vector2i findFlattenPosition(int x, int y, PersonData person) {
		final BuildingData currentBuilding = this.buildingService.building(x, y);
		if (currentBuilding != null) {
			final Vector2i position = findFlattenPosition(currentBuilding, person);
			if (position != null) {
				return position;
			}
		}
		for (final BuildingData building : this.buildingService.data.buildings) {
			if (this.areaService.isSameArea(x, y, building.x, building.y)) {
				final Vector2i position = findFlattenPosition(building, person);
				if (position != null) {
					return position;
				}
			}
		}
		return null;
	}

	public Vector2i findFlattenPosition(BuildingData building, PersonData person) {
		if (building.state == BuildingState.FLATTEN) {
			/*
			 * TODO no other person of this building is at position (then increase max
			 * persons)
			 */
			if ((building.personIds.length == 0) || building.contains(person)) {
				final float referenceHeight = getFlattenReferenceHeight(building);
				final Vector2i position = building.type.tileGrid.findPosition(needflattenPredicate(building, referenceHeight));
				if (position != null) {
					return position.add(building.x, building.y);
				}
			}
		}
		return null;
	}

	public boolean isFlattened(BuildingData building) {
		final float referenceHeight = getFlattenReferenceHeight(building);
		return building.type.tileGrid.noneMatch(needflattenPredicate(building, referenceHeight));
	}

	private PositionPredicate needflattenPredicate(BuildingData building, float referenceHeight) {
		return (ax, ay) -> {
			final boolean isBuildingCollision = building.type.collisionMask.test(ax, ay);
			final boolean isNotReferenceHeight = this.worldService.getHeight(building.x + ax, building.y + ay) != referenceHeight;
			final boolean isNotFlattenedGround = this.worldService.getGround(building.x + ax, building.y + ay) != Ground.FLATTENED;
			return isBuildingCollision && (isNotReferenceHeight || isNotFlattenedGround);
		};
	}

	public float getFlattenReferenceHeight(int x, int y) {
		final BuildingData building = this.buildingService.building(x, y);
		if (building != null) {
			return getFlattenReferenceHeight(building);
		} else {
			return this.worldService.getHeight(x, y);
		}
	}

	public float getFlattenReferenceHeight(BuildingData building) {
		final Vector2i referenceHeightPosition = building.type.tileGrid.findPosition((fx, fy) -> {
			return building.type.collisionMask.test(fx, fy);
		});
		if (referenceHeightPosition != null) {
			return this.worldService.getHeight(building.x + referenceHeightPosition.x, building.y + referenceHeightPosition.y);
		} else {
			return this.worldService.getHeight(building.x, building.y);
		}
	}

	public void flatten(int x, int y) {
		final float currentHeight = this.worldService.getHeight(x, y);
		final float referenceHeight = getFlattenReferenceHeight(x, y);
		final float progress = currentHeight < referenceHeight ? +0.1f : -0.1f;
		this.worldService.setHeight(x, y, progress > 0f ? Math.min(currentHeight + progress, referenceHeight) : Math.max(currentHeight + progress, referenceHeight));
		this.worldService.setGround(x, y, Ground.FLATTENED);
	}
}
