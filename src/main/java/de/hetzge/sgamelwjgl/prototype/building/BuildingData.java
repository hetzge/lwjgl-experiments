package de.hetzge.sgamelwjgl.prototype.building;

import java.util.Arrays;

import com.badlogic.gdx.math.MathUtils;

import de.hetzge.sgamelwjgl.prototype.person.PersonData;
import de.hetzge.sgamelwjgl.prototype.util.ArrayUtil;

public final class BuildingData {

	public static final int MAX_PROGRESS = 1000;

	public final int id;

	// x/y = top left corner
	int x;
	int y;
	BuildingType type;
	BuildingState state;
	Long[] personIds;
	long startFrameId;
	long endFrameId;
	int progress;

	public BuildingData(int id, int x, int y, BuildingType type, BuildingState state, Long[] personIds, long startFrameId, long endFrameId, int progress) {
		this.id = id;
		this.x = x;
		this.y = y;
		this.type = type;
		this.state = state;
		this.personIds = personIds;
		this.startFrameId = startFrameId;
		this.endFrameId = endFrameId;
		this.progress = progress;
	}

	public void manipulateProgress(int value, int max) {
		this.progress = MathUtils.clamp(this.progress + value, 0, Math.min(max, MAX_PROGRESS));
	}

	public boolean isMaxProgress() {
		return this.progress == MAX_PROGRESS;
	}

	public void enter(PersonData person) {
		if (person.isInBuilding()) {
			throw new IllegalStateException(String.format("Person %s is already in a building. Can't enter %s", person, this));
		}
		if (contains(person)) {
			throw new IllegalStateException(String.format("Person %s already is in the building %s", person, this));
		}
		this.personIds = ArrayUtil.add(this.personIds, person.id);
		person.setBuilding(this);
	}

	public void leave(PersonData person) {
		if (!person.isInBuilding(this)) {
			throw new IllegalStateException(String.format("Persin %s can't leave the building %s because it is not in it", person, this));
		}
		if (!contains(person)) {
			throw new IllegalStateException(String.format("Person %s can't leave building it is not contained in %s", person, this));
		}
		this.personIds = ArrayUtil.remove(this.personIds, ArrayUtil.indexOf(this.personIds, person.id));
		person.unsetBuilding();
	}

	public boolean contains(PersonData person) {
		for (int i = 0; i < this.personIds.length; i++) {
			if (this.personIds[i] == person.id) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format("BuildingData [id=%s, x=%s, y=%s, type=%s, state=%s, personIds=%s, startFrameId=%s, endFrameId=%s]", this.id, this.x, this.y, this.type, this.state, Arrays.toString(this.personIds), this.startFrameId, this.endFrameId);
	}
}
