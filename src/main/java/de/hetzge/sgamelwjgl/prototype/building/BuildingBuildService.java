package de.hetzge.sgamelwjgl.prototype.building;

import org.joml.Vector2i;
import org.tinylog.Logger;

import de.hetzge.sgamelwjgl.prototype.area.AreaService;
import de.hetzge.sgamelwjgl.prototype.economy.EconomyService;
import de.hetzge.sgamelwjgl.prototype.economy.ItemState;
import de.hetzge.sgamelwjgl.prototype.person.PersonData;
import de.hetzge.sgamelwjgl.prototype.world.WorldService;

public final class BuildingBuildService {

	final WorldService worldService;
	final AreaService areaService;
	final BuildingsService buildingsService;
	final EconomyService economyService;

	public static BuildingBuildService create(WorldService worldService, AreaService areaService, BuildingsService buildingsService, EconomyService economyService) {
		return new BuildingBuildService(worldService, areaService, buildingsService, economyService);
	}

	BuildingBuildService(WorldService worldService, AreaService areaService, BuildingsService buildingsService, EconomyService economyService) {
		this.worldService = worldService;
		this.areaService = areaService;
		this.buildingsService = buildingsService;
		this.economyService = economyService;
	}

	public void startBuildBuilding(BuildingData building) {
		if (building.state == BuildingState.FLATTEN) {
			// enable collision
			building.type.tileGrid.forEach((ex, ey) -> {
				this.worldService.setCollision(building.x + ex, building.y + ey, building.type.collisionMask.test(ex, ey));
			});
			building.state = BuildingState.BUILD;
		} else {
			Logger.info("Start build building for non flatten building {}", building);
		}
	}

	public void finalizeBuildBuilding(BuildingData building) {
		if (building.state == BuildingState.BUILD) {
			building.state = BuildingState.DONE;
		}
	}

	public Vector2i findBuildPosition(int x, int y, PersonData person) {
		final BuildingData currentBuilding = this.buildingsService.building(x, y);
		if (currentBuilding != null) {
			final Vector2i position = findBuildPosition(currentBuilding, person);
			if (position != null) {
				return position;
			}
		}
		for (final BuildingData building : this.buildingsService.data.buildings) {
			if (this.areaService.isSameArea(x, y, building.x, building.y)) {
				final Vector2i position = findBuildPosition(building, person);
				if (position != null) {
					return position;
				}
			}
		}
		return null;
	}

	private Vector2i findBuildPosition(BuildingData building, PersonData person) {
		if (building.state == BuildingState.BUILD) {
			/*
			 * TODO no other person of this building is at position (then increase max
			 * persons)
			 */
			if ((building.personIds.length == 0) || building.contains(person)) {
				if (building.progress < maxBuildProgress(building)) {
					return new Vector2i(building.x, building.y);
				}
			}
		}
		return null;
	}

	private int maxBuildProgress(BuildingData building) {
		final int totalRequiredItemCount = building.type.buildRequirements.size();
		final int availableItemCount = building.type.buildRequirements.stream().distinct().mapToInt(requiredItemType -> {
			final Vector2i demandStockPosition = building.type.demandStockPositions.get(requiredItemType);
			return this.economyService.itemCount(demandStockPosition.x, demandStockPosition.y, requiredItemType, ItemState.BLOCKED);
		}).sum();

		return (int) (((float) availableItemCount / totalRequiredItemCount) * BuildingData.MAX_PROGRESS);
	}

	public void build(int x, int y) {
		final BuildingData building = this.buildingsService.building(x, y);
		if (building != null) {
			building.manipulateProgress(100, maxBuildProgress(building));
			if (building.isMaxProgress()) {
				finalizeBuildBuilding(building);
			}
		}
	}
}
