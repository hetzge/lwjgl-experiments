package de.hetzge.sgamelwjgl.prototype.building;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.joml.Vector2f;
import org.joml.Vector2i;

import de.hetzge.sgamelwjgl.base.PositionPredicate;
import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.prototype.base.Mask;
import de.hetzge.sgamelwjgl.prototype.economy.ItemType;

public enum BuildingType {

	MAIN(Masks.MAIN, new Vector2i(1, 3), new Vector2f(0.5f, 2.25f), Collections.emptyMap(), Collections.emptyMap(), List.of(ItemType.WOOD, ItemType.STONE)), //
	DEMANDER(Masks.MAIN, new Vector2i(1, 3), new Vector2f(0.5f, 2.25f), Map.of(ItemType.WOOD, new Vector2i(2, 3)), Collections.emptyMap(), List.of(ItemType.WOOD)), //
	SUPPLIER(Masks.MAIN, new Vector2i(1, 3), new Vector2f(0.5f, 2.25f), Collections.emptyMap(), Map.of(ItemType.WOOD, new Vector2i(2, 3)), List.of(ItemType.WOOD));

	public final TileGrid tileGrid;
	public final PositionPredicate reservedMask;
	public final PositionPredicate collisionMask;
	public final Vector2i door;
	public final Map<ItemType, Vector2i> demandStockPositions;
	public final Map<ItemType, Vector2i> supplyStockPositions;
	public final List<ItemType> buildRequirements;
	public final Vector2f offset;

	private BuildingType(String[] mask, Vector2i door, Vector2f offset, Map<ItemType, Vector2i> demandStockPositions, Map<ItemType, Vector2i> supplyStockPositions, List<ItemType> buildRequirements) {
		this(tileGrid(mask), Mask.create('R', mask), Mask.create('C', mask), door, offset, demandStockPositions, supplyStockPositions, buildRequirements);
	}

	private BuildingType(TileGrid tileGrid, PositionPredicate reservedMask, PositionPredicate collisionMask, Vector2i door, Vector2f offset, Map<ItemType, Vector2i> demandStockPositions, Map<ItemType, Vector2i> supplyStockPositions, List<ItemType> buildRequirements) {
		this.tileGrid = tileGrid;
		this.reservedMask = reservedMask;
		this.collisionMask = collisionMask;
		this.door = door;
		this.offset = offset;
		this.demandStockPositions = demandStockPositions;
		this.supplyStockPositions = supplyStockPositions;
		this.buildRequirements = buildRequirements;

		if (!reservedMask.test(door.x, door.y)) {
			throw new IllegalStateException("Door have to be at a reserved tile");
		}
	}
	
	private static TileGrid tileGrid(String[] mask) {
		return new TileGrid(Mask.create(' ', mask).getWidth(), Mask.create(' ', mask).getHeight());
	}

	private static class Masks {
		private static final String[] MAIN = new String[] { //
				"RRRR", //
				"RCCR", //
				"RCCR", //
				"RRRR" //
		};
	}
}
