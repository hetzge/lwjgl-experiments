package de.hetzge.sgamelwjgl.prototype.building;

import de.hetzge.sgamelwjgl.graphics.base.Camera;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.graphics.base.TextureRegion;
import de.hetzge.sgamelwjgl.prototype.Assets;
import de.hetzge.sgamelwjgl.prototype.base.Clock;
import de.hetzge.sgamelwjgl.prototype.world.WorldService;

public final class BuildingsRenderable {

	final BuildingsData data;
	final Assets assets;
	final Camera camera;
	final Clock clock;
	final WorldService worldService;

	private BuildingsRenderable(BuildingsData data, Assets assets, Camera camera, Clock clock, WorldService worldService) {
		this.data = data;
		this.assets = assets;
		this.camera = camera;
		this.clock = clock;
		this.worldService = worldService;
	}

	public void update(SpriteBatch spriteBatch) {
		final int tileSize = this.camera.getViewport().pixelGrid.getTileSize();
		for (final BuildingData building : this.data.buildings) {
			final float tileHeight = this.worldService.getHeight(building.x, building.y);
			final float x = ((building.x - (building.y / 2f)) + building.type.offset.x) * tileSize;
			final float y = ((building.y - tileHeight) + building.type.offset.y) * tileSize;
			if (building.state == BuildingState.DONE) {
				// TODO visible check ??? oder besser nicht ???
				final TextureRegion textureRegion = this.assets.textureRegions.building;
				final int renderWidth = textureRegion.getOriginalWidth();
				final int renderHeight = textureRegion.getOriginalHeight();
				spriteBatch.add(textureRegion, x, -y, 0.0f, renderWidth, renderHeight);
			}
			spriteBatch.add(this.assets.textureRegions.marker2, x, -y - (tileSize / 2f), 1.0f, tileSize, tileSize);
		}
	}

	public static BuildingsRenderable create(BuildingsData data, Assets assets, Camera camera, Clock clock, WorldService worldService) {
		return new BuildingsRenderable(data, assets, camera, clock, worldService);
	}
}
