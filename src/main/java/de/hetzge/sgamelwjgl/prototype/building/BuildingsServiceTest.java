package de.hetzge.sgamelwjgl.prototype.building;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.graphics.base.Camera;
import de.hetzge.sgamelwjgl.prototype.area.AreaService;
import de.hetzge.sgamelwjgl.prototype.base.Clock;
import de.hetzge.sgamelwjgl.prototype.economy.EconomyService;
import de.hetzge.sgamelwjgl.prototype.person.PathfinderService;
import de.hetzge.sgamelwjgl.prototype.world.WorldService;

class BuildingsServiceTest {

	@Test
	void test_building() {

		final TileGrid tileGrid = new TileGrid(10, 10);
		final Camera camera = Camera.create(100, 100);
		final WorldService worldService = WorldService.createDummy(tileGrid, camera);
		final PathfinderService pathfinderService = PathfinderService.create(tileGrid);
		final AreaService areaService = AreaService.create(worldService, pathfinderService);
		final EconomyService economyService = EconomyService.createDummy(camera, worldService, areaService);
		final BuildingsService buildingsService = BuildingsService.create(worldService, areaService, economyService, null, camera, new Clock(60, 0L));

		// Given
		buildingsService.buildBuildingLot(3, 3, BuildingType.MAIN);

		// Then
		assertTrue(BuildingType.MAIN.tileGrid.allMatch((ax, ay) -> {
			// When
			return buildingsService.building(3 + ax, 3 + ay) != null;
		}));

		// Then
		assertTrue(tileGrid.allMatchAroundSize(3, 3, BuildingType.MAIN.tileGrid.getWidth(), BuildingType.MAIN.tileGrid.getHeight(), (ax, ay) -> {
			// When
			return buildingsService.building(ax, ay) == null;
		}));
	}
}
