package de.hetzge.sgamelwjgl.prototype.building;

import java.time.Duration;
import java.util.Arrays;

import org.joml.Vector2i;
import org.tinylog.Logger;

import de.hetzge.sgamelwjgl.graphics.base.Camera;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.prototype.Assets;
import de.hetzge.sgamelwjgl.prototype.area.AreaService;
import de.hetzge.sgamelwjgl.prototype.base.Clock;
import de.hetzge.sgamelwjgl.prototype.economy.EconomyService;
import de.hetzge.sgamelwjgl.prototype.economy.ItemState;
import de.hetzge.sgamelwjgl.prototype.economy.ItemType;
import de.hetzge.sgamelwjgl.prototype.world.WorldService;

public final class BuildingsService {

	final BuildingsData data;
	final WorldService worldService;
	final AreaService areaService;
	final EconomyService economyService;
	final BuildingsRenderable renderable;
	final Clock clock;

	public static BuildingsService create(WorldService worldService, AreaService areaService, EconomyService economyService, Assets assets, Camera camera, Clock clock) {
		final BuildingsData data = new BuildingsData(new BuildingData[0]);
		final BuildingsRenderable renderable = BuildingsRenderable.create(data, assets, camera, clock, worldService);

		return new BuildingsService(data, worldService, areaService, economyService, renderable, clock);
	}

	BuildingsService(BuildingsData data, WorldService worldService, AreaService areaService, EconomyService economyService, BuildingsRenderable renderable, Clock clock) {
		this.data = data;
		this.worldService = worldService;
		this.areaService = areaService;
		this.economyService = economyService;
		this.renderable = renderable;
		this.clock = clock;
	}

	public void update() {
		final long frameId = this.clock.getFrameId();
		for (final BuildingData building : this.data.buildings) {
			if (building.endFrameId == frameId) {

				updateDemand(building);
				updateSupply(building);

				building.startFrameId = frameId;
				building.endFrameId = this.clock.getFrameId(Duration.ofSeconds(10));
			}
		}
	}

	private void updateDemand(BuildingData building) {
		building.type.demandStockPositions.forEach((type, position) -> {
			final int x = building.x + position.x;
			final int y = building.y + position.y;
			while (this.economyService.itemCount(x, y) < 8) {
				this.economyService.createItem(x, y, type, ItemState.DEMAND);
			}
		});
	}

	private void updateSupply(final BuildingData building) {
		building.type.supplyStockPositions.forEach((type, position) -> {
			final int x = building.x + position.x;
			final int y = building.y + position.y;
			if (this.economyService.itemCount(x, y) < 8) {
				System.out.println("update");
				this.economyService.createItem(x, y, type, ItemState.SUPPLY);
			}
		});
	}

	public void draw(SpriteBatch spriteBatch) {
		this.renderable.update(spriteBatch);
	}

	public boolean canBuildBuilding(int x, int y, BuildingType type) {
		return type.tileGrid.noneMatch((ax, ay) -> {
			return (type.collisionMask.test(ax, ay) || type.reservedMask.test(ax, ay)) && (this.worldService.isCollision(x + ax, y + ay) || this.worldService.isReserved(x + ax, y + ay));
		});
	}

	public void buildBuildingLot(int x, int y, BuildingType type) {
		if (!canBuildBuilding(x, y, type)) {
			throw new IllegalStateException(String.format("Can't build building at %sx%s with type '%s'", x, y, type.name()));
		}
		Logger.info("Build building at {}x{} with type '{}'", x, y, type);

		this.data.buildings = Arrays.copyOf(this.data.buildings, this.data.buildings.length + 1);
		this.data.buildings[this.data.buildings.length - 1] = new BuildingData(this.worldService.getTileGrid().index(x, y), x, y, type, BuildingState.FLATTEN, new Long[0], this.clock.getFrameId(), this.clock.getFrameId(Duration.ofSeconds(10)), 0);

		type.tileGrid.forEach((ex, ey) -> {
			this.worldService.setReserved(x + ex, y + ey, type.reservedMask.test(ex, ey));
		});

		// demand items for build
		for (final ItemType requiredItemType : type.buildRequirements) {
			final Vector2i demandStockPosition = type.demandStockPositions.get(requiredItemType);
			this.economyService.createItem(demandStockPosition.x, demandStockPosition.y, requiredItemType, ItemState.BLOCKED); // TODO DEMAND
		}
	}

	public BuildingData building(int x, int y) {
		for (final BuildingData building : this.data.buildings) {
			if (building.type.tileGrid.isValid(x - building.x, y - building.y)) {
				return building;
			}
		}
		return null;
	}

	public BuildingData building(int id) {
		for (final BuildingData building : this.data.buildings) {
			if (building.id == id) {
				return building;
			}
		}
		return null;
	}
}
