package de.hetzge.sgamelwjgl.prototype;

import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;

import com.badlogic.gdx.graphics.Color;

import de.hetzge.sgamelwjgl.DefaultShader.ColorMode;
import de.hetzge.sgamelwjgl.base.PixelGrid;
import de.hetzge.sgamelwjgl.base.TileGrid;
import de.hetzge.sgamelwjgl.graphics.base.Camera;
import de.hetzge.sgamelwjgl.graphics.base.GeometryBuffer;
import de.hetzge.sgamelwjgl.graphics.base.Shape;
import de.hetzge.sgamelwjgl.graphics.base.SpriteBatch;
import de.hetzge.sgamelwjgl.graphics.base.Window;
import de.hetzge.sgamelwjgl.prototype.area.AreaService;
import de.hetzge.sgamelwjgl.prototype.base.Clock;
import de.hetzge.sgamelwjgl.prototype.base.effect.EffectLayer;
import de.hetzge.sgamelwjgl.prototype.building.BuildingBuildService;
import de.hetzge.sgamelwjgl.prototype.building.BuildingFlattenService;
import de.hetzge.sgamelwjgl.prototype.building.BuildingType;
import de.hetzge.sgamelwjgl.prototype.building.BuildingsService;
import de.hetzge.sgamelwjgl.prototype.economy.EconomyService;
import de.hetzge.sgamelwjgl.prototype.environment.EnvironmentService;
import de.hetzge.sgamelwjgl.prototype.person.PathfinderService;
import de.hetzge.sgamelwjgl.prototype.person.PersonType;
import de.hetzge.sgamelwjgl.prototype.person.PersonsService;
import de.hetzge.sgamelwjgl.prototype.world.Marker;
import de.hetzge.sgamelwjgl.prototype.world.MarkerType;
import de.hetzge.sgamelwjgl.prototype.world.WorldService;

public class ApplicationWindow extends Window {

	public static void main(String[] args) {
		new ApplicationWindow().run();
	}

	private Application application;

	public ApplicationWindow() {
		super(800, 600);
	}

	@Override
	protected void init(long windowHandle) {
		this.application = new Application(this);
	}

	@Override
	protected void loop() {
		this.application.loop();
	}

	@Override
	protected void onResize(int width, int height) {
		this.application.onResize(width, height);
	}

	private static class Application {

		private final Camera camera;
		private final ApplicationWindow window;
		private final WorldService worldService;
		private final EnvironmentService environmentService;
		private final Assets assets;
		private final Clock clock;
		private final PersonsService personsService;
		private final SpriteBatch spriteBatch;
		private final GeometryBuffer geometryBuffer;
		private final PathfinderService pathfinderService;
		private final AreaService areaService;
		private final EconomyService economyService;
		private final BuildingsService buildingsService;
		private final BuildingBuildService buildingBuildService;
		private final BuildingFlattenService buildingFlattenService;

		private final EffectLayer effectLayer;

		private boolean showWorld;
		private boolean showWorldMarker;
		private boolean showEnvironment;
		private boolean showPersons;
		private boolean showBuildings;
		private boolean showEconomy;

		public Application(ApplicationWindow window) {
			final TileGrid tileGrid = new TileGrid(100, 100);
			final PixelGrid pixelGrid = new PixelGrid(16);
			this.camera = Camera.create(800, 600, pixelGrid, tileGrid, true);
			this.window = window;

			this.clock = new Clock(30, 0L);
			this.assets = new Assets();
			this.spriteBatch = SpriteBatch.create(1000000);
			this.geometryBuffer = GeometryBuffer.create(Shape.QUAD, 1000000, this.assets.shader.getVertexDescription());
			this.worldService = WorldService.create(this.assets, this.camera);
			this.environmentService = EnvironmentService.create(this.assets, this.camera, this.worldService);
			this.pathfinderService = PathfinderService.create(this.worldService.getTileGrid());
			this.areaService = AreaService.create(this.worldService, this.pathfinderService);
			this.economyService = EconomyService.create(this.assets, this.camera, this.worldService, this.areaService);
			this.buildingsService = BuildingsService.create(this.worldService, this.areaService, this.economyService, this.assets, this.camera, this.clock);
			this.buildingFlattenService = BuildingFlattenService.create(this.buildingsService, this.areaService, this.worldService);
			this.buildingBuildService = BuildingBuildService.create(this.worldService, this.areaService, this.buildingsService, this.economyService);

			this.personsService = PersonsService.create(this.assets, this.camera, this.clock, this.worldService, this.pathfinderService, this.areaService, this.economyService, this.buildingsService, this.buildingFlattenService, this.buildingBuildService);

			this.effectLayer = EffectLayer.create();
			this.effectLayer.add(new MousePositionTileMarkerEffect(this.camera, window.getInput(), this.worldService));

			this.showWorld = true;
			this.showWorldMarker = true;
			this.showEnvironment = true;
			this.showPersons = true;
			this.showBuildings = true;
			this.showEconomy = true;

//			for (int y = 0; y < 100; y++) {
//				for (int x = 0; x < 100; x++) {
//					this.worldService.setFogOfWar(x, y, 0.0f);
//				}
//			}

			this.worldService.getTileGrid().forEach((x, y) -> {
				final BuildingType type = BuildingType.DEMANDER;
				if (this.buildingsService.canBuildBuilding(x, y, type)) {
					this.buildingsService.buildBuildingLot(x, y, type);
				}
			});

			for (int i = 0; i < 2; i++) {
				for (int j = 0; j < 2; j++) {
					if (!this.worldService.isCollision(3 + i, 3 + j)) {
						if (this.personsService.canCreatePerson(3 + i, 3 + j)) {
							this.personsService.createPerson(3 + i, 3 + j, PersonType.FLATTENER);
						}
					}
				}
			}
			for (int i = 0; i < 2; i++) {
				for (int j = 0; j < 2; j++) {
					if (!this.worldService.isCollision(8 + i, 8 + j)) {
						if (this.personsService.canCreatePerson(8 + i, 8 + j)) {
							this.personsService.createPerson(8 + i, 8 + j, PersonType.BUILDER);
						}
					}
				}
			}

//			final Vector2i buildingPosition = this.worldService.getTileGrid().findPosition((x, y) -> this.buildingService.canBuildBuilding(x, y, BuildingType.MAIN));
//			if(buildingPosition != null) {
//				this.buildingService.buildBuilding(buildingPosition.x, buildingPosition.y, BuildingType.MAIN);
//			}

//			for (int i = 0; i < 1000; i++) {
//				this.economyService.createItem((int) (Math.random() * 100), (int) (Math.random() * 100), ItemType.WOOD, 10, 10);
//			}

			this.camera.setCameraPosition(new Vector3f(400f, 400f, 0f));
			this.assets.font.addText("123abcfklgjfdjk", 0f, 0f);

			// https://stackoverflow.com/a/32224290/7662651 glMapBuffer ?
			// https://hacksoflife.blogspot.com/2015/06/glmapbuffer-no-longer-cool.html
		}

		public void onResize(int width, int height) {
			this.camera.setupScreen(width, height);
		}

		public void loop() {

			this.clock.tick();
			this.economyService.update();

			this.camera.getViewport().forEach((x, y) -> {
				if (this.worldService.isCollision(x, y)) {
					this.worldService.setMarker(x, y, new Marker(MarkerType.A, new Color(1f, 0f, 0f, 1f)));
				} else {
//					this.worldService.setLabel(x, y, String.valueOf(this.areaService.getGlobalArea(x, y)));
				}
			}, 0);

			final WindowInput input = this.window.getInput();
			if (input.isKeyPressed(GLFW.GLFW_KEY_UP)) {
				this.camera.move(0.0f, -1.0f);
			}
			if (input.isKeyPressed(GLFW.GLFW_KEY_DOWN)) {
				this.camera.move(0.0f, 1.0f);
			}
			if (input.isKeyPressed(GLFW.GLFW_KEY_LEFT)) {
				this.camera.move(-1.0f, 0.0f);
			}
			if (input.isKeyPressed(GLFW.GLFW_KEY_RIGHT)) {
				this.camera.move(1.0f, 0.0f);
			}
			if (input.isKeyPressed(GLFW.GLFW_KEY_PAGE_DOWN)) {
				this.camera.zoom(1.0f);
			}
			if (input.isKeyPressed(GLFW.GLFW_KEY_PAGE_UP)) {
				this.camera.zoom(-1.0f);
			}
			if (input.isKeyUp(GLFW.GLFW_KEY_F1)) {
				this.showWorld = !this.showWorld;
			}
			if (input.isKeyUp(GLFW.GLFW_KEY_F2)) {
				this.showWorldMarker = !this.showWorldMarker;
			}
			if (input.isKeyUp(GLFW.GLFW_KEY_F3)) {
				this.showEnvironment = !this.showEnvironment;
			}
			if (input.isKeyUp(GLFW.GLFW_KEY_F5)) {
				this.showPersons = !this.showPersons;
			}
			if (input.isKeyUp(GLFW.GLFW_KEY_F6)) {
				this.showBuildings = !this.showBuildings;
			}
			if (input.isKeyUp(GLFW.GLFW_KEY_F7)) {
				this.showEconomy = !this.showEconomy;
			}

			this.camera.update();

			this.personsService.update();
			this.buildingsService.update();

			if (this.showWorld) {
				this.worldService.draw(this.camera.getViewport());
			}

			this.effectLayer.render();

			if (this.showPersons) {
				this.personsService.draw(this.spriteBatch);
			}
			if (this.showBuildings) {
				this.buildingsService.draw(this.spriteBatch);
			}
			if (this.showEnvironment) {
				this.environmentService.draw(this.spriteBatch);
			}
			if (this.showEconomy) {
				this.economyService.draw(this.spriteBatch);
			}

			draw();

			if (this.showWorldMarker) {
				this.worldService.drawInterface();
				this.worldService.drawFont();
			}

			// https://www.informit.com/articles/article.aspx?p=328646&seqNum=6

//			this.assets.shader.use(ColorMode.MULTIPLY, this.camera.getCombinedMatrix());
//			final GeometryBuffer buffer = GeometryBuffer.create(Shape.QUAD, 5000, this.assets.shader.getVertexDescription());
//			this.assets.font.render(buffer);
//			this.assets.font.bind();
//			buffer.flush();
//			buffer.draw();
		}

		private void draw() {
			this.assets.shader.use(ColorMode.REPLACE, this.camera.getCombinedMatrix());

			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

			this.assets.textureRegions.texture.bind();
			this.spriteBatch.render(this.geometryBuffer);
			this.geometryBuffer.flush();
			this.geometryBuffer.draw();
		}
	}
}
