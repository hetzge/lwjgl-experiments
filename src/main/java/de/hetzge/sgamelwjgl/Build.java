package de.hetzge.sgamelwjgl;

import java.io.File;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType.Bitmap;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType.Face;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType.Library;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;

import de.hetzge.sgamelwjgl.base.EditableTexture;

public class Build {

	public static void main(String[] args) {
		System.out.println("Run build");

		generateFontTextures(Assets.FONT_FILE, Assets.FONT_SIZE, Assets.FONT_PREFIX);
		packTextures();

		System.out.println("Build finished");
	}

	private static void packTextures() {
		final Settings settings = new TexturePacker.Settings() {
			{
				this.square = true;
			}
		};

		new File("generated/packed.atlas").delete();
		new File("generated/packed.png").delete();
		TexturePacker.process(settings, "raw", "generated", "packed");
	}

	private static void generateFontTextures(File fontFile, int fontSize, String fontPrefix) {

		Library library = null;
		Face face = null;
		try {

			library = FreeType.initFreeType();
			face = library.newFace(new FileHandle(fontFile), 0);
			face.setPixelSizes(0, fontSize);

			int i = 0;
			for (char c = 0; c < 128; c++) {
				if (face.loadChar(c, FreeType.FT_LOAD_RENDER)) {
					final Bitmap bitmap = face.getGlyph().getBitmap();
					final int width = bitmap.getWidth();
					final int height = bitmap.getRows();

					if ((width > 0) && (height > 0)) {
						final EditableTexture editableTexture = EditableTexture.create(width, height);
						for (int y = 0; y < height; y++) {
							for (int x = 0; x < width; x++) {
								final byte red = bitmap.getBuffer().get((y * width) + x);
								editableTexture.set(x, y, red, (byte) 0, red, red);
							}
						}
						final File file = new File("raw/" + fontPrefix + i + ".png");
						editableTexture.save(file);
					}

					System.out.println(String.format("Loaded '%s'", c));
				} else {
					System.out.println(String.format("Failed to load '%s'", c));
				}
				i++;
			}

		} finally {
			if (face != null) {
				face.dispose();
			}
			if (library != null) {
				library.dispose();
			}
		}
	}

}
