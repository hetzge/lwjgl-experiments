package de.hetzge.sgamelwjgl;

import java.io.File;
import java.util.List;

import de.hetzge.sgamelwjgl.base.Orientation;
import de.hetzge.sgamelwjgl.entity.BuildingState;
import de.hetzge.sgamelwjgl.entity.BuildingType;
import de.hetzge.sgamelwjgl.entity.ItemType;
import de.hetzge.sgamelwjgl.entity.PersonState;
import de.hetzge.sgamelwjgl.entity.PersonType;
import de.hetzge.sgamelwjgl.entity.ResourceType;
import de.hetzge.sgamelwjgl.entity.TileType;
import de.hetzge.sgamelwjgl.entity.WayTileType;
import de.hetzge.sgamelwjgl.graphics.base.Animation;
import de.hetzge.sgamelwjgl.graphics.base.Texture;
import de.hetzge.sgamelwjgl.graphics.base.TextureAtlas;
import de.hetzge.sgamelwjgl.graphics.base.TextureRegion;
import de.hetzge.sgamelwjgl.graphics.font.FontSet;
import de.hetzge.sgamelwjgl.tile.AutoTileDefinition;
import de.hetzge.sgamelwjgl.tile.AutoTilesets;
import de.hetzge.sgamelwjgl.tile.TileTypeDefinition;

public final class Assets {

	public static final File FONT_FILE = new File("/home/hetzge/Downloads/OpenSans-Regular.ttf");
	public static final int FONT_SIZE = 16;
	public static final String FONT_PREFIX = "font";

	public final Texture mainTexture = Texture.create("texture.png", true, false);
	public final TextureAtlas atlas = TextureAtlas.create("generated/packed.atlas");
	public final TextureRegion dotRegion = this.atlas.getRegion("dot");
	public final ResourceAssets resource = new ResourceAssets();
	public final OwnerAssets owner = new OwnerAssets();
	public final PersonAssets person = new PersonAssets();
	public final BuildingAssets building = new BuildingAssets();
	public final ItemAssets item = new ItemAssets();
	public final FontSet defaultFontSet = FontSet.create(FONT_FILE, FONT_SIZE, this.atlas, FONT_PREFIX);
	public final AutoTilesets<TileType> autoTilesets = AutoTilesets.generate("tiles", List.of( //
			new TileTypeDefinition<>(TileType.SLOPE, AutoTileDefinition.create(new File("a.png"), AutoTileDefinition.MASK_C, new File("b.png"), AutoTileDefinition.MASK_A)), //
			new TileTypeDefinition<>(TileType.DESERT, AutoTileDefinition.create(new File("a.png"), AutoTileDefinition.MASK_A, new File("b.png"), AutoTileDefinition.MASK_B)), //
			new TileTypeDefinition<>(TileType.GRASS, AutoTileDefinition.create(new File("c.png"), AutoTileDefinition.MASK_A, new File("b.png"), AutoTileDefinition.MASK_B)) //
	));
	public final AutoTilesets<WayTileType> wayAutoTilesets = AutoTilesets.generate("ways", List.of( //
			new TileTypeDefinition<WayTileType>(WayTileType.STONE, AutoTileDefinition.create(new File("d.png"), AutoTileDefinition.MASK_A, new File("d.png"), AutoTileDefinition.MASK_B))));

	public final class ResourceAssets {
		private final TextureRegion tree = Assets.this.atlas.getRegion("tree");

		private ResourceAssets() {
		}

		public TextureRegion get(ResourceType resourceType) {
			switch (resourceType) {
			case FOREST:
				return this.tree;
			case STONE:
				return Assets.this.dotRegion;
			default:
				throw new IllegalStateException();
			}
		}
	}

	public final class OwnerAssets {
		public final TextureRegion border = Assets.this.atlas.getRegion("border");

		private OwnerAssets() {
		}
	}

	public final class PersonAssets {
		private final int PERSON_TYPE_LENGTH = PersonState.VALUES.length * Orientation.VALUES.length;
		private final int PERSON_STATE_LENGTH = Orientation.VALUES.length;
		private final Animation[] animations = new Animation[PersonType.VALUES.length * this.PERSON_TYPE_LENGTH];

		public PersonAssets() {
			final TextureRegion walkTextureRegion = Assets.this.atlas.getRegion("link");
			final TextureRegion[] walkTextureRegions = TextureRegion.create(walkTextureRegion, 8, 1);

			final TextureRegion carryTextureRegion = Assets.this.atlas.getRegion("link-carry");
			final TextureRegion[] carryTextureRegions = TextureRegion.create(carryTextureRegion, 8, 1);

			setPersonAssets(walkTextureRegions, carryTextureRegions, PersonType.CARRIER);
			setPersonAssets(walkTextureRegions, carryTextureRegions, PersonType.WOODWORKER);
			setPersonAssets(walkTextureRegions, carryTextureRegions, PersonType.GRADER);
			setPersonAssets(walkTextureRegions, carryTextureRegions, PersonType.BUILDER);
		}

		private void setPersonAssets(TextureRegion[] walkTextureRegions, TextureRegion[] carryTextureRegions, PersonType personType) {
			set(personType, PersonState.IDLE, Orientation.SOUTH, Animation.create(new TextureRegion[] { walkTextureRegions[0], walkTextureRegions[1] }, 20));
			set(personType, PersonState.IDLE, Orientation.NORTH, Animation.create(new TextureRegion[] { walkTextureRegions[2], walkTextureRegions[3] }, 20));
			set(personType, PersonState.IDLE, Orientation.EAST, Animation.create(new TextureRegion[] { walkTextureRegions[4], walkTextureRegions[5] }, 20));
			set(personType, PersonState.IDLE, Orientation.WEST, Animation.create(new TextureRegion[] { walkTextureRegions[6], walkTextureRegions[7] }, 20));

			set(personType, PersonState.WALK, Orientation.SOUTH, Animation.create(new TextureRegion[] { walkTextureRegions[0], walkTextureRegions[1] }, 20));
			set(personType, PersonState.WALK, Orientation.NORTH, Animation.create(new TextureRegion[] { walkTextureRegions[2], walkTextureRegions[3] }, 20));
			set(personType, PersonState.WALK, Orientation.EAST, Animation.create(new TextureRegion[] { walkTextureRegions[4], walkTextureRegions[5] }, 20));
			set(personType, PersonState.WALK, Orientation.WEST, Animation.create(new TextureRegion[] { walkTextureRegions[6], walkTextureRegions[7] }, 20));

			set(personType, PersonState.WORK, Orientation.SOUTH, Animation.create(new TextureRegion[] { walkTextureRegions[0], walkTextureRegions[1] }, 20));
			set(personType, PersonState.WORK, Orientation.NORTH, Animation.create(new TextureRegion[] { walkTextureRegions[2], walkTextureRegions[3] }, 20));
			set(personType, PersonState.WORK, Orientation.EAST, Animation.create(new TextureRegion[] { walkTextureRegions[4], walkTextureRegions[5] }, 20));
			set(personType, PersonState.WORK, Orientation.WEST, Animation.create(new TextureRegion[] { walkTextureRegions[6], walkTextureRegions[7] }, 20));

			set(personType, PersonState.CARRY, Orientation.SOUTH, Animation.create(new TextureRegion[] { carryTextureRegions[0], carryTextureRegions[1] }, 20));
			set(personType, PersonState.CARRY, Orientation.NORTH, Animation.create(new TextureRegion[] { carryTextureRegions[2], carryTextureRegions[3] }, 20));
			set(personType, PersonState.CARRY, Orientation.EAST, Animation.create(new TextureRegion[] { carryTextureRegions[4], carryTextureRegions[5] }, 20));
			set(personType, PersonState.CARRY, Orientation.WEST, Animation.create(new TextureRegion[] { carryTextureRegions[6], carryTextureRegions[7] }, 20));
		}

		public Animation get(PersonType personType, PersonState personState, Orientation orientation) {
			return this.animations[index(personType, personState, orientation)];
		}

		private void set(PersonType personType, PersonState personState, Orientation orientation, Animation animation) {
			this.animations[index(personType, personState, orientation)] = animation;
		}

		private int index(PersonType personType, PersonState personState, Orientation orientation) {
			return (personType.ordinal() * this.PERSON_TYPE_LENGTH) + (personState.ordinal() * this.PERSON_STATE_LENGTH) + orientation.ordinal();
		}
	}

	public final class BuildingAssets {

		private final TextureRegion main = Assets.this.atlas.getRegion("building");
		private final TextureRegion buildingLot = Assets.this.dotRegion;

		public TextureRegion get(BuildingType buildingType, BuildingState buildingState) {
			return this.main;
		}

		public TextureRegion getBuildingLot(BuildingType buildingType, float done) {
			return this.buildingLot;
		}
	}

	public final class ItemAssets {

		private final TextureRegion wood = Assets.this.atlas.getRegion("item-wood");
		private final TextureRegion stone = Assets.this.atlas.getRegion("item-stone");

		private ItemAssets() {
		}

		public TextureRegion get(ItemType itemType) {
			switch (itemType) {
			case STONE:
				return this.wood;
			case WOOD:
				return this.stone;
			default:
				throw new IllegalArgumentException("Unexpected value: " + itemType);
			}
		}
	}

	private Assets() {
	}

	public static Assets INSTANCE;

	public static void load() {
		INSTANCE = new Assets();
	}
}
