#version 330 core
layout (points) in;
layout (triangle_strip, max_vertices = 6) out;

layout (std140) uniform Matrices
{
    mat4 u_combined;
};

out float height;
out float light;
out vec2 TexCoord;
out vec4 color;

in float[] tile;
in float[] index;
in float[] color2;

uniform samplerBuffer u_texture2; // heighmap texture  
uniform samplerBuffer u_texture3; // shading texture
uniform int u_tile_size;
uniform float u_tileset_columns;
uniform int u_use_color_shading;

float size = u_tile_size;
float tileSetTileSize = 1.0 / u_tileset_columns;

vec2 getTextureCoords(float tileIndex) 
{
	return vec2(mod(tileIndex, u_tileset_columns) * tileSetTileSize, 1.0 - tileSetTileSize - floor(tileIndex / u_tileset_columns) * tileSetTileSize);
}

void main() 
{   
    // TODO or use https://www.khronos.org/opengl/wiki/Shader_Storage_Buffer_Object


    if(tile[0] >= 0.0) {

        vec4 heights = texelFetch(u_texture2, int(index[0])) * 32.0;
        color = texelFetch(u_texture3, int(index[0]));

        vec2 textureCoords = getTextureCoords(tile[0]);

        vec4 root = vec4(gl_in[0].gl_Position.xy * size, gl_in[0].gl_Position.zw);

		light = 1.0 + (heights.y - ((heights.x + heights.z) / 2.0)) / 64.0; 

        TexCoord = textureCoords + vec2(0.0, tileSetTileSize);
        height = heights.x;
        gl_Position = u_combined * vec4(root.x, root.y + height, root.z, root.w); 
        EmitVertex();
        
        TexCoord = textureCoords + vec2(tileSetTileSize, tileSetTileSize);
        height = heights.y;
        gl_Position = u_combined * vec4(root.x + size, root.y + height, root.z, root.w);
        EmitVertex();
        
        TexCoord = textureCoords + vec2(0.0, 0.0);
        height = heights.z;
        gl_Position = u_combined * vec4(root.x, root.y - size + height, root.z, root.w);
        EmitVertex();
        
        EndPrimitive();
        
        light = 1.0 + (((heights.y + heights.w) / 2.0) - heights.z) / 64.0; 

 		TexCoord = textureCoords + vec2(tileSetTileSize, tileSetTileSize);
        height = heights.y;
        gl_Position = u_combined * vec4(root.x + size, root.y + height, root.z, root.w);
        EmitVertex();
        
        TexCoord = textureCoords + vec2(0.0, 0.0);
        height = heights.z;
        gl_Position = u_combined * vec4(root.x, root.y - size + height, root.z, root.w);
        EmitVertex();

        TexCoord = textureCoords + vec2(tileSetTileSize, 0.0);
        height = heights.w;
        gl_Position = u_combined * vec4(root.x + size, root.y - size + height, root.z, root.w);
        EmitVertex();
        
        EndPrimitive();
    }
}  