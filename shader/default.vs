#version 330 core
in vec3 aPos;
in vec2 aTexCoord;
in vec4 aColor; 

out vec2 TexCoord;
out vec4 color;

uniform mat4 u_combined;

void main()
{
    gl_Position = u_combined * vec4(aPos, 1.0);
    TexCoord = aTexCoord;
    color = aColor;
    color.a = aColor.a * (255.0/254.0);
}