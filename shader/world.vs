#version 330 core
in float a_index;
in float a_tile;
in float a_height;
in float a_color;

uniform int u_columns;

out vec4 textures;
out float tile;
out vec4 heights;
out float index;
out float color2;


void main()
{
	float x = mod(a_index, u_columns);
	float y = -floor(a_index / u_columns);
    gl_Position = vec4(x, y, 1.0, 1.0);
    color2 = a_color;
    tile = a_tile;
    index = a_index;
}