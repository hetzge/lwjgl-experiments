#version 330 core

#define LOWP

out vec4 FragColor;
  
in vec2 TexCoord;
in lowp vec4 color;

uniform sampler2D ourTexture;
uniform int u_color_mode;


void main()
{
    FragColor = texture(ourTexture, TexCoord);
    
    if(u_color_mode == 0) {
        if(FragColor.a > 0.0 && FragColor.r == FragColor.b && FragColor.g == 0.0 && FragColor.r > 0.0) {
        	FragColor = color;
        }
    } else if (u_color_mode == 1) {
        FragColor = FragColor * color;
    }
    
    //if(FragColor.a == 0.0) {
    //	discard;
    //}
}