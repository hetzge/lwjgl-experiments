#version 330 core
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

layout (std140) uniform Matrices
{
    mat4 u_combined;
};

in VS_OUT {
    vec2 TexCoord;
    vec4 color;
} gs_in[];  

out vec2 TexCoord;
out vec4 color;

void main() {    

	TexCoord = gs_in[0].TexCoord;
	color = gs_in[0].color;
    gl_Position = u_combined * gl_in[0].gl_Position; 
    EmitVertex();
    
    TexCoord = gs_in[1].TexCoord;
    color = gs_in[1].color;
    gl_Position = u_combined * gl_in[1].gl_Position; 
    EmitVertex();
    
    TexCoord = gs_in[2].TexCoord;
    color = gs_in[2].color;
    gl_Position = u_combined * gl_in[2].gl_Position; 
    EmitVertex();
    
    EndPrimitive();
}  