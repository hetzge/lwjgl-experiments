#version 330 core
out vec4 FragColor;
  
in vec2 TexCoord;
in float height;
in float light;
in vec4 color;

uniform sampler2D u_texture1; // main texture
uniform int u_use_height_shading;
uniform int u_use_color_shading;

float plusMinus(float value) {
	return (value - 0.5) * 2.0;
}

vec4 plusMinus(vec4 value) {
	return vec4(plusMinus(value.r), plusMinus(value.g), plusMinus(value.b), plusMinus(value.a));
}

void main()
{
    float heightValue = u_use_height_shading == 1 ? 0.9 + (height / 50.0 * 0.2) : 1.0;
    float lightValue = u_use_height_shading == 1 ? light : 1.0;
    
    FragColor = texture(u_texture1, TexCoord);
    
    if(u_use_color_shading == 1) {
    	FragColor = FragColor * color;
    }
    
    FragColor = FragColor * vec4(lightValue, lightValue, lightValue, 1.0) * vec4(heightValue, heightValue, heightValue, 1.0);
}